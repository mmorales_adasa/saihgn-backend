<?php
class DatoDiarioModel
{
    var $date;
	var $value;
    var $indConfirmacion;
    var $fiabilidad;
    var $media;
    var $acum;
    var $minimo;
    var $maximo;
    var $timestamp_min;
    var $timestamp_max;
    var $is_acum;
    var $cod_variable;

    function getValue()	 			    	{return $this->value;}
    function getDate()	 					{return $this->date;}
    function getIndConfirmacion()		{return $this->indConfirmacion;}
    function getFiabilidad()		{return $this->fiabilidad;}
    function getMedia()		{return $this->media;}
    function getAcum()		{return $this->acum;}
    function getMin()		{return $this->minimo;}
    function getMax()		{return $this->maximo;}
    function getTimestampMin()		{return $this->timestamp_min;}
    function getTimestampMax()		{return $this->timestamp_max;}    
    function setValue($valor)       			{$this->value= $valor;  }
    function getIsAcum()		{return $this->is_acum;}
    function getCodVariable()		{return $this->cod_variable;}


    function setDate($valor)       		     {$this->date= $valor;  }   
    function setIndConfirmacion($valor)       		    {$this->indConfirmacion= $valor;  }   
    function setFiabilidad($valor)       		    {$this->fiabilidad= $valor;  }   
    function setMedia($valor)       		    {$this->media= $valor;  }   
    function setAcum($valor)       		    {$this->acum= $valor;  }   
    function setMin($valor)       		    {$this->minimo= $valor;  }   
    function setMax($valor)       		    {$this->maximo= $valor;  }   
    function setTimestampMin($valor)       		    {$this->timestamp_min= $valor;  }   
    function setTimestampMax($valor)       		    {$this->timestamp_max= $valor;  }   
    function setIsAcum($valor)       		    {$this->is_acum= $valor;  }   
    function setCodVariable($valor)       		    {$this->cod_variable= $valor;  }   

}
?>