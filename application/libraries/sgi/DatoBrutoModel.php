<?php
class DatoBrutoModel
{
	var $value;
    var $date;
    var $valido;
    var $motivoInvalidacion;
    var $cod_variable;


    function getValue()	 			    	{return $this->value;}
    function getDate()	 					{return $this->date;}
    function getValido()	 					{return $this->valido;}
    function getMotivoInvalidacion()	 					{return $this->motivoInvalidacion;}
    function getCodVariable()		{return $this->cod_variable;}

    function setValue($valor)       			{$this->value= $valor;  }
    function setDate($valor)       		    {$this->date= $valor;  }   
    function setValido($valor)       		    {$this->valido= $valor;  }   
    function setMotivoInvalidacion($valor)       		    {$this->motivoInvalidacion= $valor;  }   
    function setCodVariable($valor)       		    {$this->cod_variable= $valor;  }   
}
?>