<?php
class VariableModel
{
	var $cod;
    var $desc;
    var $hasDataInvalids;
    var $tipoVariable;
    var $estacion;
    var $type;
    var $is_acum;

    function getCod()	 			    	{return $this->cod;}
    function getDesc()	 					{return $this->desc;}
    function getDataInvalids()	 		    {return $this->hasDataInvalids;}
    function getTipoVariable()	 			{return $this->tipoVariable;}
    function getEstacion()	 				{return $this->estacion;}
    function getType()	 				{return $this->type;}
    function getIsAcum()		{return $this->is_acum;}


    function setCod($valor)       			{$this->cod= $valor;  }
    function setDesc($valor)       		    {$this->desc= $valor;  }   
    function setDataInvalids($valor)        {$this->hasDataInvalids= $valor;  }   
    function setTipoVariable($valor)        {$this->tipoVariable= $valor;  }   
    function setEstacion($valor)       	    {$this->estacion= $valor;  }   
    function setType($valor)       	        {$this->type= $valor;  }   
    function setIsAcum($valor)       		    {$this->is_acum= $valor;  }   

}
?>