<?php
class ValoresEmbalse
{
    var $cod_estacion;
    var $nombre;
    var $acronimo;
    var $fecha;
    var $NE1;
    var $VE1;
    var $NR1;
    var $QR1;

    function getCodEstacion()	 		    {return $this->cod_estacion;}
    function getNombre()	 		        {return $this->nombre;}
    function getAcronimo()	 		        {return $this->acronimo;}
    function getFecha()	 				    {return $this->fecha;}
    function getNE1()	 					{return $this->NE1;}
    function getVE1()                       {return $this->VE1;}
    function getNR1()                       {return $this->NR1;}
    function getQR1()                       {return $this->QR1;}

    function setCodEstacion($valor)       	{$this->cod_estacion= $valor;  }
    function setNombre($valor)       	    {$this->nombre= $valor;  }
    function setAcronimo($valor)       	    {$this->acronimo= $valor;  }
    function setFecha($valor)       		{$this->fecha= $valor;  }
    function setNE1($valor)       			{$this->NE1= $valor;  }
    function setVE1($valor)       			{$this->VE1= $valor;  }
    function setNR1($valor)       			{$this->NR1= $valor;  }
    function setQR1($valor)       			{$this->QR1= $valor;  }
}
?>