<?php
class FichaBiblioteca
{
    var $autor;
    var $clave;
    var $contratista;
    var $fecha;
    var $id_categoria;
    var $id_clase;
    var $id_municipio;
    var $id_provincia;
    var $id_proyecto;
    var $id_subcategoria;
    var $id_subtipo;
    var $id_tipo;
    var $id_zona;
    var $notas;
    var $prestado;
    var $ref_letra;
    var $ref_numero;
    var $referencia;
    var $titulo;
    var $toponimo;
    var $balda;
    var $mueble;
    var $id_ubicacion;

    function getAutor()	 						{return $this->autor;}
    function getClave()	 					    {return $this->clave;}
    function getContratista()	 				{return $this->contratista;}
    function getFecha()	 					    {return $this->fecha;}
    function getIdCategoria()	 				{return $this->id_categoria;}
    function getIdClase()	 			        {return $this->id_clase;}
    function getIdMunicipio()	 			    {return $this->id_municipio;}
    function getIdProvincia()	 			    {return $this->id_provincia;}
    function getIdProyecto()	 			    {return $this->id_proyecto;}
    function getIdSubcategoria()	 			{return $this->id_subcategoria;}
    function getIdSubtipo()	 			        {return $this->id_subtipo;}
    function getIdTipo()	 			        {return $this->id_tipo;}
    function getIdZona()	 			        {return $this->id_zona;}
    function getNotas()	 			            {return $this->notas;}
    function getPrestado()	 			        {return $this->prestado;}
    function getRefLetra()	 			        {return $this->ref_letra;}
    function getRefNumero()	 			        {return $this->ref_numero;}
    function getReferencia()	 			    {return $this->referencia;}
    function getTitulo()	 			        {return $this->titulo;}
    function getToponimo()	 			        {return $this->toponimo;}
    function getBalda()	 			            {return $this->balda;}
    function getMueble()	 			        {return $this->mueble;}
    function getIdUbicacion()	 			    {return $this->id_ubicacion;}

    function setAutor($valor)	 				{$this->autor= $valor;  }				
    function setClave($valor)                   {$this->clave= $valor;  }
    function setContratista($valor)             {$this->contratista= $valor;  }
    function setFecha($valor)	 				{$this->fecha= $valor;  }	    
    function setIdCategoria($valor)	 			{$this->id_categoria= $valor;  }
    function setIdClase($valor)	 			    {$this->id_clase= $valor;  }
    function setIdMunicipio($valor)	 			{$this->id_municipio= $valor;  }
    function setIdProvincia($valor)	 			{$this->id_provincia= $valor;  }
    function setIdProyecto($valor)	 			{$this->id_proyecto= $valor;  }
    function setIdSubcategoria($valor)	 	    {$this->id_subcategoria= $valor;  }
    function setIdSubtipo($valor)	 			{$this->id_subtipo= $valor;  } 
    function setIdTipo($valor)	 			    {$this->id_tipo= $valor;  }
    function setIdZona($valor)	 			    {$this->id_zona= $valor;  }
    function setNotas($valor)	 			    {$this->notas= $valor;  }  
    function setPrestado($valor)	 			{$this->prestado= $valor;  }  
    function setRefLetra($valor)	 			{$this->ref_letra= $valor;  }   
    function setRefNumero($valor)	 			{$this->ref_numero= $valor;  } 
    function setReferencia($valor)	 			{$this->referencia= $valor;  }
    function setTitulo($valor)	 			    {$this->titulo= $valor;  }
    function setToponimo($valor)	 			{$this->toponimo= $valor;  }     
    function setBalda($valor)	 			    {$this->balda= $valor;  }   
    function setMueble($valor)	 			    {$this->mueble= $valor;  } 
    function setIdUbicacion($valor)	 			{$this->id_ubicacion= $valor;  } 
}
?>