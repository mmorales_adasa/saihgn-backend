<?php
class TermFormula
{
	var $code;
    var $translation;
    var $value;

    function getCode()	 						{return $this->code;}
    function getTranslation()	 			    {return $this->translation;}
    function getValue()	 			            {return $this->value;}

    function setCode($valor)       			    {$this->code= $valor;  }
    function setTranslation($valor)       	    {$this->translation= $valor;  }       
    function setValue($valor)       	        {$this->value= $valor;  }       
}
?>