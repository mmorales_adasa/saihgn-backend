<?php
class EstacionTelegram
{
    var $cod_estacion;
    var $nombre;
    var $tipo;
    var $latitud;
    var $longitud;
    var $disponible;
    var $zona;
    var $orden;
    var $profundidad;
    var $cota_boca_sondeo;
    var $acronimo;
    var $ult_res;


    function getCodEstacion()	 				{return $this->cod_estacion;}
    function getNombre()                        {return $this->nombre;}
    function getTipo()                          {return $this->tipo;}
    function getLatitud()                       {return $this->latitud;}
    function getLongitud()                      {return $this->longitud;}
    function getDisponible()                    {return $this->disponible;}
    function getZona()                          {return $this->zona;}
    function getOrden()                         {return $this->orden;}
    function getProfundidad()                   {return $this->profundidad;}
    function getCotaBocaSondeo()                {return $this->cota_boca_sondeo;}
    function getAcronimo()                      {return $this->acronimo;}
    function getUltres()                        {return $this->ult_res;}
    

    function setCodEstacion($valor)       	    {$this->cod_estacion= $valor;}
    function setNombre($valor)       	        {$this->nombre= $valor;}
    function setTipo($valor)       	            {$this->tipo= $valor;}
    function setLatitud($valor)       	        {$this->latitud= $valor;}
    function setLongitud($valor)       	        {$this->longitud= $valor;}
    function setDisponible($valor)       	    {$this->disponible= $valor;}
    function setZona($valor)       	            {$this->zona= $valor;}
    function setOrden($valor)       	        {$this->orden= $valor;}
    function setProfundidad($valor)       	    {$this->profundidad= $valor;}
    function setCotaBocaSondeo($valor)       	{$this->cota_boca_sondeo= $valor;}
    function setAcronimo($valor)       	        {$this->acronimo= $valor;}
    function setUltres($valor)       	        {$this->ult_res= $valor;}
}
?>