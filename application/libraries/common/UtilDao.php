<?php
class UtilDao
{
    function getLogica($and){   
        if ($and){
            $logica = ' AND ';
        }
        else{
        $logica = ' OR ';
        }
        return $logica;
    }
    function bindingParameters($sql, $model){        
		foreach ($model as $key => $item) {
            
            if (isset($item["value"])){
                $value = $item["value"];
			    $type = $item["type"];
                if ($type!=Constants::TYPE_FILTER_ARRAY_STRING)
                {
                    if ($type==Constants::TYPE_TEXT){
                        $value = "'" . $value . "'";
                    }
                    $sql = str_replace(":" . $key  .":", $value , $sql);	
                }
            }
            else{
                $sql = str_replace(":" . $key  .":", 'null' , $sql);	
            }

			
        }
        return $sql;
    }
    function applyFiltersText($filter, $logica){           
        $sql =  $this->getLogica($logica) . "  UPPER(TRIM(" . $filter->getColumn() . ")) LIKE ('%" . trim(strtoupper($filter->getValues()[0])) . "%')"; 
        return $sql;
    }
    function applyFiltersDate($filter, $logica, $operador='>='){           
        $hora = ' 00:00:00';
        if (trim($operador)=='<') { $hora = ' 23:59:59';}

        $sql =  $this->getLogica($logica) . $filter->getColumn() . " " . $operador . " TO_TIMESTAMP('" . $filter->getValues()[0] . $hora ."','YYYY-MM-DD HH24:MI:SS')";
        return $sql;
    }  
    function applyFiltersTimestamp($filter, $logica, $operador='>='){           
        $sql =  $this->getLogica($logica) . $filter->getColumn() . " " . $operador . " TO_TIMESTAMP('" . $filter->getValues()[0] ."','YYYY-MM-DD HH24:MI:SS')";
        return $sql;
    }        
    function applyFiltersNull($filter, $logica, $operador='>='){           
        $sql =  $this->getLogica($logica) . $filter->getColumn() . " IS NULL";
        return $sql;
    }      
    function applyFiltersInteger($filter, $logica){        
        $sql =  $this->getLogica($logica) . $filter->getColumn()  . " = " . $filter->getValues()[0];
        return $sql;
    }    
    function applyFiltersArrayString($filter, $logica){              
        $sql =  $this->getLogica($logica) . '  trim(' . $filter->getColumn() . ') IN ('; 
        //$sql =  ' ' . $filter->getTypeCondition() . '  ' . $filter->getColumn() . ' IN (';
        $i=0;
        foreach ($filter->getValues() as $value) {  
            $sql.= "'" . trim($value) . "'";
            $i++;
            if ($i<sizeof($filter->getValues())){
                $sql.= ",";
            }
        }
        $sql.=')';

        return $sql;
    }
    function applyFiltersArrayInteger($filter, $logica){              
        $sql =  $this->getLogica($logica) .  $filter->getColumn() . ' IN ('; 
        //$sql =  ' ' . $filter->getTypeCondition() . '  ' . $filter->getColumn() . ' IN (';
        $i=0;
        foreach ($filter->getValues() as $value) {  
            $sql.= trim($value) ;
            $i++;
            if ($i<sizeof($filter->getValues())){
                $sql.= ",";
            }
        }
        $sql.=')';

        return $sql;
    }    
    
    function applyFiltersCondition($filters, $and){
        $sql = "";
        foreach ($filters as $filter) {            
            if ($filter->getType() == Constants::TYPE_FILTER_ARRAY_STRING){
                    $sql.= $this->applyFiltersArrayString($filter, $and);
             }  
             if ($filter->getType() == Constants::TYPE_FILTER_ARRAY_INTEGER){
                $sql.= $this->applyFiltersArrayInteger($filter, $and);
         }                         
             if ($filter->getType() == Constants::TYPE_TEXT){
                  $sql.= $this->applyFiltersText($filter, $and);
             }
             if ($filter->getType() == Constants::TYPE_INTEGER){
                  $sql.= $this->applyFiltersInteger($filter, $and);
             }      
             if ($filter->getType() == Constants::TYPE_DATE){
                $sql.= $this->applyFiltersDate($filter, $and);
             }
             if ($filter->getType() == Constants::TYPE_FILTER_DATE_MINOR){
                $sql.= $this->applyFiltersDate($filter, $and, ' < ');
             }  
             if ($filter->getType() == Constants::TYPE_FILTER_TIMESTAMP_EQUAL){
                $sql.= $this->applyFiltersTimestamp($filter, $and, ' = ');
             }                                  
            if ($filter->getType() == Constants::TYPE_NULL){
                $sql.= $this->applyFiltersNull($filter, $and);                
            }
        }     
        return $sql;
    }

    function applyFilters($filters){
        $sql= " WHERE 1=1 ";
        $filtersAND  = array();
        $filtersOR   = array();
        foreach ($filters as $filter) {   
            if ($filter->getTypeCondition()=='AND'){
                array_push($filtersAND,$filter); 
            }
            else {
                array_push($filtersOR,$filter); 
            }            
        }
        if (sizeof($filtersAND)>0){
            $sql.= $this->applyFiltersCondition($filtersAND,true);
            //echo $sql;
        }
        if (sizeof($filtersOR)>0){
            $sql.= " AND ( 1=0 " . $this->applyFiltersCondition($filtersOR, false) . ')';
            //echo $sql;
        }        
        
        return $sql;
    }   
    function applySort($sort){        
        $sql = '';
        if (isset($sort)){
            $sql =  " ORDER BY " . $sort ;
            
        }
        return $sql;
    }
    function applyLimit($limit, $offset){
        if (isset($limit)){
            return " LIMIT " . $limit . " OFFSET " . $offset;        
        }
        return "";
    }
    /*
    function applyFilters($filters, $and=true){
        $sql= " WHERE 1=1 ";
        foreach ($filters as $filter) {            
            if ($filter->getType() == Constants::TYPE_FILTER_ARRAY_STRING){
                    $sql.= $this->applyFiltersArrayString($filter, $and);
             }            
             if ($filter->getType() == Constants::TYPE_TEXT){
                  $sql.= $this->applyFiltersText($filter, $and);
             }
             if ($filter->getType() == Constants::TYPE_INTEGER){
                  $sql.= $this->applyFiltersInteger($filter, $and);
             }      
             if ($filter->getType() == Constants::TYPE_DATE){
                $sql.= $this->applyFiltersDate($filter, $and);
             }
            if ($filter->getType() == Constants::TYPE_NULL){
                $sql.= $this->applyFiltersNull($filter, $and);                
            }
        }                                                   
        return $sql;
    }  */     
  
}
?>
