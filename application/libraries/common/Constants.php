<?php
class Constants
{		
    const TYPE_FILTER_ARRAY_STRING = 'ARRAY_STRING';
    const TYPE_FILTER_ARRAY_INTEGER = 'ARRAY_INTEGER';
    const TYPE_TEXT = 'TEXT';
    const TYPE_INTEGER = 'INTEGER';
    const TYPE_DATE = 'DATE';
    const TYPE_NULL = 'NULL';

    const DATABASE_OPERATION_EDITION = 'E';
    const DATABASE_OPERATION_INSERT = 'I';
    const DATABASE_OPERATION_DELETE = 'D';

    const DAYS_TOKEN = 1000;
    const APP_VISOR = 'VISOR';
    const APP_SGI = 'SGI';
    const APP_TELEGRAM = 'TELEGRAM';


    const EMAIL_SMTP_HOST = 'smtp.dondominio.com';
    const EMAIL_SMTP_PORT = 587;
    const EMAIL_SMTP_USER = 'soporte@saihguadiana.com';
    const EMAIL_SMTP_PWD = 'Saihguad1';

    const EMAIL_SMTP_FROM_EMAIL = 'soporte@saihguadiana.com';
    const EMAIL_SMTP_FROM_NAME = 'Soporte SAIHG Guadiana';

    const NIVEL_ACCESO_PUBLIC = 0;
    const NIVEL_ACCESO_INVITADO = 1;

    const TYPE_FILTER_DATE_MINOR = 'DATE_MINOR';
    const TYPE_FILTER_TIMESTAMP_EQUAL= 'TIMESTAMP_EQUAL';

    const BASE_URL_JASPER= 'http://172.21.0.233:8087/jasperserver';
    const USER_JASPER= 'usrjasper';
    const PWD_JASPER= 'Saihguad';

    //const PATH_ESQUEMAS= 'C:\\Test';
    const PATH_ESQUEMAS = '/esquemas';
    
    const LDAP_SERVER= '10.31.228.16:389';

    //const ENTITIES_SERVER_27 = ['SONDAS_AQUADAM_27','DATOS_SONDAS_AQUADAM_27','ESTACIONES_27', 'VARIABLES_PIEZO_27', 'ESTACIONES_PIEZO_27','VARIABLES_27','MOTIVOS_INVALIDACION_27'];
    const ENTITIES_SERVER_27 = [];

    const DATEFORMAT = 'd-m-Y H:i:s';
}
?>