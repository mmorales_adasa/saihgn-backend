<?php
class Filter
{
	var $column;
    var $type;
    var $typeCondition;
    var $values;

    function getColumn()	 						{return $this->column;}
    function getValues()	 					{return $this->values;}
    function getType()	 					    {return $this->type;}
    function getTypeCondition()	 					    {return $this->typeCondition;}

    function setColumn($valor)       			    {$this->column= $valor;  }
    function setValues($valor)       		    {$this->values= $valor;  }   
    function setType($valor)       		    {$this->type= $valor;  }
    function setTypeCondition($valor)       		    {$this->typeCondition= $valor;  }   

    function clone(){   
        $f = new Filter();
        $f->setColumn($this->getColumn());
        $f->setValues($this->getValues());
        $f->setType($this->getType());
        $f->setTypeCondition($this->getTypeCondition());
        return $f;

    }
    function readCriteria($filters){        
        $criteria  = array();      
        foreach ($filters as $item) {          
            $filter = new Filter();
            $filter->setColumn($item["id"]);            
            $filter->setType($item["type"]);
            if (isset($item["type_condition"])){
                $filter->setTypeCondition($item["type_condition"]);   
            }
            else{
                $filter->setTypeCondition("AND");
            }
            $values = array();
            if (is_array($item["values"])){
               foreach ($item["values"] as $val) {
                array_push($values,$val);
               }
             }
             else{
                array_push($values,$item["values"]);
             }             
             $filter->setValues($values);
            array_push($criteria,$filter);
        }
        return $criteria;
    }
}
?>