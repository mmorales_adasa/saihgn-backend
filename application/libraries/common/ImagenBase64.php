<?php
class ImagenBase64
{
    var $nombre;
    var $descripcion;
    var $base64;

    function getNombre()	 				        {return $this->nombre;}
    function getDescripcion()	 				    {return $this->descripcion;}
    function getBase64()	 						{return $this->base64;}

    function setNombre($valor)       			    {$this->nombre= $valor;  }
    function setDescripcion($valor)       			{$this->descripcion= $valor;  }
    function setBase64($valor)       			    {$this->base64= $valor;  }
}
?>