<?php
class User
{
    var $cod_usuario;
    var $username;
    var $nombre;
    var $email;
    var $sesionId;
    var $enabledSGI;
    var $origen;

    function getCodUsuario()	 						{return $this->cod_usuario;}
    function getUsername()	 						{return $this->username;}
    function getNombre()	 					{return $this->nombre;}
    function getEmail()	 					{return $this->email;}
    function getSesionId()	 					    {return $this->sesionId;}
    function getEnabledSGI()	 			 {return $this->enabledSGI;}
    function getOrigen()	 			 {return $this->origen;}

    function setCodUsuario($valor)       			    {$this->cod_usuario= $valor;  }
    function setUsername($valor)       			    {$this->username= $valor;  }
    function setNombre($valor)       		    {$this->nombre= $valor;  }   
    function setEmail($valor)       		    {$this->email= $valor;  }   
    function setSesionId($valor)       		    {$this->sesionId= $valor;  } 
    function setEnabledSGI($valor)       		    {$this->enabledSGI= $valor;  } 
    function setOrigen($valor)       		    {$this->origen= $valor;  } 
}
?>
