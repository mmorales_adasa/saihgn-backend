<?php
require_once APPPATH.'/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class EvolucionEstacion {

    static function obtener($DB, $zoom, $variables, $fecha_i, $fecha_f, $encabezado = false, $order = 'ASC'){

        $return = [];

        if(isset($variables) && count($variables)>0){

            $variablesInf = EvolucionEstacion::variablesInfo($DB, $variables);

            $datos = [];
            switch($zoom){

                case 'a':
                    $datos = EvolucionEstacion::getEvolucionAnual($DB, $variables, $variablesInf, $fecha_i, $fecha_f);
                    break;

                case 'm':
                    $datos = EvolucionEstacion::getEvolucionMensual($DB, $variables, $variablesInf, $fecha_i, $fecha_f);
                    break;

                case 's':
                    $datos = EvolucionEstacion::getEvolucionSemanal($DB, $variables, $variablesInf, $fecha_i, $fecha_f);
                    break;

                case 'd':
                    $datos = EvolucionEstacion::getEvolucionDiario($DB, $variables, $variablesInf, $fecha_i, $fecha_f);
                    break;

                case 'i':
                    $datos = EvolucionEstacion::getEvolucionDiezminutal($DB, $variables, $variablesInf, $fecha_i, $fecha_f, $order);
                    break;
		    }

            // postprocesado general
            foreach($datos as &$lineaDatos){
                foreach($lineaDatos as $key => &$columnaDatos){
                    if($key != 'timestamp'){
                        $columnaDatos = floatval($columnaDatos);
                        $columnaDatos = round($columnaDatos,6);
                    }
                }
            }
        
            // postprocesado specífico embalses, Generar variables COR / ALIV / RE / VRED si se piden, necesitan la variable VE1
            $CORALIVRES = [];
            foreach($variables as $variable){
                $estacionVariable = explode("/", $variable);
                if(in_array($estacionVariable[1], ["COR","ALIV","RE","VRED"])){
                    $CORALIVRES[$estacionVariable[0]][] = $estacionVariable[1];
                }
            }

            if(count($CORALIVRES)>0){
                $datos = EvolucionEstacion::insertarCORALIV($DB, $datos, $CORALIVRES);
            }
            // final postprocesado variables de embalse

            $return["valores"] = $datos;

            if($encabezado){
                $cabeceras = EvolucionEstacion::crearEncabezado($variablesInf);
                $return["encabezado"] = $cabeceras[2];
                $return["encabezadoAdicional"] = [$cabeceras[0], $cabeceras[1]];
            }

        }

		return $return;
    }

    static function variablesInfo($DB, $variables){

        $in = "'". implode("','", $variables) ."'";

        // se obtiene la info del tipo de variable y a la estación que pertenece
        $query = "select cod_variable, v.red_control, v.cod_tipo_variable, tv.agregacion, v.descripcion descripcion_variable, cod_estacion, tipo as tipo_estacion, t.descripcion descripcion_tipo, categoria  
                    from tbl_variable v, tbl_estacion, tbl_tipo_estacion t, tbl_tipo_variable tv 
                    where cod_variable in (".$in.") 
                    and cod_estacion = split_part(cod_variable,'/',1)
                    and tipo = cod_tipo_estacion
                    and v.cod_tipo_variable = tv.cod_tipo_variable
                    order by tipo";

        $variableInfoTemp = ConexionBD::EjecutarConsultaConDB($DB, $query);
        $variableInfo = [];

        // ordenar variables llegadas de base de datos por el orden de array
        foreach($variables as $variable){
            foreach($variableInfoTemp as $varInfo){
                if($varInfo["cod_variable"]==$variable){
                    $variableInfo[] = $varInfo;
                }
            }
        }

        return $variableInfo;

    }

    static function getEvolucionAnual($DB, $variables, $variablesInfo, $fechaIni, $fechaFin){
        
        $coalesce = [];
        $case = [];
        foreach($variablesInfo as $variable){
            $coalesce[] = "coalesce(SUM(\"".$variable["cod_variable"]."\"),null) AS \"".$variable["cod_variable"]."\"";
            if($variable["agregacion"]=="MEDIA")    $case[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_media END AS \"". $variable["cod_variable"]."\"";
            if($variable["agregacion"]=="ACUMUL")   $case[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_acum END AS \"". $variable["cod_variable"]."\"";
        }
        $coalesce = implode(",", $coalesce);
        $case = implode(",", $case);
        $in = "'". implode("','", $variables) ."'";

        $where1 = "";
        $where2 = "";

        if($fechaIni != null && $fechaFin != null){
            $where1 = "and anyo >= date_part('year', to_timestamp(".$fechaIni.")) and anyo <= date_part('year', to_timestamp(".$fechaFin."))";
            $where2 = "WHERE fecha >= to_timestamp(".$fechaIni.") and fecha <= to_timestamp(".$fechaFin.")";
        }

        $query = "SELECT
        fecha as timestamp,
        ".$coalesce."
        FROM(
            SELECT
                fecha,
                ".$case."
            FROM (
                select to_date(LPAD(anyo::text, 4, '0'), 'YYYY') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_anyos where (cod_variable in (".$in.")) 
                ".$where1."
                AND anyo >= 2011
                and ind_confirmacion like 'S'
            ) AS valores
            ".$where2."
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionMensual($DB, $variables, $variablesInfo, $fechaIni, $fechaFin){
        
        //$useTblPiezoA = false;
        $useTblPiezoM = false;
        $useTblOlap = false;

        //$variablesPiezoA = [];
        $variablesPiezoM = [];
        $variablesOlap = [];

        $coalesce = [];

        //$casePiezoA = [];
        $casePiezoM = [];
        $caseOLAP = [];

        //$inPiezoA = [];
        $inPiezoM = [];
        $inOLAP = [];

        foreach($variablesInfo as $variable){

            $coalesce[] = "coalesce(SUM(\"".$variable["cod_variable"]."\"),null) AS \"".$variable["cod_variable"]."\"";

            /*if($variable["red_control"] == "PIEZO") {
                $useTblPiezoA = true;
                $variablesPiezoA[] = $variable["cod_variable"];
                $casePiezoA[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor END AS \"". $variable["cod_variable"]."\"";
            }*/

            if($variable["red_control"] == "PZM"){
                $useTblPiezoM = true;
                $variablesPiezoM[] = $variable["cod_variable"];
                $casePiezoM[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor END AS \"". $variable["cod_variable"]."\"";
            }

            if(in_array($variable["red_control"],["SEC","SAIH","CC","AUSC"])) {
                $variablesOlap[] = $variable["cod_variable"];
                $useTblOlap = true;
                if($variable["agregacion"]=="MEDIA")    $caseOLAP[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_media END AS \"". $variable["cod_variable"]."\"";
                if($variable["agregacion"]=="ACUMUL")   $caseOLAP[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_acum END AS \"". $variable["cod_variable"]."\"";
            }
            
        }
        
        $coalesce = implode(",", $coalesce);
        
        /*if($useTblPiezoA){
            $casePiezoA = implode(",", $casePiezoA);
            $inPiezoA = "'". implode("','", $variablesPiezoA) ."'";
        }*/

        if($useTblPiezoM){
            $casePiezoM = implode(",", $casePiezoM);
            $inPiezoM = "'". implode("','", $variablesPiezoM) ."'";
        }

        if($useTblOlap){
            $caseOLAP = implode(",", $caseOLAP);
            $inOLAP = "'". implode("','", $variables) ."'";
        }

        $query = "SELECT
        timestamp,
        ".$coalesce."
        FROM ";
        
        if($useTblOlap){
        
            $query.="(
                SELECT
                    fecha as timestamp,
                    ".$caseOLAP."
                FROM (
                    select to_date(concat(LPAD(anyo::text, 4, '0'), LPAD(mes::text, 2, '0')), 'YYYYMM') fecha, cod_variable, valor_media, valor_acum 
                    from tbl_olap_medidas_meses where cod_variable in (".$inOLAP.")
                    and anyo >= date_part('year', to_timestamp(".$fechaIni.")::date) and anyo <= date_part('year', to_timestamp(".$fechaFin.")::date)
                    and ind_confirmacion like 'S'
                ) AS valores
                WHERE
                    fecha >= to_timestamp(".$fechaIni.")::date and fecha <= to_timestamp(".$fechaFin.")::date
            ) AS valores_olap";

        }

        if($useTblPiezoM){

            if($useTblOlap) $query.=" UNION ";

            $query.="(
                select
                    timestamp::date,
                    ".$casePiezoM."
                from tbl_dato_piezo_manual
                where timestamp >= to_timestamp(".$fechaIni.")::date and timestamp <= to_timestamp(".$fechaFin.")::date
                and cod_variable IN (" . $inPiezoM . ")
            ) AS valores_piezom";

        }
        
        
        $query .= " GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionSemanal($DB, $variables, $variablesInfo, $fechaIni, $fechaFin){
        
        $coalesce = [];
        $case = [];
        foreach($variablesInfo as $variable){
            $coalesce[] = "coalesce(SUM(\"".$variable["cod_variable"]."\"),null) AS \"".$variable["cod_variable"]."\"";
            if($variable["agregacion"]=="MEDIA")   $case[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_media END AS \"". $variable["cod_variable"]."\"";
            if($variable["agregacion"]=="ACUMUL")  $case[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_acum END AS \"". $variable["cod_variable"]."\"";
        }
        $coalesce = implode(",", $coalesce);
        $case = implode(",", $case);
        $in = "'". implode("','", $variables) ."'";

        $query = "SELECT
        fecha as timestamp,
        ".$coalesce."
        FROM(
            SELECT
                fecha,
                ".$case."
            FROM (
                select num_semana, to_date(concat(LPAD(anyo::text, 4, '0'), LPAD(num_semana::text, 2, '0')), 'iyyyiw') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_semanas where cod_variable in (".$in.")
                and anyo >= date_part('year', to_timestamp(".$fechaIni.")::date) 
                and anyo <= date_part('year', to_timestamp(".$fechaFin.")::date)
                and ind_confirmacion like 'S'
                and num_semana != 53
            ) AS valores
            WHERE
                fecha >= to_timestamp(".$fechaIni.")::date and fecha <= to_timestamp(".$fechaFin.")::date
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionDiario($DB, $variables, $variablesInfo, $fechaIni, $fechaFin){
        
        $useTblPiezoA = false;
        $useTblPiezoM = false;
        $useTblOlap = false;

        $variablesPiezoA = [];
        $variablesPiezoM = [];
        $variablesOlap = [];

        $coalesce = [];

        $casePiezoA = [];
        $casePiezoM = [];
        $caseOLAP = [];

        $inPiezoA = [];
        $inPiezoM = [];
        $inOLAP = [];

        foreach($variablesInfo as $variable){

            $coalesce[] = "coalesce(SUM(\"".$variable["cod_variable"]."\"),null) AS \"".$variable["cod_variable"]."\"";

            if($variable["red_control"] == "PIEZO") {
                $useTblPiezoA = true;
                $variablesPiezoA[] = $variable["cod_variable"];
                $casePiezoA[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor END AS \"". $variable["cod_variable"]."\"";
            }

            if($variable["red_control"] == "PZM"){
                $useTblPiezoM = true;
                $variablesPiezoM[] = $variable["cod_variable"];
                $casePiezoM[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor END AS \"". $variable["cod_variable"]."\"";
            }

            if(in_array($variable["red_control"],["SEQ","SAIH","CC","AUSC"])) {
                $variablesOlap[] = $variable["cod_variable"];
                $useTblOlap = true;
                if($variable["agregacion"] == "MEDIA")    $caseOLAP[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_media END AS \"". $variable["cod_variable"]."\"";
                if($variable["agregacion"] == "ACUMUL")   $caseOLAP[] = "CASE WHEN cod_variable LIKE '". $variable["cod_variable"] ."' THEN valor_acum END AS \"". $variable["cod_variable"]."\"";
            }
            
        }

        $coalesce = implode(",", $coalesce);

        if($useTblPiezoA){
            $casePiezoA = implode(",", $casePiezoA);
            $inPiezoA = "'". implode("','", $variablesPiezoA) ."'";
        }

        if($useTblPiezoM){
            $casePiezoM = implode(",", $casePiezoM);
            $inPiezoM = "'". implode("','", $variablesPiezoM) ."'";
        }

        if($useTblOlap){
            $caseOLAP = implode(",", $caseOLAP);
            $inOLAP = "'". implode("','", $variables) ."'";
        }

        $query = "SELECT
            timestamp,
            ".$coalesce."
            FROM";

            if($useTblOlap){
                $query.="
                (
                    SELECT
                        fecha as timestamp,
                        ".$caseOLAP."
                    FROM (
                        select to_date(concat(LPAD(anyo::text, 4, '0'), LPAD(mes::text, 2, '0'), LPAD(dia::text, 2, '0')), 'YYYYMMDD') fecha, cod_variable, valor_media, valor_acum 
                        from tbl_olap_medidas_dias
                        where cod_variable in (".$inOLAP.") and data_medicion >= to_timestamp(".$fechaIni.")::date 
                        and data_medicion <= to_timestamp(".$fechaFin.")::date
                        and ind_confirmacion like 'S'
                    ) AS valores
                ) AS valores_olap";
            }
            
            if($useTblPiezoA){

                if($useTblOlap) $query.=" UNION ";

                $query.="(
                    select
                        timestamp::date,
                        ".$casePiezoA."
                    from tbl_dato_piezo
                    where timestamp >= to_timestamp(".$fechaIni.")::date and timestamp <= to_timestamp(".$fechaFin.")::date
                    and cod_variable IN (" . $inPiezoA . ")
                    and validez = 1
                ) AS valores_piezoa";
            }

            if($useTblPiezoM){

                if($useTblOlap || $useTblPiezoA) $query.=" UNION ";

                $query.="(
                    select
                        timestamp::date,
                        ".$casePiezoM."
                    from tbl_dato_piezo_manual
                    where timestamp >= to_timestamp(".$fechaIni.")::date and timestamp <= to_timestamp(".$fechaFin.")::date
                    and cod_variable IN (" . $inPiezoM . ")
                ) AS valores_piezom";
            }

            $query.="
            GROUP BY timestamp
            ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionDiezminutal($DB, $variables, $variablesInfo, $fechaIni, $fechaFin, $order){

        $coalesce = [];
        foreach($variablesInfo as $variable){
            $coalesce[] = "coalesce(SUM(\"".$variable["cod_variable"]."\"),null) AS \"".$variable["cod_variable"]."\"";
        }
        $coalesce = implode(",", $coalesce);

        $cases = [];
        foreach($variables as $variable){
            $cases[] = "case when cod_variable like '".$variable."' then valor end as \"".$variable."\"";
        }
        $cases = implode(",", $cases);

        $in = "'". implode("','", $variables) ."'";

        $query = "with 
            medidas AS (
                select cod_variable, valor, timestamp_medicion as timestamp 
                from tbl_hist_medidas 
                where timestamp_medicion >= to_timestamp(".$fechaIni.")
                and timestamp_medicion <= to_timestamp(".$fechaFin.") 
                and cod_variable IN (".$in.")
                and ind_validacion = 'S'
            ),
            max_medidas AS (
                select cod_variable, max(timestamp) from medidas group by cod_variable
            ),
            datostrymedidas AS (
                select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                where timestamp >= to_timestamp(".$fechaIni.")  
                and timestamp <= to_timestamp(".$fechaFin.") 
                and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                and tbl_dato_tr.cod_variable IN (".$in.")
                union
                select cod_variable, valor, timestamp from medidas
            )
            
            select 
                timestamp,
                ".$coalesce."
                from( 
                    select timestamp, 
                    substring(cod_variable,0,6) as estacion,
                    ".$cases."
                    from datostrymedidas
                    WHERE (extract(epoch from DATE_TRUNC('minute',timestamp))/600 - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/600))=0
                ) as valores
                group by timestamp 
                order by timestamp ". $order;

        $data = ConexionBD::EjecutarConsultaConDB($DB, $query);



        return $data;
    
    }

    static function crearEncabezado($variablesInfo){
        
        // grupos por tipo de estación 
        $cabecera1Temp = [];
        $cabecera2Temp = [];
        $cabecera3Temp = [];
        $cabeceras = [];
        
        // crear la primera cabecera temporal que agrupa y realiza un conteo de las variables que se incluyen en cada
        foreach($variablesInfo as $variable){
            if(!isset($cabecera1Temp[$variable["tipo_estacion"]])) $cabecera1Temp[$variable["tipo_estacion"]] = ["id"=>$variable["descripcion_tipo"], "colspan"=>0, "nombre" => $variable["descripcion_tipo"], "variables" => []];
            $cabecera1Temp[$variable["tipo_estacion"]]["colspan"] += 1;
            // dar nombre a la variable e incluirla en el array de variables del tipo de estación
            $cabecera1Temp[$variable["tipo_estacion"]]["variables"][] = ["id"=>$variable["cod_estacion"], "nombre" => trim($variable["cod_estacion"]).": ".$variable["descripcion_variable"], "cod_variable" => $variable["cod_variable"]];
        }

        // crear la segunda cabecera temporal a partir de la primera cabecera
        foreach($cabecera1Temp as $cabecera1TipoEstacion){
            foreach($cabecera1TipoEstacion['variables'] as $variable){
                $cabecera2Temp[] = $variable;
            }
        }

        // crear la cabecera principal temporal a partir de la segunda cabecera
        //$cabecera3Temp[] = ["id" => "timestamp", "cod_campo" => "timestamp"]; // añadir columna timestamp
        foreach($cabecera2Temp as $cabecera2variable){
            $variablePartida = explode("/", $cabecera2variable["cod_variable"]);
            $cabecera3Temp[] = ["id" => $cabecera2variable["cod_variable"], "cod_campo" => $variablePartida[1]]; 
        }

        $cabeceras[0][] = ["id"=>"vacio","nombre"=>'', "rowspan"=>2]; // añadir columna vacía a la primera cabecera comlementaria

        // crear un array definitivo con la primera cabecera complementaria de tipos de estacion
        foreach($cabecera1Temp as $cabecera1){
            $cabeceras[0][] = ["id"=>$cabecera1["nombre"], "nombre"=>$cabecera1["nombre"], "colspan"=>$cabecera1["colspan"]];
        }

        // crear un array definitivo con la segunda cabecera complementaria de tipos de variable
        foreach($cabecera2Temp as $cabecera2){
            $cabeceras[1][] = ["id"=>$cabecera2["nombre"], "nombre"=>$cabecera2["nombre"]];
        }

        $cabeceras[2][] = ["id" => "timestamp", "cod_campo" => "timestamp"]; // añadir columna timestamp a la cabecera principal

        // crear un array definitivo con la cabecera principal
        foreach($cabecera3Temp as $cabecera){
            $cabeceras[2][] = $cabecera;
        }

        return $cabeceras;

    }

    static function insertarCORALIV($DB, $datos, $CORALIVRES){

        foreach($CORALIVRES as $estacion => $variables){
            

            $query2 = "select cod_nivel, nivel, volumen, fecha_inicio, fecha_fin from tbl_nivel where cod_nivel like '".$estacion."/ALIV' OR cod_nivel like '".$estacion."/COR' OR cod_nivel like '".$estacion."/RE1' OR cod_nivel like '".$estacion."/RE2'";
            $dataNiveles = ConexionBD::EjecutarConsultaConDB($DB, $query2);

            // formatear a decimales los resultados
            foreach($dataNiveles as &$lineaNivelesformatear){
                foreach($lineaNivelesformatear as $key => &$columnaNiveles){
                    if(!in_array($key, ['cod_nivel','fecha_inicio','fecha_fin'])){
                        $columnaNiveles = floatval($columnaNiveles);
                        $columnaNiveles = round($columnaNiveles,6);
                    }
                }
            }

            $niveles = [];

            foreach ($dataNiveles as $lineaNiveles){
               
                switch($lineaNiveles['cod_nivel']){
                    case $estacion."/COR":
                        $niveles["COR"] = $lineaNiveles['nivel'];
                        break;
                    case $estacion."/ALIV":
                        $niveles["ALIV"] = $lineaNiveles['nivel'];
                        break;
                    case $estacion."/RE1":
                        //echo 'test';
                        $niveles["NRE1"] = $lineaNiveles['nivel'];
                        $niveles["VRE1"] = round($lineaNiveles['volumen'], 2);
                        $niveles["fecha_inicio_RE1"] = $lineaNiveles['fecha_inicio'];
                        $niveles["fecha_fin_RE1"] = $lineaNiveles['fecha_fin'];
                        break;
                    case $estacion."/RE2":
                        $niveles["NRE2"] = $lineaNiveles['nivel'];
                        $niveles["VRE2"] = round($lineaNiveles['volumen'], 2);
                        $niveles["fecha_inicio_RE2"] = $lineaNiveles['fecha_inicio'];
                        $niveles["fecha_fin_RE2"] = $lineaNiveles['fecha_fin'];
                        break;
                }
            }     
    
            $dataRetorno = [];
    
            // Insertar los datos estaticos de la segunda query a los valores dinámicos por cada fecha
            foreach ($datos as $lineaDatos){

                $timestamp = $lineaDatos["timestamp"];

                if(in_array('COR', $variables)  && isset($niveles["COR"]))  $lineaDatos[$estacion."/COR"] = $niveles["COR"];
                if(in_array('ALIV', $variables) && isset($niveles["ALIV"])) $lineaDatos[$estacion."/ALIV"] = $niveles["ALIV"];
                if(in_array('RE', $variables))   $lineaDatos[$estacion."/RE"] = null;
                if(in_array('VRED', $variables))  $lineaDatos[$estacion."/VRED"] = null;
    
                // aplicar la coronación si existe (RE1 o RE2 según la fecha) y la diferencia de volumen del embalse y de la coronación
                if(isset($niveles["fecha_inicio_RE1"]) && isset($niveles["fecha_fin_RE1"]) && $lineaDatos[$estacion."/VE1"] != null){

                    $dt = new DateTime($timestamp);
                    $anyo_dato = $dt->format('Y');
                    $valTimestamp = $dt->getTimestamp();

                    $fechaInicioRE1Array = explode("-",$niveles["fecha_inicio_RE1"]);
                    $fecha_inicio_RE1_epoch = strtotime($anyo_dato."-".$fechaInicioRE1Array[1]."-".$fechaInicioRE1Array[2]); 
                    $fechaFinRE1Array = explode("-",$niveles["fecha_fin_RE1"]);
                    $fecha_fin_RE1_epoch = strtotime($anyo_dato."-".$fechaFinRE1Array[1]."-".$fechaFinRE1Array[2]); 
    
                    $fin_anyo = strtotime($anyo_dato."-12-31"); // final de año
                    $inicio_anyo = strtotime($anyo_dato."-01-01"); // principios de año
    
                    // si la fecha inicial es superior a la final significa que no existe un rango dentro del mismo año
                    if($fecha_inicio_RE1_epoch > $fecha_fin_RE1_epoch){
    
                        if( 
                            ( $valTimestamp >= $fecha_inicio_RE1_epoch && $valTimestamp <= $fin_anyo ) || 
                            ( $valTimestamp >= $inicio_anyo && $valTimestamp <= $fecha_fin_RE1_epoch )
                        ){
                            if(in_array('RE', $variables))  $lineaDatos[$estacion."/RE"] = $niveles["NRE1"];
                            if(in_array('VRED', $variables)) $lineaDatos[$estacion."/VRED"] =  $niveles["VRE1"]  -  $lineaDatos[$estacion."/VE1"] ;
                        } 
    
                    } else {
    
                        if($valTimestamp >= $fecha_inicio_RE1_epoch && $valTimestamp <= $fecha_fin_RE1_epoch){
                            if(in_array('RE', $variables))  $lineaDatos[$estacion."/RE"] = $niveles["NRE1"];
                            if(in_array('VRED', $variables)) $lineaDatos[$estacion."/VRED"] =  $niveles["VRE1"]  -  $lineaDatos[$estacion."/VE1"] ;
                        }
    
                    }
    
                }
    
                if(isset($niveles["fecha_inicio_RE2"]) && isset($niveles["fecha_fin_RE2"]) && $lineaDatos[$estacion."/VE1"] != null){

                    $dt = new DateTime($timestamp);
                    $anyo_dato = $dt->format('Y');
                    $valTimestamp = $dt->getTimestamp();
    
                    $fechaInicioRE2Array = explode("-",$niveles["fecha_inicio_RE2"]);
                    $fecha_inicio_RE2_epoch = strtotime($anyo_dato."-".$fechaInicioRE2Array[1]."-".$fechaInicioRE2Array[2]); 
                    $fechaFinRE2Array = explode("-",$niveles["fecha_fin_RE2"]);
                    $fecha_fin_RE2_epoch = strtotime($anyo_dato."-".$fechaFinRE2Array[1]."-".$fechaFinRE2Array[2]); 
    
                    $fin_anyo = strtotime($anyo_dato."-12-31"); // final de año
                    $inicio_anyo = strtotime($anyo_dato."-01-01"); // principios de año
    
                    if($fecha_inicio_RE2_epoch > $fecha_fin_RE2_epoch){

                        if( 
                            ( $valTimestamp >= $fecha_inicio_RE2_epoch && $valTimestamp <= $fin_anyo ) || 
                            ( $valTimestamp >= $inicio_anyo && $valTimestamp <= $fecha_fin_RE2_epoch )
                        ){
                            if(in_array('RE', $variables))  $lineaDatos[$estacion."/RE"] = $niveles["NRE2"];
                            if(in_array('VRED', $variables)) $lineaDatos[$estacion."/VRED"] = $niveles["VRE2"] - $lineaDatos[$estacion."/VE1"];
                        } 
    
                    } else {

                        if($valTimestamp >= $fecha_inicio_RE2_epoch && $valTimestamp <= $fecha_fin_RE2_epoch){
                            if(in_array('RE', $variables))  $lineaDatos[$estacion."/RE"] = $niveles["NRE2"];
                            if(in_array('VRED', $variables)) $lineaDatos[$estacion."/VRED"] = $niveles["VRE2"] - $lineaDatos[$estacion."/VE1"];
                        }
    
                    }
    
                }
    
                $dataRetorno[] = $lineaDatos;
            }
        }

        return $dataRetorno;

    }
}