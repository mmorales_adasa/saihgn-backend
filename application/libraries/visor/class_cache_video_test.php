<?php 

class CacheVideoTest { 

    static function CachearVideo(){
        $cambiosGlobales = 0;
        $videoPath = APPPATH . "libraries/visor/video/";
        $videoCachePath = APPPATH . "libraries/visor/video_cache_test/";

        if (!file_exists($videoCachePath)) {
            mkdir($videoCachePath, 0777, true);
        }
        $files = scandir($videoPath);
        foreach($files as $file){
            if(is_dir($videoPath . $file) and substr( $file, 0, 1 ) !== "."){
                $embalseDirPath = $videoPath . $file . '/';
                $embalseDirCachePath = $videoCachePath . $file . '/';
                if (!file_exists($embalseDirCachePath)) {
                    chdir($videoCachePath);
                    mkdir($file, 0777, true);
                }

                $cambios = CacheVideoTest::copyFilesVideo($embalseDirPath, $embalseDirCachePath, 1);
                $cambiosGlobales = $cambiosGlobales + $cambios;
            }
        }
        return $cambiosGlobales;
    }

    private static function copyFilesVideo($path, $pathCache, $selectFileModulus){
       
        $cambios = 0;
        chdir($path);

        // obtener ficheros 
        $allfiles = glob("*.*");

        $files = [];
        foreach($allfiles as $file) {
            $files[] = $file;
        }

        chdir($pathCache);
        
        // obtener ficheros por orden de modificación en directorio de destino
        $files2 = array_reverse(glob("*.*"));

        $files2MD5 = [];
        foreach($files2 as $file2){
            $files2MD5[] = md5_file($pathCache.$file2);
        }

        // realizar una copia de los ficheros existentes
        foreach($files as $file){
            
            $fileMD5 = md5_file($path.$file);

            if (is_dir($path.$file)) {
                break;
            }

            //echo $fileMD5;
            //var_dump($files2MD5);
            //echo "------------------------------------------------------------------------------------";

            if(!in_array($fileMD5, $files2MD5)){
                $cambios++;

                copy($path.$file, $pathCache.$file);
            }

        }

        // borrar ficheros de destino que no existan en origen
        /*foreach($files2 as $file2){
            if(!in_array($file2, $files)){
                //$cambios++;
                unlink($file2);
            }
        }*/

        return $cambios;
    }

}
?>