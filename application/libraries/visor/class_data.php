<?php 
require_once APPPATH.'/libraries/visor/AppRTAPServlet.php';
require_once APPPATH.'/libraries/visor/class_operaciones.php';
require_once APPPATH.'/libraries/visor/class_conexion.php';

class ObtenerDatos {

	/**
     * @nombre 		RTAP
     * @descripcion obtiene datos del scada RTAP de datos en tiempo real
	 * v�lido para E, CR, CP
	 */
    static function DatosRTAP() {

        $request = new AppRTAPServlet("http://172.21.0.4:2222/RTAPiWS/GetVariablesValues");
        $resultado = $request->get_xml(APPPATH.'/libraries/visor/xmls/datos_estaciones.xml');

        $datosE =  ObtenerDatos::TraducirDatos($resultado, 'E');
        $datosCR = ObtenerDatos::TraducirDatos($resultado, 'CR');
		$datosCP = ObtenerDatos::TraducirDatos($resultado, 'CP');
        $datosEM = ObtenerDatos::TraducirDatos($resultado, 'EM');
        $datosNR = ObtenerDatos::TraducirDatos($resultado, 'NR');

        $datos = Array();
        if(isset($datosE))  array_push($datos, $datosE);
        if(isset($datosCR)) array_push($datos, $datosCR);
        if(isset($datosCP)) array_push($datos, $datosCP);
        if(isset($datosEM)) array_push($datos, $datosEM);
        if(isset($datosNR)) array_push($datos, $datosNR);

		$datos_ordenado=array();
		$redes=array("E","CR","CP","EM","NR");
		foreach($redes as $key=>$red){
            if(isset($datos[$key])){
                foreach($datos[$key] as $dato){
                    // revisar si el dato es numérico
                    if(isset($dato['valor']) && is_numeric($dato['valor'])){
                        $datos_ordenado[$red][$dato['cod_variable']]['cod_variable']=$dato['cod_variable'];
                        $datos_ordenado[$red][$dato['cod_variable']]['valor']=$dato['valor'];
                        $datos_ordenado[$red][$dato['cod_variable']]['timestamp']=$dato['timestamp'];
                    }
                }
            }
		}
        return $datos_ordenado; //Aqui estan todos del visor tr la posicion 0 es de E, la posicion 1 es de CR, la posicion 2 es de CP y la posici�n 3 es EM
    }

    /**
     * @nombre 		RTAP
     * @descripcion obtiene datos del scada RTAP de datos en tiempo real
	 * v�lido para E, CR, CP
	 */
    static function DatosRTAPRAW() {
        $request = new AppRTAPServlet("http://172.21.0.4:2222/RTAPiWS/GetVariablesValues");
        $resultado = $request->get_xml(APPPATH.'/libraries/visor/xmls/datos_estaciones.xml');
        return $resultado;
    }
	
	static function TraducirDatos($resultado, $tipo) {
		require APPPATH.'/libraries/visor/config.php';
        $aux = array();
        if(isset($resultado[$tipo])){
            foreach ($resultado[$tipo] as $filas) {
                $estacion = $filas['label'];
                $variable = "";
                $valor = "";
                $tiempo = Operaciones::CambioFormatoFechaRTAP($filas['Ultima_com']); //cambia el formato de la fecha del RTAP a la habitual del SAIH
                // si la hora es nula no se procesa el dato
                if($tiempo!=null){
                    foreach ($filas as $clave => $fila) {
                        foreach ($diccionario as $i => $termino){
                            if ($clave == $termino[0]){
                                $variable = $estacion.'/'.$diccionario[$i][1];
                                $valor = $fila;
                                array_push($aux, array('cod_variable' => $variable, 'valor' => $valor, 'timestamp' => $tiempo));
                            }
                        }
                    }
                }
            }
            return $aux;
        }
    }
    
	//Obtiene la cota actual de los embalses
	static function CotaActualEmbalse($datosRTAP) {
        $aux=array();
		$resultado=array();
		foreach($datosRTAP['E'] as $datosV){
			$aux = explode('/',$datosV['cod_variable']);
			if($aux[1]=='NE1'){
				$row['cod_nivel']=$datosV['cod_variable'];
				$row['nivel']=$datosV['valor'];
				array_push($resultado, $row);
			}
		}
		return $resultado;
    }

    static function DatosFijos($campos, $tabla, $categoria, $estacion = null, $cod_masasub = null, $cod_zona_regable = null, $DB = null) {

        if($campos) $seleccion = Operaciones::campos2seleccion($campos);

        if($cod_masasub != null){
            $andMasasub = "AND cod_masasub = '". $cod_masasub . "'";
        } else {
            $andMasasub = "";
        }

        if($cod_zona_regable != null){
            $andZonaRegable = "AND tezr.cod_zona_regable = '". $cod_zona_regable . "'";
        } else {
            $andZonaRegable = "";
        }

        switch ($categoria) {

            case ($categoria == "E" or $categoria == "AEX"):

                if($estacion) {
                    
                    $query = "SELECT " . $seleccion . " 
                        FROM tbl_estacion e 
                        LEFT JOIN tbl_presa p ON e.cod_estacion = p.cod_estacion
                        LEFT JOIN tbl_estacion_zona_regable tezr ON e.cod_estacion = tezr.cod_estacion
                        WHERE e.tipo='E' 
                        AND e.disponible = true " 
                        . $andZonaRegable . " 
                        AND e.cod_estacion = '" . $estacion . "' 
                        ORDER by e.orden asc";

                } else {

                    $query = "SELECT " . $seleccion . " 
                        FROM tbl_estacion e 
                        LEFT JOIN tbl_presa p ON e.cod_estacion = p.cod_estacion
                        LEFT JOIN tbl_estacion_zona_regable tezr ON e.cod_estacion = tezr.cod_estacion
                        WHERE e.tipo='E' and e.zona <> 'Z8'
                        AND e.disponible = true " 
                        . $andZonaRegable . " 
                        ORDER by e.orden asc";

                }

                break;

            case ($categoria == "Z12" or $categoria == "Z3" or $categoria == "Z5" or $categoria == "Z67"):
                                
                $query = "
                    SELECT " . $seleccion . " 
                    FROM tbl_estacion e 
                    LEFT JOIN tbl_presa p ON e.cod_estacion = p.cod_estacion
                    LEFT JOIN tbl_estacion_zona_regable tezr ON e.cod_estacion = tezr.cod_estacion
                    WHERE e.cod_estacion = p.cod_estacion
                     AND e.tipo='E' 
                     and e.zona <> 'Z8' 
                     and e.zona = '" . $categoria . "' 
                    AND e.disponible = true 
                    ORDER by e.orden asc";

                break;
                
            case ($categoria == "NR" 
                or $categoria == "CR" 
                or $categoria == "EAA" 
                or $categoria == "ECC" 
                or $categoria == "EXC"  
                or $categoria == "R" 
                or $categoria == "CC"
                or $categoria == "PZM"
                or $categoria == "PZ"
            ):
                
                if($estacion) { //estacion específica

                    $query = "
                        SELECT " . $seleccion . " 
                        FROM tbl_estacion e 
                        LEFT JOIN tbl_estacion_zona_regable tezr on tezr.cod_estacion  = e.cod_estacion 
                        LEFT JOIN tbl_masasub m on e.cod_masasub = m.code
                        WHERE disponible 
                        AND e.tipo='" . $categoria . "' 
                        AND e.disponible = true 
                        and e.cod_estacion = '" . $estacion . "' 
                        ". $andMasasub ." 
                        ". $andZonaRegable;

                } else {

                    $query = "
                        SELECT " . $seleccion . " 
                        FROM tbl_estacion e 
                        LEFT JOIN tbl_estacion_zona_regable tezr on tezr.cod_estacion  = e.cod_estacion 
                        LEFT JOIN tbl_masasub m on e.cod_masasub = m.code
                        WHERE disponible 
                        AND e.tipo='" . $categoria . "' 
                        AND e.disponible = true 
                        ". $andMasasub ." 
                        ". $andZonaRegable ." 
                        ORDER by e.zona asc, e.orden asc, e.cod_estacion asc";

                }   
                
                break;
            case ($categoria == "EMAEMET"):
            
                if($estacion) {
                    $query = "SELECT 'EMAEMET' as tipo, cod_estacion, nombre FROM tbl_estacion_aemet WHERE cod_estacion = '" . $estacion . "' AND disponible = true and automatica = true ORDER by cod_estacion asc";
                } else {
                    $query = "SELECT 'EMAEMET' as tipo, cod_estacion, nombre FROM tbl_estacion_aemet WHERE disponible = true and automatica = true ORDER by cod_estacion asc";
                }   
                
                break;

            case ($categoria == "EM"): // EM contiene CR con meteo

                if($estacion) { //estacion específica

                    $query = "
                        SELECT " . $seleccion . " 
                        FROM tbl_estacion e 
                        LEFT JOIN tbl_estacion_zona_regable tezr on tezr.cod_estacion  = e.cod_estacion 
                        LEFT JOIN tbl_masasub m on e.cod_masasub = m.code
                        WHERE disponible 
                        AND (e.tipo='EM' OR (e.tipo='CR' AND e.estacion_meteo) OR (e.tipo='E' AND e.estacion_meteo))
                        AND e.disponible = true 
                        and e.cod_estacion = '" . $estacion . "' 
                        ". $andMasasub ." 
                        ". $andZonaRegable;

                } else {

                    $query = "
                        SELECT " . $seleccion . " 
                        FROM tbl_estacion e 
                        LEFT JOIN tbl_estacion_zona_regable tezr on tezr.cod_estacion  = e.cod_estacion 
                        LEFT JOIN tbl_masasub m on e.cod_masasub = m.code
                        WHERE disponible 
                        AND (e.tipo='EM' OR (e.tipo='CR' AND e.estacion_meteo) OR (e.tipo='E' AND e.estacion_meteo))
                        AND e.disponible = true 
                        ". $andMasasub ." 
                        ". $andZonaRegable ." 
                        ORDER by e.zona asc, e.orden asc, e.cod_estacion asc";

                }  

                break;

            
        }

        return ConexionBD::EjecutarConsultaConDB($DB, $query);
    }


	static function DatosVariables($campos, $tabla, $rango, $datosRTAP=null, $estacion = null, $DB = null) {
		
		switch ($rango){
			
			case '0':
				return $datosRTAP['E'];
				break;
			
			case (in_array($rango, array("E","CR","EM","AEX","Z12","Z3","Z5","Z67"))):
				
				if(in_array($rango, array("E","AEX","Z12","Z3","Z5","Z67"))){
					return $datosRTAP['E'];
				} else {
					return $datosRTAP[$rango];
				}
				break;

            case (in_array($rango, array("PZ"))):

                if($estacion!=null){
                    $query = "SELECT cod_variable, timestamp, valor, validez, estado
                    FROM tbl_ultimo_dato_medido d LEFT JOIN tbl_estacion e ON e.cod_estacion = '".$estacion."' WHERE '".$estacion."' = Split_part(d.cod_variable,'/',1)";
                } else {
                    $query = "SELECT cod_variable, timestamp, valor, validez, estado
                    FROM tbl_ultimo_dato_medido d LEFT JOIN tbl_estacion e ON e.tipo like 'PZ' WHERE e.cod_estacion = Split_part(d.cod_variable,'/',1)";
                }

                $datos = [];
                $resultado = ConexionBD::EjecutarConsultaConDB($DB, $query);

                // formatear y aplicar invalidaciones a los datos recibidos
                foreach($resultado as $variable){
                    $datos[$variable['cod_variable']] = $variable;
                }

                return $datos;
                break;

			case (in_array($rango, array("EAA","ECC","EXC"))): 
				
                if($estacion!=null){

                    // datos de la estación
                    $query = "SELECT cod_variable,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN timestamp
                    END as timestamp,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN d.valor
                    END as valor,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN d.validez
                    END as validez,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN d.estado
                    END as estado
                    
                    FROM tbl_ultimo_dato_medido d LEFT JOIN tbl_estacion e ON e.cod_estacion = '".$estacion."' WHERE '".$estacion."' = Split_part(d.cod_variable,'/',1)";
                   
                } else {

                    // datos de la estación
                    $query = "SELECT cod_variable,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN timestamp
                    END as timestamp,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN d.valor
                    END as valor,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN d.validez
                    END as validez,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN d.estado
                    END as estado
                    
                    FROM tbl_ultimo_dato_medido d LEFT JOIN tbl_estacion e ON e.cod_estacion like '".$rango."%' WHERE e.cod_estacion = Split_part(d.cod_variable,'/',1)";
                }

                $datos = [];
                $resultado = ConexionBD::EjecutarConsultaConDB($DB, $query);

                // formatear y aplicar invalidaciones a los datos recibidos
				foreach($resultado as $variable){
                    if(isset($variable['validez']) && is_numeric($variable['validez']) && intval($variable['validez']) == 2){
                        $variable['valor'] = 'No válido';
                    } 
                    $datos[$variable['cod_variable']] = $variable;
				}

				return $datos;
				break;
            
            case (in_array($rango, array("NR"))): 
                // datos de db
                $query = "SELECT * FROM ".$tabla." WHERE d.cod_variable like '$rango%'";
				$resultado = ConexionBD::EjecutarConsultaConDB($DB, $query);
				foreach($resultado as $variable){
					$datos[$variable['cod_variable']] = $variable;
				}
                // parche: insertar datos de rtap de las estaciones NR2-35/36
                if(isset($datosRTAP['NR'])){
                    if(isset($datosRTAP['NR']['NR2-35/NR1'])){
                        $datos['NR2-35/NR1'] = $datosRTAP['NR']['NR2-35/NR1'];
                    }
                    if(isset($datosRTAP['NR']['NR2-36/NR1'])){
                        $datos['NR2-36/NR1'] = $datosRTAP['NR']['NR2-36/NR1'];
                    }
                }
				return $datos;
                break;

            case (in_array($rango, array("EMAEMET"))): 
        
                if($estacion!=null){
                    $query = "SELECT cod_variable, timestamp, valor, true as validez, estado
                    FROM tbl_ultimo_dato_aemet d LEFT JOIN tbl_estacion_aemet e ON e.cod_estacion = '".$estacion."' WHERE '".$estacion."' = Split_part(d.cod_variable,'/',1)";
                } else {
                    $query = "SELECT cod_variable, timestamp, valor, true as validez, estado FROM tbl_ultimo_dato_aemet d";
                }

                $datos = [];
                $resultado = ConexionBD::EjecutarConsultaConDB($DB, $query);

                // formatear y aplicar invalidaciones a los datos recibidos
                foreach($resultado as $variable){
                    $datos[$variable['cod_variable']] = $variable;
                }

                return $datos;
                break;

			case ($rango=="NIV"):
			///case ($rango=="NIV" or $rango=="ECC" or $rango=="EM" or $rango=="R"):
				$query = "SELECT * FROM $tabla";
				return ConexionBD::EjecutarConsultaConDB($DB, $query);
				break;
			
			default: //para las presas
				return $datosRTAP['E'];
		}
    }

    static function UltimosDatos($DB) {

        // Llamada que obtiene los ultimos datos de distintos origenes señalado por la tabla de variables

        // EXC: Evitar mostrar resto de datos de estación cuando el caudal vertido tiene el valor 0.

        $query = "WITH

        ultimos_datos_postprocesado AS (
            select 
            cod_variable,
            CASE 
                WHEN exc_nocaudal IS NOT NULL AND split_part(udm.cod_variable,'/',2) NOT LIKE 'QV' THEN NULL
                ELSE udm.timestamp 
            END AS timestamp,
            CASE 
                WHEN exc_nocaudal IS NOT NULL AND split_part(udm.cod_variable,'/',2) NOT LIKE 'QV' THEN NULL
                ELSE valor 
            END AS valor,
            validez,
            estado
    
            from tbl_ultimo_dato_medido udm left join
                (select 
                    split_part(udm.cod_variable,'/',1) exc_nocaudal
                from tbl_ultimo_dato_medido udm 
                join tbl_estacion e ON split_part(udm.cod_variable,'/',1) = TRIM(e.cod_estacion) and e.tipo like 'EXC'
                where split_part(udm.cod_variable,'/',2) like 'QV' and udm.valor = 0 and udm.timestamp != '1970-01-01 00:00:00') EXCNC 
            ON split_part(udm.cod_variable,'/',1) = EXCNC.exc_nocaudal
        ) 
    
        select 
            datos.cod_variable,
            max(timestamp) as timestamp,
            val as valor
        FROM (	
            SELECT
                variables.cod_variable, 
                CASE 
                    WHEN origen_ultimo_dato like 'tbl_ultimo_dato_medido' AND udm.timestamp != '1970-01-01 00:00:00' THEN udm.timestamp
                    WHEN origen_ultimo_dato like 'tbl_ultimo_dato_rtap' AND udmr.timestamp != '1970-01-01 00:00:00' THEN udmr.timestamp
                    WHEN origen_ultimo_dato like 'tbl_olap_medidas_dias' THEN omd.data_medicion
                    ELSE null
                END as timestamp,
                CASE 
                    WHEN origen_ultimo_dato like 'tbl_ultimo_dato_medido' AND udm.timestamp != '1970-01-01 00:00:00' THEN udm.valor
                    WHEN origen_ultimo_dato like 'tbl_ultimo_dato_rtap' AND udmr.timestamp != '1970-01-01 00:00:00' THEN udmr.valor
                    WHEN origen_ultimo_dato like 'tbl_olap_medidas_dias' and agregacion like 'MEDIA' THEN omd.valor_media
                    WHEN origen_ultimo_dato like 'tbl_olap_medidas_dias' and agregacion like 'ACUMUL' THEN omd.valor_acum
                    ELSE null
                END as val
            FROM (	
                SELECT 
                    v.cod_variable, 
                    v.origen_ultimo_dato,
                    tv.agregacion,
                    e.estado_adquisicion
                FROM tbl_variable v
                LEFT JOIN tbl_tipo_variable tv ON v.cod_tipo_variable = tv.cod_tipo_variable
                LEFT JOIN tbl_estacion e ON e.cod_estacion = Split_part(v.cod_variable,'/',1)
                WHERE v.disponible = 1
            ) AS variables
            LEFT JOIN ultimos_datos_postprocesado udm ON origen_ultimo_dato like 'tbl_ultimo_dato_medido' AND variables.cod_variable = udm.cod_variable /*AND udm.validez = 1*/ AND estado_adquisicion != 2
            LEFT JOIN tbl_ultimo_dato_rtap udmr ON origen_ultimo_dato like 'tbl_ultimo_dato_rtap' AND variables.cod_variable = udmr.cod_variable AND estado_adquisicion != 2
            LEFT JOIN tbl_olap_medidas_dias omd ON origen_ultimo_dato like 'tbl_olap_medidas_dias' AND omd.data_medicion > current_date - interval '30' day  AND variables.cod_variable = omd.cod_variable AND estado_adquisicion != 2
        ) datos
        group by datos.cod_variable, val";

        $variables = ConexionBD::EjecutarConsultaConDB($DB, $query);

        return $variables;
		
    }

    /*

    static function UltimosDatos($DB, $datosRTAP) {

        // 1.conocer variables disponibles y sus origenes, por optimización se obtienen ya los datos del origen tbl_ultimo_dato_medido
        $query = "
        select 
            v.cod_variable, 
            v.origen_ultimo_dato,
            Split_part(v.cod_variable,'/',1) as cod_estacion,
            Split_part(v.cod_variable,'/',2) as tipo_variable,
            CASE 
                WHEN e.estado_adquisicion = 2  THEN null
                WHEN e.estado_adquisicion != 2  THEN udm.timestamp
            END as timestamp,
            CASE 
                WHEN e.estado_adquisicion = 2  THEN null
                WHEN e.estado_adquisicion != 2  THEN udm.valor
            END as valor,
            CASE 
                WHEN e.estado_adquisicion = 2  THEN null
                WHEN e.estado_adquisicion != 2  THEN udm.validez
            END as validez,
            e.estado_adquisicion
        from tbl_variable v LEFT JOIN tbl_ultimo_dato_medido udm ON origen_ultimo_dato like 'tbl_ultimo_dato_medido' AND v.cod_variable = udm.cod_variable
        LEFT JOIN tbl_estacion e ON e.cod_estacion = Split_part(v.cod_variable,'/',1)
        where v.disponible = 1";

        $variables = ConexionBD::EjecutarConsultaConDB($DB, $query);

        // 2.insertar datos de rtap para variables de origen rtap
        // generar un array de todas las variables de rtap, iterando por grupos de estación e iterando entre datos de estación
        $variablesRTAP = [];
        if(isset($datosRTAP) && count($datosRTAP)>0 ){
            foreach($datosRTAP as $categoriaRTAP){
                foreach($categoriaRTAP as $variableRTAP){
                    $variablesRTAP[$variableRTAP["cod_variable"]] = $variableRTAP;
                }
            }
        }

        $variablesAssoc = [];
        // 3.indexar variables por cod_variable
        if(isset($variables) && count($variables)>0 ){
            foreach($variables as $variable){
                $variablesAssoc[$variable["cod_variable"]] = $variable;
            }
        }

        // 4.rellenar datos de rtap con el array de datos de la tabla tbl_ultimo_dato_medido
        if(isset($variablesAssoc) && count($variablesAssoc)>0 ){
            foreach($variablesAssoc as $key => $variable){
                // se hace la edición en el array de variables general si la variable es de tipo rtap y existe en el array de datos de variables de rtap
                if($variable["estado_adquisicion"] != 2 && $variable["origen_ultimo_dato"] == "rtap" && isset($variablesRTAP[$variable["cod_variable"]])){

                    if($variable["tipo_variable"] == "PRA") $variablesRTAP[$variable["cod_variable"]]["valor"] = null;

                    $variablesAssoc[$key]["valor"] = $variablesRTAP[$variable["cod_variable"]]["valor"];
                    $variablesAssoc[$key]["timestamp"] = $variablesRTAP[$variable["cod_variable"]]["timestamp"];
                }
            }

            // parche para calcular el PRA de la estación a partir del PI de la estación
            foreach($variablesAssoc as $key => $variable){
                if($variable["tipo_variable"] == "PI"){
                    if(isset($variablesAssoc[$variable["cod_estacion"] . "/PRA"])){
                        $variablesAssoc[$variable["cod_estacion"] . "/PRA"]["valor"] = $variable['valor'] / 12; 
                    }
                }
            }
        }

        // parche para pasar datos de lluvia de E2-20 a E2-21: se buscan los datos de E2-20 para su posterior edición en E2-21
        if(isset($variablesAssoc["E2-20/PI"])){
            $variablesAssoc["E2-21/PI"]["valor"] = $variablesAssoc["E2-20/PI"]["valor"];
            $variablesAssoc["E2-21/PI"]["timestamp"] = $variablesAssoc["E2-20/PI"]["timestamp"];
        }

        if(isset($variablesAssoc["E2-20/PRA"])){
            $variablesAssoc["E2-21/PRA"]["valor"] = $variablesAssoc["E2-20/PRA"]["valor"];
            $variablesAssoc["E2-21/PRA"]["timestamp"] = $variablesAssoc["E2-20/PRA"]["timestamp"];
        }

        return $variablesAssoc;
		
    }

    */
	
	//FUNCI�N PARA UNIR DATOS FIJOS Y VARIABLES CON C�DIGO DE ACR�NIMO
    static function UnirDatosFijosVariablesConTimestamp($codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV) {

        //var_dump($datosV);

        $datos = $datosF;
        foreach ($datosF as $claveF => $datoF) {
            $aux = "";
            foreach ($camposV as $claveV => $campoV) {
                $datos[$claveF][$campoV]['cod_variable'] = $campoV;
                $datos[$claveF][$campoV]['valor'] = ""; //inicializo todas las asociaciones de las variables y por orden para que si no existe valor ya se haya creado la posici�n y no d� problema en la tabla
                foreach ($datosV as $datoV) {

      

                    if ($datoV[$codigoV] == trim($datoF[$codigoF]) . '/' . $campoV) {
                        
                        if(is_numeric($datoV[$valor])){
                            /*if ($datoV[$valor] < -1000 or $datoV[$valor] > 100000) {
                                $datos[$claveF][$campoV]['valor'] = '';
                            } else {*/
                                $datos[$claveF][$campoV]['valor'] = floatval($datoV[$valor]);
                            //}
                        } else {
                            $datos[$claveF][$campoV]['valor'] = $datoV[$valor];
                        }

                        // No se tiene en cuenta el timestamp de variables de PI para evitar descuadres de ultimo timestamp entre estaciones que no tienen datos PI

                        if($campoV!="PI"){
                            $aux = max($aux, $datoV['timestamp']);
                        }

                    }

                }
            }
            $datos[$claveF]['timestamp']['valor'] = $aux; //establece como timestamp el mayor de todos tras recorrer todas las variables
        }

        // limpiar cod_variable,valor
        foreach($datos as &$linea){
            foreach($linea as &$columna){
                if(is_array($columna) && array_key_exists("valor", $columna)) $columna = $columna['valor'];
            }
        }

        return $datos;
    }

    //FUNCI�N PARA UNIR DATOS FIJOS Y VARIABLES CON C�DIGO DE ACR�NIMO
    static function UnirDatosFijosVariablesConTimestamp2($codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV) {
        $datos = $datosF;
        foreach ($datosF as $claveF => $datoF) {
            foreach ($camposV as $claveV => $campoV) {
                $datos[$claveF][$campoV]['cod_variable'] = $campoV;
                $datos[$claveF][$campoV]['valor'] = ''; //inicializo todas las asociaciones de las variables y por orden para que si no existe valor ya se haya creado la posici�n y no d� problema en la tabla
                foreach ($datosV as $clave_datoV => $datoV) {
                    if ($datoV[$codigoV] == trim($datoF[$codigoF])) {
                        if ($campoV == 'valor') {
                            $datos[$claveF][$campoV]['valor'] = $datoV['valor'];
                        } else {
                            $datos[$claveF][$campoV]['valor'] = $datoV['timestamp'];
                        }
                    }
                }
            }
        }

        // limpiar cod_variable,valor
        foreach($datos as &$linea){
            foreach($linea as &$columna){
                if(is_array($columna) && array_key_exists("valor",$columna)) $columna = $columna['valor'];
            }
        }

        return $datos;
    }

    //FUNCI�N PARA UNIR DATOS FIJOS Y VARIABLES CON C�DIGO DE ACR�NIMO
	//ARREGLAR el c�digo para limitar los valores muy altos y muy bajos
	static function UnirDatosFijosVariablesSinTimestamp($codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV) {
		$datos=$datosF;
		foreach ($datosF as $claveF => $datoF){
			foreach ($camposV as $claveV => $campoV){
				$datos[$claveF][$campoV]['cod_variable']=''; //inicializo todas las asociaciones de las variables y por orden para que si no existe valor ya se haya creado la posici�n y no d� problema en la tabla
				$datos[$claveF][$campoV]['valor']=''; //inicializo todas las asociaciones de las variables y por orden para que si no existe valor ya se haya creado la posici�n y no d� problema en la tabla
				foreach($datosV as $clave_datoV => $datoV){
					if ($datoV[$codigoV]==trim($datoF[$codigoF]).'/'.$campoV){
						$datos[$claveF][$campoV]['cod_variable']=trim($datoF[$codigoF]).'/'.$campoV;
						$datos[$claveF][$campoV]['valor'] = $datoV[$valor];
					}
				}
			}
		}

        // limpiar cod_variable,valor
        foreach($datos as &$linea){
            foreach($linea as &$columna){
                if(is_array($columna) && array_key_exists("valor",$columna)) $columna = $columna['valor'];
            }
        }

		return $datos;
    }

    //Obtiene el encabezado completo a partir de la matriz con los $campos con información de descripción de detalle y de qué variables se acumulan
	//COMPROBADO 24.05.2020
	/*static function obtenerEncabezado($campos) {
        $resultado=array();
		foreach ($campos as $campo){
			
			$campo=trim($campo);
			if (strpos($campo,'.')>0){ //Mira si los campos comienzan por p.*, c.* para quitar esa parte inicial
				$campo=explode('.',$campo)[1];
			}
			$aux=$campo; //Para no perder los números en el cod_campo
			//Caso de nombre de campos tipo variable
			if(strpos($campo,'/')){
				$campo=explode('/',$campo)[1];
			}

            // Borramos el código de campo antiguo en caso de renombrarlo en la query
            if(strpos($campo, ' as ')) {
                $campo = explode(' as ', $campo)[1];
            }

			//Otras excepciones
			if(substr($campo,0,3)<>'ext' and substr($campo,0,3)<>'ope' and substr($campo,0,3)<>'ABA' and substr($campo,0,3)<>'USR' and substr($campo,0,2)<>'RE'){ //Excepción para los campos que son extensiones y sí se deben diferencia de esa manera
				$campo=trim($campo, '1'); //Quita los valores 1, 2, 3, 4, 5 al final de una variable para buscar en la genérica
				$campo=trim($campo, '2');
				$campo=trim($campo, '3');
				$campo=trim($campo, '4');
				$campo=trim($campo, '5');
			}

			$query = "SELECT * FROM tbl_campo_encabezado WHERE cod_campo = '".$campo."' ";
            $formato_campo = ConexionBD::EjecutarConsultaConDB($DB, $query);
            if(isset($formato_campo[0])){
                $resultado[$aux] = $formato_campo[0];
            }
		}
		return $resultado;
	}*/
	
	//FUNCI�N PARA UNIR DATOS FIJOS Y VARIABLES CON SERIES TEMPORALES Y CON RESUMEN
    //REVISADO EL 11.06.2020
    static function UnirDatosFijosVariablesAgregados($codEstacion,$codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV) {

        $resultado = array();

        $campos = array_merge($camposF, $camposV); //mezcla los campos del encabezado
        $infoCampos = Operaciones::ObtenerInformacionCampos($codEstacion,$camposV);
		//Inicializo todas las variables del resumen menos la de las semanas que van al bucle
		$resumen = array();
        foreach ($campos as $campo) {
            $resumen['total'][$campo] = '';
        }
        $resumen['total'][$codigoF] = 'DIF./TOTAL';
        $resumen['total']['semana'] = '';
        $resumen['total']['dia_semana'] = '';

        $datos = $datosF; //se inicializa la matriz con los datos fijos a los que se van a a�adir el resto

        foreach ($camposV as $claveV => $campoV) {

            foreach ($datosF as $claveF => $datoF) {
                $semana = 's. ' . $datoF['semana'];
                //Inicializo las matrices a cero
                $datos[$claveF][$campoV] = '';

                $resumen[$semana]['semana'] = ''; //se va sobreescribiendo mientras que no cambie la semana
                $resumen[$semana][$codigoF] = $semana;
                $resumen[$semana]['dia_semana'] = '';

                $resumen[$semana][$campoV] = '';
            }
        }

        foreach ($datosV as $claveV => $datoV) {

            foreach ($datosF as $claveF => $datoF) {
                $semana = 's. ' . $datoF['semana'];

                foreach ($datoV as $linea) {
                    if ($linea[$codigoV] == $datosF[$claveF][$codigoF]) {

                        switch ($infoCampos[$claveV]['agregacion']) {
                            
                            case 'diferencia':

                                $datos[$claveF][$claveV] = floatval($linea[$valor[0]]);

                                if ($datos[$claveF][$claveV] <> '') {
                                    $resumen['total'][$claveV] = $datos[$claveF][$claveV];
                                    $resumen['total'][$claveV] = $datos[$claveF][$claveV];
                                }
                                //No hay parciales semanales de diferencia
                                break;

                            case 'suma':
                                $datos[$claveF][$claveV] = floatval($linea[$valor[1]]);

                                if($resumen['total'][$claveV]=='') $resumen['total'][$claveV] = 0;
                                if($resumen[$semana][$claveV]=='') $resumen[$semana][$claveV] = 0;

                                $resumen['total'][$claveV] = $datos[$claveF][$claveV] + $resumen['total'][$claveV];
                                $resumen[$semana][$claveV] = $datos[$claveF][$claveV] + $resumen[$semana][$claveV];
                                break;

                            default:
                                $datos[$claveF][$claveV] = floatval($linea[$valor[0]]);
                                $resumen['total'][$claveV] = '';
                        }

                        //A�ade estas propiedades si existen
                        if (array_key_exists('origen_dato', $linea)) {
                            $datos[$claveF][$claveV]['origen_dato'] = $linea['origen_dato'];
                        }
                        if (array_key_exists('validez', $linea)) {
                            $datos[$claveF][$claveV]['validez'] = $linea['validez'];
                        }
                    }
                }
            }
            //Hace la anotaci�n de la diferencia s�lo en los campos que no son ACUMUL
            if ($infoCampos[$claveV]['agregacion'] == 'diferencia' && is_numeric($datos[0][$claveV])) {

                $resumen['total'][$claveV] = $resumen['total'][$claveV] - floatval($datos[0][$claveV]); //Se resta el primer dato indexado como 0.
              
            }
            
            //var_dump($resumen);
            //return;
        }

        /*foreach($datos as &$linea){
            foreach($linea as &$columna){
                if(is_array($columna) && array_key_exists("valor",$columna)) $columna = $columna['valor'];
            }
        }*/

        $resultado['campos'] = $campos;
        $resultado['cuerpo'] = $datos;

        // se borran las llaves de resumen
        foreach($resumen as &$linea){
            $resultado['resumen'][] = $linea;
        }

        //var_dump($resumen);
        return $resultado;
    }
	
	//FUNCI�N PARA UNIR DATOS FIJOS Y VARIABLES SIN TIMESTAMP NI RESUMEN
    //REVISADO EL 28.05.2020
    static function UnirDatosFijosVariablesSinTimestampNiResumen($codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV) {
        $resultado = array();
        $campos = array_merge($camposF, $camposV); //mezcla los campos del encabezado
        $datos = $datosF; //se inicializa la matriz con los datos fijos a los que se van a a�adir el resto

        foreach ($datosV as $claveV => $datoV) {
            foreach ($datosF as $claveF => $datoF) {
                $datos[$claveF][$claveV]['cod_variable'] = $claveV; //inicializa todo a cero
                $datos[$claveF][$claveV]['valor'] = ''; //inicializa todo a cero
                foreach ($datoV as $linea) {
                    if ($linea[$codigoF] == $datosF[$claveF][$codigoF]) {
                        $datos[$claveF][$claveV]['valor'] = floatval($linea[$valor[0]]);
                    }
                }
            }
        }

        // limpiar cod_variable,valor
        foreach($datos as &$linea){
            foreach($linea as &$columna){
                if(is_array($columna) && array_key_exists("valor",$columna)) $columna = $columna['valor'];
            }
        }

        $resultado['campos'] = $campos;
        $resultado['cuerpo'] = $datos;

        return $resultado;
    }

    static function ObtenerCodCampoCompleto($cod_campo_completo) {
        if(strpos($cod_campo_completo, ' as ')) {
            $cod_campo_completo = explode(' as ', $cod_campo_completo)[1];
        }
        return $cod_campo_completo;
    }
}
