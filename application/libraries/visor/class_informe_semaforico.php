<?php
date_default_timezone_set("UTC");

require_once APPPATH . '/libraries/visor/class_conexion.php';

class InformeSemaforico
{
	private static $TOTAL_COLUMNS = ['sinAlerta', 'alerta', 'alarma', 'otros'];

	static function getEncabezado()
	{
		$encabezadoV2 = [
			["id" => "cod_estacion", "cod_campo" => "cod_estacion", "sticky" => true],
			["id" => "nombre", "cod_campo" => "nombre"],
		];

		for ($i = 1; $i <= 31; $i++) {
			$encabezadoV2[] = ["id" => "dia" . $i, "cod_campo" => "semaforo_calidad", "label" => $i];
		}
		array_push(
			$encabezadoV2,
			["id" => "sinAlerta", "cod_campo" => "sinAlerta", "label" => "Sin alerta"],
			["id" => "alerta", "cod_campo" => "alerta", "label" => "Alerta"],
			["id" => "alarma", "cod_campo" => "alarma", "label" => "Alarma"],
			["id" => "otros", "cod_campo" => "otros", "label" => "Otros"]
		);
		return $encabezadoV2;
	}

	static function getStationInfo($type)
	{
		$queryEstaciones = "select cod_estacion, nombre from tbl_estacion where tipo like '" . $type . "' and disponible order by orden";
		$estaciones = ConexionBD::EjecutarConsulta($queryEstaciones);
		$resultado = [];
		foreach ($estaciones as $estacion) {
			$resultado[$estacion["cod_estacion"]] = [
				"cod_estacion" => $estacion["cod_estacion"],
				"nombre" => $estacion["nombre"]
			];
		}
		return $resultado;
	}

	static function getInfomeS($fecha) {
		// TODO
	}

	static function getInfomeEXC($fecha) {
		$type = "EXC";
		$query = "select
					cod_estacion,
					EXTRACT(day FROM date_trunc('day', timestamp)) as dia,
					max(alarma) as valor
				from (
					select
						timestamp,
						split_part(d.cod_variable,'/', 1) as cod_estacion,
						valor,
						umbral_min_alerta,
						case 
							when d.valor < umbral_min_alerta then 1
							when d.valor between umbral_min_alerta and umbral_max_alerta then 2
							when d.valor > umbral_min_alarma then 3
						END as alarma
					from tbl_dato_reacar d
					LEFT JOIN tbl_campo_encabezado ce ON 
						(
							split_part(ce.cod_campo,'_', 2) = split_part(d.cod_variable,'/', 1) 
							AND split_part(ce.cod_campo,'_', 1) = split_part(d.cod_variable,'/', 2)
						) OR (
							ce.cod_campo = split_part(d.cod_variable,'/', 2)
						)	
					where timestamp between DATE_TRUNC('month', TO_TIMESTAMP('".$fecha."','YYYY-MM')) - '1 MONTH'::INTERVAL 
						and DATE_TRUNC('month', TO_TIMESTAMP('".$fecha."','YYYY-MM')) - '1 DAY'::INTERVAL 
					and cod_variable like '".$type."-%'
				) valores
				group by dia, cod_estacion";

		return InformeSemaforico::getInforme($fecha, $query, $type);
	}

	static function getInfomeECC($fecha) {
		$type = "ECC";
		$query = "select
					cod_estacion,
					EXTRACT(day FROM date_trunc('day', timestamp)) as dia,
					max(alarma) as valor
				from (
					select
						timestamp,
						split_part(d.cod_variable,'/', 1) as cod_estacion,
						valor,
						umbral_min_alerta,
						case 
							when d.valor < umbral_min_alerta then 1
							when d.valor between umbral_min_alerta and umbral_max_alerta then 2
							when d.valor > umbral_min_alarma then 3
						END as alarma
					from tbl_dato_reacar d
					LEFT JOIN tbl_campo_encabezado ce ON 
						(
							split_part(ce.cod_campo,'_', 2) = split_part(d.cod_variable,'/', 1) 
							AND split_part(ce.cod_campo,'_', 1) = split_part(d.cod_variable,'/', 2)
						) OR (
							ce.cod_campo = split_part(d.cod_variable,'/', 2)
						)	
					where timestamp between DATE_TRUNC('month', TO_TIMESTAMP('".$fecha."','YYYY-MM')) - '1 MONTH'::INTERVAL 
						and DATE_TRUNC('month', TO_TIMESTAMP('".$fecha."','YYYY-MM')) - '1 DAY'::INTERVAL 
					and cod_variable like '".$type."-%'
				) valores
				group by dia, cod_estacion";
		return InformeSemaforico::getInforme($fecha, $query, $type);
	}

	static function getInfomeEAA($fecha) {
		$type = "EAA";
		$query = "select
					cod_estacion,
					EXTRACT(day FROM date_trunc('day', timestamp)) as dia,
					max(alarma) as valor
				from (
					select
						timestamp,
						split_part(eaa.cod_variable,'/', 1) as cod_estacion,
						valor,
						umbral_min_alerta,
						case 
							when eaa.valor < umbral_min_alerta then 1
							when eaa.valor between umbral_min_alerta and umbral_max_alerta then 2
							when eaa.valor > umbral_min_alarma then 3
						END as alarma
					from tbl_dato_saica eaa
					LEFT JOIN tbl_campo_encabezado ce ON 
						(
							split_part(ce.cod_campo,'_', 2) = split_part(eaa.cod_variable,'/', 1) 
							AND split_part(ce.cod_campo,'_', 1) = split_part(eaa.cod_variable,'/', 2)
						) OR (
							ce.cod_campo = split_part(eaa.cod_variable,'/', 2)
						)	
					where timestamp between DATE_TRUNC('month', TO_TIMESTAMP('".$fecha."','YYYY-MM')) - '1 MONTH'::INTERVAL 
						and DATE_TRUNC('month', TO_TIMESTAMP('".$fecha."','YYYY-MM')) - '1 DAY'::INTERVAL 
					and cod_variable like '".$type."-%'
				) valores
				group by dia, cod_estacion";
		return InformeSemaforico::getInforme($fecha, $query, $type);
	}

	static function getInforme($fecha, $query, $type)
	{
		$datos = ConexionBD::EjecutarConsulta($query);
		$resultado = InformeSemaforico::getStationInfo($type);
		for ($i = 1; $i <= 31; $i++) {
			foreach ($resultado as &$res) {
				$res["dia" . $i] = null;
				foreach ($datos as $dato) {
					$dataStation = $dato['cod_estacion'];
					if (isset($resultado[$dataStation]) && $dataStation === $res['cod_estacion'] && $i == $dato["dia"]) {
						$res["dia" . $i] = $dato["valor"];
						break;
					}
				}
			}
		}

		$tokens = explode('-', $fecha);
		$year = intval($tokens[0]);
		$month = intval($tokens[1]);

		InformeSemaforico::fillTotals($resultado, $year, $month);

		$headers =  InformeSemaforico::getEncabezado();
		return [
			"encabezado" => $headers,
			"valores" => array_values($resultado),
			"footer" => InformeSemaforico::getFooter($resultado, $headers, $year, $month),
		];
	}

	static function getFooter($data, $headers, $year, $month)
	{
		$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$total = $daysInMonth * count($data);
		$totalCount = [];
		foreach ($data as &$res) {
			$count = InformeSemaforico::countAlarms($res);
			foreach ($count as $alarmValue => $value) {
				$previous = $totalCount[$alarmValue] ?? 0;
				$totalCount[$alarmValue] = $previous + $value;
			}
		}

		$footer = [];
		foreach ($headers as $header) {
			$codCampo = $header['cod_campo'];
			if (in_array($codCampo, InformeSemaforico::$TOTAL_COLUMNS)) {
				$alarmValue = InformeSemaforico::mapCodcampoToValue($codCampo);
				$value = $alarmValue === null ? null : InformeSemaforico::getTotalString($totalCount[$alarmValue], $total);
			} else {
				$value = null;
			}
			$footer[] = $value;
		}
		return $footer;
	}

	static function mapCodcampoToValue($codCampo)
	{
		$index = array_search($codCampo, InformeSemaforico::$TOTAL_COLUMNS);
		return $index === false ? null : $index;
	}

	static function getTotalString($count, $total)
	{
		$percentage = round($count / $total * 100, 1);
		return "$count $percentage%";
	}

	static function countAlarms($stationInfo)
	{
		$count = [0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0];
		for ($i = 1; $i <= 31; $i++) {
			$header = "dia" . $i;
			$value = $stationInfo[$header] ?? 4;
			$count[$value] += 1;
		}
		return $count;
	}

	static function fillTotals(&$data, $year, $month)
	{
		$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$total = $daysInMonth;
		foreach ($data as &$res) {
			$count = InformeSemaforico::countAlarms($res);
			$res['sinAlerta'] = InformeSemaforico::getTotalString($count[0], $total);
			$res['alerta'] = InformeSemaforico::getTotalString($count[1], $total);
			$res['alarma'] = InformeSemaforico::getTotalString($count[2], $total);
			$res['otros'] = InformeSemaforico::getTotalString($count[3], $total);
		}
	}
}
