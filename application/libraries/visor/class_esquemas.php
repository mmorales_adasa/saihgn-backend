<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Esquemas
{
	static function getEsquemaFecha($DB, $id, $fecha, $cabeceras) {
		$currentDate = date_create($fecha);
		$previousDate = clone $currentDate;
		date_sub($previousDate, date_interval_create_from_date_string("10 minutes"));

		$currentData = Esquemas::getEsquemaData($DB, $currentDate);
		$previousData = Esquemas::getEsquemaData($DB, $previousDate);
		return [
			"esquema" => json_decode(Esquemas::generateEsquema($id, $currentData, $previousData, $cabeceras["cabeceras"])),
			"leyenda" => Esquemas::getEsquemaLegend($DB),
		];
	}

	static function getEsquemaV2($DB, $id){
		$query = "select esquema_procesado from tbl_esquemas where id_esquema = '".$id."' and activo is true";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);

		if(count($data)>0){
			$esquema = json_decode($data[0]["esquema_procesado"]);
			return [
				"esquema" => $esquema,
				"leyenda" => Esquemas::getEsquemaLegend($DB),
			];
		} else {
			return null;
		}
	}

	static function getEsquema($DB, $id){
		$query = "select esquema_procesado from tbl_esquemas where id_esquema = '".$id."' and activo is true";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);

		if(count($data)>0){
			return json_decode($data[0]["esquema_procesado"]);
		} else {
			return null;
		}
	}

	static function getEsquemaData($DB, $date) {
		$dateString = date_format($date, "d/m/Y H:i:s");
		$query = "SELECT cod_variable, valor, timestamp_medicion as timestamp from tbl_hist_medidas where timestamp_medicion = '".$dateString."' and ind_validacion = 'S'";
        $variables = ConexionBD::EjecutarConsultaConDB($DB, $query);
		return Esquemas::getDatosExplotacion($DB, $variables);
	}

	static function getEsquemaLegend($DB) {
		$legendQuery = "select esquema_procesado from tbl_esquemas where id_esquema = 'leyenda'";
		$legend = ConexionBD::EjecutarConsultaConDB($DB, $legendQuery);
		return json_decode($legend[0]["esquema_procesado"]);
	}

	static function generateEsquema($idEsquema, $currentData, $previousData, $cabeceras) {
		$url = APPPATH . "/esquemas/";
		$esquemaHTML = @file_get_contents($url . $idEsquema .'.htm');
		$esquemaHTML = iconv(mb_detect_encoding($esquemaHTML, mb_detect_order(), true), "UTF-8//IGNORE", $esquemaHTML);
		if ($esquemaHTML == false) {
			return null;
		}
		$esquemaHTMLProcesado = Esquemas::procesarEsquemaHTML($esquemaHTML, $currentData, $previousData, $cabeceras);
		$jsonEsquema = json_encode($esquemaHTMLProcesado, JSON_HEX_APOS);
		return $jsonEsquema;
	}

    static function getDatosExplotacion($DB, $ultimosDatos)
    {        
        $ultimosDatosVar = [];
        // crear array asociativo de últimos datos usando de indice la variable y de valor el valor actual
        foreach($ultimosDatos as $ultimoDato){
            if(isset($ultimoDato["cod_variable"]) && isset($ultimoDato["valor"])){
                $ultimosDatosVar[$ultimoDato["cod_variable"]] = $ultimoDato["valor"];
            }
        }

        // obtención de todas las variables y datos de estación
        $query = "select cod_variable, split_part(cod_variable ,'/', 2) as nombre_variable, trim(cod_estacion) as cod_estacion, trim(zona) as zona, e.disponible as estacion_disponible, v.disponible as variable_disponible, TRIM(e.tipo) as estacion_tipo 
            from tbl_variable v 
                left join tbl_estacion e on split_part(cod_variable ,'/', 1) = cod_estacion
            where cod_estacion is not null
            order by length(cod_variable) desc";
        $variables = ConexionBD::EjecutarConsultaConDB($DB, $query);

        $query = "select TRIM(cod_estacion) cod_estacion, nombre, TRIM(tipo) tipo, TRIM(zona) zona, disponible from tbl_estacion order by length(cod_estacion) desc";
        $estaciones = ConexionBD::EjecutarConsultaConDB($DB, $query);

        // unir ultimos datos con el listado de variables
        $variablesVar = [];
        foreach($variables as &$variable){

            if(isset($ultimosDatosVar[$variable["cod_variable"]])){
                $variable["valor"] = $ultimosDatosVar[$variable["cod_variable"]];
            } else {
                $variable["valor"] = null;
            }

            $variablesVar[$variable["cod_variable"]] = $variable;


        }

        // tener solo claves de array de variables
        //array_keys(array $array): array


        foreach($estaciones as &$estacion){

            // crear variables NO1 (nombres de estación)
            $variablesVar[$estacion["cod_estacion"]."/NO1"] = [
                "cod_variable" => $estacion["cod_estacion"]."/NO1", 
                "nombre_variable" => "NO1", 
                "cod_estacion" => $estacion["cod_estacion"], 
                "zona" => $estacion["zona"], 
                "estacion_disponible" => $estacion["disponible"], 
                "variable_disponible" => true,
                "estacion_tipo" => $estacion["tipo"],
                "valor"=> $estacion["nombre"] ];

            // crear variables QS (caudales de salida, solo para embalses)
            if($estacion["tipo"]=="E"){

                 // Cálculo del caudal de salida para los embalses
                //$QT1 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT1', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT1']['valor'] : 0;
                //$QT2 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT2', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT2']['valor'] : 0;
                //$QT3 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT3', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT3']['valor'] : 0;
                //$QT4 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT4', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT4']['valor'] : 0;
                $QT1 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT1', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT1']['valor'] : 0;
                $QT2 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT2', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT2']['valor'] : 0;
                $QT3 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT3', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT3']['valor'] : 0;
                $QT4 = array_key_exists(trim($estacion["cod_estacion"]) . '/QT4', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QT4']['valor'] : 0;
                $QAT = array_key_exists(trim($estacion["cod_estacion"]) . '/QAT', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QAT']['valor'] : 0;
                $QFT = array_key_exists(trim($estacion["cod_estacion"]) . '/QFT', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QFT']['valor'] : 0;
                $QWT = array_key_exists(trim($estacion["cod_estacion"]) . '/QWT', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QWT']['valor'] : 0;
                $QTR = array_key_exists(trim($estacion["cod_estacion"]) . '/QTR', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QTR']['valor'] : 0;
                $CA1 = array_key_exists(trim($estacion["cod_estacion"]) . '/CA1', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/CA1']['valor'] : 0;
                $QC1 = array_key_exists(trim($estacion["cod_estacion"]) . '/QC1', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QC1']['valor'] : 0;
                $QC2 = array_key_exists(trim($estacion["cod_estacion"]) . '/QC2', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QC2']['valor'] : 0;
                $QWC = array_key_exists(trim($estacion["cod_estacion"]) . '/QWC', $variablesVar) ? (float)$variablesVar[$estacion["cod_estacion"] . '/QWC']['valor'] : 0;

                $tuberia = $QT1 + $QT2 + $QT3 + $QT4;

                switch ($estacion["cod_estacion"]) {
                    case 'E2-04': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                        
                        $qCanal = $CA1 + $QC1 + $QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);

                        break;
                    case 'E2-11':    //Caso de Sierra Brava CA1 es de entrada
                        
                        $qCanal = $CA1 + $QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);

                        break;
                    case 'E2-12':    //Presa de Garg�ligas y Cubilar
                        
                        $qCanal = $CA1 + $QC1 + $QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);

                        break;
                    case 'E2-13':    //Presa de Garg�ligas y Cubilar
                        
                        $qCanal = $CA1 + $QC1 + $QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);

                        break;
                    case 'E2-24': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                       
                        $qCanal = $CA1 + $QC1 + $QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);

                        break;
                    case 'E1-10':
                    
                        $Qs = $QAT + $QFT + ($QT1 / 1000);

                        break;
                    default:
                        
                        $qCanal = $CA1 + $QWC + $QC1 + $QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);

                }


                //$esquemaHTML = str_replace(trim($valor["cod_estacion"]) . "/QS", "<div data-link='" . trim($valor["zona"]) . '/' . trim($valor["cod_estacion"]) . "'>" . number_format($Qs, 2, ',', '.') . " m3/s" . "</div>", $esquemaHTML);

                $variablesVar[$estacion["cod_estacion"]."/QS"] = [
                    "cod_variable" => $estacion["cod_estacion"]."/QS", 
                    "nombre_variable" => "QS", 
                    "cod_estacion" => $estacion["cod_estacion"], 
                    "zona" => $estacion["zona"], 
                    "estacion_disponible" => $estacion["disponible"], 
                    "variable_disponible" => true,
                    "estacion_tipo" => $estacion["tipo"],
                    "valor"=> $Qs ];

            }

        }

        // crear variable TE1 (tendencia)
        /*if ($cod_estacion_var["1"] == "NR1" && is_numeric($valorCR['valor']) && is_numeric($datos_anteriores['CR'][$index]['valor'])) {
    
            if($estacion['disponible'] == true){

                $deltaH = floatval($valorCR['valor']) - floatval($datos_anteriores['CR'][$index]['valor']);
                if ($deltaH > 0) {
                    $tendencia = "<div class='bg-red' data-link='CR/" . trim($cod_estacion_var[0]) . "'>sube</div>";
                } else {
                    if ($deltaH < 0) {
                        $tendencia = "<div class='bg-green' data-link='CR/" . trim($cod_estacion_var[0]) . "'>baja</div>";
                    } else {
                        $tendencia = "<div data-link='CR/" . trim($cod_estacion_var[0]) . "'>igual</div>";
                    }
                }
                $esquemaHTML = str_replace(trim($cod_estacion_var[0]) . "/TE1", "<div data-link='CR/" . trim($cod_estacion_var[0]) . "'>" . $tendencia . "</div>", $esquemaHTML);
            } else {
                $esquemaHTML = str_replace(trim($index), "<div ></div>", $esquemaHTML);
            }
        
        }*/

        return ["variables"=>$variablesVar,"estaciones"=>$estaciones];
    }

    static function procesarEsquemaHTML($esquemaHTML, $datosActuales, $datosAnteriores, $cabeceras){

        // preparar array asociativo de cabeceras
        $cabecerasAssoc = [];
        foreach($cabeceras as $cabecera){
            $cabecerasAssoc[$cabecera["cod_campo"]] = $cabecera;
        }

        $variables = $datosActuales["variables"];
        $estaciones = $datosActuales["estaciones"];
        $variablesAnteriores = $datosAnteriores["variables"];

        //var_dump($datosActuales);

        //return;

        foreach($variables as $codVar => $variable){

            $formato = Esquemas::getFormato(trim($variable["cod_variable"]), $cabecerasAssoc);
            //$formato = isset($cabecerasAssoc[$cod_campo]) ? $cabecerasAssoc[$cod_campo] : ["decimales"=>2,"unidad"=>""];

            if($variable["nombre_variable"] == "NO1"){ // sustitucion de nombres de estacion

                if ($variable['estacion_tipo'] == 'E') {
                    $esquemaHTML = str_replace(">".trim($variable["cod_variable"])."<", "><div data-link='" . trim($variable["zona"]) . '/' . trim($variable["cod_estacion"]) . "'>" . $variable["valor"] . "</div><", $esquemaHTML);
                } else {
                    $esquemaHTML = str_replace(">".trim($variable["cod_variable"])."<", "><div data-link='".$variable['estacion_tipo']. "/" . trim($variable["cod_estacion"]) . "'>" . $variable["valor"] . "</div><", $esquemaHTML);
                }


            } else {

                if($variable['estacion_disponible'] == true){
                    
                    if($variable["nombre_variable"] == "QS") {
                        $esquemaHTML = str_replace(">".$variable["cod_estacion"] . "/QS"."<", "><div data-link='" . trim($variable["zona"]) . '/' . trim($variable["cod_estacion"]) . "'>" . number_format($variable["valor"], 2, ',', '.') . " m3/s" . "</div><", $esquemaHTML);
                    } else if ($variable['estacion_tipo'] == 'E') {
                        $esquemaHTML = str_replace(">".$variable["cod_variable"]."<", "><div data-link='" . $variable['zona'] . "/" . trim($variable["cod_estacion"]) . "'>" . number_format($variable["valor"], $formato["decimales"], ',', '.') . " " . $formato["unidad"] . "</div><", $esquemaHTML);
                    } else {
                        $esquemaHTML = str_replace(">".$variable["cod_variable"]."<", "><div data-link='" . $variable['estacion_tipo'] . "/" . trim($variable["cod_estacion"]) . "'>" . number_format($variable["valor"], $formato["decimales"], ',', '.') . " " . $formato["unidad"] . "</div><", $esquemaHTML);
                    }

                } else {

                    $esquemaHTML = str_replace(">".$variable["cod_variable"]."<", ">"."<div></div>"."<", $esquemaHTML);

                }

            }

            // generar tendencias (TE1) a partir de NR1, NE1
            if($variable["nombre_variable"] == "NR1" || $variable["nombre_variable"] == "NE1") {           
            

                if($variable['estacion_disponible'] == true && $variablesAnteriores && isset($variablesAnteriores[$variable["cod_variable"]])){

                    $deltaH = $variable["valor"] - $variablesAnteriores[$variable["cod_variable"]]["valor"];

                    if ($deltaH > 0) {
                        $tendencia = "<div class='bg-red' data-link='" .$variable['zona'] . "/" . trim($variable["cod_estacion"]) . "'>sube</div>";
                    } else {
                        if ($deltaH < 0) {
                            $tendencia = "<div class='bg-green' data-link='" . $variable['zona'] . "/" . trim($variable["cod_estacion"]) . "'>baja</div>";
                        } else {
                            $tendencia = "<div data-link='" . $variable['zona'] . "/" . trim($variable["cod_estacion"]) . "'>igual</div>";
                        }
                    }
                    
                    $esquemaHTML = str_replace($variable["cod_estacion"] . "/TE1", "<div data-link='" . $variable['zona'] . "/" . trim($variable["cod_estacion"]) . "'>" . $tendencia . "</div>", $esquemaHTML);

                } else {

                    $esquemaHTML = str_replace($variable["cod_estacion"] . "/TE1", "<div></div>", $esquemaHTML);

                }
            
            }
        }

        // limpieza de variables restantes
        foreach($estaciones as $estacion){
            $patron = '~[>]' . trim($estacion['cod_estacion']) . '[/][0-z0-Z]{1,4}[<]~';
            $esquemaHTML = preg_replace($patron, "><", $esquemaHTML);
        }
    
        // Reemplazamos elementos para que no de problemas el css
        $esquemaHTML = str_replace('<body link=blue vlink=purple class=xl130>', '<body class="esquema" link=blue vlink=purple class=xl130>', $esquemaHTML);
        $esquemaHTML = str_replace('a:link {', '.esquema a:link {', $esquemaHTML);
        $esquemaHTML = str_replace('a:visited {', '.esquema a:visited {', $esquemaHTML);
        $esquemaHTML = str_replace('tr { {', '.esquema tr { {', $esquemaHTML);
        $esquemaHTML = str_replace('col { {', '.esquema col { {', $esquemaHTML);
        $esquemaHTML = str_replace('br { {', '.esquema br { {', $esquemaHTML);

        return $esquemaHTML;
    }

    static function getFormato($var, $cabeceras){

        $variableArray = explode("/", $var);
        $denominacion = (isset($variableArray[1])) ? $variableArray[1] : null;

        if ($denominacion && !isset($cabeceras[$denominacion])) {
            $denominacion = preg_replace('/[0-9]/', '', $denominacion);
        }

        $formato = isset($cabeceras[$denominacion]) ? $cabeceras[$denominacion] : ["decimales"=>2,"unidad"=>""];

        return $formato;
    }

}