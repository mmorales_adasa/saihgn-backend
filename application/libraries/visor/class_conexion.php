<?php
class ConexionBD
{

    static function Conexion($nombreDB='prod'){
        $CI =& get_instance();
        $db = $CI->load->database($nombreDB, TRUE);
        return $db;
    }

    static function CerrarConexion($db){
        $db->close();
    }

    static function EjecutarConsulta($query, $resultAsArray = true){
        $CI = &get_instance();
        $db = $CI->load->database('prod', TRUE);

        $consulta = $db->query($query);
        $db->close();

        if (!$consulta) {
            $error = $db->error();
            throw new Exception($error);
        }

        $resultado = array();
        if ($resultAsArray) {
            foreach ($consulta->result_array() as $row) {
                array_push($resultado, $row);
            }
            return $resultado;
        } else {
            return $resultado;
        }
    }

    static function EjecutarConsultaConDB($conexion, $query, $resultAsArray=true) {
        $consulta = $conexion->query($query);

        if(!$consulta){
            $error = $conexion->error();
            throw new Exception($error);
        }

        $resultado = array();
        if($resultAsArray){
            foreach ($consulta->result_array() as $row){
                array_push($resultado, $row);
            }
            return $resultado;
        } else {
            return $resultado;
        }
    }

    static function EjecutarConsulta2($query){
        $CI = &get_instance();
        $db = $CI->load->database('visor2', TRUE);
        $consulta = $db->query($query);
        $resultado = array();
        foreach ($consulta->result_array() as $row) {
            array_push($resultado, $row);
        }
        $db->close();
        return $resultado;
    }

    static function Insert($table, $data){
        $CI = &get_instance();
        $db = $CI->load->database('prod', TRUE);
        $consulta = $db->insert_batch($table, $data);
        $db->close();

        if (!$consulta) {
            $error = $db->error();
            throw new Exception($error);
        }

        $resultado = array();
        return $resultado;
    }

    static function EjecutarConsultaDev($query, $resultAsArray = true){
        $CI = &get_instance();
        //$db = $CI->load->database('localdev', TRUE);
        $db = $CI->load->database('prod', TRUE);


        $consulta = $db->query($query);
        $db->close();

        if (!$consulta) {
            $error = $db->error();
            throw new Exception($error);
        }

        $resultado = array();
        if ($resultAsArray) {
            foreach ($consulta->result_array() as $row) {
                array_push($resultado, $row);
            }
            return $resultado;
        } else {
            return $consulta;
        }
    }

    static function EjecutarConsultaConDBDev($conexion, $query, $resultAsArray=true) {
        $consulta = $conexion->query($query);

        if(!$consulta){
            $error = $conexion->error();
            throw new Exception($error);
        }

        $resultado = array();
        if($resultAsArray){
            foreach ($consulta->result_array() as $row){
                array_push($resultado, $row);
            }
            return $resultado;
        } else {
            return $resultado;
        }
    }

    static function InsertDev($table, $data){
        $CI = &get_instance();
        $db = $CI->load->database('localdev', TRUE);
        $consulta = $db->insert_batch($table, $data);
        $db->close();

        if (!$consulta) {
            $error = $db->error();
            throw new Exception($error);
        }

        $resultado = array();
        return $resultado;
    }

    static function PrepararConsultaDev($query){
        $CI = &get_instance();
        $db = $CI->load->database('localdev', TRUE);
        $pQuery = $db->prepare(static function ($db) {
            return (new Query($db))->setQuery($query);
        });
    }

    static function PrepararConsultaConDBDev($conexion, $query){
        $pQuery = $conexion->prepare(static function ($db) {
            return (new Query($db))->setQuery($query);
        });
    }
}
