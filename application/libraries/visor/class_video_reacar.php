<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';

class VideoReacar
{

    static function getTimelapse($estacion)
    {
        $url = APPPATH . "/libraries/visor/videoReacar_cache/";
        $estacion = VideoReacar::checkReacarName($estacion);

        if (file_exists($url . $estacion)) {
            $ficheros = scandir($url . $estacion);

            $datos = array();

            for ($i = count($ficheros) - 1; $i > 1; $i--) {
                $fecha = strtok($ficheros[$i], ".");
                $fecha = substr($fecha, 0, -6) . "-" . substr($fecha, 4, -4) . "-" . substr($fecha, 6, -2) . " " . substr($fecha, 8) . ":00:00";
                array_push($datos, array("fecha" => $fecha, "fichero" => $ficheros[$i]));
            }

            return $datos;
        } else {
            return null;
        }
    }

    static function getTimelapsePhotos($estacion)
    {
        $url = APPPATH . "/libraries/visor/videoReacar_cache/";
        $images = [];
        $estacion = VideoReacar::checkReacarName($estacion);

        if (file_exists($url . $estacion)) {
            $ficheros = scandir($url . $estacion);
            $files = array();
            for ($i = count($ficheros) - 1; $i > 2; $i--) {
                $fecha = strtok($ficheros[$i], ".");
                $fecha = substr($fecha, 0, -6) . "-" . substr($fecha, 4, -4) . "-" . substr($fecha, 6, -2) . " " . substr($fecha, 8) . ":00:00";
                array_push($files, array("fecha" => $fecha, "fichero" => $ficheros[$i]));
            }
            // cortar el array a 30 imágenes máximo
            $files = array_slice($files, 0, 30);
            foreach ($files as $file) {
                $image = file_get_contents($url . $estacion . "/" . $file['fichero']);
                $b64image = base64_encode($image);
                array_push($images, array("fecha" => $file['fecha'], "fichero" => $file['fichero'], "src" => $b64image));
            }
        }
        return $images;
    }

    static function getPhoto($estacion, $fichero)
    {
        $url = APPPATH . "/libraries/visor/videoReacar_cache/";
        $estacion = VideoReacar::checkReacarName($estacion);
        $image = file_get_contents($url . $estacion . "/" . $fichero);
        $b64image = base64_encode($image);
        $fecha = strtok($fichero, ".");
        $fecha = substr($fecha, 0, -6) . "-" . substr($fecha, 4, -4) . "-" . substr($fecha, 6, -2) . " " . substr($fecha, 8) . ":00:00";
        $file["fecha"] = $fecha;
        $file["src"] = $b64image;
        return $file;
    }

    static function checkReacarName($estacion)
    {
        if ($estacion == "Alcázar") {
            $estacion = "Alcazar";
        }

        if ($estacion == "Valdepeñas") {
            $estacion = "Valdepena";
        }

        if ($estacion == "Don") {
            $estacion = "DonBenito";
        }

        if ($estacion == "Jerez") {
            $estacion = "JerezDeLosCaballeros";
        }

        return $estacion;
    }
}
