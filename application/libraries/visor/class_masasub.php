<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class MasasSubterraneas
{

    static function getEvolucion($id_PZ, $id_CR, $id_EM, $periodo, $fechaIni, $fechaFin)
    {

        $ids = [];
        if(isset($id_PZ) && $id_PZ != "") $ids[] = $id_PZ; // CP
        if(isset($id_CR) && $id_CR != "") $ids[] = $id_CR; // QR1
        if(isset($id_EM) && $id_EM != "") $ids[] = $id_EM; // PRA

        $idsDiezminutal = [];

        $idsDBPZ = ConexionBD::EjecutarConsulta("select cod_estacion from tbl_estacion where tipo like 'PZ' and disponible = true AND cod_estacion like '04.%'");
        foreach($idsDBPZ as $id){
            $idsPZAuto[] = trim($id["cod_estacion"]);
        }

        $idsStringDatosPiezo = "";
        $idsString = "";
        $idsStringTr = "";

        $useTblDateMedidas = false;
        $useTblDatePiezo = false;

        if (isset($id_PZ) && in_array($id_PZ, $idsPZAuto)) {
            $useTblDatePiezo = true;
            if ($idsStringDatosPiezo != "") $idsStringDatosPiezo .= " OR ";
            $idsStringDatosPiezo .= "cod_variable like '" . $id_PZ . "/CP'";
        }

        foreach ($ids as $id) {
            if ($idsString != "") $idsString .= " OR ";
            if($id == $id_CR) $idsString .= "cod_variable like '" . $id . "/QR1'";
            if($id == $id_EM) $idsString .= "cod_variable like '" . $id . "/PRA'";
            if (!in_array($id, $idsPZAuto)) if($id == $id_PZ) $idsString .= "cod_variable like '" . $id . "/CP'";
            
            if ($idsStringTr != "") $idsStringTr .= " OR ";
            if($id == $id_CR) $idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/QR1'";
            if($id == $id_EM) $idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/PRA'";
            if($id == $id_PZ) $idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/CP'";
        }

        if($id_CR || $id_EM || $id_PZ) $useTblDateMedidas = true;

        $query = "
            with ";
            
        if($useTblDateMedidas){

            $query .= "medidas AS (
                select 
                data_medicion as timestamp,
                cod_variable, 

                CASE
                    WHEN cod_variable LIKE '%/PRA' THEN valor_acum
                    WHEN cod_variable NOT LIKE '%/PRA' THEN valor_media
                END valor

                from tbl_olap_medidas_dias
                where data_medicion >= '" . $fechaIni . "' 
                and data_medicion <= '" . $fechaFin . "' 
                and (" . $idsString . ")
            ),
            max_medidas AS (
                select cod_variable, max(timestamp) from medidas group by cod_variable
            ),";

        }
            
        $query .= "datostrymedidas AS (";

            if ($useTblDatePiezo) {

                $query .= "(select
                    DATE_TRUNC('minute',timestamp) as timestamp,
                    cod_variable,
                    valor
                    from tbl_dato_piezo
                    where timestamp >= '" . $fechaIni . "' and timestamp <= '" . $fechaFin . "'
                    AND cod_variable like '%CP'
                    AND (" . $idsStringDatosPiezo . ")
                    AND validez = 1
                    AND (extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . " - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . "))=0)";
            }

            if($useTblDatePiezo && $useTblDateMedidas){
                $query .= "UNION ";
            }

            if($useTblDateMedidas){

                $query .= "(select 
                    timestamp, 
                    tbl_dato_tr.cod_variable,
                    valor from tbl_dato_tr
                    left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                    where timestamp >= '" . $fechaIni . "' 
                    and timestamp <= '" . $fechaFin . "'
                    and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                    and (" . $idsStringTr . ")
                    and (extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . " - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . "))=0
                    union
                    select timestamp, cod_variable, valor from medidas)";
            }

        $query .= ")
    
        select
            timestamp,
            coalesce(SUM(QR1), null) AS \"QR\",
            coalesce(SUM(CP), null) AS \"CP\",
            coalesce(SUM(PRA), null) as \"PRA\"
            from(
                select
                timestamp,
                split_part(cod_variable, '/', 1) as estacion,
                case when cod_variable like '%/QR1' then valor end as QR1,
                case when cod_variable like '%/CP' then valor end as CP,
                case when cod_variable like '%/PRA' then valor end as PRA
                from datostrymedidas
            ) as valores
            group by timestamp
            order by timestamp asc";

        $data_masasub = ConexionBD::EjecutarConsulta($query);

        return $data_masasub;
    }

    static function InfoMASb($id, $DB){
        $res = ConexionBD::EjecutarConsultaConDB($DB,"select * from tbl_masasub where code = '".$id."'");
        return (isset($res) && isset($res[0])) ? $res[0]:null;
    }
}
