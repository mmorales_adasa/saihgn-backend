<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class FichaPiezometro
{

    public static function getFicha($pzId)
    {
        $filePath = FichaPiezometro::getFilePath($pzId);
        return file_get_contents($filePath);
    }

    public static function fichaExists($pzId) 
    {
        $filePath = FichaPiezometro::getFilePath($pzId);
        return file_exists($filePath);
    }

    public static function getFilePath($pzId) 
    {
        return APPPATH . '/esquemas_piezo/PDF\'s Guadiana/' . $pzId . '.pdf';
    }
}