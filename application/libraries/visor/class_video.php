<?php 
require_once APPPATH.'/libraries/visor/class_conexion.php';

class Video { //ARREGLAR: falta la rutina del timelapse

    //CASO DE VIDEOVIGILANCIA AEX/E
    static function CamarasAEX() {

        $query = "SELECT TRIM(e.zona) as zona, TRIM(e.cod_estacion) as cod_estacion, e.nombre AS nombre_estacion, e.tipo as tipo_estacion, p.acronimo AS acronimo_estacion, v.cod_video, v.tipo, v.descripcion, v.modelo, v.ip, v.mac, v.timelapse, v.destacado 
        FROM tbl_estacion e 
        JOIN tbl_presa p ON e.cod_estacion = p.cod_estacion AND e.disponible = true AND e.tipo = 'E'
        LEFT JOIN tbl_video v ON split_part(v.cod_video,'-',1) = p.acronimo AND v.cod_video like '%-cam01%'
        ORDER by e.zona asc, e.orden asc, e.cod_estacion asc";

        $camaras = ConexionBD::EjecutarConsulta($query);
        if (count($camaras)>0){
            $data = Array();
            foreach($camaras as $camara){
                if($camara['cod_video']){
                    $data[] = Video::Camara($camara, 'sistema');
                }
                else {
                    $data[] = Video::Camara($camara, 'no_disponible');
                }
            }
            return $data;
        }
        return null;
    }

    static function CamarasRedesGeneral($category) {
        $query = "SELECT e.tipo as tipo_estacion, TRIM(e.zona) as zona, TRIM(e.cod_estacion) as cod_estacion, e.nombre AS nombre_estacion, e.acronimo AS acronimo_estacion, v.cod_video, v.tipo, v.descripcion, v.modelo, v.ip, v.mac, v.timelapse, v.destacado 
                    FROM tbl_estacion e 
                    JOIN  tbl_video v on split_part(v.cod_video, '-', 1) = e.acronimo
                    WHERE v.cod_video like '%-cam01%' AND e.disponible AND e.tipo like TRIM('$category')
                    ORDER by e.zona asc, e.orden asc, e.cod_estacion asc, v.cod_video asc";

        $camaras = ConexionBD::EjecutarConsulta($query);
       if (count($camaras)>0){
            $data = Array();
            foreach($camaras as $camara){
                if($camara['cod_video']){
                    $data[] = Video::Camara($camara, 'presa');
                }
                else {
                    $data[] = Video::Camara($camara, 'no_disponible');
                }
            }
            return $data;
        }
        return null;
    }

    static function CamarasRedes($idEstacion) {
        $query = "SELECT e.tipo as tipo_estacion, TRIM(e.zona) as zona, TRIM(e.cod_estacion) as cod_estacion, e.nombre AS nombre_estacion, e.acronimo AS acronimo_estacion, v.cod_video, v.tipo, v.descripcion, v.modelo, v.ip, v.mac, v.timelapse, v.destacado 
                FROM tbl_estacion e 
                JOIN  tbl_video v on split_part(v.cod_video, '-', 1) = e.acronimo
                WHERE e.cod_estacion = TRIM('$idEstacion') AND e.disponible
                ORDER by e.zona asc, e.orden asc, e.cod_estacion asc, v.cod_video asc";

        $camaras = ConexionBD::EjecutarConsulta($query);
       if (count($camaras)>0){
            $data = Array();
            foreach($camaras as $camara){
                if($camara['cod_video']){
                    $data[] = Video::Camara($camara, 'presa');
                }
                else {
                    $data[] = Video::Camara($camara, 'no_disponible');
                }
            }
            return $data;
        }
        return null;
    }

    //CASO DE VIDEOVIGILANCIA DE UNA ZONA
    static function CamarasZona($idZona) {

        $query = "SELECT TRIM(e.zona) as zona, TRIM(e.cod_estacion) as cod_estacion, e.nombre AS nombre_estacion, e.tipo as tipo_estacion, p.acronimo AS acronimo_estacion, v.cod_video, v.tipo, v.descripcion, v.modelo, v.ip, v.mac, v.timelapse, v.destacado 
        FROM tbl_estacion e 
        JOIN tbl_presa p ON e.cod_estacion = p.cod_estacion AND e.disponible = true AND e.tipo = 'E' AND e.zona = '" . $idZona . "'
        LEFT JOIN tbl_video v ON split_part(v.cod_video,'-',1) = p.acronimo AND v.cod_video like '%-cam01%'
        ORDER by e.zona asc, e.orden asc, e.cod_estacion asc";

        $camaras = ConexionBD::EjecutarConsulta($query);
        if (count($camaras)>0){
            $data = Array();
            foreach($camaras as $camara){
                if($camara['cod_video']){
                    $data[] = Video::Camara($camara, 'sistema');
                }
                else {
                    $data[] = Video::Camara($camara, 'no_disponible');
                }
            }
            return $data;
        }
        return null;
    }

    //CASO DE VIDEOVIGILANCIA DE UN EMBALSE
    static function CamarasEmbalse($idEmbalse) {

        $query = "SELECT TRIM(e.zona) as zona, TRIM(e.cod_estacion) as cod_estacion, e.nombre AS nombre_estacion, e.tipo AS tipo_estacion, p.acronimo AS acronimo_estacion, v.cod_video, v.tipo, v.descripcion, v.modelo, v.ip, v.mac, v.timelapse, v.destacado 
        FROM tbl_estacion e
        JOIN tbl_presa p ON e.cod_estacion = p.cod_estacion AND p.cod_estacion = '" . $idEmbalse . "'
        JOIN tbl_video v ON split_part(v.cod_video,'-',1) = p.acronimo ORDER BY cod_video ASC";

        $camaras = ConexionBD::EjecutarConsulta($query);
        if (count($camaras)>0){
            $data = Array();
            foreach($camaras as $camara){
                $data[] = Video::Camara($camara, 'presa');
            }
            return $data;
        }
        return null;
    }
	
	
	static function Camara($camara, $tipo) {
		require APPPATH.'/libraries/visor/config.php';
        
        $array = Array();
        $array['nombre'] = null;
        $array['descripcion'] = null;
        $array['src'] = null;
        $array['url'] = null;
        $array['urlInterna'] = null;
        $array["timelapse"] = false;
        $array['timelapse_directory'] = false;
        $array['tipo'] = $tipo;
        $array['zona'] = $camara['zona'];
        $array['cod_estacion'] = $camara['cod_estacion'];
        $array['tipo_estacion'] = $camara['tipo_estacion'];

        // si no existe la camara en db no buscar el fichero
        if($tipo != "no_disponible") {
            $file = APPPATH  ."libraries/visor/video_cache/" . $camara['acronimo_estacion'] . "-video/" . $camara['cod_video'] . ".jpg";
            if (file_exists($file)){
                $array['src'] = $camara['acronimo_estacion'] . "-video/" . $camara['cod_video'] . ".jpg";
            }else{
                $array['src'] = "";
            }
        }

        if($camara['timelapse']){
            $dir_timelapse = APPPATH  ."libraries/visor/timelapse_cache/" . $camara['acronimo_estacion'] . "-video/" . $camara['cod_video'] . "_timelapse/";
            
            if(is_dir($dir_timelapse)){
                $array["timelapse"] = true;
                $array['timelapse_directory'] = $camara['acronimo_estacion'] . "-video/" . $camara['cod_video'] . "_timelapse";
            }
        }

        switch ($tipo){
            case 'sistema':
                $array['nombre'] = $camara['acronimo_estacion'] . '-video';
                $array['descripcion'] = 'Presa de ' . $camara['nombre_estacion'];
                
                $aux = explode('.',trim($camara['ip']));
                $puerto_externo = '5'. $aux[2].'0';
                $ip_interna = $aux[0] .'.'. $aux[1] .'.'. $aux[2] .'.200';
                $array["url"] = 'http://www.saihguadiana.com:' . $puerto_externo;
                $array["urlInterna"] = 'http://'.$ip_interna.':80';

                break;
            case 'presa':
                $array['nombre'] = $camara['cod_video'];
                $array['descripcion'] = $camara['descripcion'];
                $array["url"] = 'http://'.$camara['ip'];
                break;
            
            case 'saica':
                $array['nombre'] = $estacion['nombre'];
                $array['descripcion'] = $camara['cod_video'] . ': ' . $camara['descripcion'];
                $array["url"] = 'http://'.$camara['ip'];
                break;

            case 'no_disponible':
                $array['nombre'] = $camara['acronimo_estacion'] . '-video';
                $array['descripcion'] = 'Presa de ' . $camara['nombre_estacion'];
                $array['status'] = "servidor no disponible";
                break;
        }

        return $array;
    }
}
?>