<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
require_once APPPATH . '/libraries/visor/class_operaciones.php';

class GraficasReacar
{
    // datos estacion saica
    static function getValoresREACAR($codigo, $tiempo, $fInicial, $fFinal)
    {
        $tiempo = intval($tiempo);
        if ($tiempo < 0) {
            $filtro = " timestamp between '$fInicial' and '$fFinal' ";
        } else {
            $filtro = " timestamp > current_date-$tiempo ";
        }

        $array_datos = array();

        $DB = ConexionBD::Conexion();

        // conocer las variables disponibles para la estación saica, el apartado ya tiene una serie de variables a usar
        $variablesDisponibles = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/', 2) variable_disponible from tbl_variable where cod_variable like '".$codigo."%' and disponible = 1");
        $variablesApartado = ['TA','TB','PH','OX','CD','RD','SAC','QV'];
        $columnas = ['timestamp'];

        $variablesDisponiblesArray = [];

        if($variablesDisponibles){
            foreach($variablesDisponibles as $variableDisponible){
                $variablesDisponiblesArray[] = $variableDisponible['variable_disponible']; 
            }
            foreach($variablesApartado as $variableApartado){
                if(in_array($variableApartado, $variablesDisponiblesArray)) {
                    $columnas[] = $variableApartado;
                }
            }
        }

        if ($tiempo <= 30) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro  order by timestamp ");
        }
        if ($tiempo > 30 && $tiempo <= 60) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro and (to_char(timestamp, 'MI')='00') order by timestamp ");
        }
        if ($tiempo > 60 && $tiempo <= 120) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro  and (to_char(timestamp, 'hh:MI:SS')='00:00:00' OR to_char(timestamp, 'hh:MI:SS')='06:00:00' OR to_char(timestamp, 'hh:MI:SS')='12:00:00' OR to_char(timestamp, 'hh:MI:SS')='18:00:00') order by timestamp ");
        }
        if ($tiempo > 120) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro  and ( to_char(timestamp, 'hh:MI:SS')='12:00:00') order by timestamp ");
        }

        ConexionBD::CerrarConexion($DB);


        $filas = array_unique(Operaciones::sacarDatos($datos, 'timestamp'));
        $filas = Operaciones::arrayEstructura($filas);
        $registros = array();
        for ($i = 0; $i < count($filas); $i++) {
            $array_aux = array();
            for ($j = 0; $j < count($columnas); $j++) {
                $array_aux = $array_aux + array($columnas[$j] => "");
            }

            $array_aux['timestamp'] = $filas[$i];
            $array_aux['cod_estacion'] = $codigo;
            array_push($registros, $array_aux);
        }
        for ($i = 0; $i < count($datos); $i++) {
            $timestamp = $datos[$i]['timestamp'];
            $posicion = array_search($timestamp, array_column($registros, 'timestamp'));

            if (intval($datos[$i]['validez']) < 2) {
                $registros[$posicion][$datos[$i]['variable']] = $datos[$i]['valor'];
            } else {
                $registros[$posicion][$datos[$i]['variable']] = 'No válido';
            }

            $registros[$posicion]['timestamp'] = $datos[$i]['timestamp'];
        }

        $registro = $registros;

        $array_datos = $array_datos + array("datos" => $registro, "cabecera" => $columnas);

        return $array_datos;
    }
}
