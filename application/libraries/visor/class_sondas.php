<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Sondas
{

    static function getSondas($DB)
    {
        require_once APPPATH . '/libraries/visor/config.php';
        $query = "SELECT
                    cod_estacion, 
                    nombre, 
                    latitud,
                    longitud, 
                    tipo,
                    CASE 
                        WHEN estado_adquisicion = 2  THEN null
                        WHEN estado_adquisicion != 2  THEN usm.timestamp
                    END as timestamp,
                    disponible 
                FROM tbl_estacion e
                LEFT JOIN tbl_ultimo_sondeo_mult usm ON e.cod_estacion = usm.cod_sonda and usm.timestamp != '1970-01-01 00:00:00'
                where tipo = 'S' and disponible = true
                order by zona, orden, cod_estacion";

        $data = ConexionBD::EjecutarConsultaConDB($DB, $query);

		$datosRetorno = array("encabezado" => ["cod_estacion","nombre","timestamp"], "valores" => $data);

        return $datosRetorno;
    }

    static function getTimestampsSondas($id)
    {
        require_once APPPATH . '/libraries/visor/config.php';

        $query = "SELECT cod_sonda as cod_estacion,timestamp_sondeo FROM tbl_dato_sonda_mult where cod_sonda ='" . $id . "' and num_lectura = 1 and (val_clorofila1 = 1 or val_conductividad = 1 or val_oxigeno = 1 or val_ph = 1 or val_redox = 1 or val_secchi = 1 or val_temperatura = 1 or val_turbidez = 1) ORDER BY timestamp_sondeo desc";
        $data = ConexionBD::EjecutarConsulta($query);
        return $data;
    }

    static function getTimestampsSondasBOT($id, $antiguedad = 1)
    {
        require_once APPPATH . '/libraries/visor/config.php';

        $query = "select timestamp_sondeo from (
                    SELECT ROW_NUMBER() OVER (
                        ORDER BY timestamp_sondeo desc
                    ) row_num, cod_sonda, timestamp_sondeo FROM tbl_dato_sonda_mult where cod_sonda ='" . $id ."' and num_lectura = 1 and (val_clorofila1 = 1 or val_conductividad = 1 or val_oxigeno = 1 or val_ph = 1 or val_redox = 1 or val_secchi = 1 or val_temperatura = 1 or val_turbidez = 1)
                  ) as foo where row_num = " . $antiguedad;
        $data = ConexionBD::EjecutarConsulta($query);
        return $data;
    }

    static function getEvolucionSondas($id, $timestamp)
    {
        require_once APPPATH . '/libraries/visor/config.php';

        $cod_embalse = str_replace("S", "E", $id);

        $query_datos = "SELECT cod_sonda as estacion, timestamp_sondeo as timestamp, num_lectura, cota, profundidad, temperatura, ph, oxigeno, conductividad, redox, turbidez, secchi, clorofila1, ficocianina, val_temperatura, val_ph, val_oxigeno, val_conductividad, val_redox, val_turbidez, val_secchi, val_clorofila1, val_ficocianina "
            . "FROM tbl_dato_sonda_mult "
            . "WHERE cod_sonda= '" . $id . "' and timestamp_sondeo='" . $timestamp . "' "
            . "ORDER BY profundidad asc ";

        //old
        //$query_nivel = "SELECT volumen, valor AS nivel, Round(valor*100/volumen::numeric, 2) AS porcentaje FROM tbl_nivel, tbl_ultimo_dato_medido WHERE  cod_nivel = '" . $cod_embalse . "/NMN' and  cod_variable = '" . $cod_embalse . "/VE1'";
        //$query_nivel = "SELECT volumen, valor AS nivel, Round(valor*100/nivel::numeric, 2) AS porcentaje FROM tbl_nivel, tbl_ultimo_dato_medido WHERE  cod_nivel = '" . $cod_embalse . "/NMN' and  cod_variable = '" . $cod_embalse . "/NE1'";
        $query_porcentaje_volumen = "SELECT Round(valor*100/volumen::numeric, 2) AS porcentaje FROM tbl_nivel, tbl_ultimo_dato_medido WHERE  cod_nivel = '" . $cod_embalse . "/CAP' and  cod_variable = '" . $cod_embalse . "/VE1'";
        $query_nivel = "SELECT valor AS nivel FROM tbl_nivel, tbl_ultimo_dato_medido WHERE cod_variable = '" . $cod_embalse . "/NE1'";

        $query_cotas = "SELECT cod_nivel,nivel,descripcion "
            . "FROM tbl_nivel  "
            . "WHERE cod_nivel = '" . $cod_embalse . "/DF' or cod_nivel = '" . $cod_embalse . "/ABA1' or cod_nivel = '" . $cod_embalse . "/ABA2' or cod_nivel = '" . $cod_embalse . "/ABA3' or cod_nivel = '" . $cod_embalse . "/NMN'";

        $query_cabeceras = "SELECT secchi, clorofila1 as clorofila, ficocianina from  tbl_sonda_mult where cod_sonda = '$id'";

        $DB = ConexionBD::Conexion();
        $data = ConexionBD::EjecutarConsultaConDB($DB, $query_datos);
        $porcentaje_volumen = ConexionBD::EjecutarConsultaConDB($DB, $query_porcentaje_volumen);
        $nivel = ConexionBD::EjecutarConsultaConDB($DB, $query_nivel);
        $cotas = ConexionBD::EjecutarConsultaConDB($DB, $query_cotas);
        $cabeceras = ConexionBD::EjecutarConsultaConDB($DB, $query_cabeceras);
        ConexionBD::CerrarConexion($DB);

        $return = array();

        if ($data) {

            foreach ($data as $i => $row) {

                $row['val_temperatura'] = (isset($row['val_temperatura'])) ? (float) $row['val_temperatura'] : null;
                $row['val_ph'] = (isset($row['val_ph'])) ? (float) $row['val_ph'] : null;
                $row['val_oxigeno'] = (isset($row['val_oxigeno'])) ? (float) $row['val_oxigeno'] : null;
                $row['val_conductividad'] = (isset($row['val_conductividad'])) ? (float) $row['val_conductividad'] : null;
                $row['val_redox'] = (isset($row['val_redox'])) ? (float) $row['val_redox'] : null;
                $row['val_turbidez'] = (isset($row['val_turbidez'])) ? (float) $row['val_turbidez'] : null;
                $row['val_secchi'] = (isset($row['val_secchi'])) ? (float) $row['val_secchi'] : null;
                $row['val_clorofila1'] = (isset($row['val_clorofila1'])) ? (float) $row['val_clorofila1'] : null;
                $row['val_ficocianina'] = (isset($row['val_ficocianina'])) ? (float) $row['val_ficocianina'] : null;
                $row['cota'] = (isset($row['cota'])) ? (float) $row['cota'] : null;
                $row['profundidad'] = (isset($row['profundidad'])) ? (float) $row['profundidad'] : null;
                $row['temperatura'] = (isset($row['temperatura'])) ? (float) $row['temperatura'] : null;
                $row['ph'] = (isset($row['ph'])) ? (float) $row['ph'] : null;
                $row['oxigeno'] = (isset($row['oxigeno'])) ? (float) $row['oxigeno'] : null;
                $row['conductividad'] = (isset($row['conductividad'])) ? (float) $row['conductividad'] : null;
                $row['redox'] = (isset($row['redox'])) ? (float) $row['redox'] : null;
                $row['turbidez'] = (isset($row['turbidez'])) ? (float) $row['turbidez'] : null;
                $row['clorofila1'] = (isset($row['clorofila1'])) ? (float) $row['clorofila1'] : null;
                $row['secchi'] = (isset($row['secchi'])) ? (float) $row['secchi'] : null;
                $row['ficocianina'] = (isset($row['ficocianina'])) ? (float) $row['ficocianina'] : null;
                $row['num_lectura'] = (isset($row['num_lectura'])) ? (float) $row['num_lectura'] : null;

                // Retorno de timestamp con hora de España
                /*$dateTime = new DateTime($row['timestamp']);
                $dateTime->setTimezone(new DateTimeZone("Europe/Madrid"));
                $row['timestamp'] = $dateTime->format("Y/m/d H:i:s");*/

                $return[str_replace(" ", "", $row["estacion"])][] = array(
                    "timestamp" => $row["timestamp"],
                    "PH" => $row["ph"],
                    "OX" => $row["oxigeno"],
                    "PRF" => $row["profundidad"],
                    "CLF" => $row["clorofila1"],
                    "CAU" => $row["cota"],
                    "TA" => $row["temperatura"],
                    "CD" => $row["conductividad"],
                    "RD" => $row["redox"],
                    "TB" => $row["turbidez"],
                    "SCC" => $row["secchi"],
                    "FIC" => $row["ficocianina"],
                    "val_TA" => $row['val_temperatura'],
                    "val_PH" => $row['val_ph'],
                    "val_OX" => $row['val_oxigeno'],
                    "val_CD" => $row['val_conductividad'],
                    "val_RD" => $row['val_redox'],
                    "val_TB" => $row['val_turbidez'],
                    "val_SCC" => $row['val_secchi'],
                    "val_CLF" => $row['val_clorofila1'],
                    "val_FIC" => $row['val_ficocianina'],
                    "num_lectura" => $row["num_lectura"]
                );
            }
        }

        // no mandar cabecera de secchi fuera de las 12h
        if (date('H', strtotime($timestamp)) != '12'){
            $cabeceras[0]["secchi"] = false;
        }
        
        $return['nivel'] = ["nivel"=>$nivel[0]["nivel"], "porcentaje"=>$porcentaje_volumen[0]["porcentaje"], "volumen"=>null];
        $return['cotas'] = $cotas;
        $return['cabeceras'] = $cabeceras[0];

        return $return;
    }

    static function getProfundidadesSonda($id)
    {
        require_once APPPATH . '/libraries/visor/config.php';
        $query = "SELECT distinct tda.num_lectura FROM tbl_dato_sonda_mult tda WHERE  tda.cod_sonda =  '" . $id . "' order by num_lectura";
        $data = ConexionBD::EjecutarConsulta($query);
        return $data;
    }

    static function getInformeSonda($cod_estacion, $profundidad, $fecha_inicial, $fecha_final){ 
        require_once APPPATH . '/libraries/visor/config.php';
        
        $return = [];
        $query = "SELECT cod_sonda as cod_estacion, timestamp_sondeo, num_lectura, cota, profundidad, temperatura, ph, oxigeno, conductividad, redox, turbidez, secchi, clorofila1, ficocianina, val_temperatura, val_ph, val_oxigeno, val_conductividad, val_redox, val_turbidez, val_secchi, val_clorofila1, val_ficocianina"
            . " FROM tbl_dato_sonda_mult WHERE cod_sonda = '" . $cod_estacion . "' and timestamp_sondeo > '$fecha_inicial' "
            . "and  timestamp_sondeo < '$fecha_final' and num_lectura = $profundidad ORDER BY timestamp_sondeo";
        $data = ConexionBD::EjecutarConsulta($query);
        foreach ($data as $i => $row) {

            $row['val_temperatura'] = (isset($row['val_temperatura'])) ? (float) $row['val_temperatura'] : null;
            $row['val_ph'] = (isset($row['val_ph'])) ? (float) $row['val_ph'] : null;
            $row['val_oxigeno'] = (isset($row['val_oxigeno'])) ? (float) $row['val_oxigeno'] : null;
            $row['val_conductividad'] = (isset($row['val_conductividad'])) ? (float) $row['val_conductividad'] : null;
            $row['val_redox'] = (isset($row['val_redox'])) ? (float) $row['val_redox'] : null;
            $row['val_turbidez'] = (isset($row['val_turbidez'])) ? (float) $row['val_turbidez'] : null;
            $row['val_secchi'] = (isset($row['val_secchi'])) ? (float) $row['val_secchi'] : null;
            $row['val_clorofila1'] = (isset($row['val_clorofila1'])) ? (float) $row['val_clorofila1'] : null;
            $row['val_ficocianina'] = (isset($row['val_ficocianina'])) ? (float) $row['val_ficocianina'] : null;
            $row['cota'] = (isset($row['cota'])) ? (float) $row['cota'] : null;
            $row['profundidad'] = (isset($row['profundidad'])) ? (float) $row['profundidad'] : null;
            $row['temperatura'] = (isset($row['temperatura'])) ? (float) $row['temperatura'] : null;
            $row['ph'] = (isset($row['ph'])) ? (float) $row['ph'] : null;
            $row['oxigeno'] = (isset($row['oxigeno'])) ? (float) $row['oxigeno'] : null;
            $row['conductividad'] = (isset($row['conductividad'])) ? (float) $row['conductividad'] : null;
            $row['redox'] = (isset($row['redox'])) ? (float) $row['redox'] : null;
            $row['turbidez'] = (isset($row['turbidez'])) ? (float) $row['turbidez'] : null;
            $row['clorofila1'] = (isset($row['clorofila1'])) ? (float) $row['clorofila1'] : null;
            $row['secchi'] = (isset($row['secchi'])) ? (float) $row['secchi'] : null;
            $row['ficocianina'] = (isset($row['ficocianina'])) ? (float) $row['ficocianina'] : null;
            $row['num_lectura'] = (isset($row['num_lectura'])) ? (float) $row['num_lectura'] : null;

            // Retorno de timestamp con hora de España
            /*$dateTime = new DateTime($row['timestamp_sondeo']);
            $dateTime->setTimezone(new DateTimeZone("Europe/Madrid"));
            $row['timestamp_sondeo'] = $dateTime->format("Y/m/d H:i:s");*/

            $return[] = array(
                "timestamp_sondeo" => $row["timestamp_sondeo"],
                "PH" => $row["ph"],
                "OX" => $row["oxigeno"],
                "PRF" => $row["profundidad"],
                "CLF" => $row["clorofila1"],
                "CAU" => $row["cota"],
                "TA" => $row["temperatura"],
                "CD" => $row["conductividad"],
                "RD" => $row["redox"],
                "TB" => $row["turbidez"],
                "SCC" => (date('H', strtotime($row["timestamp_sondeo"]))=='12') ? $row["val_secchi"] : null,
                "FIC" => round($row["ficocianina"], 2),
                "val_TA" => $row['val_temperatura'],
                "val_PH" => $row['val_ph'],
                "val_OX" => $row['val_oxigeno'],
                "val_CD" => $row['val_conductividad'],
                "val_RD" => $row['val_redox'],
                "val_TB" => $row['val_turbidez'],
                "val_SCC" => (date('H', strtotime($row["timestamp_sondeo"]))=='12') ? $row["val_secchi"] : null,
                "val_CLF" => $row['val_clorofila1'],
                "val_FIC" => $row['val_ficocianina'],
                "num_lectura" => round($row["num_lectura"], 2),
            );
        }
        return $return;
    }
}
