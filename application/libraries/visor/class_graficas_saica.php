<?php 
require_once APPPATH.'/libraries/visor/class_conexion.php';
require_once APPPATH.'/libraries/visor/class_operaciones.php';

class GraficasSaica {
    
    // datos estacion saica
    static function getValoresSAICA($codigo, $tiempo, $fechaIni, $fechaFin){

        if($tiempo != -1){
            $tiempo = intval($tiempo);
            if (isset($tiempo)) {
                $fechaIni = date('Y-m-d H:i', strtotime("-".$tiempo." day"));
                $fechaFin = date('Y-m-d H:i');
            }
        }

        $array_datos = array();

        $DB = ConexionBD::Conexion();

        // conocer las variables disponibles para la estación saica, el apartado ya tiene una serie de variables a usar
        $variablesDisponibles = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/', 2) variable_disponible 
                                                                        from tbl_variable v
                                                                        join tbl_campo_encabezado ca ON CONCAT(split_part(cod_variable,'/', 2), '_', '".$codigo."') = ca.cod_campo
                                                                        where v.cod_variable like '".$codigo."%' 
                                                                        and split_part(cod_variable,'/', 2) IN('TA','TB','PH','OX','CD','NH','NO','PO','SAC')");

        $variablesApartado = ['TA','TB','PH','OX','CD','NH','NO','PO','SAC'];
        $columnas = ['timestamp'];

        $variablesDisponiblesArray = [];

        if($variablesDisponibles){
            foreach($variablesDisponibles as $variableDisponible){
                $variablesDisponiblesArray[] = $variableDisponible['variable_disponible']; 
            }
            foreach($variablesApartado as $variableApartado){
                if(in_array($variableApartado, $variablesDisponiblesArray)) {
                    $columnas[] = $variableApartado;
                }
            }
        }

        if ($tiempo <= 30 || $tiempo -1) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable, timestamp, valor, validez from tbl_dato_saica tdr  where split_part(cod_variable,'/',1)='$codigo' and timestamp between '$fechaIni' and '$fechaFin' order by timestamp ",true);
        }
        if ($tiempo > 30 && $tiempo <= 60) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable, timestamp, valor, validez from tbl_dato_saica tdr  where split_part(cod_variable,'/',1)='$codigo' and timestamp between '$fechaIni' and '$fechaFin' and (to_char(timestamp, 'MI')='00') order by timestamp ",true);
        }
        if ($tiempo > 60 && $tiempo <= 120) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable, timestamp, valor, validez from tbl_dato_saica tdr  where split_part(cod_variable,'/',1)='$codigo' and timestamp between '$fechaIni' and '$fechaFin' and (to_char(timestamp, 'hh:MI:SS')='00:00:00' OR to_char(timestamp, 'hh:MI:SS')='06:00:00' OR to_char(timestamp, 'hh:MI:SS')='12:00:00' OR to_char(timestamp, 'hh:MI:SS')='18:00:00')  order by timestamp ",true);
        }
        if ($tiempo > 120) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable, timestamp, valor, validez from tbl_dato_saica tdr  where split_part(cod_variable,'/',1)='$codigo' and timestamp between '$fechaIni' and '$fechaFin' and ( to_char(timestamp, 'hh')='12')  order by timestamp ",true);
        }

        $filas = array_unique(Operaciones::sacarDatos($datos, 'timestamp'));
        $filas = Operaciones::arrayEstructura($filas);
        $registros = array();
        for ($i = 0; $i < count($filas); $i++) {
            $array_aux = array();
            for ($j = 0; $j < count($columnas); $j++) {
                $array_aux = $array_aux + array($columnas[$j] => "");
            }

            $array_aux['timestamp'] = $filas[$i];
            $array_aux['cod_estacion'] = $codigo;
            array_push($registros, $array_aux);
        }
        for ($i = 0; $i < count($datos); $i++) {
            $timestamp = $datos[$i]['timestamp'];
            $posicion = array_search($timestamp, array_column($registros, 'timestamp'));

            if (intval($datos[$i]['validez']) < 2) {
                $registros[$posicion][$datos[$i]['variable']] = $datos[$i]['valor'];
            } else {
                $registros[$posicion][$datos[$i]['variable']] = 'No válido';
            }

            $registros[$posicion]['timestamp'] = $datos[$i]['timestamp'];
        }

        $registro = $registros;
        
        // aforo relacionado
        $queryAforoRelacionado = "SELECT cod_saih, tipo FROM tbl_saica_saih join tbl_estacion on cod_saih = cod_estacion WHERE tipo in ('CR','NR') AND disponible = true AND cod_saica LIKE '".$codigo."'";
        $aforoRelacionado = ConexionBD::EjecutarConsultaConDB($DB,$queryAforoRelacionado);

        if(isset($aforoRelacionado) && count($aforoRelacionado)>0 && isset($aforoRelacionado[0]) && isset($aforoRelacionado[0]['cod_saih'])){
            
            $ids = [];
            $ids[] = $aforoRelacionado[0]['cod_saih'];

            $idsString = "";
            foreach ($ids as $id) {
                $id = str_replace(" ", "", $id);
                if ($idsString != "") $idsString .= " OR ";
                $idsString .= "cod_variable like '" . $id . "/%'";
            }

            $idsStringTr = "";
            foreach ($ids as $id) {
                $id = str_replace(" ", "", $id);
                if ($idsStringTr != "") $idsStringTr .= " OR ";
                $idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/%'";
            }

            $periodo = 600; // 10 minutos

            $query = "
            with 
                medidas AS (
                    select cod_variable, valor, timestamp_medicion as timestamp 
                    from tbl_hist_medidas 
                    where timestamp_medicion >= '" . $fechaIni . "' 
                    and timestamp_medicion <= '" . $fechaFin . "' 
                    and (" . $idsString . ")
                    and ind_validacion = 'S'
                    and (extract(epoch from timestamp_medicion)/" . $periodo . " - TRUNC(extract(epoch from timestamp_medicion)/" . $periodo . "))=0
                ),
                max_medidas AS (
                    select cod_variable, max(timestamp) from medidas group by cod_variable
                ),
                datostrymedidas AS (
                    select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                    left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                    where timestamp >= '" . $fechaIni . "' 
                    and timestamp <= '" . $fechaFin . "'
                    and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                    and (" . $idsStringTr . ")
                    and (extract(epoch from timestamp)/" . $periodo . " - TRUNC(extract(epoch from timestamp)/" . $periodo . "))=0
                    union
                    select cod_variable, valor, timestamp from medidas
                )
            
                select
                    timestamp,
                    estacion,
                    coalesce(SUM(QR),null) AS \"QR\",
                    coalesce(SUM(NR),null) AS \"NR\"
                    from(
                        select
                        timestamp,
                        split_part(cod_variable, '/', 1) as estacion,
                        CASE WHEN cod_variable LIKE '%/QR1' THEN valor END AS QR,
                        CASE WHEN cod_variable LIKE '%/NR1' THEN valor END AS NR
                        from datostrymedidas
                    ) as valores
                    group by estacion, timestamp
                    order by timestamp asc";

            $datosAforoRelacionado = ConexionBD::EjecutarConsultaConDB($DB, $query);

        }

        ConexionBD::CerrarConexion($DB);

        $array_datos["datosCR"] = [];

        // AFORO RELACIONADO: juntar los datos con el caudal y nivel de río de estación de aforo relacionada usando el timestamp (si el aforo existe)
        if(isset($datosAforoRelacionado) && count($datosAforoRelacionado)>0){

            $array_datos["datosCR"] = array("datos" => $datosAforoRelacionado, "cabecera" => ["NR","QR"]);

            // obtener id estación de aforo
            $idEstacionAforo = $datosAforoRelacionado[0]["estacion"];
            $tipoEstacion = $aforoRelacionado[0]['tipo'];

            // poner las dos columnas de NR y QR al final de la cabecera
            $columnas[] = 'NR';
            $columnas[] = 'QR';

            // inserción de los datos de aforo a saica
            for ($i = 0; $i < count($registro); $i++) {
                $timestamp = $registro[$i]['timestamp'];
                $posicion = array_search($timestamp, array_column($datosAforoRelacionado, 'timestamp'));
                if (false !== $posicion){
                    // si se ha encontrado la línea de timestamp
                    if($datosAforoRelacionado[$posicion]["QR"] != null && $datosAforoRelacionado[$posicion]["QR"] != ""){
                        $valQR = round($datosAforoRelacionado[$posicion]["QR"],2);
                        $registro[$i]["QR"] = $valQR;
                    }
                    if($datosAforoRelacionado[$posicion]["NR"] != null && $datosAforoRelacionado[$posicion]["NR"] != ""){
                        $valNR = round($datosAforoRelacionado[$posicion]["NR"],2);
                        $registro[$i]["NR"] = $valNR;
                    }
                }
            }
            $array_datos = $array_datos + array("idEstacionAforoRelacionado" => $idEstacionAforo, "tipoEstacionRelacionada" => $tipoEstacion);
        }
        
        $array_datos = $array_datos + array("datos" => $registro, "cabecera" => $columnas);

        return $array_datos;
    }
}