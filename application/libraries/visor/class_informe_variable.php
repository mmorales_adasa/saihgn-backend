<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class InformeVariable
{

	static function init() {

		$query = "select valor from tbl_aplicacion_parametro where cod_parametro like 'INFORMES_ZONAS'";

		$conf = ConexionBD::EjecutarConsulta($query);

		$conf = json_decode($conf[0]["valor"],true);

		return $conf["informes"]; 
	
	}

	static function getVarsSumarizacion($DB){

		$query = "select valor from tbl_aplicacion_parametro where cod_parametro like 'INFORMES-ZONAS-VARIABLES-SUMARIZACION'";

		$conf = ConexionBD::EjecutarConsulta($query);

		$conf = json_decode($conf[0]["valor"],true);

		return $conf["variables"];

	}

	static function getDataEvolucion($DB, $ult_res_embalses, $variables, $zona = null) {

		$variablesSumarizables = InformeVariable::getVarsSumarizacion($DB);

		$retorno = [
			"encabezado" => [
				["id" => "zona", "cod_campo" => "zona"],
				["id" => "cod_estacion", "cod_campo" => "cod_estacion"],
				["id" => "nombre", "cod_campo" => "nombre"]
			],
			"valores" => [],
			"encabezadoAdicional" => [
				[
					["id"=>"vacio","nombre"=>'', "colspan"=>3],
				]
			] 
		];

		$constHoyStr = date("Y-m-d",strtotime("now"));
		/*$constAyerStr = date("Y-m-d",strtotime("-1 days"));
		$constAntesAyerStr = date("Y-m-d",strtotime("-2 days"));*/

		foreach($variables as $variable){

			$startdate = strtotime("now");
			$enddate = strtotime("-11 days");

			$dias[$variable] = [];
			$ndia[$variable] = 0;

			while ($startdate > $enddate) {

				$dateString = date("Y-m-d", $startdate);
				$dias[$variable][] = $dateString;
	
				// solo visual encabezado
				/*if($dateString == $constHoyStr) $dateString = "Hoy";
				if($dateString == $constAyerStr) $dateString = "Ayer";
				if($dateString == $constAntesAyerStr) $dateString = "Anteayer";*/
	
				// construir encabezado dinámico
				$retorno["encabezado"][] = ["id" => "dia_". $ndia[$variable] . "_" . $variable, "cod_campo" => $variable, "label" => date("Y.m.d", $startdate)];
	
				$startdate = strtotime("-1 day", $startdate);
	
				$ndia[$variable]++;
			}

			$retorno["encabezadoAdicional"][0][] = ["id" => $variable, "nombre" => $variable, "cod_campo" => $variable, "colspan" => $ndia[$variable]];
	
			$queryZona = ($zona != null) ? " AND e.zona = '$zona' " : "";
			
			$query = "select
			e.zona,
			e.cod_estacion,
			e.nombre,
			json_agg(json_build_object('timestamp', datos.timestamp::date, 'valor', datos.valor)) AS datos
			from 
				tbl_estacion e
				left join (
					select 
						e.cod_estacion, 
						data_medicion as timestamp,
						v.cod_variable,
						CASE
							WHEN agregacion LIKE 'MEDIA' THEN valor_media
							WHEN agregacion LIKE 'ACUMUL' THEN valor_acum
						END AS valor
					from tbl_estacion e
					left join tbl_variable v on e.cod_estacion = split_part(v.cod_variable,'/',1) 
					left join tbl_tipo_variable tv on v.cod_tipo_variable = tv.cod_tipo_variable
					left join tbl_olap_medidas_dias d on v.cod_variable = d.cod_variable
					where e.tipo like 'E' AND e.disponible $queryZona AND d.data_medicion > current_date - INTERVAL '11' day and v.cod_variable like '%/".$variable."'
					order by d.cod_estacion, d.data_medicion desc
				) datos on e.cod_estacion = datos.cod_estacion
			where e.tipo like 'E' AND e.disponible $queryZona
			group by e.cod_estacion
			order by zona, orden, cod_estacion";
	
			$datos[$variable] = ConexionBD::EjecutarConsultaConDB($DB, $query); 

		}
		
		// [Rellenado de días]: se revisan los datos de cada línea, en concreto los días, si falta algun día sin dato de variable se rellena la fecha creada por el php
		//y se deja en nulo el valor

		$datosRetorno = []; // array indexado por codigo de estación

		foreach($variables as $i => $variable){

			foreach($datos[$variable] as $datosEstacion){
				
				if(isset($datosEstacion["datos"])){

					$datosValores = json_decode($datosEstacion["datos"],true);
					$datosTimestampIndex = [];
					$datosTemp = [];

					$datosTemp["zona"] = $datosEstacion["zona"];
					$datosTemp["cod_estacion"] = $datosEstacion["cod_estacion"];
					$datosTemp["nombre"] = $datosEstacion["nombre"];

					// indexar datos por timestamp
					foreach($datosValores as $dato) {
						$datosTimestampIndex[$dato["timestamp"]] = $dato;
					}

					foreach($dias[$variable] as $x => $dia) { // por cada día revisar si existe fecha indexada de dato

						if(isset($datosTimestampIndex[$dia])) { // si existe día volcar datos sql del día

							$datosTemp["dia_".$x."_".$variable] = $datosTimestampIndex[$dia]["valor"];

						} else { // si no existe día en el array de datos crear dato nulo

							$datosTemp["dia_".$x."_".$variable] = null;

						}

						// incluir dato de tiempo real si el día es el actual y existe dato
						if($constHoyStr == $dia && isset($ult_res_embalses) && isset($ult_res_embalses["valores"])){

							foreach($ult_res_embalses["valores"] as $datosEmbalse){
								if( isset($datosEmbalse["cod_estacion"]) 
									&& $datosEmbalse["cod_estacion"] == $datosEstacion["cod_estacion"] 
									&& isset($datosEmbalse[$variable])){
										
										$datosTemp["dia_".$x."_".$variable] = $datosEmbalse[$variable];
								
								}
							}
						}
					}

					if(!isset($datosRetorno[$datosTemp['cod_estacion']])) $datosRetorno[$datosTemp['cod_estacion']] = [];

					$datosRetorno[$datosTemp['cod_estacion']] = array_merge($datosRetorno[$datosTemp['cod_estacion']], $datosTemp);

				}
			}
		}

		// proceso de suma y totales por zona
		$valores = array_values($datosRetorno);
		$retorno["valores"] = $valores;
		$valoresTotalesZona = []; // valores sumados por indice de zona
		$valoresTotales = []; // valores sumados de todas las estaciones

		foreach($valores as $linea){

			foreach($linea as $key => $valor){

				if(!in_array($key,["zona","cod_estacion"]) && strpos($key, "_") && isset(explode("_", $key)[2]) && in_array(explode("_", $key)[2], $variablesSumarizables)){

					if(!isset($valoresTotalesZona[$linea["zona"]])){ // init array de zona
						$valoresTotalesZona[$linea["zona"]] = ["zona"=>$linea["zona"],"cod_estacion"=>"TOTAL ZONA"];
					}

					if(count($valoresTotales)==0){ // init array total
						$valoresTotales = ["zona"=>"", "cod_estacion"=>"TOTAL"]; 
					}

					$var = explode("_", $key)[2];

					if(!isset($valoresTotalesZona[$linea["zona"]][$key]) && is_numeric($valor)){

						$valoresTotalesZona[$linea["zona"]][$key] = $valor;
											
					}

					else if(isset($valoresTotalesZona[$linea["zona"]][$key]) && is_numeric($valor)){

						$valoresTotalesZona[$linea["zona"]][$key] += $valor;

					}

					if(!isset($valoresTotales[$key]) && is_numeric($valor)){

						$valoresTotales[$key] = $valor;

					}

					else if(isset($valoresTotales[$key]) && is_numeric($valor)){

						$valoresTotales[$key] += $valor;

					}
				}
			}
		}

		foreach($valoresTotalesZona as $totales){
			$retorno["valores"][] = $totales;
		}

		if(count($valoresTotales)>0){
			$retorno["valores"][] = $valoresTotales;
		}
		
		return $retorno; 
	
	}

	static function getDataDiferenciaULTRES($DB, $ult_res_embalses, $variables, $zona = null, $startdateselected = null) {

		$variablesSumarizables = InformeVariable::getVarsSumarizacion($DB);

		$retorno = [
			"encabezado" => [
				["id" => "zona", "cod_campo" => "zona"],
				["id" => "cod_estacion", "cod_campo" => "cod_estacion"],
				["id" => "nombre", "cod_campo" => "nombre"]
			],
			"valores" => [],
			"encabezadoAdicional" => [
				[
					["id"=>"vacio","nombre"=>'', "colspan"=>3],
				]
			] 
		];

		$datos = [];

		$queryZona = ($zona != null) ? " AND e.zona = '$zona' " : "";

		foreach($variables as $variable){

			// construir encabezado dinámico
			$retorno["encabezado"][] = ["id" => "ini_" . $variable, "cod_campo" => $variable, "label" => 'Inicial'];
			$retorno["encabezado"][] = ["id" => "fin_" . $variable, "cod_campo" => $variable, "label" => 'Final'];
			$retorno["encabezado"][] = ["id" => "dif_" . $variable, "cod_campo" => $variable, "label" => 'Diferencia'];
			$retorno["encabezadoAdicional"][0][] = ["id" => $variable, "nombre" => $variable, "cod_campo" => $variable, "colspan" => 3];

			$query = "select 
							e.zona,
							e.cod_estacion,
							e.nombre,
							CASE
								WHEN agregacion LIKE 'MEDIA' THEN valor_media
								WHEN agregacion LIKE 'ACUMUL' THEN valor_acum
							END AS valor
						from tbl_estacion e
						left join tbl_variable v on e.cod_estacion = split_part(v.cod_variable,'/',1) 
						left join tbl_tipo_variable tv on v.cod_tipo_variable = tv.cod_tipo_variable
						left join tbl_olap_medidas_dias d on v.cod_variable = d.cod_variable
						where e.tipo like 'E' AND e.disponible $queryZona AND d.data_medicion = '".$startdateselected."' and v.cod_variable like '%/".$variable."'
						order by e.zona, e.orden, e.cod_estacion";
	
			$datos[$variable] = ConexionBD::EjecutarConsultaConDB($DB, $query); 

		}

		// [Rellenado de días]: se revisan los datos de cada línea, en concreto los días, si falta algun día sin dato de variable se rellena la fecha creada por el php
		//y se deja en nulo el valor

		$datosRetorno = []; // array indexado por codigo de estación

		foreach($variables as $i => $variable){

			foreach($datos[$variable] as $datosEstacion){
				
				if(isset($datosEstacion["valor"])){

					$datosTemp = [];

					$datosTemp["zona"] = $datosEstacion["zona"];
					$datosTemp["cod_estacion"] = $datosEstacion["cod_estacion"];
					$datosTemp["nombre"] = $datosEstacion["nombre"];
					$datosTemp["ini_".$variable] = $datosEstacion["valor"];

					// incluir dato de tiempo real si el día es el actual y existe dato
					if(isset($ult_res_embalses) && isset($ult_res_embalses["valores"])){
						foreach($ult_res_embalses["valores"] as $datosEmbalse){
							if( isset($datosEmbalse["cod_estacion"]) 
								&& $datosEmbalse["cod_estacion"] == $datosEstacion["cod_estacion"] 
								&& isset($datosEmbalse[$variable])){

									$datosTemp["fin_".$variable] = $datosEmbalse[$variable];
									$datosTemp["dif_".$variable] = $datosEmbalse[$variable] - $datosEstacion["valor"];
							}
						}
					}

					if(!isset($datosRetorno[$datosTemp['cod_estacion']])) $datosRetorno[$datosTemp['cod_estacion']] = [];

					$datosRetorno[$datosTemp['cod_estacion']] = array_merge($datosRetorno[$datosTemp['cod_estacion']], $datosTemp);

				}
			}
		}

		// proceso de suma y totales por zona
		$valores = array_values($datosRetorno);
		$retorno["valores"] = $valores;
		$valoresTotalesZona = []; // valores sumados por indice de zona
		$valoresTotales = []; // valores sumados de todas las estaciones

		foreach($valores as $linea){

			foreach($linea as $key => $valor){

				if(!in_array($key,["zona","nombre","cod_estacion"]) && strpos($key, "_") && isset(explode("_", $key)[1]) && in_array(explode("_", $key)[1], $variablesSumarizables)){

					if(!isset($valoresTotalesZona[$linea["zona"]])){ // init array de zona
						$valoresTotalesZona[$linea["zona"]] = ["zona"=>$linea["zona"],"cod_estacion"=>"TOTAL ZONA"];
					}

					if(count($valoresTotales)==0){ // init array total
						$valoresTotales = ["zona"=>"", "cod_estacion"=>"TOTAL"]; 
					}

					$var = explode("_", $key)[1];

					if(!isset($valoresTotalesZona[$linea["zona"]][$key]) && is_numeric($valor)){

						$valoresTotalesZona[$linea["zona"]][$key] = $valor;
											
					}

					else if(isset($valoresTotalesZona[$linea["zona"]][$key]) && is_numeric($valor)){

						$valoresTotalesZona[$linea["zona"]][$key] += $valor;

					}

					if(!isset($valoresTotales[$key]) && is_numeric($valor)){

						$valoresTotales[$key] = $valor;

					}

					else if(isset($valoresTotales[$key]) && is_numeric($valor)){

						$valoresTotales[$key] += $valor;

					}
				}
			}
		}

		foreach($valoresTotalesZona as $totales){
			$retorno["valores"][] = $totales;
		}

		if(count($valoresTotales)>0){
			$retorno["valores"][] = $valoresTotales;
		}
		

		return $retorno; 
	
	}

	static function getDataDiferencia($DB, $variables, $zona = null, $startdateselected = null, $enddateselected = null) {

		$variablesSumarizables = InformeVariable::getVarsSumarizacion($DB);

		$retorno = [
			"encabezado" => [
				["id" => "zona", "cod_campo" => "zona"],
				["id" => "cod_estacion", "cod_campo" => "cod_estacion"],
				["id" => "nombre", "cod_campo" => "nombre"]
			],
			"valores" => [],
			"encabezadoAdicional" => [
				[
					["id"=>"vacio","nombre"=>'', "colspan"=>3],
				]
			] 
		];

		$queryZona = ($zona != null) ? " AND e.zona = '$zona' " : "";

		foreach($variables as $variable){

			// construir encabezado dinámico
			$retorno["encabezado"][] = ["id" => "ini_" . $variable, "cod_campo" => $variable, "label" => 'Inicial'];
			$retorno["encabezado"][] = ["id" => "fin_" . $variable, "cod_campo" => $variable, "label" => 'Final'];
			$retorno["encabezado"][] = ["id" => "dif_" . $variable, "cod_campo" => $variable, "label" => 'Diferencia'];
			$retorno["encabezadoAdicional"][0][] = ["id" => $variable, "nombre" => $variable, "cod_campo" => $variable, "colspan" => 3];
	
			$query = "select 
						ini.zona, 
						ini.cod_estacion, 
						ini.nombre, 
						ini.valor as valor_inicial, 
						fin.valor as valor_final, 
						(fin.valor - ini.valor) as diferencia from
					(
						select 
							e.zona,
							e.cod_estacion,
							e.nombre,
							e.orden,
							CASE
								WHEN agregacion LIKE 'MEDIA' THEN valor_media
								WHEN agregacion LIKE 'ACUMUL' THEN valor_acum
							END AS valor
						from tbl_estacion e
						left join tbl_variable v on e.cod_estacion = split_part(v.cod_variable,'/',1) 
						left join tbl_tipo_variable tv on v.cod_tipo_variable = tv.cod_tipo_variable
						left join tbl_olap_medidas_dias d on v.cod_variable = d.cod_variable
						where e.tipo like 'E' AND e.disponible $queryZona AND d.data_medicion = '".$startdateselected."' and v.cod_variable like '%/".$variable."'
						
					) ini left join (
						
						select 
							e.zona,
							e.cod_estacion,
							e.nombre,
							e.orden,
							CASE
								WHEN agregacion LIKE 'MEDIA' THEN valor_media
								WHEN agregacion LIKE 'ACUMUL' THEN valor_acum
							END AS valor
						from tbl_estacion e
						left join tbl_variable v on e.cod_estacion = split_part(v.cod_variable,'/',1) 
						left join tbl_tipo_variable tv on v.cod_tipo_variable = tv.cod_tipo_variable
						left join tbl_olap_medidas_dias d on v.cod_variable = d.cod_variable
						where e.tipo like 'E' AND e.disponible $queryZona AND d.data_medicion = '".$enddateselected."' and v.cod_variable like '%/".$variable."'
						
					) fin on ini.cod_estacion = fin.cod_estacion
					
					order by ini.zona, ini.orden, ini.cod_estacion";
	
			$datos[$variable] = ConexionBD::EjecutarConsultaConDB($DB, $query); 

		}

		$datosRetorno = []; // array indexado por codigo de estación

		foreach($variables as $i => $variable){

			foreach($datos[$variable] as $datosEstacion){
				
				$datosTemp = [];

				$datosTemp["zona"] = $datosEstacion["zona"];
				$datosTemp["cod_estacion"] = $datosEstacion["cod_estacion"];
				$datosTemp["nombre"] = $datosEstacion["nombre"];
				$datosTemp["fin_".$variable] = $datosEstacion["valor_final"];
				$datosTemp["ini_".$variable] = $datosEstacion["valor_inicial"];
				$datosTemp["dif_".$variable] = $datosEstacion["diferencia"];

				if(!isset($datosRetorno[$datosTemp['cod_estacion']])) $datosRetorno[$datosTemp['cod_estacion']] = [];

				$datosRetorno[$datosTemp['cod_estacion']] = array_merge($datosRetorno[$datosTemp['cod_estacion']], $datosTemp);

			}
		}

		// proceso de suma y totales por zona
		$valores = array_values($datosRetorno);
		$retorno["valores"] = $valores;
		$valoresTotalesZona = []; // valores sumados por indice de zona
		$valoresTotales = []; // valores sumados de todas las estaciones

		foreach($valores as $linea){

			foreach($linea as $key => $valor){

				if(!in_array($key,["zona","nombre","cod_estacion"]) && strpos($key, "_") && isset(explode("_", $key)[1]) && in_array(explode("_", $key)[1], $variablesSumarizables)){

					if(!isset($valoresTotalesZona[$linea["zona"]])){ // init array de zona
						$valoresTotalesZona[$linea["zona"]] = ["zona"=>$linea["zona"],"cod_estacion"=>"TOTAL ZONA"];
					}

					if(count($valoresTotales)==0){ // init array total
						$valoresTotales = ["zona"=>"", "cod_estacion"=>"TOTAL"]; 
					}

					$var = explode("_", $key)[1];

					if(!isset($valoresTotalesZona[$linea["zona"]][$key]) && is_numeric($valor)){

						$valoresTotalesZona[$linea["zona"]][$key] = $valor;
											
					}

					else if(isset($valoresTotalesZona[$linea["zona"]][$key]) && is_numeric($valor)){

						$valoresTotalesZona[$linea["zona"]][$key] += $valor;

					}

					if(!isset($valoresTotales[$key]) && is_numeric($valor)){

						$valoresTotales[$key] = $valor;

					}

					else if(isset($valoresTotales[$key]) && is_numeric($valor)){

						$valoresTotales[$key] += $valor;

					}
				}
			}
		}

		foreach($valoresTotalesZona as $totales){
			$retorno["valores"][] = $totales;
		}

		if(count($valoresTotales)>0){
			$retorno["valores"][] = $valoresTotales;
		}
		
		return $retorno; 
	
	}

}
