<?php
require_once APPPATH.'/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Explotacion {

    static function crearEncabezado($variables){

        $in = "'". implode("','", $variables) ."'";

        // se obtiene la info del tipo de variable y a la estación que pertenece
        $query = "select cod_variable, cod_tipo_variable, v.descripcion descripcion_variable, cod_estacion, tipo as tipo_estacion, t.descripcion descripcion_tipo  from tbl_variable v, tbl_estacion, tbl_tipo_estacion t 
                    where cod_variable in (".$in.") 
                    and cod_estacion = split_part(cod_variable,'/',1)
                    and tipo = cod_tipo_estacion
                    order by tipo";

        $variableInfoTemp = ConexionBD::EjecutarConsulta($query, true);
        $variableInfo = [];

        // ordenar variables llegadas de base de datos por el orden de array
        foreach($variables as $variable){
            foreach($variableInfoTemp as $varInfo){
                if($varInfo["cod_variable"]==$variable){
                    $variableInfo[] = $varInfo;
                }
            }
        }
        
        // grupos por tipo de estación 
        $cabecera1Temp = [];
        $cabecera2Temp = [];
        $cabecera3Temp = [];
        $cabeceras = [];
        
        // crear la primera cabecera temporal que agrupa y realiza un conteo de las variables que se incluyen en cada
        foreach($variableInfo as $variable){
            if(!isset($cabecera1Temp[$variable["tipo_estacion"]])) $cabecera1Temp[$variable["tipo_estacion"]] = ["id"=>$variable["descripcion_tipo"], "colspan"=>0, "nombre" => $variable["descripcion_tipo"], "variables" => []];
            $cabecera1Temp[$variable["tipo_estacion"]]["colspan"] += 1;
            // dar nombre a la variable e incluirla en el array de variables del tipo de estación
            $cabecera1Temp[$variable["tipo_estacion"]]["variables"][] = ["id"=>$variable["cod_estacion"], "nombre" => trim($variable["cod_estacion"]).": ".$variable["descripcion_variable"], "cod_variable" => $variable["cod_variable"]];
        }

        // crear la segunda cabecera temporal a partir de la primera cabecera
        foreach($cabecera1Temp as $cabecera1TipoEstacion){
            foreach($cabecera1TipoEstacion['variables'] as $variable){
                $cabecera2Temp[] = $variable;
            }
        }

        // crear la cabecera principal temporal a partir de la segunda cabecera
        //$cabecera3Temp[] = ["id" => "timestamp", "cod_campo" => "timestamp"]; // añadir columna timestamp
        foreach($cabecera2Temp as $cabecera2variable){
            $variablePartida = explode("/", $cabecera2variable["cod_variable"]);
            $cabecera3Temp[] = ["id" => $cabecera2variable["cod_variable"], "cod_campo" => $variablePartida[1]]; 
        }

        $cabeceras[0][] = ["id"=>"vacio","nombre"=>'', "rowspan"=>2]; // añadir columna vacía a la primera cabecera comlementaria

        // crear un array definitivo con la primera cabecera complementaria de tipos de estacion
        foreach($cabecera1Temp as $cabecera1){
            $cabeceras[0][] = ["id"=>$cabecera1["nombre"], "nombre"=>$cabecera1["nombre"], "colspan"=>$cabecera1["colspan"]];
        }

        // crear un array definitivo con la segunda cabecera complementaria de tipos de variable
        foreach($cabecera2Temp as $cabecera2){
            $cabeceras[1][] = ["id"=>$cabecera2["nombre"], "nombre"=>$cabecera2["nombre"]];
        }

        $cabeceras[2][] = ["id" => "timestamp", "cod_campo" => "timestamp"]; // añadir columna timestamp a la cabecera principal

        // crear un array definitivo con la cabecera principal
        foreach($cabecera3Temp as $cabecera){
            $cabeceras[2][] = $cabecera;
        }

        return $cabeceras;

    }

    static function getExplotacion($variables, $periodo, $fechaIni, $fechaFin){

        $coalesce = [];
        foreach($variables as $variable){
            $coalesce[] = "coalesce(SUM(\"".$variable."\"),null) AS \"".$variable."\"";
        }
        $coalesce = implode(",", $coalesce);

        $cases = [];
        foreach($variables as $variable){
            $cases[] = "case when cod_variable like '".$variable."' then valor end as \"".$variable."\"";
        }
        $cases = implode(",", $cases);

        $in = "'". implode("','", $variables) ."'";

        $query = "with 
            medidas AS (
                select cod_variable, valor, timestamp_medicion as timestamp 
                from tbl_hist_medidas 
                where timestamp_medicion >= '" . $fechaIni . "' 
                and timestamp_medicion <= '" . $fechaFin . "' 
                and cod_variable IN (".$in.")
                and ind_validacion = 'S'
                and (extract(epoch from timestamp_medicion)/" . $periodo . " - TRUNC(extract(epoch from timestamp_medicion)/" . $periodo . "))=0
            ),
            max_medidas AS (
                select cod_variable, max(timestamp) from medidas group by cod_variable
            ),
            datostrymedidas AS (
                select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                where timestamp >= '" . $fechaIni . "' 
                and timestamp <= '" . $fechaFin . "'
                and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                and tbl_dato_tr.cod_variable IN (".$in.")
                and (extract(epoch from timestamp)/" . $periodo . " - TRUNC(extract(epoch from timestamp)/" . $periodo . "))=0
                union
                select cod_variable, valor, timestamp from medidas
            )
            
            select 
                timestamp,
                ".$coalesce."
                from( 
                    select timestamp, 
                    substring(cod_variable,0,6) as estacion,
                    ".$cases."
                    from datostrymedidas
                ) as valores
                group by timestamp 
                order by timestamp asc";

        $data = ConexionBD::EjecutarConsulta($query);



        return $data;
    
    }

    static function obtenerVariables($estacionesUsuario){

        $in = "'". implode("','", $estacionesUsuario) ."'";

        // se obtiene la info del tipo de variable y a la estación que pertenece
        $query = "select tipo, tipo || ' - ' || t.descripcion as tipo_descripcion, cod_estacion, cod_estacion || ' - ' || e.nombre as cod_estacion_descripcion, cod_variable, split_part(cod_variable,'/',2) || ' - ' || v.descripcion as cod_variable_descripcion from tbl_variable v, tbl_estacion e, tbl_tipo_estacion t 
        where v.disponible = 1                    
        and cod_estacion in (".$in.")
        and split_part(cod_variable,'/',1) = e.cod_estacion
        and t.cod_tipo_estacion = e.tipo
        order by tipo, cod_estacion, cod_variable";

        $data = ConexionBD::EjecutarConsulta($query);

        return $data;
    
    }

    static function obtenerSistemas(){

        $sistemas = [
            [ "nombre" => "MONTIJO", "variables" => ['CR2-06/QR1','CR2-13/QR1','CR2-51/QR1','CR2-19/QR1','CR2-25/QR1','CR2-31/QR1','CR2-34/QR1','E2-25/QR1','E2-19/NE1'] ],
            [ "nombre" => "GUADIANA", "variables" => ['CR2-01/QR1','CR2-02/QR1','CR2-03/QR1','CR2-04/QR1','CR2-05/QR1','CR2-52/QR1','CR2-06/QR1','CR2-13/QR1','CR2-51/QR1','E2-01/NE1','E2-03/NE1','E2-04/NE1'] ],
            [ "nombre" => "ZÚJAR", "variables" => ['CR2-08/QR1','CR2-07/QR1','CR2-09/QR1','CR2-11/QR1','CR2-12/QR1','CR2-13/QR1','CR2-14/QR1','CR2-06/QR1','CR2-51/QR1','E2-06/NE1','E2-07/NE1'] ],
            [ "nombre" => "MATACHEL", "variables" => ['CR2-26/QR1','CR2-27/QR1','CR2-28/QR1','CR2-29/QR1','CR2-30/QR1','CR2-31/QR1','CR2-34/QR1','E2-15/NE1','E2-16/NE1'] ],
            [ "nombre" => "LÁCARA", "variables" => ['CR2-32/QR1','CR2-35/QR1','CR2-31/QR1','CR2-34/QR1','E2-20/NE1','E2-21/NE1','E2-22/NE1'] ],
            [ "nombre" => "GÉVORA", "variables" => ['CR2-34/QR1','CR2-43/QR1','CR2-44/QR1','CR2-45/QR1','E2-25/QR1','E2-24/NE1'] ],
            [ "nombre" => "VEGAS BAJAS", "variables" => ['CR2-25/QR1','CR2-34/QR1','CR2-35/QR1','CR2-37/QR1','CR2-39/QR1','CR2-40/QR1','CR2-41/QR1','E2-25/NE1','E2-19/NE1'] ],
            [ "nombre" => "Vegas Bajas-Niveles", "variables" => ['CR2-34/NR1', 'NR2-30/NR1', 'NR2-36/NR1', 'E2-25/NR1', 'CR2-55/NR1', 'CR2-34/QR1', 'CR2-55/QR1'] ],
            [ "nombre" => "MONTIJO niveles", "variables" => ['CR2-06/NR1','CR2-13/NR1','CR2-51/NR1','NR2-35/NR1','CR2-25/NR1','E2-19/NE1'] ],
        ];

        return $sistemas;
    
    }
}