<?php 
//Conexión postgre
$conection = "host='172.21.0.27' dbname='SAIHG' user='saihg' password='saihguad' port=5432";

//Definición de variables
$AH = "2019-2020";				//año hidrológico
$versión="3.0";					//versión del paniSAIHG
$ultModificacion= "04.03.2017";	//última modificación

$datos='<p>Nota: Datos automáticos sin validar. Datos horarios según hora local.</p>';
//$datos='<p>Nota: Datos automáticos sin validar. '.$horalocal.'</p>';

$horalocal = "Hora local = hora UTC + 1 (en horario de invierno).";
//$horalocal = "Hora local = hora UTC + 2 (en horario de verano).";

$nota="Aplicaciones creadas por el Área de Explotación";

//Ruta tipo para el directorio actual
$ruta_config =	"./config/"; 					//ruta de los archivos de configuración

$ruta_datos  =	"./datos/"; 	//ruta de los archivos de datos
$ruta_video =	  $ruta_datos.'DocGrafica/video/'; 			//ruta de la videovigilancia
$ruta_timelapse = $ruta_datos.'DocGrafica/timelapse/'; 		//ruta de las fotos del timelapse

$menu_app= array ("APP","RCI","EXPED.","CONTR.","BETA","DIARIO");

$redes=array('RCI'=>'RED DE CONTROL INTEGRADA','S'=>'SISTEMA','AEX'=>'ÁREA DE EXPLOTACIÓN','E'=>'EMBALSES','Z12'=>'ZONAS 1ª Y 2ª','Z3'=>'ZONA 3ª','Z5'=>'ZONA 5ª','Z67'=>'ZONAS 6ª Y 7ª','CR'=>'AFOROS','EM'=>'E. METEOROLÓGICAS','CP'=>'PIEZÓMETROS','CAL'=>'REDES DE CALIDAD','EAA'=>'SAICA','ECC'=>'REACAR','EXC'=>'IND.EXTERNOS','R'=>'REPETIDORES','ADM'=>'ADMINISTRADOR');


$menu= array (
	array ("RCI","RCI","RCI","RED DE CONTROL INTEGRADA","INFO","","","SYS","","",""),
	array ("AEX","AEX","AEX","ÁREA DE EXPLOTACIÓN","INFO","PER1","EMB","AEX","","",""),
	//array ("ELE","AEX","","","","","","","","",""),
	//array ("REP","AEX","","","","","","","","",""),
	//array ("MET","AEX","","","","","","","","",""),
	array ("Z12","Z12","Z12","Zonas 1ª y 2ª","INFO","PER1","EMB","VIDEO","","",""),
	array ("PYA","Z12","E1-01","Peñarroya","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("PUV","Z12","E1-02","Vallehermoso","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("PUN","Z12","E1-03","Puente Navarro","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("GAS","Z12","E1-05","Gasset","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("VIC","Z12","E1-06","El Vicario","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("CAB","Z12","E1-07","La Cabezuela","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("JAB","Z12","E1-08","Vega Jabalón","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("TAB","Z12","E1-09","Torre Abraham","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("CAM","Z12","E1-10","Campos del Paraíso","INFO","PER2","AVE","VIDEO","","PE","A.T."),
	array ("Z3","Z3","Z3","Zona 3ª","INFO","PER1","EMB","VIDEO","","",""),
	array ("CIJ","Z3","E2-01","Cijara","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("GAR","Z3","E2-03","García Sola","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("ORE","Z3","E2-04","Orellana","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("SER","Z3","E2-06","La Serena","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("ZUJ","Z3","E2-07","Zújar","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("Z5","Z5","Z5","Zona 5ª","INFO","PER1","EMB","VIDEO","","",""),
	array ("CFR","Z5","E2-08","Cancho de Fresno","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("RUE","Z5","E2-09","Ruecas","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("AZR","Z5","E2-10","Azud de Lavadero","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("SIB","Z5","E2-11","Sierra Brava","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("GRG","Z5","E2-12","Gargáligas","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("CUB","Z5","E2-13","Cubilar","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("ALC","Z5","E2-33","Alcollarín","INFO","PER2","AVE","VIDEO","","PE","A.T."),
	array ("BUR","Z5","E2-34","Búrdalo","INFO","PER2","AVE","VIDEO","","PE","A.T."),
	array ("Z67","Z67","Z67","Zonas 6ª y 7ª","INFO","PER1","EMB","VIDEO","","",""),
	array ("ALA","Z67","E2-16","Alange","INFO","PER2","AVE","VIDEO","OFICIAL","PE","A.T."),
	array ("MOL","Z67","E2-15","Los Molinos","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("VIB","Z67","E2-32","Villalba de los Barros","INFO","PER2","AVE","VIDEO","","PE","A.T."),
	array ("MON","Z67","E2-19","Montijo","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("PRO","Z67","E2-18","Proserpina","INFO","PER2","AVE","VIDEO","","PE","A.T."),
	array ("COR","Z67","E2-17","Cornalbo","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("VIL","Z67","E2-24","Villar del Rey","INFO","PER2","AVE","VIDEO","AUSCUL.","","A.T."),
	array ("TEN","Z67","E2-28","Tentudía","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("CAN","Z67","E2-22","Canchales","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("HOR","Z67","E2-20","Hornotejero","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("BOQ","Z67","E2-21","Boquerón","INFO","PER2","AVE","VIDEO","","","A.T."),
	array ("EMB","EMB","E","EMBALSES","INFO","DATOS","","SISTEMA","","",""),
	array ("AFO","AFO","CR","RED DE AFOROS","INFO","DATOS","","SISTEMA","","",""),
	array ("CALIDAD","CALIDAD","CAL","CALIDAD","INFO","","","SISTEMA","","",""),
	array ("SAICA","CALIDAD","EAA","RED SAICA","INFO","DATOS","VIDEO","SISTEMA","","",""),
	array ("REACAR","CALIDAD","ECC","RED REACAR","INFO","DATOS","","SISTEMA","","",""),
	array ("INDUSTR.","CALIDAD","EXC","VERTIDOS INDUSTRIALES","INFO","DATOS","","SISTEMA","","",""),
	//array ("CUALIT.","CALIDAD","","RED CUALITATIVA DE CALIDAD","","","","","","",""),
	array ("PIEZO","PIEZO","CP","RED DE PIEZOMETRÍA","INFO","DATOS","","SISTEMA","","",""),
	array ("REP","REP","R","SISTEMA DE REPETIDORES","INFO","","","SISTEMA","","",""),
	array ("METEO","METEO","EM","RED METEOROLÓGICA","INFO","DATOS","MET-01","SISTEMA","","",""),
	array ("ADM","ADM","ADM","ADMINISTRADOR","ADM","CFG","TABLAS","INFORME","UPDATE","",""),


	);

/*$id2=array(
		"ACC"=>"Accidente",
		"AUS"=>"Auscultación",
		"AUT"=>"Autorización",
		"AVE"=>"Avenidas",
		"AVR"=>"Avería",
		"CSG"=>"Consigna",
		"EXP"=>"Explotación",
		"EVE"=>"Evento",
		"FOR"=>"Formación",
		"INC"=>"Incidencia",
		"INS"=>"Inspección",
		"MAN"=>"Mantenimiento",
		"MBR"=>"Maniobra, turbinado",
		"OBR"=>"Obra",
		"REP"=>"Reparación",
		"ROB"=>"Robo, vandalismo",
		"SS"=>"Seguridad y salud",
		"SEQ"=>"Sequía",
		"VIS"=>"Visita",
		);*/
		
/*
$presas=array (
//"array (""Acro."",""Cód."",""Nombre"",""Zona"",Color,latitud,longitud,umbral para gráfico,N.M.N.[msnm],máximo nivel embalse,Vol N.M.N. [hm3],operativa,n_camara,rango_IP,puerto_ext),"
	array ("PYA","E1-01","Peñarroya","Z12",1,39.0630493,-3.007425,735,735,735,50.32,0,4,101,51010),
	array ("PUV","E1-02","Vallehermoso","Z12",0,38.8686636,-3.1675612,749.5,746,749.5,6.92,0,4,102,51020),
	array ("PUN","E1-03","Puente Navarro","Z12",1,39.1119517,-3.7602812,606,603.25,735,2.2,0,4,103,51030),
	array ("GAS","E1-05","Gasset","Z12",0,39.128642,-3.9369692,625.5,624.16,625.5,38.87,0,4,105,51050),
	array ("VIC","E1-06","El Vicario","Z12",1,39.0583682,-3.9965198,599,594.65,599,32.86,0,4,106,51060),
	array ("CAB","E1-07","La Cabezuela","Z12",0,38.6943244,-3.280959,763,763,763,42.83,0,4,107,51070),
	array ("JAB","E1-08","Vega Jabalón","Z12",1,38.7604319,-3.7838715,639,636.1,639,33.54,0,4,108,51080),
	array ("TAB","E1-09","Torre Abraham","Z12",0,39.3684227,-4.2505144,673.5,673.5,763,183.36,0,4,109,51090),
	array ("CAM","E1-10","Campos del Paraíso","Z12",1,0,0,863.4,862.5,639,4.95,0,4,110,51100),
	array ("CIJ","E2-01","Cijara","Z3",1,39.3738185,-5.0177366,427.49,418.6,427.49,1505.19,1,1,201,52010),
	array ("GAR","E2-03","García Sola","Z3",0,39.1444102,-5.1842479,362.6,353.4,362.6,554.17,1,16,203,52030),
	array ("ORE","E2-04","Orellana","Z3",1,38.9868213,-5.5392008,318,312,318,807.91,1,16,204,52040),
	array ("SER","E2-06","La Serena","Z3",0,38.9165797,-5.4080722,352.09,346,352.09,3219.18,1,14,206,52060),
	array ("ZUJ","E2-07","Zújar","Z3",1,38.91662,-5.478007,318,313.5,318,301.9,1,4,207,52070),
	array ("CFR","E2-08","Cancho de Fresno","Z5",0,39.3935287,-5.3936138,626,626,626,15.21,1,8,208,52080),
	array ("RUE","E2-09","Ruecas","Z5",1,39.2635564,-5.4930247,389.05,389.05,389.05,41.94,1,8,209,52090),
	array ("AZR","E2-10","Azud de Lavadero","Z5",0,39.2331079,-5.5225979,338.5,338.53,338.5,0.43,1,5,210,52100),
	array ("SIB","E2-11","Sierra Brava","Z5",1,39.195107,-5.6396553,331,331,331,232.4,1,12,211,52110),
	array ("GRG","E2-12","Gargáligas","Z5",0,39.185893,-5.3587326,355.65,355.65,355.65,21.32,1,9,212,52120),
	array ("CUB","E2-13","Cubilar","Z5",1,39.2385532,-5.4766959,348,348,348,5.98,1,2,213,52130),
	array ("ALC","E2-33","Alcollarín","Z5",0,39.24985,-5.7453557,328,328,542.75,51.64,1,12,233,52330),
	array ("BUR","E2-34","Búrdalo","Z5",1,39.186558,-5.9782943,315.5,315.5,350,79.3,1,14,234,52340),
	array ("ALA","E2-16","Alange","Z67",0,38.7902632,-6.2727677,275.56,270.5,275.56,851.7,1,19,216,52160),
	array ("MOL","E2-15","Los Molinos","Z67",1,38.5314381,-6.1201799,350,350,350,33.65,1,8,215,52150),
	array ("VIB","E2-32","Villalba de los Barros","Z67",1,38.58191,-6.5049,326,326,326,106.43,1,15,232,52320),
	array ("MON","E2-19","Montijo","Z67",1,38.9232757,-6.4299413,202.15,202.15,202.15,11.17,1,31,219,52190),
	array ("PRO","E2-18","Proserpina","Z67",0,38.9741932,-6.3648794,244.58,244.58,244.58,5.04,1,12,218,52180),
	array ("COR","E2-17","Cornalvo","Z67",1,38.9897294,-6.1920371,308,308,308,3.13,1,6,217,52170),
	array ("VIL","E2-24","Villar del Rey","Z67",1,39.1525316,-6.8678606,242.9,242.9,242.9,130,1,13,224,52240),
	array ("TEN","E2-28","Tentudía","Z67",0,38.0989128,-6.3170981,667.95,667.95,667.95,5,1,9,228,52280),
	array ("CAN","E2-22","Canchales","Z67",0,38.9652651,-6.5198656,222.1,222.1,222.1,25.9,1,11,222,52220),
	array ("HOR","E2-20","Hornotejero","Z67",0,39.1515426,-6.4330143,324,324,324,24.42,1,14,220,52200),
	array ("BOQ","E2-21","Boquerón","Z67",1,39.1511891,-6.4183973,330,330,330,5.51,1,6,220,52200),

);
*/

$diccionario= array (
	
	array ("Estado com","ET", "Estado comunicación", ""),
	array ("EA.Caudal","QR1","Caudal en río","m³/s"),
	array ("EA.Nivel","NR1","Nivel en río","m"),
	
	array ("CP.Cota","CP","Cota piezoresistiva","m"),

	array ("EMB.Capacidad","VMAX","Capacidad de embalse",""),
	array ("EMB.Cota aliviadero","ALIV","Cota de aliviadero",""),
	array ("EMB.Cota maxima","MAX","Cota máxima",""),
	array ("EMB.Cota minima","MIN","Cota mínima",""),
	array ("EMB.Nivel","NE1","Nivel de embalse","msnm"),
	array ("AQ.Porcentaje_n","AQ","Porcentaje nivel embalsado",""),
	array ("EMB.Porcentaje_n","PN1","Porcentaje nivel embalsado",""),
	array ("EMB.Porcentaje_v","PV1","Porcentaje volumen embalsado",""),
	array ("EMB.Superficie","SE1","Superficie de embalse",""),
	array ("EMB.Volumen","VE1","Volumen embalsado",""),
	array (".Ultima_com","UC","Última comunicación",""),
	array (".Estado com","ET","Estado comunicación",""),

	array ("EM.Direccion viento","DV","Dirección del ciento",""),
	array ("EM.Evaporacion","EV","Evaporación",""),
	array ("EM.Humedad relativa","HR","Humedad relativa",""),
	array ("EM.Intensidad lluvia","PI","Intensidad de lluvia",""),
	array ("EM.Lluvia acumulada","PRA","Lluvia acumulada",""),
	array ("EM.Presion","PA","Presión atmosférica",""),
	array ("EM.Radiacion","RT","Radiacion solar",""),
	array ("EM.Temperatura","TA","Temperatura ambiente",""),
	array ("EM.Velocidad viento","VV","Velocidad del viento",""),

	array ("PL.Intensidad lluvia","PI","Intensidad de lluvia",""),
	array ("PL.Lluvia acumulada","PRA","Lluvia acumulada",""),

	array ("QAT.Caudal","QAT","Caudal de aliviadero total",""),
	array ("QFT.Caudal","QFT","Caudal de fondo total",""),
	
	array ("CA1.Nivel","NC1","Nivel de canal",""),
	array ("CA2.Nivel","NC2","Nivel de canal",""),
	array ("CA3.Nivel","NC3","Nivel de canal",""),
	array ("CA4.Nivel","NC4","Nivel de canal",""),
	array ("CA5.Nivel","NC5","Nivel de canal",""),
	array ("CA6.Nivel","NC6","Nivel de canal",""),
	
	array ("NC1.Nivel","NC1","Nivel de canal",""),
	array ("NC2.Nivel","NC2","Nivel de canal",""),
	array ("NC3.Nivel","NC3","Nivel de canal",""),
	array ("NC4.Nivel","NC4","Nivel de canal",""),
	array ("NC5.Nivel","NC5","Nivel de canal",""),
	array ("NC6.Nivel","NC6","Nivel de canal",""),

	array ("CA1.Caudal","QC1","Caudal de canal",""),
	array ("CA2.Caudal","QC2","Caudal de canal",""),
	array ("QC1.Caudal","QC1","Caudal de canal",""),
	array ("QC2.Caudal","QC2","Caudal de canal",""),
	array ("QC3.Caudal","QC3","Caudal de canal",""),
	array ("QT1.Caudal","QT1","Caudal de tuberia",""),
	array ("QT2.Caudal","QT2","Caudal de tuberia",""),
	array ("QT3.Caudal","QT3","Caudal de tuberia",""),
	array ("QT4.Caudal","QT4","Caudal de tuberia",""),
	array ("QTR.Caudal","QTR","Caudal de trasvase",""),
	array ("QWC.Caudal","QWC","Caudal turbinado a canal",""),
	array ("QWR.Caudal","QWR","Caudal turbinado a río",""),
	array ("QWT.Caudal","QWT","Caudal turbinado total",""),
	array ("PT1.PotenciaTurbinada","PT1","Potencia turbinada",""),
	array ("PT2.PotenciaTurbinada","PT2","Potencia turbinada",""),
	array ("PT3.PotenciaTurbinada","PT3","Potencia turbinada",""),
	array ("PT4.PotenciaTurbinada","PT4","Potencia turbinada",""),
	array ("CL1.Posicion","CV1","Compuerta vertical",""),
	array ("CL2.Posicion","CV2","Compuerta vertical",""),
	array ("CL3.Posicion","CV3","Compuerta vertical",""),
	array ("CL4.Posicion","CV4","Compuerta vertical",""),
	array ("CL5.Posicion","CV5","Compuerta vertical",""),
	array ("CL6.Posicion","CV6","Compuerta vertical",""),
	array ("CL7.Posicion","CV7","Compuerta vertical",""),
	array ("TA1.Posicion","CTA1","Compuerta Taintor aliviadero",""),
	array ("TA2.Posicion","CTA2","Compuerta Taintor aliviadero",""),
	array ("TA3.Posicion","CTA3","Compuerta Taintor aliviadero",""),
	array ("TA4.Posicion","CTA4","Compuerta Taintor aliviadero",""),
	array ("TA5.Posicion","CTA5","Compuerta Taintor aliviadero",""),
	array ("TA6.Posicion","CTA6","Compuerta Taintor aliviadero",""),
	array ("TA7.Posicion","CTA7","Compuerta Taintor aliviadero",""),
	array ("TA8.Posicion","CTA8","Compuerta Taintor aliviadero",""),
	array ("TC1.Posicion","CTC1","Compuerta Taintor de canal",""),
	array ("TC2.Posicion","CTC2","Compuerta Taintor de canal",""),
	array ("TC3.Posicion","CTC3","Compuerta Taintor de canal",""),
	array ("TC4.Posicion","CTC4","Compuerta Taintor de canal",""),
	array ("TC5.Posicion","CTC5","Compuerta Taintor de canal",""),


);







//Tabla con los embalses de cada zona para hacer subtotales (nombre corto, nombre medio, nombre largo, presa inicio, presa final)
/*
$zonas = array
   (
   array("12","zona12","TOTAL ZONAS 1ª Y 2ª",0,8),
   array("3","zona3","TOTAL ZONA 3ª",9,13),
   array("5","zona5","TOTAL ZONA 5ª",14,21),
   array("67","zona67","TOTAL ZONAS 6ª y 7ª",22,32),
   array("TODO","zona123567","TOTAL ZONAS 1ª, 2ª, 3ª, 5ª, 6ª y 7ª",0,32),
   );
*/

//Base de datos de estaciones de aforo (código SAIHG, descripción, zona de explotación, cambio de color, latitud, longitud, umbral gráfico)
/*
$bdCR = array
   (
	array ("CR1-26","Guadiana en La Cubeta",12,1,38.977559,-2.893239,10),
	array ("CR1-01","Guadiana Peñarroya",12,0,39.0550102,-3.0179181,10),
	array ("CR1-02","Azuer Vallehermoso",12,0,38.8739328,-3.1754471,10),
	array ("CR1-03","Azuer Daimiel",12,0,39.0755991,-3.6000445,10),
	array ("CR1-04","Cigüela Saelices",12,0,39.8836195,-2.8008124,10),
	array ("CR1-05","Cigüela Quintanar",12,0,39.642662,-3.0808864,10),
	array ("CR1-06","Riansares Corral",12,0,39.7596758,-3.1665014,10),
	array ("CR1-07","Cigüela Villafranca",12,0,39.4150249,-3.310013,10),
	array ("CR1-08","Záncara Provencio",12,0,39.3585566,-2.5785506,10),
	array ("CR1-09","Záncara Socuéllamos",12,0,39.3432006,-2.879839,10),
	array ("CR1-10","Córcoles Cabecera",12,0,39.086579,-2.5630964,10),
	array ("CR1-11","Córcoles",12,0,39.3018525,-2.7919525,10),
	array ("CR1-12","Záncara Alcázar",12,0,39.3088552,-3.1552209,10),
	array ("CR1-13","Amarguillo",12,0,39.4628227,-3.5937428,10),
	array ("CR1-14","Amarguillo Herencia",12,0,39.3779663,-3.3225507,10),
	array ("CR1-15","Cigüela Herencia",12,0,39.3016194,-3.3650302,10),
	array ("CR1-16","Bañuelos",12,0,39.1021591,-3.9222899,10),
	array ("CR1-17","Guadiana Vicario",12,1,39.0419401,-4.0288387,10),
	array ("CR1-18","Jabalón Cabezuela",12,0,38.6885384,-3.2811548,10),
	array ("CR1-19","Jabalón V.Jabalón",12,0,38.7593024,-3.7870012,10),
	array ("CR1-20","Bullaque T.Abra.",12,0,39.2583636,-4.2669846,10),
	array ("CR1-21","Bullaque Luciana",12,1,38.9851478,-4.2872102,10),
	array ("CR1-22","Tirteafuera",12,0,38.8892318,-4.4048207,10),
	array ("CR1-23","Becea Gasset",12,0,39.1268422,-3.9344234,10),
	array ("CR1-24","Guadiana Villarrubia",12,0,39.198925,-3.5872186,10),
	array ("CR1-25","Valdejudíos",12,0,39.581761,-2.442507,10),
	array ("CR2-01","Guadiana Puebla",3,1,39.1015956,-4.5833778,10),
	array ("CR2-02","Estena Helechosa",3,0,39.3805691,-4.7361792,10),
	array ("CR2-03","Estenilla Anchuras",3,0,39.4506853,-4.8202917,10),
	array ("CR2-04","Fresnedoso",3,0,39.4545564,-4.9776821,10),
	array ("CR2-05","Guadarranque en Alía",3,0,39.3766416,-5.029473,10),
	array ("CR2-52","Guadalupejo en Alía",3,0,39.339778,-5.164508,10),
	array ("CR2-06","A.ab. Orellana",3,1,38.9844928,-5.5402142,10),
	array ("CR2-07","Guadamatilla",3,0,38.5437701,-5.0113948,10),
	array ("CR2-08","R. Zújar, conf. Gua",3,0,38.6753236,-5.1628888,10),
	array ("CR2-09","Guadalmez",3,0,38.3825539,-4.792316,10),
	array ("CR2-10","Esteras Serena",3,0,38.8781532,-4.9305565,10),
	array ("CR2-11","R. Guadalemar",3,0,39.0424935,-4.9782343,10),
	array ("CR2-12","Siruela",3,0,39.0130093,-4.9457564,10),
	array ("CR2-13","A.ab. Zújar",3,1,38.9098771,-5.4921405,10),
	array ("CR2-14","Guadalefra",3,0,38.87726,-5.5666569,10),
	array ("CR2-51","Guadiana en Vva.",3,1,39.024283,-5.819194,10),
	array ("CR2-15","Cañamero",5,0,39.3796719,-5.37784,10),
	array ("CR2-16","Pizarroso",5,0,39.174622,-5.6562269,10),
	array ("CR2-17","A.ab. Azud Lavadero",5,0,39.2201906,-5.5428091,10),
	array ("CR2-18","R. Alcollarín",5,0,39.2419356,-5.7409864,10),
	array ("CR2-19","A.ab. confl. Ruecas",5,0,39.0322327,-5.9112377,10),
	array ("CR2-20","Ortigas",5,0,38.9501139,-5.9300814,10),
	array ("CR2-53","Guadiana en Medellín ",5,1,38.971388888,-5.95666667,10),
	array ("CR2-21","R. Guadámez",5,0,38.8920939,-5.9873836,10),
	array ("CR2-22","Búrdalo Miajadas",5,0,39.1602213,-5.9963869,10),
	array ("CR2-23","Búrdalo",5,0,39.0141072,-6.0496853,10),
	array ("CR2-24","Fresneda C.Verdes",5,0,38.9983436,-6.1368731,10),
	array ("CR2-25","Valverde",5,1,38.8897488,-6.1889476,10),
	array ("CR2-26","A.ab. Los Molinos",67,0,38.5382556,-6.118238,10),
	array ("CR2-27","San Juan",67,0,38.726116,-6.139804,10),
	array ("CR2-28","Palomillas",67,0,38.6501769,-6.0684043,10),
	array ("CR2-29","Valdemedez",67,0,38.7007644,-6.2958334,10),
	array ("CR2-30","Bonhabal ent. E. Al",67,0,38.7054501,-6.3294577,10),
	array ("CR2-31","A. Ab. E. Alange",67,0,38.7908608,-6.2684787,10),
	array ("CR2-32","A.Ab. Hornotejero",67,0,39.1423837,-6.4323375,10),
	array ("CR2-33","Aljucén",67,0,39.0459332,-6.339821,10),
	array ("CR2-34","Montijo",67,1,38.9040085,-6.4421437,200),
	array ("CR2-35","A.ab. Canchales",67,0,38.963576,-6.5215125,16),
	array ("CR2-36","Guadajira Villalba",67,0,38.59723,-6.4952306,10),
	array ("CR2-37","Guadajira",67,0,38.8519328,-6.689684,10),
	array ("CR2-38","Entrín en Lobón",67,0,38.8591838,-6.7217171,10),
	array ("CR2-54","Guadiana en Talavera",67,1,38.90166666,-6.76277777,10),
	array ("CR2-39","Alcazaba confl. Lor",67,0,38.9484085,-6.7383926,10),
	array ("CR2-40","Lorianilla aaC. Alc",67,0,38.961465,-6.7575616,10),
	array ("CR2-41","Guerrero",67,0,38.9407111,-6.8163687,10),
	array ("CR2-42","R. Albuera",67,0,38.8800499,-6.7761956,10),
	array ("CR2-43","Zapatón en Villar del Rey",67,0,39.1450286,-6.8708498,80),
	array ("CR2-44","Gévora Riscos",67,0,39.1315751,-7.0369824,10),
	array ("CR2-45","Gévora en Botoa",67,0,38.9639032,-6.9286109,10),
	array ("CR2-46","Rivilla Badajoz",67,0,38.86813,-6.957413,10),
	array ("CR2-47","Calamón a.ab. Embal",67,0,38.8270838,-6.9596417,10),
	array ("CR2-48","Tripero",67,0,38.8534622,-6.4611074,10),
	array ("CR2-49","Guadiana entr. Port",67,1,38.884258,-6.973814,10),
	array ("CR2-50","Ardila Olivar",67,0,38.2305472,-6.8854767,10),
	array ("E2-25","Azud Badajoz",67,1,38.8619992,-7.0174294,10),
	array ("CR2-55","Guadiana en Charco de los P.",67,1,38.8448565,-7.045233,10),
	//array ("CR3-01","Murtigas Encinasola",8,0,38.1334832,-6.9273598,10),
   );
*/
//Base de datos de presas (""Cód."",""Nombre"",Zona,Color,latitud,longitud,umbral para gráfico, N.M.N.[msnm], máximo nivel embalse, Vol N.M.N. [hm³])

$staticEMB = array(
    "E1-01" => array ("E1-01","Peñarroya",12,1,39.0630493,-3.007425,735,735,735,50.32),
    "E1-02" => array ("E1-02","Vallehermoso",12,0,38.8686636,-3.1675612,749.5,746,749.5,6.92),
	"E1-03" => array ("E1-03","Puente Navarro",12,1,39.1119517,-3.7602812,606,603.25,735,2.2),
	"E1-05" => array ("E1-05","Gasset",12,0,39.128642,-3.9369692,625.5,624.16,625.5,38.87),
	"E1-06" => array ("E1-06","El Vicario",12,1,39.0583682,-3.9965198,599,594.65,599,32.86),
	"E1-07" => array ("E1-07","La Cabezuela",12,0,38.6943244,-3.280959,763,763,763,42.83),
	"E1-08" => array ("E1-08","Vega Jabalón",12,1,38.7604319,-3.7838715,639,636.1,639,33.54),
	"E1-09" => array ("E1-09","Torre Abraham",12,0,39.3684227,-4.2505144,673.5,673.5,763,183.36),
	"E1-10" => array ("E1-10","Campos del Paraíso",12,1,39.973330,-2.734447,863.4,862.5,639,4.95),
	"E2-01" => array ("E2-01","Cijara",3,1,39.3738185,-5.0177366,427.49,418.6,427.49,1505.19),
	"E2-03" => array ("E2-03","García Sola",3,0,39.1444102,-5.1842479,362.6,353.4,362.6,554.17),
	"E2-04" => array ("E2-04","Orellana",3,1,38.9868213,-5.5392008,318,312,318,807.91),
	"E2-06" => array ("E2-06","La Serena",3,0,38.9165797,-5.4080722,352.09,346,352.09,3219.18),
	"E2-07" => array ("E2-07","Zújar",3,1,38.91662,-5.478007,318,313.5,318,301.9),
	"E2-08" => array ("E2-08","Cancho de Fresno",5,0,39.3935287,-5.3936138,626,626,626,15.21),
	"E2-09" => array ("E2-09","Ruecas",5,1,39.2635564,-5.4930247,389.05,389.05,389.05,41.94),
	"E2-10" => array ("E2-10","Azud de Lavadero",5,0,39.2331079,-5.5225979,338.5,338.53,338.5,0.43),
	"E2-11" => array ("E2-11","Sierra Brava",5,1,39.195107,-5.6396553,331,331,331,232.4),
	"E2-12" => array ("E2-12","Gargáligas",5,0,39.185893,-5.3587326,355.65,355.65,355.65,21.32),
	"E2-13" => array ("E2-13","Cubilar",5,1,39.2385532,-5.4766959,348,348,348,5.98),
	"E2-33" => array ("E2-33","Alcollarín",5,0,39.24985,-5.7453557,328,328,542.75,51.64),
	"E2-34" => array ("E2-34","Búrdalo",5,1,39.186558,-5.9782943,315.5,315.5,350,79.3),
	// "E2-14" => //array ("E2-14","Llerena",67,0,38.3134786,-5.9149166,,542.75,542.75,9.02),
	"E2-15" => array ("E2-15","Los Molinos",67,1,38.5314381,-6.1201799,350,350,350,33.65),
	"E2-16" => array ("E2-16","Alange",67,0,38.7902632,-6.2727677,275.56,270.5,275.56,851.7),
	"E2-17" => array ("E2-17","Cornalvo",67,1,38.9897294,-6.1920371,308,308,308,3.13),
	"E2-18" => array ("E2-18","Proserpina",67,0,38.9741932,-6.3648794,244.58,244.58,244.58,5.04),
	"E2-19" => array ("E2-19","Montijo",67,1,38.9232757,-6.4299413,202.15,202.15,202.15,11.17),
	"E2-20" => array ("E2-20","Hornotejero",67,0,39.1515426,-6.4330143,324,324,324,24.42),
	"E2-21" => array ("E2-21","Boquerón",67,1,39.1511891,-6.4183973,330,330,330,5.51),
	"E2-22" => array ("E2-22","Canchales",67,0,38.9652651,-6.5198656,222.1,222.1,222.1,25.9),
	"E2-24" => array ("E2-24","Villar del Rey",67,1,39.1525316,-6.8678606,242.9,242.9,242.9,130),
	"E2-28" => array ("E2-28","Tentudía",67,0,38.0989128,-6.3170981,667.95,667.95,667.95,5),
	"E2-32" => array ("E2-32","Villalba de los Barros",67,1,38.58191,-6.5049,326,326,326,106.43)
   );

//Videovigilancia
/*
$descripcion = array
   (
	   //Zona12
		array ("PYA","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("PUV","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("PUN","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("GAS","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("VIC","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("CAB","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("JAB","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("TAB","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array ("CAM","Cámara 1","Cámara 2","Cámara 3","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		//Zona3
		array("CIJ","Coronación","Aparcamiento MI","Acceso a torre de vigilancia","Grupo electrógeno MD","Cámara 5"),
		array("GAR","Coronación","Estribo MD","Toma Canal de las Dehesas","Caseta de regulación MD","Caseta de regulación MI","Centro de transformación MI","Caudalímetro Canal de las Dehesas","Acceso a oficina","Acceso a caseta SAIH","Acceso a recinto oficina","Acceso a recinto taller","Órganos de desagüe","Acceso a recinto oficina puerta trasera","Estribo MD","Entrada galería inferior MD","Desagüe de fondo MD"),
		array("ORE","Órganos de desagüe","Coronación","Oficina de explotación","Centro de transformación","Parque MD","Canal de Orellana","Entrada a recinto","Almacén","Búnkers aliviadero MI","Búnkers aliviadero MD","Acceso a galería MI","Acceso a galería MD","Cámara DF izquierda","Cámara DF derecha","Acceso a galería superior MI","Acceso a galería superior MD"),
		array("SER","Acceso a coronación MD","Centro de transformación MD","Aparcamiento MD","Aceeso a coronación MI","Centro de transformación MI","Aparcamiento MI","Acceso a recinto oficina MI","Acceso a recinto meteorológico","Acceso a recinto galería MD","Caseta depuradora","Acceso a galería superior 0 MD","Acceso a galería superior 0 MI","Acceso a galería inferior 1 MD","Acceso a galería superior 1 MI"),
		array("ZUJ","Coronación","Aparcamiento MI","Acceso a torre de vigilancia","Grupo electrógeno MD","Cámara 5"),

		//Zona5
		array("CFR","Acceso a recinto oficina","Acesso a coronación Md","Coronación","Acceso a coronación","Acceso a galería MD","Acceso a galería MI","Centro de transformación","Oficina"),
		array("RUE","Acceso a recinto oficina","Recinto oficina","Acceso aparcamiento MD","Acceso a coronación MI","Acceso a galería MI","Coronación MD","Coronación MI","Acceso a galería MD"),
		array("AZR","Acceso a galería MD","Compuertas","Acceso a coronación MI","Coronación","Acceso a recinto oficina-almacén"),
		array("SIB","Acceso almacén","Recinto oficina","Entrada a recinto oficina","Acceso a recinto urbanización","Coronación","Acceso desagüe de fondo","Acceso galería MD","Grupo electrógeno","Mini central","Acceso galería MI","Recinto mini central","Acceso a recinto mini central"),
		array("GRG","Cuerpo presa","Acceso a recinto oficinas","Centro de transformación","Entrada presa","Caseta desagües de fondo","Caseta compuertas","Entrada oficinas","GC1 Canal de Pela","GC5 Canal de las Dehesas"),
		array("CUB","Acceso a coronación","Recinto oficina MD","Recinto oficina MI","Caseta desagüe de fondo","Caseta regulación canal","Cámara 6"),
		array("ALC","Recinto oficina","Coronación MI","Oficina","Cámara 4","Cámara 5","Cámara 6","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12"),
		array("BUR","Órganos de desagüe","Acceso a coronación MI","Acceso a coronación MD","Acceso a recinto oficina","Oficina","Almacén","Acceso a centro de transformación","Centro de transformación","Acceso galería superior MI","Acceso galería superior MD","Acceso galería inferior MI","Acceso galería inferior MD","Conexión canal Orellana","Sonda autoposic."),
		
		
		//Zona67
		array("ALA","Coronación","Aparcamiento MI","Acceso a torre de vigilancia","Grupo electrógeno MD","Órganos de desagüe","Entrada a recinto oficina","Entrada taller","Oficina de explotación","Almacén","Acceso a aparcamiento MD","Aparcamiento MD","Galería superior MD","Galería superior MI","Galería intermedia MD","Galería intermedia MI","Galería inferior MD","Galería inferior MI","Cámara DF derecha 1-2","Cámara DF izquierda 3-4"),
		array("MOL","Coronación","Acceso a caseta SAIH","Acceso a oficina","Acceso a galería DF","Acceso a galería toma abastecimiento","Cámara DF","Embocadura aliviadero","Rápida aliviadero","Cámara 9","Cámara 10","Cámara 11","Cámara 12","Cámara 13","Cámara 14","Cámara 15","Cámara 16"),
		array("VIB","Acceso a coronación MI","Embocadura aliviadero","Acceso a coronación MD","Rápida aliviadero","Acceso a galería","Desagües de fondo","Acceso a galería MD","Acceso a galería MI","Aparcamiento MI","Oficina-Acceso a viales","Oficina-Grupo electrógeno","Oficina-Sala de emergencia","Cámara de desagües de fondo","Pozo de bombeo","Sirena del Cerro de la Mina"),
		array("MON","Compuertas verticales","Cauce, escala de peces y mirador","Entrada oficina","Entrada poblado (túnel)","Compuertas canal de Montijo","Coronación y embocadura canal Montijo","Compuertas canal de Lobón","Aparcamiento canal de Lobón","Acceso compuertas canal de Lobón","Aliviadero sobre túnel de Lobón","Acceso a recinto taller","Entrada taller","Entrada almacén obra civil","Acceso y caseta SAICA","Embarcadero","Entrada poblado","Coronación hacia margen izquierda","Compuertas aguas arriba","Coronación","Compuerta Taintor nº1","Embocadura aliviadero","Compuerta Taintor nº2","AZM Caseta de maniobras","AZM Acceso Margen Derecha (parque)","AZM Pozo de bombeo","AZM Acceso Margen Izquierda","AZM Margen Izquierda","AZM Aguas Abajo","AZM Margen Derecha","Acceso a aparcamiento MI","Acceso a aparcamiento MD"),
		array("PRO","Coronación y acceso MI","Mirador bocín MD","Mirador bocín MI","Acceso a coronación por vía pecuaria","Entrada oficina","Acceso a oficina","Aparcamiento MI","Cámara DF","Aliviadero Sangradera","Acceso a bocín MD","Acceso a cámara DF","Acceso a bocín MI","Cámara 13","Cámara 14","Cámara 15","Cámara 16"),
		array("COR","Coronación","Acceso a bocín","Desagüe de fondo","Acceso a oficina-almacén","Estribo margen izquierda","Estribo margen derecha","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12","Cámara 13","Cámara 14","Cámara 15","Cámara 16"),
		array("VIL","Coronación","Órganos de desagüe","Central hidroeléctrica","Oficina","Vial de acceso","Galería superior MI","Galería superior MD","Galería inferior MI","Galería inferior MD","Cámara DF","Pozo de bombeo","Aliviadero canal","Coronación aguas arriba","Cámara 14","Cámara 15","Cámara 16"),
		array("TEN","Coronación","Embalse","Recinto oficina","Transformador MD","Acceso galería superior MD","Acceso galería inferior MD","Acceso galería superior MI","Cámara de válvulas","Órganos de desagüe","Cámara 10","Cámara 11","Cámara 12","Cámara 13","Cámara 14","Cámara 15","Cámara 16"),
		array("CAN","Órganos de desagüe","Coronación","Oficina-Acceso principal","Oficina-Acceso a edificio","Acceso a Desagües de Fondo","Acceso a estación de aforo","Grupo electrógeno","Acceso a galería nivel inf.","Acceso a galería nivel sup.","Acceso a almacén","Instalaciones SAIH"),
		array("HOR","Acceso a coronación MD","Acceso a recinto oficina","Oficina","Acceso a galería DF","Salida DF","Caseta de regulación","Cámara de válvulas","Pozo de bombeo","Acceso galería MD","Acceso galería MI","Grupo electrógeno","Acceso a coronación MI","Acceso a caseta SAIH","Embocadura aliviadero","Cámara 15","Cámara 16"),
		array("BOQ","Coronación","Órganos de desagüe","Grupo electrógeno","Acceso galería MD","Acceso galería MI","Cámara de válvulas","Cámara 7","Cámara 8","Cámara 9","Cámara 10","Cámara 11","Cámara 12","Cámara 13","Cámara 14","Cámara 15","Cámara 16"),
		array("ZRL","Aparcamiento oficina","Aparcamiento oficina","Vial acceso a oficina","Acceso a recinto","Vial acceso a almacén exterior","Almacén exterior de materiales","Elev-Alvarado sala de bombeo MI","Elev-Alvarado sala de bombeo MD","Centro de transformación de Alvarado","Centro de transformación F2.asp","Elev-F2.asp sala de bombeo","Elev-Albuera sala de bombeo","Elev-Alvarado Ext.C.transformación","Cámara 14","Cámara 15","Cámara 16"),
   );

*/
//Base de datos de las cámaras que tienen timelapse (id, número de la cámara que tiene timelapse activado)
/*
$camtimelapse = array
   (
	//Zona12
	array ("PYA",1),
	array ("PUV",1),
	array ("PUN",1),
	array ("GAS",1),
	array ("VIC",1),
	array ("CAB",1),
	array ("JAB",1),
	array ("TAB",1),
	array ("CAM",1),
   //Zona3
   array("CIJ",1,2),
   array("GAR",1,3,7),
   array("ORE",1,2,5),
   array("SER",1,2),
   array("ZUJ",1,2),
   //Zona5
   array("CFR",1),
   array("RUE",1),
   array("AZR",1),
   array("SIB",10),
   array("GRG",1),
   array("CUB",1),
   array("ALC",1),
   array("BUR",1,14),
   //Zona67
   array("ALA",1,2,4,5,10,11),
   array("MOL",1,7,8),
   array("VIB",1,2,3,4,6,9),
   array("MON",1,5,6,7,15,17,18,19,20,21,22,27,28,29),
   array("PRO",1),
   array("COR",1,5,6),
   array("VIL",1,2,3,12,13),
   array("TEN",1,2,9),
   array("CAN",1,2),
   array("HOR",1,5,12),
   array("BOQ",1,2),
   array("ZRL"),   
   );
   */
?>