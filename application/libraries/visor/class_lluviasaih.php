<?php
date_default_timezone_set("UTC");

class LLUVIASAIH
{

	static function UltimosDatosLluviaSAIH($DB){

		$campos = array(
			array("cod_campo"=> "tipo", "id" => "tipo"),
			array("cod_campo"=> "zona", "id" => "zona"),
			array("cod_campo"=> "cod_estacion", "id" => "cod_estacion"),
			array("cod_campo"=> "nombre", "id" => "nombre"),
			array("cod_campo"=> "PR", "id" => "pr"),
			array("cod_campo"=> "P1H", "id" => "p1h"),
			array("cod_campo"=> "P6H", "id" => "p6h"),
			array("cod_campo"=> "P12H", "id" => "p12h"),
			array("cod_campo"=> "P24H", "id" => "p24h"),
			array("cod_campo"=> "PRA", "id" => "pra"),
			array("cod_campo"=> "TA", "id" => "ta"),
			array("cod_campo"=> "timestamp", "id" => "timestamp")
		);

		$query = "WITH

		ultimos_datos AS (
			SELECT 
			ud.valor,
			ud.cod_variable,
			ud.timestamp
			FROM
				tbl_ultimo_dato_rtap ud
			WHERE (
				ud.cod_variable like '%/PR' OR ud.cod_variable like '%/TA' OR ud.cod_variable like '%/PRA'
			)
		),
			
		datos AS (
			SELECT tr.cod_variable,
				tr.timestamp,
				tr.valor,
				ud.timestamp AS timestamp_mayor
			FROM 
				tbl_dato_tr tr,
				ultimos_datos ud
			WHERE
			ud.cod_variable LIKE '%/PR'
			AND ((tr.timestamp > (ud.timestamp - '24:00:00'::interval)) AND ((tr.cod_variable)::text = (ud.cod_variable)::text))
		),

		estaciones AS (
			select zona, cod_estacion, nombre, orden, tipo from tbl_estacion where disponible = true and pluviometro = true
		),

		ta AS (
			SELECT 
				ultimos_datos.cod_variable,
			    ultimos_datos.valor valor,
			  	ultimos_datos.timestamp AS timestamp_mayor
			FROM ultimos_datos
			WHERE cod_variable like '%/TA'
		), 

		p10m AS (
			SELECT 
				ultimos_datos.cod_variable,
			   	ultimos_datos.valor AS valor,
				ultimos_datos.timestamp AS timestamp_mayor
			FROM ultimos_datos
			WHERE cod_variable like '%/PR'
		), 
		p1h AS (
			SELECT datos.cod_variable,
			   round(sum(datos.valor), 1) AS valor,
			   min(datos.timestamp) AS timestamp_menor
			  FROM datos
			 WHERE (datos.timestamp > (datos.timestamp_mayor - '01:00:00'::interval))
			AND cod_variable like '%/PR'
			GROUP BY datos.cod_variable
		), 
		p6h AS (
			SELECT datos.cod_variable,
			   round(sum(datos.valor), 1) AS valor,
			   min(datos.timestamp) AS timestamp_menor
			  FROM datos
			 WHERE (datos.timestamp > (datos.timestamp_mayor - '06:00:00'::interval))
			 AND cod_variable like '%/PR'
			 GROUP BY datos.cod_variable
		), 
		p12h AS (
			SELECT datos.cod_variable,
			   round(sum(datos.valor), 1) AS valor,
			   min(datos.timestamp) AS timestamp_menor
			  FROM datos
			 WHERE (datos.timestamp > (datos.timestamp_mayor - '12:00:00'::interval))
			 AND cod_variable like '%/PR'
			 GROUP BY datos.cod_variable
		), 
		p24h AS (
			SELECT datos.cod_variable,
			   round(sum(datos.valor), 1) AS valor,
			   min(datos.timestamp) AS timestamp_menor
			  FROM datos
			  WHERE cod_variable like '%/PR'
			 GROUP BY datos.cod_variable
		), 
		pra AS (
			SELECT 
				ultimos_datos.cod_variable,
			    ultimos_datos.valor valor,
			  	ultimos_datos.timestamp AS timestamp_mayor
			FROM ultimos_datos
			WHERE cod_variable like '%/PRA'
		)
			
		select
			estaciones.tipo,
			estaciones.zona,
			estaciones.cod_estacion,
			estaciones.nombre,
			p10m.valor as pr,
			p1h.valor as p1h,
			p6h.valor as p6h,
			p12h.valor as p12h,
			p24h.valor as p24h,
			pra.valor as pra,
			ta.valor as ta,
			p10m.timestamp_mayor as timestamp
		from 
			estaciones
		LEFT JOIN ta ON estaciones.cod_estacion = split_part(ta.cod_variable,'/',1)
		LEFT JOIN p10m ON estaciones.cod_estacion = split_part(p10m.cod_variable,'/',1)
		LEFT JOIN pra ON estaciones.cod_estacion = split_part(pra.cod_variable,'/',1) 
		LEFT JOIN p1h ON estaciones.cod_estacion = split_part(p1h.cod_variable,'/',1) 
		LEFT JOIN p6h ON estaciones.cod_estacion = split_part(p6h.cod_variable,'/',1) 
		LEFT JOIN p12h ON estaciones.cod_estacion = split_part(p12h.cod_variable,'/',1)
		LEFT JOIN p24h ON estaciones.cod_estacion = split_part(p24h.cod_variable,'/',1)
		ORDER BY estaciones.zona, estaciones.tipo, estaciones.orden, estaciones.cod_estacion";

		$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);

		return array("encabezado" => $campos, "valores" => $datos);
	}

	static function UltimosDatosEmbalseSAIH($idEstaciones, $idEmbalse, $DB){

		$campos = array(
			array("cod_campo"=> "zona", "id" => "zona"),
			array("cod_campo"=> "cod_estacion", "id" => "cod_estacion"),
			array("cod_campo"=> "nombre", "id" => "nombre"),
			array("cod_campo"=> "creparto_thiessen", "id" => "creparto_thiessen"),
			array("cod_campo"=> "PI", "id" => "pi"),
			array("cod_campo"=> "PR", "id" => "pr"),
			array("cod_campo"=> "P1H", "id" => "p1h"),
			array("cod_campo"=> "P6H", "id" => "p6h"),
			array("cod_campo"=> "P12H", "id" => "p12h"),
			array("cod_campo"=> "P24H", "id" => "p24h"),
			array("cod_campo"=> "PRA", "id" => "pra"),
			array("cod_campo"=> "TA", "id" => "ta"),
			array("cod_campo"=> "timestamp", "id" => "timestamp")
		);

		$query = "WITH 

		ultimos_datos AS (
			SELECT 
			ud.valor,
			ud.cod_variable,
			ud.timestamp
			FROM
				tbl_ultimo_dato_rtap ud
			WHERE (
				ud.cod_variable like '%/PR' OR ud.cod_variable like '%/TA' OR ud.cod_variable like '%/PRA'
			)
		), 
			
		datos AS (
			SELECT tr.cod_variable,
				tr.timestamp,
				tr.valor,
				ud.timestamp AS timestamp_mayor
			FROM tbl_dato_tr tr,
				ultimos_datos ud
			WHERE 
				ud.cod_variable LIKE '%/PR'
				AND ((tr.timestamp > (ud.timestamp - '24:00:00'::interval)) AND ((tr.cod_variable)::text = (ud.cod_variable)::text))
		), 

		estaciones AS (
			select 
				e.zona, 
				e.cod_estacion, 
				e.nombre, 
				c.creparto_thiessen,
				e.disponible
			from 
				tbl_estacion e, 
				tbl_estacion_creparto_embalse c 
			where c.cod_embalse = '".$idEmbalse."' 
				AND e.cod_estacion = c.cod_estacion 
				AND e.pluviometro = true
		),

		ta AS (
			SELECT 
				ultimos_datos.cod_variable,
				ultimos_datos.valor valor,
				ultimos_datos.timestamp AS timestamp_mayor
			FROM ultimos_datos
			WHERE cod_variable like '%/TA'
		), 
		
		p10m AS (
			SELECT 
				ultimos_datos.cod_variable,
				ultimos_datos.valor AS valor,
				ultimos_datos.timestamp AS timestamp_mayor
			FROM ultimos_datos
			WHERE cod_variable like '%/PR'
		), 
	
		p1h AS (
			SELECT datos.cod_variable,
				sum(datos.valor) AS valor,
				min(datos.timestamp) AS timestamp_menor
			FROM datos
			WHERE (datos.timestamp > (datos.timestamp_mayor - '01:00:00'::interval))
			AND cod_variable like '%/PR'
			GROUP BY datos.cod_variable, datos.timestamp_mayor
		), 
	
		p6h AS (
			SELECT datos.cod_variable,
				sum(datos.valor) AS valor,
				min(datos.timestamp) AS timestamp_menor
			FROM datos
			WHERE (datos.timestamp > (datos.timestamp_mayor - '06:00:00'::interval))
			AND cod_variable like '%/PR'
			GROUP BY datos.cod_variable, datos.timestamp_mayor
		), 
	
		p12h AS (
			SELECT cod_variable,
				sum(datos.valor) AS valor,
				min(datos.timestamp) AS timestamp_menor
			FROM datos
			WHERE (datos.timestamp > (datos.timestamp_mayor - '12:00:00'::interval))
			AND cod_variable like '%/PR'
			GROUP BY datos.cod_variable, datos.timestamp_mayor
		), 
	
		p24h AS (
			SELECT cod_variable,
				sum(datos.valor) AS valor,
				min(datos.timestamp) AS timestamp_menor
			FROM datos
			WHERE cod_variable like '%/PR'
			GROUP BY datos.cod_variable, datos.timestamp_mayor
		), 
	
		pra AS (
			SELECT 
				ultimos_datos.cod_variable,
			    ultimos_datos.valor valor,
			  	ultimos_datos.timestamp AS timestamp_mayor
			FROM ultimos_datos
			WHERE cod_variable like '%/PRA'
		)
		
		SELECT 
			e.zona,
			e.cod_estacion,
			e.nombre,
			e.disponible,
			round( e.creparto_thiessen, 7 ) as creparto_thiessen,
			(p10m.valor * (6)::numeric) AS pi,
			p10m.valor as pr,
			p1h.valor as p1h,
			p6h.valor as p6h,
			p12h.valor as p12h,
			p24h.valor as p24h,
			pra.valor as pra,
			ta.valor as ta,
			p10m.timestamp_mayor as timestamp
		FROM 
			estaciones e
			LEFT JOIN ta ON e.cod_estacion = split_part(ta.cod_variable,'/',1)
			LEFT JOIN p10m ON e.cod_estacion = split_part(p10m.cod_variable,'/',1)
			LEFT JOIN p1h ON e.cod_estacion = split_part(p1h.cod_variable,'/',1)
			LEFT JOIN p6h ON e.cod_estacion = split_part(p6h.cod_variable,'/',1)
			LEFT JOIN p12h ON e.cod_estacion = split_part(p12h.cod_variable,'/',1)
			LEFT JOIN p24h ON e.cod_estacion = split_part(p24h.cod_variable,'/',1)
			LEFT JOIN pra ON e.cod_estacion = split_part(pra.cod_variable,'/',1)
		ORDER BY e.cod_estacion";

		$todos_datos = ConexionBD::EjecutarConsultaConDB($DB, $query);
		$retorno_datos = [];

		// filtrar estaciones a mostrar usando la lista de estaciones recibida, se filtra también estaciones inactivas para el recalculado thiessen
		/*foreach($todos_datos as $lineaDato) {
			$lineaDato["creparto_thiessen"] = number_format($lineaDato["creparto_thiessen"],6,",",".");
			if(in_array($lineaDato["cod_estacion"], $idEstaciones)) $retorno_datos[] = $lineaDato;
		}*/

		// sumar los coef.thiessen de las estaciones disponibles y no disponibles por si es necesario recalcular

		$sumaCoefThiessenDisponibles = 0;
		$sumaCoefThiessenNoDisponibles = 0;

		foreach($todos_datos as &$lineaDato){
			//$lineaDato["creparto_thiessen"] = number_format($lineaDato["creparto_thiessen"],6,",",".");


			// si la estación no está disponible solo se suma su coef.thiessen
			if($lineaDato["disponible"] == true){

				$sumaCoefThiessenDisponibles += $lineaDato["creparto_thiessen"];
				
			} else {

				$sumaCoefThiessenNoDisponibles += $lineaDato["creparto_thiessen"];

			}

		}

		// recalcular coef.thiessen si existe coeficiente no disponible
		if($sumaCoefThiessenNoDisponibles!=0){
			foreach($todos_datos as &$lineaDato){
				$lineaDato["creparto_thiessen"] = ($sumaCoefThiessenNoDisponibles / $sumaCoefThiessenDisponibles + 1) * $lineaDato["creparto_thiessen"];
			}
		}


		// crear promedios usando todos los datos
		$promedios = ["cod_estacion"=> "TOTAL", "creparto_thiessen"=> 0, "nombre"=> "PROMEDIO", "pi" => 0, "pr" => 0, "p1h"=>0, "p6h"=>0,"p12h"=>0,"p24h"=>0,"pr"=>0,"pra"=>0,"timestamp"=>""];

		foreach($todos_datos as &$lineaDato){
			if($lineaDato["disponible"] == true){
				
				$promedios["creparto_thiessen"] += $lineaDato["creparto_thiessen"];
				$promedios["pi"] += ($lineaDato["pi"] * $lineaDato["creparto_thiessen"]);
				$promedios["pr"] += ($lineaDato["pr"] * $lineaDato["creparto_thiessen"]);
				$promedios["p1h"] += ($lineaDato["p1h"] * $lineaDato["creparto_thiessen"]);
				$promedios["p6h"] += ($lineaDato["p6h"] * $lineaDato["creparto_thiessen"]);
				$promedios["p12h"] += ($lineaDato["p12h"] * $lineaDato["creparto_thiessen"]);
				$promedios["p24h"] += ($lineaDato["p24h"] * $lineaDato["creparto_thiessen"]);
				$promedios["pra"] += ($lineaDato["pra"] * $lineaDato["creparto_thiessen"]);

				// formatear dato
				$lineaDato["creparto_thiessen"] = number_format($lineaDato["creparto_thiessen"],6,",",".");

				// mostrar solo datos de estación vinculada al usuario
				if(in_array($lineaDato["cod_estacion"], $idEstaciones)) $retorno_datos[] = $lineaDato;

			}
		}

		$promedios["creparto_thiessen"] = number_format(round($promedios["creparto_thiessen"],0),6,",",".");

		$retorno_datos[] = $promedios;

		return array("encabezado" => $campos, "valores" => $retorno_datos);
	}

	static function UltimosDatosEmbalsesLluviaSAIH($DB){

		$campos = array(
			array("cod_campo"=> "zona", "id" => "zona"),
			array("cod_campo"=> "cod_estacion", "id" => "cod_estacion"),
			array("cod_campo"=> "nombre", "id" => "nombre"),
			array("cod_campo"=> "PI", "id" => "pi"),
			array("cod_campo"=> "PR", "id" => "pr"),
			array("cod_campo"=> "P1H", "id" => "p1h"),
			array("cod_campo"=> "P6H", "id" => "p6h"),
			array("cod_campo"=> "P12H", "id" => "p12h"),
			array("cod_campo"=> "P24H", "id" => "p24h"),
			array("cod_campo"=> "PRA", "id" => "pra"),
			array("cod_campo"=> "timestamp", "id" => "timestamp")
		);

		$query = "WITH 

			ultimos_datos AS (
				SELECT 
					ud.valor,
					ud.cod_variable,
					ud.timestamp
				FROM
					tbl_ultimo_dato_rtap ud
				WHERE (
					ud.cod_variable like '%/PR' OR ud.cod_variable like '%/TA' OR ud.cod_variable like '%/PRA'
				)
			), 
				
			datos AS (
				SELECT 
					tr.cod_variable,
					tr.timestamp,
					tr.valor,
					ud.timestamp AS timestamp_mayor
				FROM tbl_dato_tr tr,
					ultimos_datos ud
				WHERE ((tr.timestamp > (ud.timestamp - '24:00:00'::interval)) AND ((tr.cod_variable)::text = (ud.cod_variable)::text))
			), 
			
			p10m AS (
				SELECT 
					ultimos_datos.cod_variable,
					ultimos_datos.valor AS valor,
					ultimos_datos.timestamp AS timestamp_mayor
				FROM ultimos_datos
				WHERE cod_variable like '%/PR'
			), 
		
			p1h AS (
				SELECT 
				 	datos.cod_variable,
					round(sum(datos.valor), 2) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE (datos.timestamp > (datos.timestamp_mayor - '01:00:00'::interval))
				AND cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			p6h AS (
				SELECT
					datos.cod_variable,
					round(sum(datos.valor), 2) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE (datos.timestamp > (datos.timestamp_mayor - '06:00:00'::interval))
				AND cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			p12h AS (
				SELECT 
					datos.cod_variable,
					round(sum(datos.valor), 2) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE (datos.timestamp > (datos.timestamp_mayor - '12:00:00'::interval))
				AND cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			p24h AS (
				SELECT 
					datos.cod_variable,
					round(sum(datos.valor), 2) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			pra AS (
				SELECT 
					ultimos_datos.cod_variable,
					ultimos_datos.valor valor,
					ultimos_datos.timestamp AS timestamp_mayor
				FROM ultimos_datos
				WHERE cod_variable like '%/PRA'
			)
			
			SELECT 
				e.zona,
				e.cod_estacion,
				e.nombre,
				(p10m.valor * (6)::numeric) AS PI,
				p10m.valor AS PR,
				p1h.valor AS P1H,
				p6h.valor AS P6H,
				p12h.valor AS P12H,
				p24h.valor AS P24H,
				pra.valor AS PRA,
				p10m.timestamp_mayor as timestamp
			FROM 
				tbl_estacion e
				LEFT JOIN p10m ON e.cod_estacion = split_part(p10m.cod_variable,'/',1)
				LEFT JOIN p1h ON e.cod_estacion = split_part(p1h.cod_variable,'/',1)
				LEFT JOIN p6h ON e.cod_estacion = split_part(p6h.cod_variable,'/',1)
				LEFT JOIN p12h ON e.cod_estacion = split_part(p12h.cod_variable,'/',1)
				LEFT JOIN p24h ON e.cod_estacion = split_part(p24h.cod_variable,'/',1)
				LEFT JOIN pra ON e.cod_estacion = split_part(pra.cod_variable,'/',1)
			WHERE 
				e.tipo = 'E'
				AND e.disponible = true
				ORDER BY e.zona, e.tipo, e.orden, e.cod_estacion";

		$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);

		return array("encabezado" => $campos, "valores" => $datos);
	}

	static function UltimosDatosEmbalsesLluviaPonderadaSAIH($DB){

		$campos = array(
			array("cod_campo"=> "zona", "id" => "zona"),
			array("cod_campo"=> "cod_estacion", "id" => "cod_estacion"),
			array("cod_campo"=> "nombre", "id" => "nombre"),
			array("cod_campo"=> "PI", "id" => "pi"),
			array("cod_campo"=> "PR", "id" => "pr"),
			array("cod_campo"=> "P1H", "id" => "p1h"),
			array("cod_campo"=> "P6H", "id" => "p6h"),
			array("cod_campo"=> "P12H", "id" => "p12h"),
			array("cod_campo"=> "P24H", "id" => "p24h"),
			array("cod_campo"=> "PRA", "id" => "pra"),
			array("cod_campo"=> "TA", "id" => "ta"),
			array("cod_campo"=> "timestamp", "id" => "timestamp")
		);

		$query = "WITH 

			ultimos_datos AS (
				SELECT 
					ud.valor,
					ud.cod_variable,
					ud.timestamp
				FROM
					tbl_ultimo_dato_rtap ud
				WHERE (
					ud.cod_variable like '%/PR' OR ud.cod_variable like '%/TA' OR ud.cod_variable like '%/PRA'
				)
			), 
		
			datos AS (
				SELECT tr.cod_variable,
					tr.timestamp,
					tr.valor,
					ud.timestamp AS timestamp_mayor
				FROM tbl_dato_tr tr,
					ultimos_datos ud
				WHERE 
					ud.cod_variable LIKE '%/PR'
					AND ((tr.timestamp > (ud.timestamp - '24:00:00'::interval)) AND ((tr.cod_variable)::text = (ud.cod_variable)::text))
			), 
		
			pluviometricas_thiessen AS (
		
				select 
					c.cod_embalse,
					e.cod_estacion,
					case WHEN totales_cthiessen.pluvio_nodisp is not NULL 
						THEN (totales_cthiessen.cthiessen_nodisponible / totales_cthiessen.cthiessen_disponible + 1) * c.creparto_thiessen
						ELSE c.creparto_thiessen end 
					as creparto_thiessen
				from tbl_estacion e
				JOIN tbl_estacion_creparto_embalse c on e.cod_estacion = c.cod_estacion
				LEFT JOIN (
					SELECT
						embalses.cod_estacion as cod_embalse,
						SUM(case WHEN pluviometricas.disponible IS false THEN 1 end) as pluvio_nodisp,
						SUM(case WHEN pluviometricas.disponible IS true THEN creparto_thiessen end) as cthiessen_disponible,
						SUM(case WHEN pluviometricas.disponible IS false THEN creparto_thiessen end) as cthiessen_nodisponible
					FROM 
						tbl_estacion embalses
						left join tbl_estacion_creparto_embalse c on embalses.cod_estacion = c.cod_embalse
						left join tbl_estacion pluviometricas on c.cod_estacion = pluviometricas.cod_estacion and pluviometricas.pluviometro = true
					WHERE
						embalses.tipo like 'E'
						AND embalses.disponible IS true
					GROUP BY embalses.cod_estacion
				) totales_cthiessen ON totales_cthiessen.cod_embalse = c.cod_embalse
				WHERE
				e.pluviometro = true
				and e.disponible = true

		
			),
		
			ta AS (
				SELECT 
					ultimos_datos.cod_variable,
					ultimos_datos.valor valor,
					ultimos_datos.timestamp AS timestamp_mayor
				FROM ultimos_datos
				WHERE cod_variable like '%/TA'
			), 
		
			p10m AS (
				SELECT 
					ultimos_datos.cod_variable,
					ultimos_datos.valor AS valor,
					ultimos_datos.timestamp AS timestamp_mayor
				FROM ultimos_datos
				WHERE cod_variable like '%/PR'
			), 
		
			p1h AS (
				SELECT datos.cod_variable,
					sum(datos.valor) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE (datos.timestamp > (datos.timestamp_mayor - '01:00:00'::interval))
				AND cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			p6h AS (
				SELECT datos.cod_variable,
					sum(datos.valor) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE (datos.timestamp > (datos.timestamp_mayor - '06:00:00'::interval))
				AND cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			p12h AS (
				SELECT cod_variable,
					sum(datos.valor) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE (datos.timestamp > (datos.timestamp_mayor - '12:00:00'::interval))
				AND cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			p24h AS (
				SELECT cod_variable,
					sum(datos.valor) AS valor,
					min(datos.timestamp) AS timestamp_menor
				FROM datos
				WHERE cod_variable like '%/PR'
				GROUP BY datos.cod_variable, datos.timestamp_mayor
			), 
		
			pra AS (
				SELECT 
					ultimos_datos.cod_variable,
					ultimos_datos.valor valor,
					ultimos_datos.timestamp AS timestamp_mayor
				FROM ultimos_datos
				WHERE cod_variable like '%/PRA'
			),
		
			calculo_thiessen AS (
			
				SELECT 
					COALESCE(e.zona) AS zona,
					COALESCE(pt.cod_embalse) AS cod_estacion,
					COALESCE(e.nombre) AS nombre,
					SUM((p10m.valor * (6)::numeric) * pt.creparto_thiessen) AS pi,
					SUM(p10m.valor * pt.creparto_thiessen )AS pr,
					SUM(p1h.valor * pt.creparto_thiessen )AS p1h,
					SUM(p6h.valor * pt.creparto_thiessen )AS p6h,
					SUM(p12h.valor * pt.creparto_thiessen )AS p12h,
					SUM(p24h.valor * pt.creparto_thiessen) AS p24h,
					SUM(pra.valor * pt.creparto_thiessen) AS pra
				FROM 
					pluviometricas_thiessen pt
					LEFT JOIN tbl_estacion e on e.cod_estacion = pt.cod_embalse
					LEFT JOIN p10m ON pt.cod_estacion = split_part(p10m.cod_variable,'/',1)
					LEFT JOIN p1h ON pt.cod_estacion = split_part(p1h.cod_variable,'/',1)
					LEFT JOIN p6h ON pt.cod_estacion = split_part(p6h.cod_variable,'/',1)
					LEFT JOIN p12h ON pt.cod_estacion = split_part(p12h.cod_variable,'/',1)
					LEFT JOIN p24h ON pt.cod_estacion = split_part(p24h.cod_variable,'/',1)
					LEFT JOIN pra ON pt.cod_estacion = split_part(pra.cod_variable,'/',1)
				WHERE e.disponible = true
				group by zona, e.orden, pt.cod_embalse, e.nombre
		
			)
			
			SELECT zona, cod_estacion, nombre, pi, pr, p1h, p6h, p12h, p24h, pra, ta.valor as ta, p10m.timestamp_mayor as timestamp
			FROM calculo_thiessen ct
			LEFT JOIN ta ON ct.cod_estacion = split_part(ta.cod_variable,'/',1)
			LEFT JOIN p10m ON ct.cod_estacion = split_part(p10m.cod_variable,'/',1)";

		$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);

		return array("encabezado" => $campos, "valores" => $datos);
	}
}
