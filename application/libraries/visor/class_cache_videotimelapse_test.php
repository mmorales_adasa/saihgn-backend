<?php 

class CacheVideoTimelapseTest { 

    static function CachearVideoTimelapse(){

        $cambiosGlobales = [0,0];

        $timelapsePath = APPPATH . "libraries/visor/timelapse2/";
        $timelapseCachePath = APPPATH . "libraries/visor/timelapse_cache2/";

        // se crea el directorio de cache si este no existe
        if (!file_exists($timelapseCachePath)) {
            mkdir($timelapseCachePath, 0777, true);
        }

        $files = scandir($timelapsePath);

        // por cada fichero del directorio origen
        foreach($files as $file){


            
            // revisamos que sea un directorio
            if(is_dir($timelapsePath . $file) and substr( $file, 0, 1 ) !== "."){

                //echo $file;
                

                $embalseDirPath = $timelapsePath . $file;
                $embalseDirCachePath = $timelapseCachePath . $file;

                // se crea directorio de embalse de destino si no existe
                if (!file_exists($embalseDirCachePath)) {
                    chdir($timelapseCachePath);
                    mkdir($file, 0777, true);
                }

                // se escanea el subdirectorio de origen
                $embalseFiles = scandir($embalseDirPath);


                // por cada fichero del subdirectorio de origen
                foreach($embalseFiles as $camFile){

                    // revisamos que sea un directorio
                    //if(is_dir($embalseDirPath . "/" . $camFile) and $camFile !== "." and $camFile !== ".."){
                    if(is_dir($embalseDirPath . "/" . $camFile) and substr( $file, 0, 1 ) !== "."){

                        $videoSubDirPath = $embalseDirPath . "/" . $camFile . "/";
                        $videoSubDirCachePath = $embalseDirCachePath . "/" . $camFile . "/";

                        // se crea directorio de timelapse de camara de destino si no existe
                        if (!file_exists($videoSubDirCachePath)) {
                            chdir($embalseDirCachePath);
                            mkdir($camFile, 0777, true);
                        }

                        $cambios = CacheVideoTimelapseTest::copyFiles($videoSubDirPath, $videoSubDirCachePath, 144, 4);
                        if($cambios){
                            $cambiosGlobales[0] += $cambios[0];
                            $cambiosGlobales[1] += $cambios[1];
                        }
                    }
                }
            }
        }

        return $cambiosGlobales;

    }

    private static function copyFiles($path, $pathCache, $length, $selectFileModulus){
       
        $cambiosBorrar = 0;
        $cambiosCopiar = 0;

        chdir($path);

        // obtener ficheros por orden de modificación en directorio de origen
        $allfiles = array_reverse(glob("*.*"));

        $files = [];
        $i = 0;
        foreach($allfiles as $file) {
            if($i==$selectFileModulus) $i=0;
            if($i==0) $files[] = $file;
            $i++;
        }

        // cortar el array al número de ficheros recibido
        $files = array_slice($files, 0, $length);

        chdir($pathCache);
        
        // obtener ficheros por orden de modificación en directorio de destino
        $files2 = array_reverse(glob("*.*"));

        // borrar ficheros de destino que no existan en origen
        foreach($files2 as $file2){
            if(!in_array($file2, $files)){
                $cambiosBorrar++;
                unlink($file2);
            }
        }

        // realizar una copia de los ficheros existentes en origen que no existen en destino
        foreach($files as $file){

            if(!in_array($file, $files2)){
                if (is_dir($path.$file)) {
                    break;
                }
                $cambiosCopiar++;
                copy($path.$file, $pathCache.$file);
            }
        }

        // revisar de nuevo el directorio de destino
        $files2 = array_reverse(glob("*.*"));

        // eliminar los ficheros restantes
        $i = 1;
        foreach($files2 as $file2){
            if($i>$length){
                $cambiosBorrar++;
                unlink($file2);
            }
            $i++;
        }
        return [$cambiosBorrar,$cambiosCopiar];
    }

}
?>