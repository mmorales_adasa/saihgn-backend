<?php
require_once APPPATH.'/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Evolucion {

    /**
     * Fecha inicial y final en UTC
     */
    static function getEvolucionEmbalse($ids, $periodo, $fechaIni, $fechaFin){
        require_once APPPATH.'/libraries/visor/config.php';

        $idsString = "";
        foreach($ids as $id){
            if($idsString != "") $idsString .= " OR ";
            $idsString .= "cod_variable like '".$id."/%'";
        }

        $idsStringTr = "";
		foreach ($ids as $id) {
			$id = str_replace(" ", "", $id);
			if ($idsStringTr != "") $idsStringTr .= " OR ";
			$idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/%'";
		}

        $query = "with 
            medidas AS (
                select cod_variable, valor, timestamp_medicion as timestamp 
                from tbl_hist_medidas 
                where timestamp_medicion >= '" . $fechaIni . "' 
                and timestamp_medicion <= '" . $fechaFin . "'
                and (" . $idsString . ")
                and ind_validacion = 'S'
                and (extract(epoch from timestamp_medicion)/" . $periodo . " - TRUNC(extract(epoch from timestamp_medicion)/" . $periodo . "))=0
            ),
            max_medidas AS (
                select cod_variable, max(timestamp) from medidas group by cod_variable
            ),
            datostrymedidas AS (
                select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                where timestamp >= '" . $fechaIni . "' 
                and timestamp <= '" . $fechaFin . "'
                and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                and (" . $idsStringTr . ")
                and (extract(epoch from timestamp)/" . $periodo . " - TRUNC(extract(epoch from timestamp)/" . $periodo . "))=0
                union
                select cod_variable, valor, timestamp from medidas
            )
        
            select
                timestamp,
                estacion,
                coalesce(SUM(VE1),null) AS \"VE1\",
                coalesce(SUM(NE1),null) AS \"NE1\",
                coalesce(SUM(PR),null) AS \"PR\",
                coalesce(SUM(QAT),null) AS \"QAT\",
                coalesce(SUM(QFT),null) AS \"QFT\",
                coalesce(SUM(QWT),null) AS \"QWT\",
                coalesce(SUM(QTR),null) AS \"QTR\",
                coalesce(SUM(QTR),null) AS \"QWC\",
                coalesce(SUM(QC1),null) AS \"CA1\",
                coalesce(SUM(QC1),null) AS \"QC1\",
                coalesce(SUM(QC2),null) AS \"QC2\",
                coalesce(SUM(QC3),null) AS \"QC3\",
                coalesce(SUM(QC4),null) AS \"QC4\",
                coalesce(SUM(QT1),null) AS \"QT1\",
                coalesce(SUM(QT2),null) AS \"QT2\",
                coalesce(SUM(QT3),null) AS \"QT3\",
                coalesce(SUM(QT4),null) AS \"QT4\",
                coalesce(SUM(ET),null) AS \"ET\"
                from(
                    select
                    timestamp,
                    substring(cod_variable,0,6) as estacion,
                    case when cod_variable like '%/VE1' then valor end as VE1,
                    case when cod_variable like '%/NE1' then valor end as NE1,
                    case when cod_variable like '%/PR' then valor end as PR,
                    case when cod_variable like '%/QAT' then valor end as QAT,
                    case when cod_variable like '%/QFT' then valor end as QFT,
                    case when cod_variable like '%/QWT' then valor end as QWT,
                    case when cod_variable like '%/QTR' then valor end as QTR,
                    case when cod_variable like '%/QWC' then valor end as QWC,
                    case when cod_variable like '%/CA1' then valor end as CA1,
                    case when cod_variable like '%/QC1' then valor end as QC1,
                    case when cod_variable like '%/QC2' then valor end as QC2,
                    case when cod_variable like '%/QC3' then valor end as QC3,
                    case when cod_variable like '%/QC4' then valor end as QC4,
                    case when cod_variable like '%/QT1' then valor end as QT1,
                    case when cod_variable like '%/QT2' then valor end as QT2,
                    case when cod_variable like '%/QT3' then valor end as QT3,
                    case when cod_variable like '%/QT4' then valor end as QT4,
                    case when cod_variable like '%/ET' then valor end as ET
                    from datostrymedidas
                ) as valores
                group by estacion, timestamp
                order by timestamp asc";

        $data = ConexionBD::EjecutarConsulta($query);

        $return = Array(); // array de retorno
        
        if($data){

            // variable para almacenar el último dato para cada estación, necesario para calcular las variables que requieren datos anteriores
            $timestampAnterior = Array();
            $VE1Anterior = Array();
            $NE1Anterior = Array();
            $PRAnterior = Array();

            // agrupar por timestamp los valores de estación y calcular variables
            foreach($data as $i => $row){
                
                // conversión de datos, no es posible acceder a configurar correctamente codeigniter para conseguir retornar los valores en su formato correcto
                $row['VE1'] = (isset($row['VE1'])) ? (float)$row['VE1']:null; 
                $row['NE1'] = (isset($row['NE1'])) ? (float)$row['NE1']:null;  
                $row['PR'] =  (isset($row['PR']))  ? (float)$row['PR'] :null;  
                $row['QAT'] = (isset($row['QAT'])) ? (float)$row['QAT']:null;  
                $row['QFT'] = (isset($row['QFT'])) ? (float)$row['QFT']:null;  
                $row['QWT'] = (isset($row['QWT'])) ? (float)$row['QWT']:null;  
                $row['QTR'] = (isset($row['QTR'])) ? (float)$row['QTR']:null;  
                $row['QWC'] = (isset($row['QWC'])) ? (float)$row['QWC']:null; 
                $row['CA1'] = (isset($row['CA1'])) ? (float)$row['CA1']:null;  
                $row['QC1'] = (isset($row['QC1'])) ? (float)$row['QC1']:null;  
                $row['QC2'] = (isset($row['QC2'])) ? (float)$row['QC2']:null;  
                $row['QC3'] = (isset($row['QC3'])) ? (float)$row['QC3']:null; 
                $row['QT1'] = (isset($row['QT1'])) ? (float)$row['QT1']:null; 
                $row['QT2'] = (isset($row['QT2'])) ? (float)$row['QT2']:null; 
                $row['QT3'] = (isset($row['QT3'])) ? (float)$row['QT3']:null; 
                $row['QT4'] = (isset($row['QT4'])) ? (float)$row['QT4']:null; 
            
                // Retorno de timestamp con hora de España
                //$dateTime = new DateTime($row['timestamp']);
                //$dateTime->setTimezone(new DateTimeZone("Europe/Madrid"));
                //$row['timestamp'] = $dateTime->format("Y/m/d H:i:s");

                // Calcular caudal de salida
                $tuberia = $row['QT1'] + $row['QT2'] + $row['QT3'] + $row['QT4'];

                switch (trim($row["estacion"])){
                    case 'E2-04': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                        $qCanal = $row['CA1'] + $row['QC1'] + $row['QC2']; 
                        $row['QSALIDA'] = $row['QAT'] + $row['QFT'] + $row['QWT'] + $qCanal + $row['QTR'] + $tuberia / 1000;		
                    break;
                    case 'E2-11':	//Caso de Sierra Brava CA1 es de entrada
                        $qCanal = $row['CA1'] + $row['QC2'] ;
                        $row['QSALIDA'] = $row['QAT'] + $row['QFT'] + $row['QWT'] + $qCanal + $row['QTR'] + $tuberia / 1000;
                    break;
                    case 'E2-12':   //Presa de Garg�ligas y Cubilar
                    case 'E2-13':	//Presa de Garg�ligas y Cubilar
                        $qCanal = $row['CA1'] + $row['QC1'] + $row['QC2'];
                        $row['QSALIDA'] = $row['QAT'] + $row['QFT'] + $row['QWT'] + $qCanal + $row['QTR'] + $tuberia / 1000;
                    break;
                    case 'E2-24': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                        $qCanal = $row['CA1'] + $row['QC1'] + $row['QC2'];
                        $row['QSALIDA'] = $row['QAT'] + $row['QFT'] + $row['QWT'] + $qCanal + $row['QTR'] + $tuberia / 1000;
                    break;
                    case 'E1-10':
                        $row['QSALIDA'] = $row['QAT'] + $row['QFT'] + ($row['QT1'] / 1000);
                        break;
                    default:
                        $qCanal = $row['CA1'] + $row['QWC'] + $row['QC1'] + $row['QC2'];
                        $row['QSALIDA'] = $row['QAT'] + $row['QFT'] + $row['QWT'] + $qCanal + $row['QTR'] + $tuberia / 1000;
                        break;
                }

                if(isset($staticEMB[$row['estacion']][7])) $row['NMN'] = $staticEMB[$row['estacion']][7]; //N.M.N.[msnm]
                else $row['NMN'] = null;

                if(isset($row['NE1']) && isset($staticEMB[$row['estacion']][7])) $row['HMAX'] = $row['NE1'] - $staticEMB[$row['estacion']][7]; //N.M.N.[msnm]
                else $row['HMAX'] = null;

                if(isset($row['VE1']) && isset($staticEMB[$row['estacion']][9])) $row['VNMN'] = $row['VE1'] - $staticEMB[$row['estacion']][9]; //Vol N.M.N. [hm³]
                else $row['VNMN'] = null;

                // Cálculos si existe timestamp anterior para esta estación
                if (isset($timestampAnterior[$row["estacion"]]) && $timestampAnterior[$row["estacion"]] != $row["timestamp"]){

                    // Cálculo de la intensidad horaria de precipitación (no se usa el PI de db)
                    if(isset($row['PR'])) $row['PI'] = $row['PR'] * 6;
                    else $row['PI'] = null;

                    // Cálculo del caudal de entrada. Para el espacio temporal se calcula con la diferencia de timestamp
                    $dateTime = new DateTime($row['timestamp']);
                    $epochTimestamp = $dateTime->getTimestamp();
                    $dateTimeAnterior = new DateTime($timestampAnterior[$row["estacion"]]);
                    $epochTimestampAnterior = $dateTimeAnterior->getTimestamp();
                    
                    // Cálculo de entrada
                    if(isset($row['VE1']) && isset($VE1Anterior[$row["estacion"]])){
                        $row['QENTRADA'] = (($row['VE1'] - $VE1Anterior[$row["estacion"]]) * 1000000 + $row['QSALIDA'] * ($epochTimestamp - $epochTimestampAnterior)) / ($epochTimestamp - $epochTimestampAnterior);		
                        if ($row['QENTRADA'] < 0) $row['QENTRADA'] = 0;
                    } else {
                        $row['QENTRADA'] = null;
                    }
                    
                    // Cálculo de la velocidad ascensional en cm/h
                    if(isset($row['NE1']) && isset($NE1Anterior[$row["estacion"]])) $row['VASCENSIONAL'] = ($row['NE1'] - $NE1Anterior[$row["estacion"]]) * 100 / ($periodo / 3600);
                    else $row['VASCENSIONAL'] = null;

                    // Cálculo de la tendencia de nivel
                    if(isset($row['NE1']) && isset($NE1Anterior[$row["estacion"]])) {
                        $delta = $row['NE1'] - $NE1Anterior[$row["estacion"]];
                        if ($delta > 0){
                            $row['NTENDENCIA'] = "sube";
                        } else if ($delta < 0){
                            $row['NTENDENCIA'] = "baja";
                        } else {
                            $row['NTENDENCIA'] = "igual";
                        }
                    } else {
                        $row['NTENDENCIA'] = null;
                    }

                } else {
                    $row['PI'] = null;
                    $row['QENTRADA'] = null;
                    $row['VASCENSIONAL'] = null;
                    $row['NTENDENCIA'] = null;
                }

                $timestampAnterior[$row["estacion"]] = $row["timestamp"];
                $VE1Anterior[$row["estacion"]] = $row["VE1"];
                $NE1Anterior[$row["estacion"]] = $row["NE1"];
                $PRAnterior[$row["estacion"]] = $row["PR"];
                
                $val = Array(
                    "timestamp" => $row["timestamp"],
                    "ESTADOCOM" => $row["ET"],
                    "VE1" => (isset($row["VE1"])) ? round($row["VE1"],2):null,
                    "NE1" => (isset($row["NE1"])) ? round($row["NE1"],2):null,
                    "PI" => (isset($row["PI"])) ? round($row["PI"],2):null,
                    "PR" => (isset($row["PR"])) ? round($row["PR"],2):null,
                    "QSALIDA"=> (isset($row["QSALIDA"])) ? round($row["QSALIDA"],2):null,
                    "HMAX" => (isset($row["HMAX"])) ? round($row["HMAX"],2):null,
                    "VNMN" => (isset($row["VNMN"])) ? round($row["VNMN"],2):null,
                    "NMN" => (isset($row["NMN"])) ? round($row["NMN"],2):null,
                    "QENTRADA" => (isset($row["QENTRADA"])) ? round($row["QENTRADA"],2):null,
                    "VASCENSIONAL" => (isset($row["VASCENSIONAL"])) ? round($row["VASCENSIONAL"],2):null,
                    "NTENDENCIA" => $row["NTENDENCIA"],
                );

                $return[$row["estacion"]][] = $val;
                
            }
        }
        return $return;
    }
}