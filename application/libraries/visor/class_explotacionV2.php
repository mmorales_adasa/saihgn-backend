<?php
require_once APPPATH.'/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class ExplotacionV2 {

    static function crearEncabezado($variables){

        $in = "'". implode("','", $variables) ."'";

        // se obtiene la info del tipo de variable y a la estación que pertenece
        $query = "select cod_variable, cod_tipo_variable, v.descripcion descripcion_variable, cod_estacion, e.nombre as nombre_estacion, tipo as tipo_estacion, t.descripcion descripcion_tipo  from tbl_variable v, tbl_estacion e, tbl_tipo_estacion t 
                    where cod_variable in (".$in.") 
                    and cod_estacion = split_part(cod_variable,'/',1)
                    and tipo = cod_tipo_estacion
                    order by tipo";

        $variableInfoTemp = ConexionBD::EjecutarConsulta($query, true);
        $variableInfo = [];

        // ordenar variables llegadas de base de datos por el orden de array
        foreach($variables as $variable){
            foreach($variableInfoTemp as $varInfo){
                if($varInfo["cod_variable"]==$variable){
                    $variableInfo[] = $varInfo;
                }
            }
        }
        
        // grupos por tipo de estación 
        $cabecera1Temp = [];
        $cabecera2Temp = [];
        $cabecera3Temp = [];
        $cabeceras = [];
        
        // crear la primera cabecera temporal que agrupa y realiza un conteo de las variables que se incluyen en cada
        foreach($variableInfo as $variable){
            if(!isset($cabecera1Temp[$variable["cod_tipo_variable"]])) $cabecera1Temp[$variable["cod_tipo_variable"]] = ["id"=>$variable["descripcion_tipo"], "colspan"=>0, "nombre" => $variable["descripcion_tipo"], "variables" => []];
            $cabecera1Temp[$variable["cod_tipo_variable"]]["colspan"] += 1;
            // dar nombre a la variable e incluirla en el array de variables del tipo de estación
            $cabecera1Temp[$variable["cod_tipo_variable"]]["variables"][] = ["id"=>$variable["cod_estacion"], "variable" => $variable, "cod_variable" => $variable["cod_variable"]];
        
        }

        // crear la segunda cabecera temporal a partir de la primera cabecera
        foreach($cabecera1Temp as $cabecera1TipoEstacion){
            foreach($cabecera1TipoEstacion['variables'] as $variable){
                $cabecera2Temp[] = $variable;
            }
        }

        // crear la cabecera principal temporal a partir de la segunda cabecera
        //$cabecera3Temp[] = ["id" => "timestamp", "cod_campo" => "timestamp"]; // añadir columna timestamp
        foreach($cabecera2Temp as $cabecera2variable){
            $variablePartida = explode("/", $cabecera2variable["cod_variable"]);
            $cabecera3Temp[] = ["id" => $cabecera2variable["cod_variable"], "cod_campo" => $variablePartida[1], "label" => $cabecera2variable["cod_variable"]]; 
        }

        $cabeceras[0][] = ["id"=>"vacio","nombre"=>'', "rowspan"=>2]; // añadir columna vacía a la primera cabecera comlementaria

        // crear un array definitivo con la primera cabecera complementaria de tipos de estacion
        $i = 0;
        foreach($cabecera1Temp as $cabecera1){
            $cabeceras[0][] = ["id"=>"a".$i, "nombre"=>$cabecera1["nombre"], "colspan"=>$cabecera1["colspan"]];
            $i++;
        }

        // crear un array definitivo con la segunda cabecera complementaria de tipos de variable
        foreach($cabecera2Temp as $cabecera2){
            $cabeceras[1][] = ["id"=>"a".$i, "nombre"=> $cabecera2["variable"]["nombre_estacion"] ];
            $i++;
        }

        $cabeceras[2][] = ["id" => "timestamp", "cod_campo" => "timestamp"]; // añadir columna timestamp a la cabecera principal

        // crear un array definitivo con la cabecera principal
        foreach($cabecera3Temp as $cabecera){
            $cabeceras[2][] = $cabecera;
        }

        return $cabeceras;

    }

    static function getExplotacion($variables, $periodo, $fechaIni, $fechaFin){

        $coalesce = [];
        foreach($variables as $variable){
            $coalesce[] = "coalesce(SUM(\"".$variable."\"),null) AS \"".$variable."\"";
        }
        $coalesce = implode(",", $coalesce);

        $cases = [];
        foreach($variables as $variable){
            $cases[] = "case when cod_variable like '".$variable."' then valor end as \"".$variable."\"";
        }
        $cases = implode(",", $cases);

        $in = "'". implode("','", $variables) ."'";

        $query = "with 
            medidas AS (
                select cod_variable, valor, timestamp_medicion as timestamp 
                from tbl_hist_medidas 
                where timestamp_medicion >= '" . $fechaIni . "' 
                and timestamp_medicion <= '" . $fechaFin . "' 
                and cod_variable IN (".$in.")
                and ind_validacion = 'S'
                and (extract(epoch from date_trunc('minute', timestamp_medicion))/" . $periodo . " - TRUNC(extract(epoch from date_trunc('minute', timestamp_medicion))/" . $periodo . "))=0
            ),
            max_medidas AS (
                select cod_variable, max(timestamp) from medidas group by cod_variable
            ),
            datostrymedidas AS (
                select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                where timestamp >= '" . $fechaIni . "' 
                and timestamp <= '" . $fechaFin . "'
                and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                and tbl_dato_tr.cod_variable IN (".$in.")
                and (extract(epoch from date_trunc('minute', timestamp))/" . $periodo . " - TRUNC(extract(epoch from date_trunc('minute', timestamp))/" . $periodo . "))=0
                union
                select cod_variable, valor, timestamp from medidas
            )
            
            select 
                date_trunc('minute', timestamp) as timestamp,
                ".$coalesce."
                from( 
                    select timestamp, 
                    substring(cod_variable,0,6) as estacion,
                    ".$cases."
                    from datostrymedidas
                ) as valores
                group by timestamp 
                order by timestamp asc";

        $data = ConexionBD::EjecutarConsulta($query);



        return $data;
    
    }

    static function obtenerVariables($estacionesUsuario){

        $in = "'". implode("','", $estacionesUsuario) ."'";

        // se obtiene la info del tipo de variable y a la estación que pertenece
        $query = "select tipo, tipo || ' - ' || t.descripcion as tipo_descripcion, cod_estacion, cod_estacion || ' - ' || e.nombre as cod_estacion_descripcion, cod_variable, split_part(cod_variable,'/',2) || ' - ' || v.descripcion as cod_variable_descripcion from tbl_variable v, tbl_estacion e, tbl_tipo_estacion t 
        where v.disponible = 1                    
        and cod_estacion in (".$in.")
        and split_part(cod_variable,'/',1) = e.cod_estacion
        and t.cod_tipo_estacion = e.tipo
        order by tipo, cod_estacion, cod_variable";

        $data = ConexionBD::EjecutarConsulta($query);

        return $data;
    
    }

    static function obtenerSistemas(){

        $query = "select id, nombre, global, cod_variable
        from tbl_sistema_explotacion se 
        left join tbl_sistema_explotacion_var sev on se.id = sev.id_sistema_explotacion
        where se.activo = true
		and cod_usuario is null
		and global
        order by se.orden, sev.orden";

        $data = ConexionBD::EjecutarConsulta($query);
        $sistemasTemp = [];
        foreach($data as $variable){
            if(!isset($sistemasTemp[$variable["id"]])) $sistemasTemp[$variable["id"]] = ["nombre" => $variable["nombre"], "global" => $variable["global"], "variables" => []];
            $sistemasTemp[$variable["id"]]["variables"][] = $variable["cod_variable"];
        }
        $sistemas = array_values($sistemasTemp);
        return $sistemas;
    
    }
}