<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class EsquemaExplotacion
{

    static function getUnit($variable): string
    {


        if(substr($variable, 0, 2)==='QC'){
            return ' m³/s';
        }
        
        else if(substr($variable, 0, 2)==='NC'){
            return ' m';
        }

        else {

            switch ($variable) {
                case 'NE1':
                    return ' msnm';
                    break;
                case 'VE1':
                    return ' hm³';
                    break;
                case 'PV1':
                    return ' %';
                    break;
                case 'PRA':
                    return ' mm';
                    break;
                case 'QR1':
                    return ' m³/s';
                    break;
                case 'NR1':
                    return ' m';
                    break;
                default:
                    return '';
                    break;
            }
        }
    }

    static function formatNumber($n, $variable): string
    {

        $n = (float)$n;

        if(!is_float($n)) return "";

        if(substr($variable, 0, 2)==='QC'){
            return number_format($n, 2, ",", ".");
        }
        
        else if(substr($variable, 0, 2)==='NC'){
            return number_format($n, 2, ",", ".");
        }

        else {

            switch ($variable) {
                
                case 'NE1':
                    return number_format($n, 2, ",", ".");
                    break;
                case 'VE1':
                    return number_format($n, 2, ",", ".");
                    break;
                case 'PV1':
                    return number_format($n, 2, ",", ".");
                    break;
                case 'PRA':
                    return number_format($n, 2, ",", ".");
                    break;
                case 'QR1':
                    return number_format($n, 2, ",", ".");
                    break;
                case 'NR1':
                    return number_format($n, 2, ",", ".");
                    break;
                default:
                    return '';
                    break;
            }
        }
    }

    static function getEsquemaExplotacion($esquemaHTML, $datos_anteriores, $datos_recientes)
    {

        if(isset($datos_anteriores['E']) && isset($datos_anteriores['CR']) && isset($datos_recientes['E']) && isset($datos_recientes['CR'])){

            $query = "select TRIM(cod_estacion) cod_estacion, nombre, TRIM(tipo) tipo, TRIM(zona) zona, disponible from tbl_estacion where tipo in ('E','CR')";
            $data = ConexionBD::EjecutarConsulta($query);

            $estaciones = array();

            foreach ($data as $d) {
                $estaciones[trim($d['cod_estacion'])] = $d;
            }

            // Modificamos los valores de embalses
            foreach ($datos_recientes['E'] as $index => &$valorEM) {
                $cod_estacion_var = explode("/", $valorEM["cod_variable"]);
                $estacion = $estaciones[$cod_estacion_var[0]];
                
                // Cálculo del nivel
                if ($cod_estacion_var[1] == "NE1" && is_numeric($valorEM['valor']) && is_numeric($datos_anteriores['E'][$index]['valor'])) {

                    if($estacion['disponible'] == true){

                        $deltaH = $valorEM['valor'] - $datos_anteriores['E'][$index]['valor'];
                        
                        if ($deltaH > 0) {
                            $tendencia = "<div class='bg-red' data-link='" . $estacion['zona'] . "/" . trim($estacion["cod_estacion"]) . "'>sube</div>";
                        } else {
                            if ($deltaH < 0) {
                                $tendencia = "<div class='bg-green' data-link='" . $estacion['zona'] . "/" . trim($estacion["cod_estacion"]) . "'>baja</div>";
                            } else {
                                $tendencia = "<div data-link='" . $estacion['zona'] . "/" . trim($estacion["cod_estacion"]) . "'>igual</div>";
                            }
                        }
                        $esquemaHTML = str_replace(trim($cod_estacion_var[0]) . "/TE1", "<div data-link='" . $estacion['zona'] . "/" . trim($estacion["cod_estacion"]) . "'>" . $tendencia . "</div>", $esquemaHTML);
                    
                    } else {
                        
                        $esquemaHTML = str_replace(trim($cod_estacion_var[0]) . "/TE1", "<div ></div>", $esquemaHTML);

                    }
                }

                if (substr($cod_estacion_var[1], 0, 2)==='QC') {
                    if($estacion['disponible'] == true){
                        $esquemaHTML = str_replace($valorEM["cod_variable"], "<div data-link='" . $estacion['zona'] . "/" . trim($estacion["cod_estacion"]) . "'>" . EsquemaExplotacion::formatNumber($valorEM["valor"], $cod_estacion_var[1]) . EsquemaExplotacion::getUnit($cod_estacion_var[1]) . "</div>", $esquemaHTML);
                    } else {
                        $esquemaHTML = str_replace($valorEM["cod_variable"], "<div ></div>", $esquemaHTML);
                    }
                }

                // NC
                else if (substr($cod_estacion_var[1], 0, 2)==='NC') {
                    if($estacion['disponible'] == true){
                        $esquemaHTML = str_replace($valorEM["cod_variable"], "<div data-link='" . $estacion['zona'] . "/" . trim($estacion["cod_estacion"]) . "'>" . EsquemaExplotacion::formatNumber($valorEM["valor"], $cod_estacion_var[1]) . EsquemaExplotacion::getUnit($cod_estacion_var[1]) . "</div>", $esquemaHTML);
                    } else {
                        $esquemaHTML = str_replace($valorEM["cod_variable"], "<div ></div>", $esquemaHTML);
                    }
                }

                else {
                    if($estacion['disponible'] == true){
                        $esquemaHTML = str_replace(trim($index), "<div data-link='" . $estacion['zona'] . "/" . trim($estacion["cod_estacion"]) . "'>" . EsquemaExplotacion::formatNumber($valorEM["valor"], $cod_estacion_var[1]) . EsquemaExplotacion::getUnit($cod_estacion_var[1]) . "</div>", $esquemaHTML);
                    } else {
                        $esquemaHTML = str_replace(trim($index), "<div ></div>", $esquemaHTML);
                    }
                }
                
            }

            // Modificamos los valores de aforos
            foreach ($datos_recientes['CR'] as $index => $valorCR) {

                $cod_estacion_var = explode("/", $valorCR["cod_variable"]);
                $estacion = $estaciones[$cod_estacion_var[0]];

                // Cálculo del nivel
                if ($cod_estacion_var["1"] == "NR1" && is_numeric($valorCR['valor']) && is_numeric($datos_anteriores['CR'][$index]['valor'])) {

                    if($estacion['disponible'] == true){

                        $deltaH = floatval($valorCR['valor']) - floatval($datos_anteriores['CR'][$index]['valor']);
                        if ($deltaH > 0) {
                            $tendencia = "<div class='bg-red' data-link='CR/" . trim($cod_estacion_var[0]) . "'>sube</div>";
                        } else {
                            if ($deltaH < 0) {
                                $tendencia = "<div class='bg-green' data-link='CR/" . trim($cod_estacion_var[0]) . "'>baja</div>";
                            } else {
                                $tendencia = "<div data-link='CR/" . trim($cod_estacion_var[0]) . "'>igual</div>";
                            }
                        }
                        $esquemaHTML = str_replace(trim($cod_estacion_var[0]) . "/TE1", "<div data-link='CR/" . trim($cod_estacion_var[0]) . "'>" . $tendencia . "</div>", $esquemaHTML);
                    } else {
                        $esquemaHTML = str_replace(trim($index), "<div ></div>", $esquemaHTML);
                    }
                
                }
                
                if($estacion['disponible'] == true){
                    $esquemaHTML = str_replace(trim($index), "<div data-link='CR/" . trim($cod_estacion_var[0]) . "'>" . EsquemaExplotacion::formatNumber($valorCR["valor"], $cod_estacion_var[1]) . EsquemaExplotacion::getUnit($cod_estacion_var[1]) . "</div>", $esquemaHTML);
                } else {
                    $esquemaHTML = str_replace(trim($index), "<div ></div>", $esquemaHTML);
                }
            
            }

            foreach ($data as $index => $valor) {
                if ($valor['tipo'] == 'CR') {
                    $esquemaHTML = str_replace(trim($valor["cod_estacion"]) . "/NO1", "<div data-link='CR/" . trim($valor["cod_estacion"]) . "'>" . $valor["nombre"] . "</div>", $esquemaHTML);
                }

                if ($valor['tipo'] == 'E') {
                    $esquemaHTML = str_replace(trim($valor["cod_estacion"]) . "/NO1", "<div data-link='" . trim($valor["zona"]) . '/' . trim($valor["cod_estacion"]) . "'>" . $valor["nombre"] . "</div>", $esquemaHTML);

                    // Cálculo del caudal de salida para los embalses
                    $QT1 = array_key_exists(trim($valor["cod_estacion"]) . '/QT1', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT1']['valor'] : 0;
                    $QT2 = array_key_exists(trim($valor["cod_estacion"]) . '/QT2', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT2']['valor'] : 0;
                    $QT3 = array_key_exists(trim($valor["cod_estacion"]) . '/QT3', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT3']['valor'] : 0;
                    $QT4 = array_key_exists(trim($valor["cod_estacion"]) . '/QT4', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT4']['valor'] : 0;

                    $tuberia = $QT1 + $QT2 + $QT3 + $QT4;

                    switch (trim($valor["cod_estacion"])) {
                        case 'E2-04': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                            $CA1 = array_key_exists(trim($valor["cod_estacion"]) . '/CA1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/CA1']['valor'] : 0;
                            $QC1 = array_key_exists(trim($valor["cod_estacion"]) . '/QC1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC1']['valor'] : 0;
                            $QC2 = array_key_exists(trim($valor["cod_estacion"]) . '/QC2', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC2']['valor'] : 0;
                            $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                            break;
                        case 'E2-11':    //Caso de Sierra Brava CA1 es de entrada
                            $CA1 = array_key_exists(trim($valor["cod_estacion"]) . '/CA1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/CA1']['valor'] : 0;
                            $QC2 = array_key_exists(trim($valor["cod_estacion"]) . '/QC2', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC2']['valor'] : 0;
                            $qCanal = (float)$CA1 + (float)$QC2;
                            break;
                        case 'E2-12':    //Presa de Garg�ligas y Cubilar
                            $CA1 = array_key_exists(trim($valor["cod_estacion"]) . '/CA1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/CA1']['valor'] : 0;
                            $QC1 = array_key_exists(trim($valor["cod_estacion"]) . '/QC1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC1']['valor'] : 0;
                            $QC2 = array_key_exists(trim($valor["cod_estacion"]) . '/QC2', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC2']['valor'] : 0;
                            $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                            break;
                        case 'E2-13':    //Presa de Garg�ligas y Cubilar
                            $CA1 = array_key_exists(trim($valor["cod_estacion"]) . '/CA1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/CA1']['valor'] : 0;
                            $QC1 = array_key_exists(trim($valor["cod_estacion"]) . '/QC1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC1']['valor'] : 0;
                            $QC2 = array_key_exists(trim($valor["cod_estacion"]) . '/QC2', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC2']['valor'] : 0;
                            $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                            break;
                        case 'E2-24': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                            $CA1 = array_key_exists(trim($valor["cod_estacion"]) . '/CA1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/CA1']['valor'] : 0;
                            $QC1 = array_key_exists(trim($valor["cod_estacion"]) . '/QC1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC1']['valor'] : 0;
                            $QC2 = array_key_exists(trim($valor["cod_estacion"]) . '/QC2', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC2']['valor'] : 0;
                            $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                            break;
                        default:
                            $CA1 = array_key_exists(trim($valor["cod_estacion"]) . '/CA1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/CA1']['valor'] : 0;
                            $QWC = array_key_exists(trim($valor["cod_estacion"]) . '/QWC', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QWC']['valor'] : 0;
                            $QC1 = array_key_exists(trim($valor["cod_estacion"]) . '/QC1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC1']['valor'] : 0;
                            $QC2 = array_key_exists(trim($valor["cod_estacion"]) . '/QC2', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC2']['valor'] : 0;
                            $qCanal = (float)$CA1 + (float)$QWC + (float)$QC1 + (float)$QC2;
                    }

                    $Qs = (array_key_exists(trim($valor["cod_estacion"]) . '/QAT', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QAT']['valor'] : 0) +
                        (array_key_exists(trim($valor["cod_estacion"]) . '/QFT', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QFT']['valor'] : 0) +
                        (array_key_exists(trim($valor["cod_estacion"]) . '/QWT', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QWT']['valor'] : 0) +
                        $qCanal +
                        (array_key_exists(trim($valor["cod_estacion"]) . '/QTR', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QTR']['valor'] : 0) +
                        $tuberia / 1000;
                    $esquemaHTML = str_replace(trim($valor["cod_estacion"]) . "/QS", "<div data-link='" . trim($valor["zona"]) . '/' . trim($valor["cod_estacion"]) . "'>" . number_format($Qs, 2, ',', '.') . " m3/s" . "</div>", $esquemaHTML);
                }
                $patron = '~' . trim($valor['cod_estacion']) . '[/][0-z]{2,3}~';
                $esquemaHTML = preg_replace($patron, "", $esquemaHTML);

                if ($valor['tipo'] == 'CR') {
                    $esquemaHTML = str_replace(trim($valor["cod_estacion"]) . "</td>", "<div data-link='CR/" . trim($valor["cod_estacion"]) . "'>" . trim($valor["cod_estacion"]) . "</div></td>", $esquemaHTML);
                    $esquemaHTML = str_replace(trim($valor["cod_estacion"]) . "</a>", "<div data-link='CR/" . trim($valor["cod_estacion"]) . "'>" . trim($valor["cod_estacion"]) . "</div></a>", $esquemaHTML);
                }

                if ($valor['tipo'] == 'E') {
                    $esquemaHTML = str_replace(trim($valor["cod_estacion"]) . "</td>", "<div data-link='" . trim($valor["zona"]) . '/' . trim($valor["cod_estacion"]) . "'>" . trim($valor["cod_estacion"]) . "</div></td>", $esquemaHTML);
                }
            }

            // Reemplazamos elementos para que no de problemas el css
            $esquemaHTML = str_replace('<body link=blue vlink=purple class=xl130>', '<body class="esquema" link=blue vlink=purple class=xl130>', $esquemaHTML);
            $esquemaHTML = str_replace('a:link {', '.esquema a:link {', $esquemaHTML);
            $esquemaHTML = str_replace('a:visited {', '.esquema a:visited {', $esquemaHTML);
            $esquemaHTML = str_replace('tr { {', '.esquema tr { {', $esquemaHTML);
            $esquemaHTML = str_replace('col { {', '.esquema col { {', $esquemaHTML);
            $esquemaHTML = str_replace('br { {', '.esquema br { {', $esquemaHTML);

            return $esquemaHTML;
        } else {
            return null;
        }

    }

    static function getCalculosQSalida($datos_anteriores, $datos_recientes)
    {

        //Metemos los nombres de las estaciones
        $query = "select cod_estacion, nombre, tipo from tbl_estacion where tipo in ('E','CR')";
        $data = ConexionBD::EjecutarConsulta($query);

        echo "<table>";

        foreach ($data as $index => $valor) {

            if ($valor['tipo'] == 'E') {

                echo "<tr>";
                echo "<td>" . $valor["cod_estacion"] . "</td>";
                //echo "<td>".$valor["nombre"]."</td>";

                // Cálculo del caudal de salida para los embalses
                $QT1 = array_key_exists(trim($valor["cod_estacion"]) . '/QT1', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT1']['valor'] : 0;
                $QT2 = array_key_exists(trim($valor["cod_estacion"]) . '/QT2', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT2']['valor'] : 0;
                $QT3 = array_key_exists(trim($valor["cod_estacion"]) . '/QT3', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT3']['valor'] : 0;
                $QT4 = array_key_exists(trim($valor["cod_estacion"]) . '/QT4', $datos_recientes['E']) ? (float)$datos_recientes['E'][trim($valor["cod_estacion"]) . '/QT4']['valor'] : 0;
                $QAT = (array_key_exists(trim($valor["cod_estacion"]) . '/QAT', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QAT']['valor'] : 0);
                $QFT = (array_key_exists(trim($valor["cod_estacion"]) . '/QFT', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QFT']['valor'] : 0);
                $QWT = (array_key_exists(trim($valor["cod_estacion"]) . '/QWT', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QWT']['valor'] : 0);
                $QTR = (array_key_exists(trim($valor["cod_estacion"]) . '/QTR', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QTR']['valor'] : 0);
                $CA1 = array_key_exists(trim($valor["cod_estacion"]) . '/CA1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/CA1']['valor'] : 0;
                $QC1 = array_key_exists(trim($valor["cod_estacion"]) . '/QC1', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC1']['valor'] : 0;
                $QC2 = array_key_exists(trim($valor["cod_estacion"]) . '/QC2', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QC2']['valor'] : 0;
                $QWC = array_key_exists(trim($valor["cod_estacion"]) . '/QWC', $datos_recientes['E']) ? $datos_recientes['E'][trim($valor['cod_estacion']) . '/QWC']['valor'] : 0;
                
                $tuberia = $QT1 + $QT2 + $QT3 + $QT4;

                switch (trim($valor["cod_estacion"])) {
                    case 'E2-04': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                        
                        $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);
                        echo "<td>CA1: " . (float)$CA1 . " + QC1: " . (float)$QC1 . " + QC2: " . (float)$QC2 . " + QAT:" . $QAT . " + QFT:" . $QFT . " + QWT:" . $QWT . " + QTR:" . $QTR . " + ((QT1:" . $QT1 . " + QT2:" . $QT2 . " + QT3:" . $QT3 . " + QT4:" . $QT4 . ") / 1000) = Qsalida: <b>" . number_format($Qs, 2, ',', '.') . " m3/s</b></td>";

                        break;
                    case 'E2-11':    //Caso de Sierra Brava CA1 es de entrada
                        
                        $qCanal = (float)$CA1 + (float)$QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);
                        echo "<td>CA1: " . (float)$CA1 . " + QC2: " . (float)$QC2 . " + QAT:" . $QAT . " + QFT:" . $QFT . " + QWT:" . $QWT . " + QTR:" . $QTR . " + ((QT1:" . $QT1 . " + QT2:" . $QT2 . " + QT3:" . $QT3 . " + QT4:" . $QT4 . ") / 1000) = Qsalida: <b>" . number_format($Qs, 2, ',', '.') . " m3/s</b></td>";

                        break;
                    case 'E2-12':    //Presa de Garg�ligas y Cubilar
                        
                        $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);
                        echo "<td>CA1: " . (float)$CA1 . " + QC1: " . (float)$QC1 . " + QC2: " . (float)$QC2 . " + QAT:" . $QAT . " + QFT:" . $QFT . " + QWT:" . $QWT . " + QTR:" . $QTR . " + ((QT1:" . $QT1 . " + QT2:" . $QT2 . " + QT3:" . $QT3 . " + QT4:" . $QT4 . ") / 1000) = Qsalida: <b>" . number_format($Qs, 2, ',', '.') . " m3/s</b></td>";

                        break;
                    case 'E2-13':    //Presa de Garg�ligas y Cubilar
                        
                        $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);
                        echo "<td>CA1: " . (float)$CA1 . " + QC1: " . (float)$QC1 . " + QC2: " . (float)$QC2 . " + QAT:" . $QAT . " + QFT:" . $QFT . " + QWT:" . $QWT . " + QTR:" . $QTR . " + ((QT1:" . $QT1 . " + QT2:" . $QT2 . " + QT3:" . $QT3 . " + QT4:" . $QT4 . ") / 1000) = Qsalida: <b>" . number_format($Qs, 2, ',', '.') . " m3/s</b></td>";
                        
                        break;
                    case 'E2-24': //El caso de Orellana (E2-04) Villar del Rey (E2-24) es diferente ya que CA1 y Qturbcanal es el mismo
                        
                        $qCanal = (float)$CA1 + (float)$QC1 + (float)$QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);
                        echo "<td>CA1: " . (float)$CA1 . " + QC1: " . (float)$QC1 . " + QC2: " . (float)$QC2 . " + QAT:" . $QAT . " + QFT:" . $QFT . " + QWT:" . $QWT . " + QTR:" . $QTR . " + ((QT1:" . $QT1 . " + QT2:" . $QT2 . " + QT3:" . $QT3 . " + QT4:" . $QT4 . ") / 1000) = Qsalida: <b>" . number_format($Qs, 2, ',', '.') . " m3/s</b></td>";
                        
                        break;
                    case 'E1-10':
                    
                        $Qs = $QAT + $QFT + ($QT1 / 1000);
                        echo "<td>QAT: " . (float)$QAT . " + QFT: " . (float)$QFT . " + (QT1: " . (float)$QT1 . " / 1000) = Qsalida: <b>" . number_format($Qs, 2, ',', '.') . " m3/s</b></td>";

                        break;
                    default:
                        
                        $qCanal = (float)$CA1 + (float)$QWC + (float)$QC1 + (float)$QC2;
                        $Qs = $QAT + $QFT + $QWT + $qCanal + $QTR + ($tuberia / 1000);
                        echo "<td>CA1: " . (float)$CA1 . " + QWC: " . (float)$QWC . " + QC1: " . (float)$QC1 . " + QC2: " . (float)$QC2 . " + QAT:" . $QAT . " + QFT:" . $QFT . " + QWT:" . $QWT . " + QTR:" . $QTR . " + ((QT1:" . $QT1 . " + QT2:" . $QT2 . " + QT3:" . $QT3 . " + QT4:" . $QT4 . ") / 1000) = Qsalida: <b>" . number_format($Qs, 2, ',', '.') . " m3/s</b></td>";
                
                    }

                

                echo "</tr>";
            }
        }

        echo "</table>";
    }
}
