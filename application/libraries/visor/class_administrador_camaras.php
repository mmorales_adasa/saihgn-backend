<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class AdministradorCamaras
{
	private static $numCameras = 40;
	private static $camNamePrefix = "-cam";

	static function getCameraTimestamp($cam, $acronim)
	{
		$camFile = APPPATH  . "libraries/visor/video_cache/" . $acronim . "-video/" . $cam->camara . ".jpg";
		if (!is_file($camFile)) {
			return "";
		}
		$modify = filemtime($camFile);
		$format = "Y-m-d\TH:i:s";
		$stringDate = gmdate($format, $modify);
		return $stringDate;
	}

	static function getResumenCamaras()
	{
		require_once APPPATH . '/libraries/visor/config.php';
		$query = "SELECT json_agg(row) as result
			from (select
			e.zona, e.cod_estacion, e.nombre, e.acronimo, e.tipo as tipoEstacion, 
			json_agg(json_build_object('camara', v.cod_video, 'descripcion', v.descripcion)) filter (where v.cod_video is not null) as camaras
			FROM tbl_estacion e
			INNER JOIN 
			tbl_video v ON split_part(v.cod_video,'-',1) = e.acronimo and v.tipo = 'camara'
			group by e.cod_estacion, e.zona, e.nombre, e.tipo, v.tipo, e.acronimo
			ORDER by e.zona asc, e.orden asc, e.cod_estacion asc) as row";
		$data = ConexionBD::EjecutarConsulta($query);
		$json = json_decode($data[0]['result']);
		AdministradorCamaras::fillData($json);

		$header = [
			["id" => "zona", "cod_campo" => "zona"],
			["id" => "cod_estacion", "cod_campo" => "cod_estacion"],
			["id" => "nombre", "cod_campo" => "nombre"]
		];
		for ($i = 1; $i <= AdministradorCamaras::$numCameras; $i++) {
			$header[] = ["id" => "timestamp" . strval($i), "cod_campo" => "timestamp"];
		}
		$result = ["encabezado" => $header, "data" => $json];
		return $result;
	}

	static function fillData($json)
	{
		foreach ($json as $station) {
			if ($station->camaras != null) {
				foreach ($station->camaras as $cam) {
					$timestampKey = "timestamp" . strval(AdministradorCamaras::getCameraNumber($cam->camara));
					$station->$timestampKey = AdministradorCamaras::getCameraTimestamp($cam, $station->acronimo);
				}
			}
			for ($i = 1; $i <= AdministradorCamaras::$numCameras; $i++) {
				$timestampKey = "timestamp" . strval($i);
				if (!property_exists($station, $timestampKey)) {
					$station->$timestampKey = "";
				}
			}
			$station->tipo = "CAM";
			unset($station->camaras);
			unset($station->acronimo);
		}
	}

	private static function getCameraNumber($camName)
	{
		$index = strpos($camName, AdministradorCamaras::$camNamePrefix);
		if (!$index) {
			return false;
		}
		$numString = substr($camName, $index + strlen(AdministradorCamaras::$camNamePrefix));
		return intval($numString);
	}
}
