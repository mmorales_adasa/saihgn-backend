<?php 

class CacheVideoReacar { 

    static function CachearVideoReacarTimelapse(){

        $cambiosGlobales = 0;

        $timelapsePath = APPPATH . "libraries/visor/videoReacar/";
        $timelapseCachePath = APPPATH . "libraries/visor/videoReacar_cache/";

        // se crea el directorio de cache si este no existe
        if (!file_exists($timelapseCachePath)) {
            mkdir($timelapseCachePath, 0777, true);
        }

        $files = scandir($timelapsePath);

        // por cada fichero del directorio origen
        foreach($files as $file){
            
            // revisamos que sea un directorio
            if(is_dir($timelapsePath . $file) and substr( $file, 0, 1 ) !== "."){

                $dirPath = $timelapsePath . $file;
                $dirCachePath = $timelapseCachePath . $file;

                // se crea directorio de embalse de destino si no existe
                if (!file_exists($dirCachePath)) {
                    chdir($timelapseCachePath);
                    mkdir($file, 0777, true);
                }

                // se escanea el subdirectorio de origen
                //$embalseFiles = scandir($dirPath);

                $dirPath = $dirPath . "/" ;
                $dirCachePath = $dirCachePath . "/";

                // se crea directorio de timelapse de camara de destino si no existe
                /*if (!file_exists($videoSubDirCachePath)) {
                    chdir($dirCachePath);
                    mkdir($camFile, 0777, true);
                }*/

                $cambios = CacheVideoReacar::copyFiles($dirPath, $dirCachePath, 720);
                $cambiosGlobales = $cambiosGlobales + $cambios;

                // se escanea el subdirectorio de origen
                /*$embalseFiles = scandir($dirPath);

                // por cada fichero del subdirectorio de origen
                foreach($embalseFiles as $camFile){

                    // revisamos que sea un directorio
                    if(is_dir($dirPath . "/" . $camFile) and substr( $file, 0, 1 ) !== "."){

                        $videoSubDirPath = $dirPath . "/" . $camFile . "/";
                        $videoSubDirCachePath = $dirCachePath . "/" . $camFile . "/";

                        // se crea directorio de timelapse de camara de destino si no existe
                        if (!file_exists($videoSubDirCachePath)) {
                            chdir($dirCachePath);
                            mkdir($camFile, 0777, true);
                        }

                        $cambios = CacheVideoReacar::copyFiles($videoSubDirPath, $videoSubDirCachePath, 144);
                        $cambiosGlobales = $cambiosGlobales + $cambios;
                    }
                }*/
            }
        }

        return $cambiosGlobales;

    }

    private static function copyFiles($path, $pathCache, $length){
       
        $cambios = 0;

        chdir($path);

        // obtener ficheros por orden de modificación en directorio de origen
        $files = array_reverse(glob("*.*"));

        // saltear las imagenes de 4 en 4 (de hora en hora)
        /*$files = [];
        $i = 0;
        foreach($allfiles as $file) {
            if($i==4) $i=0;
            if($i==0) $files[] = $file;
            $i++;
        }*/

        // cortar el array al número de ficheros recibido
        $files = array_slice($files, 0, $length);

        chdir($pathCache);
        
        // obtener ficheros por orden de modificación en directorio de destino
        $files2 = array_reverse(glob("*.*"));

        // borrar ficheros de destino que no existan en origen
        foreach($files2 as $file2){
            if(!in_array($file2, $files)){
                $cambios++;
                unlink($file2);
            }
        }

        // realizar una copia de los ficheros existentes en origen que no existen en destino
        foreach($files as $file){
            if(!in_array($file, $files2)){
                $cambios++;
                copy($path.$file, $pathCache.$file);
            }
        }

        // revisar de nuevo el directorio de destino
        $files2 = array_reverse(glob("*.*"));

        // eliminar los ficheros restantes
        $i = 1;
        foreach($files2 as $file2){
            if($i>$length){
                $cambios++;
                unlink($file2);
            }
            $i++;
        }

        return $cambios;
    }


}
?>