<?php

class Operaciones {

	//Convierte el formato de fecha del RTAP en uno más adecuado
	//[Ultima_com] => 19-may-20 17H49' UTC
	//[timestamp] => 19-may-20 17H57' UTC
	static function CambioFormatoFechaRTAP($dato) {
		$dato =str_replace("ene", "01", $dato); //dan problemas los meses con iniciales en inglés distintas de castellano
		$dato =str_replace("abr", "04", $dato);
		$dato =str_replace("ago", "08", $dato);
		$dato =str_replace("dic", "12", $dato);
        if ($dato != "01-01-70 00H00' UTC" && $dato != "---") {
            $fecha = strtok($dato,' ');
			$fecha = explode("-", $fecha);
            $hora = strtok(' ');
			$hora =str_replace("'", "", $hora); //quita el apóstrofe
            $hora = explode("H", $hora);
            //$hora[1] = substr($hora[1], -3, -1); //ya no hace falta
            //$fecha;
            $date = new DateTime($fecha[0].'-'.$fecha[1].'-20'.$fecha[2]." ".$hora[0].":".$hora[1]); //añade el 20 antes del año (válido hasta 2099...)
            return $date->format('Y-m-d H:i:s');
        }
    }

	//Convierte la matriz con la relación de variables en un formato adecuado para empleo en el SELECT quitando las comillas
	static function campos2seleccion($campos){
		$resultado="";
		foreach($campos as $campo){
			$resultado = $resultado.','.trim($campo,'"');
		}
		$resultado = trim($resultado,'Array,');
		return $resultado;
	}
	
	//APLICAR FORMATO AL VALOR NUMÉRICO DE UNA CELDA
	//REVISADO: 12.05.2020
	static function AplicarFormatoValor($valor,$formato,$campo) {


		switch ($formato){
			
			case ($formato=="txtc"):
				return $valor;
				break;
			
			case ($formato=="phonec"):
				return $valor['valor'];
				break;
			
			case ($formato=="bool"):
				if ($valor=='t'){
					return "Si";
				}else{
					return "No";
				}
				break;
			
			case ($formato=="dd/mm"):	//ARREGLAR LA RUTINA D GENERACIÓN DE LA TABLA PARA METER 'VALOR'
				return $valor;
				break;
			
			case ($formato=="timestamp"):
				return $valor['valor'];
				break;
			
			case ($formato=="txti" or $formato=="zona"):
				return $valor;
				break;
			
			case ($formato=="dec2c"):
				if (is_numeric($valor['valor'])){
					
				}
				break;
			
			case ($formato=="dec2d"):
				if (is_numeric($valor['valor'])){
					return round($valor['valor'],2);
				}
				break;
				
			case ($formato=="dec1d"):
				if (is_numeric($valor['valor'])){
					return round($valor['valor'],1);
				}
				break;
			
			case ($formato=="dec0d_1k6"): //para pasar de hm³ a m³
				if (is_numeric($valor['valor'])){
					if(strpos($campo,'X')){
						return round($valor['valor']);
					}else{
						return round($valor['valor']*1000000);
					}
				}
				break;
			
			case ($formato=="dec0d"):
				if (is_numeric($valor['valor'])){
					return round($valor['valor']);
				}
				break;
				
			case ($formato=="prec1d"):
				if (is_numeric($valor['valor'])){
					return round($valor['valor'],1);
				}
				break;
			
			case ($formato=="semana"):
				if(array_key_exists(1,explode('-',$valor))){
					return explode('-',$valor)[1]; //pone sólo el número de la semana para ocupar menos
				}else{
					return $valor;
				}
				break;
			
			case ($formato=="diasem"):
				if ($valor<>""){
					return $valor;
				}
				break;
		
			default:
				return $valor['valor'];
		}
	}

	//Obtiene una matriz con el rango de las horas
	//REVISADO 13.05.2020
	static function ObtenerRangoMinutos($f_inicio,$f_final) {
        $resultado = array();
		$i=0;
		$fecha=$f_inicio;
		while ($fecha<=$f_final) {
			$valor=date("Y-m-d H:i:s", $fecha);
			$resultado[$i]['data_medicion']=$valor;
			$fecha=$fecha+600;
			$i++;
        }
        return $resultado;
	}

	static function ObtenerRangoMinutosV2($f_inicio,$f_final,$segundos) {
        $resultado = array();
		$i=0;
		$fecha=$f_inicio;
		while ($fecha<=$f_final) {
			$valor=date("Y-m-d H:i:s", $fecha);
			$resultado[$i]['data_medicion']=$valor;
			$fecha=$fecha+$segundos;
			$i++;
        }
        return $resultado;
	}

	//Obtiene una matriz con el rango de las horas
	//REVISADO 13.05.2020
	static function ObtenerRangoHoras($f_inicio,$f_final) {
        $resultado = array();
		$i=0;
		$fecha=$f_inicio;
		while ($fecha<=$f_final) {
			$valor=date("Y-m-d H:i:s", $fecha);
			$resultado[$i]['data_medicion']=$valor;
			$fecha=$fecha+3600;
			$i++;
        }
        return $resultado;
    }

	//Obtiene una matriz con el rango de fechas, día se la semana y semana (la semana incluye el número de año para poder presentar datos entre años) para serie temporal agregada
	//REVISADO 18.03.2021
	static function ObtenerRangoFechas($f_inicio, $f_final) {
		$f_inicio = beggining_of_day($f_inicio);
		$f_final = end_of_day($f_final);
        $resultado = array();
		$i=0;
		$fecha=$f_inicio;
		$dia_semana=array("D","L","M","X","J","V","S");
		while ($fecha<=$f_final) {
			$resultado[$i]['semana'] = date("o-W",$fecha);
			$resultado[$i]['data_medicion'] = date("o-m-d", $fecha);
			$resultado[$i]['dia_semana'] = $dia_semana[date("w",$fecha)];
			$fecha = strtotime("+1 day", $fecha);
			$i++;
        }
        return $resultado;
	}
	
	
	//Cambia en la matriz $camposV[$campoV] el siguiente index para que coincida con $camposF
	//COMPROBADO 01.06.2020
	static function AñadirIndexCamposV($codigoF,$datosV) {
        $resultado = array();
		foreach ($datosV as $claveV => $datoV){
			foreach ($datoV as $claveL => $linea){
				$resultado[$claveV][$linea[$codigoF]]= $linea;
			}
		}
		return $resultado;
	}
	
	//Obtiene una matriz con la variable y su agregacion (suma, diferencia, ninguna)
	static function ObtenerInformacionCampos($codEstacion,$campos) {
		$resultado = array();
		foreach($campos as $campo){
			$resultado[$campo]=Operaciones::ObtenerInformacionVariable($codEstacion,$campo);
		}
		return $resultado;
	}

	//Obtiene el nombre de la variable y el tipo de una variable concreta
	static function ObtenerInformacionVariable($codEstacion,$variable) {

        if($variable == 'semana' or $variable == 'data_medicion' or $variable == 'dia_semana'){
			$resultado['cod_variable']=$variable;
			$resultado['agregacion']='ninguna';

		}else{
			require_once APPPATH.'/libraries/visor/class_conexion.php';

			$query = "SELECT v.cod_variable, e.nombre, v.descripcion as detalle_descripcion, t.agregacion 
				FROM tbl_estacion e, tbl_variable v, tbl_tipo_variable t 
				WHERE e.cod_estacion = '".$codEstacion."' AND v.cod_tipo_variable = t.cod_tipo_variable AND v.cod_variable = '".$codEstacion."/".$variable."' ";

			$resultado = ConexionBD::EjecutarConsulta($query)[0]; 
			
			if($variable){
				if($variable == 'NE1' or $variable == 'VE1' or $variable == 'QR1' or $variable == 'NR1' or $variable == 'CP'){
					$resultado['agregacion']='diferencia';
				}
				if($variable == 'VTOTAL' ){
					$resultado['cod_variable'] = $codEstacion."/".$variable;
					$resultado['nombre']='Suma';
					$resultado['agregacion']='suma';
					$resultado['detalle_descripcion']='Detalle descripción pendiente';
				}
				
				if($variable == 'VACUM' ){
					$resultado['cod_variable'] = $codEstacion."/".$variable;
					$resultado['nombre']='Acumulado';
					$resultado['agregacion']='suma';
					$resultado['detalle_descripcion']='Detalle descripción pendiente';
				}
			}
			
			switch($resultado['agregacion']){
				case 'ACUMUL':
					$resultado['agregacion']='suma';
					break;
				default:
					//$resultado['agregacion']='ninguna';
			}
		}
		return $resultado;
	}
	
	//Obtiene una matriz con el rango de los días
	//REVISADO 13.05.2020
	static function ObtenerRangoDias($f_inicio,$f_final) {
        $resultado = array();
		$i=0;
		$fecha=$f_inicio;
		while ($fecha<=$f_final) {
			$valor=date("Y-m-d H:i:s", $fecha);
			$resultado[$i]['data_medicion']=$valor;
			$fecha=$fecha+86400;
			$i++;
        }
        return $resultado;
	}
	
	//Obtiene los datos para la matriz de la leyenda de los informes
	//REVISADO 13.05.2020
	static function ObtenerLeyendaInforme($codEstacion,$camposV) {
		$infoCampos=Operaciones::ObtenerInformacionCampos($codEstacion,$camposV);
		$leyenda = Array();
		foreach ($camposV as $claveV => $campoV){
			$leyenda[] = Array( $codEstacion."/".$campoV, $infoCampos[$campoV]['nombre'] . ": " . $infoCampos[$campoV]['detalle_descripcion'] );
		}
		return $leyenda;
    }

	static function sacarDatos($array, $tipo)
    {
        $array_datos = array();
        for ($i = 0; $i < count($array); $i++) {
            array_push($array_datos, $array[$i][$tipo]);
        }
        return $array_datos;
    }

    static function arrayEstructura($array)
    {
        $array_aux = array();
        foreach ($array as $dato) {
            array_push($array_aux, $dato);
        }
        return $array_aux;
    }

    static function sacarHuecosVacios($datos, $columnas, $primer_dato)
    {
        $aux = "";

        for ($i = 0; $i < count($columnas); $i++) {
            $aux = "";
            for ($j = 0; $j < count($datos); $j++) {
                if (empty($aux) && empty($datos[$j][$columnas[$i]]) && $i >= 1) {


                    if (isset($primer_dato[$columnas[$i]])) {
                        $aux = $primer_dato[$columnas[$i]];
                    } else {
                        $aux = "No válido";
                    }
                } else if (empty($aux) && !empty($datos[$j][$columnas[$i]])) {
                    $aux = $datos[$j][$columnas[$i]];
                }
                if (empty($datos[$j][$columnas[$i]])) {
                    $datos[$j][$columnas[$i]] = $aux;
                } else {
                    $aux = $datos[$j][$columnas[$i]];
                }
            }
        }
        return $datos;
    }
}
?>