<?php 
require_once APPPATH.'/libraries/visor/class_conexion.php';

class Cache { 

    static function GuardarRecursoCache($DB, $idRecurso, $json){
        $insert_query = "INSERT INTO tbl_cache_visor (id_recurso, contenido) VALUES ('" . $idRecurso . "', '" . $json . "') ON CONFLICT (id_recurso) DO UPDATE SET contenido = '". $json ."';";
        ConexionBD::EjecutarConsultaConDB($DB, $insert_query, false);
    }

    static function CargarRecursoCache($DB, $idRecurso){
        $query = "SELECT contenido FROM tbl_cache_visor WHERE id_recurso = '" . $idRecurso . "';";
        $contenido = ConexionBD::EjecutarConsultaConDB($DB, $query);
        if(isset($contenido) && isset($contenido[0])){
			return $contenido[0]["contenido"];
		} else {
			return null;
		}
    }

    static function GuardarUltimosDatos($DB, $datos){
        $json = json_encode($datos, JSON_HEX_APOS);
        Cache::GuardarRecursoCache($DB, "ultimos_datos", $json);
    }

    static function CargarUltimosDatos($DB){
        $ultimosDatos = json_decode(Cache::CargarRecursoCache($DB, "ultimos_datos"), true);
        return $ultimosDatos;
    }
    
    static function GuardarMasterULTRESCategorias($DB, $datos){
        $json_categorias = json_encode($datos, JSON_HEX_APOS);
        Cache::GuardarRecursoCache($DB, "master_ultres_categorias", $json_categorias);
    }

    static function CargarMasterULTRESCategorias($DB, $idCategoria){
        $ultres = json_decode(Cache::CargarRecursoCache($DB, "master_ultres_categorias"), true);
        if(isset($idCategoria) && isset($ultres[$idCategoria])){
            return $ultres[$idCategoria];
        };
    }

    static function GuardarMasterULTRESEstaciones($DB, $datos){
        $json_estaciones = json_encode($datos, JSON_HEX_APOS);
        Cache::GuardarRecursoCache($DB, "master_ultres_estaciones", $json_estaciones);
    }

    static function CargarMasterULTRESEstaciones($DB, $idEstacion){
        $ultres = json_decode(Cache::CargarRecursoCache($DB, "master_ultres_estaciones"), true);
        if(isset($idEstacion) && isset($ultres[$idEstacion])){
            return $ultres[$idEstacion];
        };
    }

    static function CargarMasterULTRESEstacionesTelegram($DB){
        $ultres = json_decode(Cache::CargarRecursoCache($DB, "master_ultres_estaciones"), true);
        return $ultres;
    }

}
?>