<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class JasperReports
{

	/**
	 * Recibir listado de informes
	 */
	static function get_informes($usuario, $password){
		
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,Constants::BASE_URL_JASPER."/rest_v2/resources?type=reportUnit");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, $usuario.':'.$password);
		curl_setopt($ch, CURLOPT_TIMEOUT, 40); // 20 segundos de límite
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $output = curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
		
		if($statusCode == 200){
			// transformar el json de los inputs de jasper a uno más sencillo
			$ic = json_decode($output,true);
			return $ic;
		} else{
			echo "Status Code: " . $statusCode;
			return null;
		}
	}

	/**
	 * Recibir el informe de jasper
	 */
	static function get_informe($uri, $params, $usuario, $password, $tipo = 'pdf'){

		$uri = implode('/', array_map('urlencode', explode('/', $uri)));

		$endpoint = Constants::BASE_URL_JASPER.'/rest_v2/reports'.$uri.'.'.$tipo;

		$params['userLocale'] = "es"; // locale en Español para mostrar correctamente los decimales

		// preparar url de parametros
		$paramsToString = "";
		foreach($params as $keyParam => $param) {
			if($paramsToString != "") $paramsToString .= "&";
			if(is_array($param)) {
				$i = 0;
				foreach($param as $arrayParam) {
					if($i!=0) $paramsToString .= "&";
					$paramsToString .= $keyParam . "=" . implode('/', array_map('urlencode', explode('/', $arrayParam)));
					$i++;
				}
			} else {
				$paramsToString .= $keyParam . "=" . implode('/', array_map('urlencode', explode('/', $param)));
			}
		}
		$url = $endpoint . '?' . $paramsToString;

		$temp = tmpfile();
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, $usuario.':'.$password);
		curl_setopt($ch, CURLOPT_FILE, $temp);
		curl_setopt($ch, CURLOPT_TIMEOUT, 200); // 20 segundos de límite
        $output = curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

		if($statusCode == 200){
			$tmpfile_path = stream_get_meta_data($temp)['uri'];
			$tmpfile_content = file_get_contents($tmpfile_path);
			fclose($temp);
			return $tmpfile_content;
		} else {
			echo "Status Code: " . $statusCode;
			return null;
		}
	}

	/**
	 * Recibir el listado de inputs del informe y retornarlo
	 */
	static function get_inputControls_infome($uri, $usuario, $password){

		$uri = implode('/', array_map('urlencode', explode('/', $uri)));

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, Constants::BASE_URL_JASPER."/rest_v2/reports".$uri."/inputControls");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, $usuario.':'.$password);
		curl_setopt($ch, CURLOPT_TIMEOUT, 40); // 20 segundos de límite
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $output = curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
		
		if($statusCode == 200){
			// transformar el json de los inputs de jasper a uno más sencillo
			$ic = json_decode($output);
			return $ic;
		} else if($statusCode == 204) {
			return null;
		} else {
			echo "Status Code: " . $statusCode;
			return null;
		}
	}

	
	
}
