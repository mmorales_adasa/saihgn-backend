<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Meteoalerta
{

	static function getMeteoAlertas()
	{
		require_once APPPATH . '/libraries/visor/config.php';
		$query = "SELECT
				row_to_json(featurecollection) as result
			from
				(
				select
					'FeatureCollection' as type,
					json_agg(r) as data
				from
					(select
						'Feature' as type,
						(select row_to_json(prop) from 
							(select 
								(json_agg(
									json_build_object(
										'timestamp_inicio', tdm.timestamp_inicio || '', 
										'timestamp_fin', tdm.timestamp_fin || '',
										'timestamp_emision', tdm.timestamp_emision || '',
										'timestamp_grupo', tdm.timestamp_inicio || ' a ' || tdm.timestamp_fin,
										'fenomeno', tdm.fenomeno,
										'probabilidad', tdm.probabilidad,
										'descripcion', tdm.descripcion,
										'nivel', tdm.nivel) order by tdm.timestamp_inicio, tdm.timestamp_fin) filter 
											(where tdm.timestamp_inicio is not null and (tdm.timestamp_emision <= now() and now() <= tdm.timestamp_fin))
								) as alertas,
								tm.nom_prov, 
								tm.nom_z) as prop) as properties,
						ST_ASGeoJSON(tm.geom)::json as geometry
					from tbl_meteoalerta tm 
					left join tbl_dato_meteoalerta tdm 
					on cast(tm.cod_z as numeric) = tdm.cod_zona_meteoalerta 
					group by tm.cod_z, tm.nom_prov, tm.nom_z, tm.geom) as r) as featurecollection";
		$data = ConexionBD::EjecutarConsulta($query);
		return $data[0]['result'];
	}
}
