<?php
date_default_timezone_set("UTC");

class AEMET
{

	static function UltimosDatosAemet($DB){

		$campos = array(
			array("cod_campo"=> "cod_estacion", "id" => "cod_estacion"),
			array("cod_campo"=> "nombre", "id" => "nombre"),
			array("cod_campo"=> "PR", "id" => "pr"),
			array("cod_campo"=> "P1H", "id" => "p1h"),
			array("cod_campo"=> "P6H", "id" => "p6h"),
			array("cod_campo"=> "P12H", "id" => "p12h"),
			array("cod_campo"=> "P24H", "id" => "p24h"),
			array("cod_campo"=> "PRA", "id" => "pra"),
			array("cod_campo"=> "TA", "id" => "ta"),
			array("cod_campo"=> "timestamp", "id" => "timestamp")
		);

		$query = "WITH
			datos AS (
				SELECT 
					tda.cod_variable,
					tda.valor,
					ud.timestamp AS ultimo_timestamp,
					tda.timestamp AS minimo_timestamp
				FROM 
					tbl_dato_aemet tda,
					tbl_ultimo_dato_aemet ud
				WHERE (((tda.cod_variable)::text = (ud.cod_variable)::text) AND (split_part((ud.cod_variable)::text, '/'::text, 2) = 'PR'::text) AND (tda.timestamp > (ud.timestamp - INTERVAL '2 day')))
			),
			ultimos_datos AS (
				select
					cod_estacion,
					COALESCE(SUM(PR),null) AS \"pr\",
					COALESCE(SUM(TA),null) AS \"ta\",
					max(timestamp) as timestamp
				from (
					select 
					timestamp,
					split_part(cod_variable, '/', 1) AS cod_estacion,
					CASE WHEN cod_variable LIKE '%/PR' THEN valor END AS PR,
					CASE WHEN cod_variable LIKE '%/TA' THEN valor END AS TA
					from  tbl_ultimo_dato_aemet where cod_variable like '%/PR' or cod_variable like '%/TA'
				) as datos
				GROUP BY cod_estacion
			),
			estaciones AS (
				select cod_estacion, nombre from tbl_estacion_aemet where disponible = true and automatica = true
			),
			pra AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM
					datos
				WHERE datos.minimo_timestamp > date(datos.ultimo_timestamp)
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p1h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '1 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p6h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '6 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p12h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '12 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p24h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '24 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			)
				
			select
				estaciones.cod_estacion,
				estaciones.nombre,
				ultimos_datos.pr,
				p1h.valor as p1h,
				p6h.valor as p6h,
				p12h.valor as p12h,
				p24h.valor as p24h,
				pra.valor as pra,
				ultimos_datos.ta,
				ultimos_datos.timestamp
			from 
				estaciones
			JOIN ultimos_datos ON estaciones.cod_estacion = ultimos_datos.cod_estacion
			JOIN pra ON estaciones.cod_estacion = split_part(pra.cod_variable,'/',1) 
			JOIN p1h ON estaciones.cod_estacion = split_part(p1h.cod_variable,'/',1) 
			JOIN p6h ON estaciones.cod_estacion = split_part(p6h.cod_variable,'/',1) 
			JOIN p12h ON estaciones.cod_estacion = split_part(p12h.cod_variable,'/',1)
			JOIN p24h ON estaciones.cod_estacion = split_part(p24h.cod_variable,'/',1)
			ORDER BY estaciones.cod_estacion";

		$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);

		return array("encabezado" => $campos, "valores" => $datos);
	}

	static function UltimosDatosEmbalseAemet($idEstaciones, $idEmbalse, $DB){

		$campos = array(
			array("cod_campo"=> "cod_estacion", "id" => "cod_estacion"),
			array("cod_campo"=> "nombre", "id" => "nombre"),
			array("cod_campo"=> "creparto_thiessen", "id" => "creparto_thiessen"),
			array("cod_campo"=> "PR", "id" => "pr"),
			array("cod_campo"=> "P1H", "id" => "p1h"),
			array("cod_campo"=> "P6H", "id" => "p6h"),
			array("cod_campo"=> "P12H", "id" => "p12h"),
			array("cod_campo"=> "P24H", "id" => "p24h"),
			array("cod_campo"=> "PRA", "id" => "pra"),
			array("cod_campo"=> "TA", "id" => "ta"),
			array("cod_campo"=> "timestamp", "id" => "timestamp")
		);

		$query = "WITH
			datos AS (
				SELECT 
					tda.cod_variable,
					tda.valor,
					ud.timestamp AS ultimo_timestamp,
					tda.timestamp AS minimo_timestamp
				FROM 
					tbl_dato_aemet tda,
					tbl_ultimo_dato_aemet ud
				WHERE (((tda.cod_variable)::text = (ud.cod_variable)::text) AND (split_part((ud.cod_variable)::text, '/'::text, 2) = 'PR'::text) AND (tda.timestamp > (ud.timestamp - INTERVAL '2 day')))
			),
			ultimos_datos AS (
				select
					cod_estacion,
					COALESCE(SUM(PR),null) AS \"pr\",
					COALESCE(SUM(TA),null) AS \"ta\",
					max(timestamp) as timestamp
				from (
					select 
					timestamp,
					split_part(cod_variable, '/', 1) AS cod_estacion,
					CASE WHEN cod_variable LIKE '%/PR' THEN valor END AS PR,
					CASE WHEN cod_variable LIKE '%/TA' THEN valor END AS TA
					from  tbl_ultimo_dato_aemet where cod_variable like '%/PR' or cod_variable like '%/TA'
				) as datos
				GROUP BY timestamp,cod_estacion
			),
			estaciones AS (
				select aemet.cod_estacion, aemet.nombre, creparto_thiessen 
				from tbl_estacion_aemet aemet, tbl_aemet_creparto_embalse creparto 
				where creparto.cod_embalse = '".$idEmbalse."' AND aemet.cod_estacion = creparto.cod_estacion AND disponible = true AND automatica = true
			),
			pra AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM
					datos
				WHERE datos.minimo_timestamp > date(datos.ultimo_timestamp)
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p1h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '1 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p6h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '6 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p12h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '12 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			),
			p24h AS (
				SELECT 
					datos.cod_variable,
					sum(datos.valor) AS valor,
					datos.ultimo_timestamp,
					min(datos.minimo_timestamp) AS minimo_timestamp
				FROM 
					datos
				WHERE datos.minimo_timestamp > (datos.ultimo_timestamp - INTERVAL '24 hour')
				GROUP BY datos.cod_variable, datos.ultimo_timestamp
			)
				
			select
				estaciones.cod_estacion,
				estaciones.nombre,
				round( estaciones.creparto_thiessen, 6 ) as creparto_thiessen,
				ultimos_datos.pr,
				p1h.valor as p1h,
				p6h.valor as p6h,
				p12h.valor as p12h,
				p24h.valor as p24h,
				pra.valor as pra,
				ultimos_datos.ta,
				ultimos_datos.timestamp
			from 
				estaciones
			JOIN ultimos_datos ON estaciones.cod_estacion = ultimos_datos.cod_estacion
			JOIN pra ON estaciones.cod_estacion = split_part(pra.cod_variable,'/',1) 
			JOIN p1h ON estaciones.cod_estacion = split_part(p1h.cod_variable,'/',1) 
			JOIN p6h ON estaciones.cod_estacion = split_part(p6h.cod_variable,'/',1) 
			JOIN p12h ON estaciones.cod_estacion = split_part(p12h.cod_variable,'/',1)
			JOIN p24h ON estaciones.cod_estacion = split_part(p24h.cod_variable,'/',1)
			ORDER BY estaciones.cod_estacion";

		$todos_datos = ConexionBD::EjecutarConsultaConDB($DB, $query);
		$retorno_datos = [];

		// filtrar estaciones
		foreach($todos_datos as $lineaDato){
			$lineaDato["creparto_thiessen"] = number_format($lineaDato["creparto_thiessen"],6,",",".");
			if(in_array($lineaDato["cod_estacion"], $idEstaciones)) $retorno_datos[] = $lineaDato;
		}

		// crear promedios
		$promedios = ["cod_estacion"=> "TOTAL", "creparto_thiessen"=> 0, "nombre"=> "PROMEDIO", "p1h"=>0, "p6h"=>0,"p12h"=>0,"p24h"=>0,"pr"=>0,"pra"=>0,"ta"=>"","timestamp"=>""];

		foreach($todos_datos as $lineaDato){
			$promedios["creparto_thiessen"] += $lineaDato["creparto_thiessen"];
			$promedios["p1h"] +=($lineaDato["p1h"] * $lineaDato["creparto_thiessen"]);
			$promedios["p6h"] += ($lineaDato["p6h"] * $lineaDato["creparto_thiessen"]);
			$promedios["p12h"] += ($lineaDato["p12h"] * $lineaDato["creparto_thiessen"]);
			$promedios["p24h"] += ($lineaDato["p24h"] * $lineaDato["creparto_thiessen"]);
			$promedios["pr"] += ($lineaDato["pr"] * $lineaDato["creparto_thiessen"]);
			$promedios["pra"] += ($lineaDato["pra"] * $lineaDato["creparto_thiessen"]);
		}

		$promedios["creparto_thiessen"] = number_format($promedios["creparto_thiessen"],6,",",".");

		$retorno_datos[] = $promedios;

		return array("encabezado" => $campos, "valores" => $retorno_datos);
	}

	static function UltimosDatosEmbalsesAemet($DB){

		$campos = array(
			array("cod_campo"=> "zona", "id" => "zona"),
			array("cod_campo"=> "cod_estacion", "id" => "cod_estacion"),
			array("cod_campo"=> "nombre", "id" => "nombre"),
			array("cod_campo"=> "PI", "id" => "PI"),
			array("cod_campo"=> "PR", "id" => "PR"),
			array("cod_campo"=> "P1H", "id" => "P1H"),
			array("cod_campo"=> "P6H", "id" => "P6H"),
			array("cod_campo"=> "P12H", "id" => "P12H"),
			array("cod_campo"=> "P24H", "id" => "P24H"),
			array("cod_campo"=> "PRA", "id" => "PRA"),
			array("cod_campo"=> "timestamp", "id" => "timestamp")
		);

		$query = "WITH

		ultimos_datos AS (
			SELECT tbl_ultimo_dato_aemet.valor,
				tbl_ultimo_dato_aemet.cod_variable,
				tbl_ultimo_dato_aemet.timestamp,
				tbl_aemet_creparto_embalse.cod_embalse,
				tbl_aemet_creparto_embalse.creparto_thiessen
			FROM
				tbl_estacion_aemet eaemet,
				tbl_aemet_creparto_embalse,
				tbl_ultimo_dato_aemet
			WHERE
				split_part((tbl_ultimo_dato_aemet.cod_variable)::text, '/'::text, 2) = 'PR'::text
				AND split_part((tbl_ultimo_dato_aemet.cod_variable)::text, '/'::text, 1) = (tbl_aemet_creparto_embalse.cod_estacion)::text
				AND split_part((tbl_ultimo_dato_aemet.cod_variable)::text, '/'::text, 1) = eaemet.cod_estacion
				AND eaemet.disponible = true
		),
		
		datos AS (
			SELECT tda.cod_variable,
				tda.timestamp,
				ud.timestamp AS timestamp_mayor,
				tda.valor,
				ud.cod_embalse,
				ud.creparto_thiessen
			FROM
				tbl_dato_aemet tda, ultimos_datos ud
			WHERE (((tda.cod_variable)::text = (ud.cod_variable)::text) AND (tda.timestamp > (ud.timestamp - '24:00:00'::interval)))
		),
		
		p10m AS (
			SELECT ultimos_datos.cod_embalse,
				round(sum((ultimos_datos.valor * ultimos_datos.creparto_thiessen)), 1) AS valor,
				max(ultimos_datos.timestamp) AS timestamp
			FROM ultimos_datos
			GROUP BY ultimos_datos.cod_embalse
		),

		p1h AS (
			SELECT datos.cod_embalse,
				round(sum((datos.valor * datos.creparto_thiessen)), 1) AS valor
			FROM datos
			WHERE (datos.timestamp > (datos.timestamp_mayor - '01:00:00'::interval))
			GROUP BY datos.cod_embalse
		),

		p6h AS (
			SELECT datos.cod_embalse,
				round(sum((datos.valor * datos.creparto_thiessen)), 1) AS valor
			FROM datos
			WHERE (datos.timestamp > (datos.timestamp_mayor - '06:00:00'::interval))
			GROUP BY datos.cod_embalse
		),

		p12h AS (
			SELECT datos.cod_embalse,
				round(sum((datos.valor * datos.creparto_thiessen)), 1) AS valor
			FROM datos
			WHERE (datos.timestamp > (datos.timestamp_mayor - '12:00:00'::interval))
			GROUP BY datos.cod_embalse
		),

		p24h AS (
			SELECT datos.cod_embalse,
				round(sum((datos.valor * datos.creparto_thiessen)), 1) AS valor
			FROM datos
			GROUP BY datos.cod_embalse
		),

		pra AS (
			SELECT datos.cod_embalse,
				round(sum((datos.valor * datos.creparto_thiessen)), 1) AS valor
			FROM datos
			WHERE (datos.timestamp > date(datos.timestamp_mayor))
			GROUP BY datos.cod_embalse
		)
		
		SELECT
			e.zona,
			e.cod_estacion,
			e.nombre,
			(p10m.valor * (6)::numeric) AS \"PI\",
			p10m.valor AS \"PR\",
			p1h.valor AS \"P1H\",
			p6h.valor AS \"P6H\",
			p12h.valor AS \"P12H\",
			p24h.valor AS \"P24H\",
			pra.valor AS \"PRA\",
			p10m.timestamp
		FROM
			tbl_estacion e
			LEFT JOIN p10m ON e.cod_estacion = split_part(p10m.cod_embalse,'/',1)
			LEFT JOIN p1h ON e.cod_estacion = split_part(p1h.cod_embalse,'/',1)
			LEFT JOIN p6h ON e.cod_estacion = split_part(p6h.cod_embalse,'/',1)
			LEFT JOIN p12h ON e.cod_estacion = split_part(p12h.cod_embalse,'/',1)
			LEFT JOIN p24h ON e.cod_estacion = split_part(p24h.cod_embalse,'/',1)
			LEFT JOIN pra ON e.cod_estacion = split_part(pra.cod_embalse,'/',1)
		WHERE
			e.tipo = 'E'
			AND e.disponible = true
			ORDER BY e.orden, e.zona, e.cod_estacion;";

		$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);

		return array("encabezado" => $campos, "valores" => $datos);
	}
}
