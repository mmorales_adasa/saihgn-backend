<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Administrador
{
	static function getResumenEstaciones($confAlarmas, $tiposEstacion, $masterUltRes)
	{
		$return = array();
		$categoriasAlarmas = array();
		$catAlarmVal = array();

		if (isset($confAlarmas) && isset($tiposEstacion) && isset($masterUltRes)) {

			$time = time();

			// obtener las categorías y configuraciones de las alarmas de timestamp de los valores de ultimos resultados
			foreach($confAlarmas as $categoriaConfAlarmas){
				if(isset($categoriaConfAlarmas["tipo"])){
					$categoriasAlarmas[] = $categoriaConfAlarmas["tipo"];
					$catAlarmVal[$categoriaConfAlarmas["tipo"]] = $categoriaConfAlarmas;
				}
			}

			// si la id de la categoría de estación de ultres existe en la configuración de alarmas de timestamp se proporciona un conteo de los estados, 
			// sinó solo se manda la cantidad de estaciones
			foreach($tiposEstacion as $tipo){
			
			//foreach($masterUltRes as $id => $categoriaUltRes){

				//$nombre = (isset($tiposEstacion[$id]) && $tiposEstacion[$id]["descripcion"]) ? $tiposEstacion[$id]["descripcion"]:"";
				$id = $tipo['cod_tipo_estacion'];
				$nombre = $tipo['descripcion'];

				$aux = [
					"codigo"=>$id,
					"nombre"=>$nombre,
					"totalEstaciones"=>0,
					"contadoresConexion"=>[],
					"url"=>$id
				];

				if(isset($catAlarmVal[$id])){
					$aux["contadoresConexion"] = [
						["colour"=>$catAlarmVal[$id]["color_contador_1"],"contador"=>0],
						["colour"=>$catAlarmVal[$id]["color_contador_2"],"contador"=>0],
						["colour"=>$catAlarmVal[$id]["color_contador_3"],"contador"=>0],
						["colour"=>$catAlarmVal[$id]["color_contador_4"],"contador"=>0]
					];
				}
				
				$contarEstados = (in_array($id, $categoriasAlarmas)) ? true:false;
					
				

				if(isset($masterUltRes[$id]) && isset($masterUltRes[$id]['valores'])){

					$valores = $masterUltRes[$id]['valores'];
					
					foreach($valores as $datoCategoria){

						$aux["totalEstaciones"]++; // contamos la estación

						if($contarEstados && isset($datoCategoria["timestamp"]) && $datoCategoria["timestamp"]!="" && isset($catAlarmVal[$id])){
	
							$sdf = $time - strtotime($datoCategoria["timestamp"]);
	
							$status = null;
							// a partir de los segundos de diferencia se busca el estado correcto

							if($sdf >= $catAlarmVal[$id]["segundos_4"]) $status = "status4";
							else if($sdf >= $catAlarmVal[$id]["segundos_3"]) $status = "status3";
							else if($sdf >= $catAlarmVal[$id]["segundos_2"]) $status = "status2";
							else if($sdf >= $catAlarmVal[$id]["segundos_1"]) $status = "status1";

							//$aux["contadoresConexion"][0]["contador"] = 5;

							// contamos el estado de la estación
							if($status!=null){
								switch($status){
									case "status1":
										$aux["contadoresConexion"][0]["contador"]++;
										break;
									case "status2":
										$aux["contadoresConexion"][1]["contador"]++;
										break;
									case "status3":
										$aux["contadoresConexion"][2]["contador"]++;
										break;
									case "status4":
										$aux["contadoresConexion"][3]["contador"]++;
										break;
								}
							}
						}
					}	
				}
				$return[] = $aux;
			}
		}
		return $return;
	}

	static function getPermisosElementos($DB){

		$query1 = "select cod_nivel_acceso, nombre from tbl_sec_nivel_acceso order by cod_nivel_acceso";
		$niveles = ConexionBD::EjecutarConsultaConDB($DB, $query1);

		$query2 = "select cod_elemento, token_code from tbl_sec_elementos order by token_code";
		$elementos = ConexionBD::EjecutarConsultaConDB($DB, $query2);

		$query3 = "select cod_nivel_acceso, cod_elemento from tbl_sec_rel_nivel_acceso_elemento";
		$niveles_elementos = ConexionBD::EjecutarConsultaConDB($DB, $query3);

		$query4 = "select cod_usuario, cod_elemento from tbl_sec_rel_usuario_elemento where visible = 1";
		$usuario_elementos = ConexionBD::EjecutarConsultaConDB($DB, $query4);

		$query5 = "select cod_usuario, cod_nivel_acceso, username from tbl_sec_usuarios order by cod_nivel_acceso asc";
		$usuarios = ConexionBD::EjecutarConsultaConDB($DB, $query5);

		$encabezado = [""];
		$encabezadoNivel = ["Nivel"];
		foreach($niveles as $nivel){
			$encabezado[] = $nivel["nombre"] . " (" . $nivel["cod_nivel_acceso"] . ")";  
		}

		foreach($usuarios as $usuario){
			$encabezado[] = $usuario["username"] . " (" . $usuario["cod_nivel_acceso"] . ")"; 
			$encabezadoNivel[] = $usuario["cod_nivel_acceso"]; 
		}

		$lineas = [];
		foreach($elementos as $i => $elemento){

			$linea = [];
			$linea[] = $elemento["cod_elemento"];

			foreach($niveles as $nivel){

				$elementoEncontrado = false;

				foreach($niveles_elementos as $nivel_elemento){
					if($elemento["cod_elemento"] == $nivel_elemento["cod_elemento"] 
					&& $nivel["cod_nivel_acceso"] == $nivel_elemento["cod_nivel_acceso"]) $elementoEncontrado = true;
				}

				if($elementoEncontrado){
					$linea[] = 1;
				} else {
					$linea[] = 0;
				}

			}

			foreach($usuarios as $usuario){
				
				$elementoEncontrado = false;

				foreach($niveles_elementos as $nivel_elemento){
					if($elemento["cod_elemento"] == $nivel_elemento["cod_elemento"] 
					&& $usuario["cod_nivel_acceso"] == $nivel_elemento["cod_nivel_acceso"]) $elementoEncontrado = true;
				}

				foreach($usuario_elementos as $usuario_elemento){
					if($elemento["cod_elemento"] == $usuario_elemento["cod_elemento"] 
					&& $usuario["cod_usuario"] == $usuario_elemento["cod_usuario"]) $elementoEncontrado = true;
				}

				if($elementoEncontrado){
					$linea[] = 1;
				} else {
					$linea[] = 0;
				}
			
			}


			$lineas[] = $linea;
		};

		//
		foreach($lineas as &$linea){
			$val = 0;
			foreach($linea as $i => &$columna){
				if($i>0 && $i == 1) $val = $columna;
				if($i>0 && $i != 1 && $val == 1 && $columna == 0) $columna = 2;
			}
		}


		echo "<html><head>
				<style>
					body{font-size: 12px;}
					.tableFixHead          { overflow: auto; height: 100%; }
					.tableFixHead thead { position: sticky; top: 0; z-index: 2; }
					.sticky { font-size: 12px; background-color:#fff; position: sticky; left: 0; z-index: 1; }
					
				
					th     { background:#eee; }
				</style>
				</head>";

		echo "<body><div class='tableFixHead'>
				<table>";
					echo "<thead><tr>";
						foreach($encabezado as $enc){
							echo "<th style='writing-mode: vertical-lr; background-color:#aad4ff;'>".$enc."</th>";
						}
					echo "</tr>";
					
					/*echo "<tr>";
						foreach($niveles as $enc){
							echo "<th style='background-color:#aad4ff;'></th>";
						}
						foreach($encabezadoNivel as $enc){
							echo "<th>".$enc."</th>";
						}
					echo "</tr>";*/
					
					echo "</thead>";
				echo "<tbody>";
				foreach($lineas as $lin){
					echo "<tr>";
					foreach($lin as $i => $col){

						if($i==0) echo "<td class='sticky'>".$col."</td>";

						else {
							if($col == 1 ) echo "<td style='background-color:green; text-align:center'> </td>";
							else if($col == 2 ) echo "<td style='background-color:orange; text-align:center'> </td>";
							else if($col == 0 ) echo "<td style='background-color:#dfdfdf;'> </td>";
							else echo "<td>".$col."</td>";
						}
					}

					echo "</tr>";
				}
			echo "</tbody>";
		echo "</table></div></body></html>";

		//var_dump($lineas);

	}
}
