<?php
require_once APPPATH.'/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Evolucion {

    static function getEvolucionEmbalseVolumenAnual($codEMB, $fechaIni, $fechaFin){

        $where1 = "";
        $where2 = "";

        if($fechaIni != null && $fechaFin != null){
            $where1 = "and anyo >= date_part('year', to_timestamp(".$fechaIni.")) and anyo <= date_part('year', to_timestamp(".$fechaFin."))";
            $where2 = "WHERE fecha >= to_timestamp(".$fechaIni.") and fecha <= to_timestamp(".$fechaFin.")";
        }

        $query = "SELECT
        extract(epoch from fecha) as timestamp,
        COALESCE(SUM(VE1),0) AS \"VE1\"
        FROM(
            SELECT
                fecha,
                CASE WHEN cod_variable LIKE '%/VE1' THEN valor_media END AS VE1
            FROM (
                select to_date(LPAD(anyo::text, 4, '0'), 'YYYY') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_anyos where (cod_variable like '".$codEMB."/%') 
                ".$where1."
            ) AS valores
            ".$where2."
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionEmbalseDetalleAnual($codEMB, $fechaIni, $fechaFin){
        
        $where1 = "";
        $where2 = "";

        if($fechaIni != null && $fechaFin != null){
            $where1 = "and anyo >= date_part('year', to_timestamp(".$fechaIni.")) and anyo <= date_part('year', to_timestamp(".$fechaFin."))";
            $where2 = "WHERE fecha >= to_timestamp(".$fechaIni.") and fecha <= to_timestamp(".$fechaFin.")";
        }

        $query = "SELECT
        extract(epoch from fecha) as timestamp,
        COALESCE(SUM(EV),null) AS \"EV\",
        COALESCE(SUM(PRA),null) AS \"PRA\",
        COALESCE(SUM(NE1),null) AS \"NE1\",
        COALESCE(SUM(VE1),null) AS \"VE1\",
        COALESCE(SUM(AFT),null) AS \"AFT\",
        COALESCE(SUM(AAT),null) AS \"AAT\",
        COALESCE(SUM(AWT),null) AS \"AWT\"
        FROM(
            SELECT
                fecha,
                CASE WHEN cod_variable LIKE '%/EV' THEN valor_acum END AS EV,
                CASE WHEN cod_variable LIKE '%/PRA' THEN valor_acum END AS PRA,
                CASE WHEN cod_variable LIKE '%/NE1' THEN valor_media END AS NE1,
                CASE WHEN cod_variable LIKE '%/VE1' THEN valor_media END AS VE1,
                CASE WHEN cod_variable LIKE '%/AFT' THEN valor_acum END AS AFT,
                CASE WHEN cod_variable LIKE '%/AAT' THEN valor_acum END AS AAT,
                CASE WHEN cod_variable LIKE '%/AWT' THEN valor_acum END AS AWT
            FROM (
                select to_date(LPAD(anyo::text, 4, '0'), 'YYYY') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_anyos where (cod_variable like '".$codEMB."/%') 
                ".$where1."
                AND anyo >= 2011
            ) AS valores
            ".$where2."
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionEmbalseDetalleMensual($codEMB, $fechaIni, $fechaFin){
        
        $query = "SELECT
        extract(epoch from fecha) as timestamp,
        COALESCE(SUM(EV),null) AS \"EV\",
        COALESCE(SUM(PRA),null) AS \"PRA\",
        COALESCE(SUM(NE1),null) AS \"NE1\",
        COALESCE(SUM(VE1),null) AS \"VE1\",
        COALESCE(SUM(AFT),null) AS \"AFT\",
        COALESCE(SUM(AAT),null) AS \"AAT\",
        COALESCE(SUM(AWT),null) AS \"AWT\"
        FROM(
            SELECT
                fecha,
                CASE WHEN cod_variable LIKE '%/EV' THEN valor_acum END AS EV,
                CASE WHEN cod_variable LIKE '%/PRA' THEN valor_acum END AS PRA,
                CASE WHEN cod_variable LIKE '%/NE1' THEN valor_media END AS NE1,
                CASE WHEN cod_variable LIKE '%/VE1' THEN valor_media END AS VE1,
                CASE WHEN cod_variable LIKE '%/AFT' THEN valor_acum END AS AFT,
                CASE WHEN cod_variable LIKE '%/AAT' THEN valor_acum END AS AAT,
                CASE WHEN cod_variable LIKE '%/AWT' THEN valor_acum END AS AWT
            FROM (
                select to_date(concat(LPAD(anyo::text, 4, '0'), LPAD(mes::text, 2, '0')), 'YYYYMM') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_meses where cod_variable like '".$codEMB."/%'
                and anyo >= date_part('year', to_timestamp(".$fechaIni.")) and anyo <= date_part('year', to_timestamp(".$fechaFin."))
            ) AS valores
            WHERE
                fecha >= to_timestamp(".$fechaIni.") and fecha <= to_timestamp(".$fechaFin.")
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionEmbalseDetalleSemanal($codEMB, $fechaIni, $fechaFin){
        
        $query = "SELECT
        extract(epoch from fecha) as timestamp,
        COALESCE(SUM(EV),null) AS \"EV\",
        COALESCE(SUM(PRA),null) AS \"PRA\",
        COALESCE(SUM(NE1),null) AS \"NE1\",
        COALESCE(SUM(VE1),null) AS \"VE1\",
        COALESCE(SUM(AFT),null) AS \"AFT\",
        COALESCE(SUM(AAT),null) AS \"AAT\",
        COALESCE(SUM(AWT),null) AS \"AWT\"
        FROM(
            SELECT
                fecha,
                CASE WHEN cod_variable LIKE '%/EV' THEN valor_acum END AS EV,
                CASE WHEN cod_variable LIKE '%/PRA' THEN valor_acum END AS PRA,
                CASE WHEN cod_variable LIKE '%/NE1' THEN valor_media END AS NE1,
                CASE WHEN cod_variable LIKE '%/VE1' THEN valor_media END AS VE1,
                CASE WHEN cod_variable LIKE '%/AFT' THEN valor_acum END AS AFT,
                CASE WHEN cod_variable LIKE '%/AAT' THEN valor_acum END AS AAT,
                CASE WHEN cod_variable LIKE '%/AWT' THEN valor_acum END AS AWT
            FROM (
                select num_semana, to_date(concat(LPAD(anyo::text, 4, '0'), LPAD(num_semana::text, 2, '0')), 'iyyyiw') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_semanas where cod_variable like '".$codEMB."/%'
                and anyo >= date_part('year', to_timestamp(".$fechaIni.")) and anyo <= date_part('year', to_timestamp(".$fechaFin."))
                and num_semana != 53
            ) AS valores
            WHERE
                fecha >= to_timestamp(".$fechaIni.") and fecha <= to_timestamp(".$fechaFin.")
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionEmbalseDetalleDiario($codEMB, $fechaIni, $fechaFin){
        
        $query = "SELECT
        extract(epoch from fecha) as timestamp,
        COALESCE(SUM(EV),null) AS \"EV\",
        COALESCE(SUM(PRA),null) AS \"PRA\",
        COALESCE(SUM(NE1),null) AS \"NE1\",
        COALESCE(SUM(VE1),null) AS \"VE1\",
        COALESCE(SUM(AFT),null) AS \"AFT\",
        COALESCE(SUM(AAT),null) AS \"AAT\",
        COALESCE(SUM(AWT),null) AS \"AWT\"
        FROM(
            SELECT
                fecha,
                CASE WHEN cod_variable LIKE '%/EV' THEN valor_acum END AS EV,
                CASE WHEN cod_variable LIKE '%/PRA' THEN valor_acum END AS PRA,
                CASE WHEN cod_variable LIKE '%/NE1' THEN valor_media END AS NE1,
                CASE WHEN cod_variable LIKE '%/VE1' THEN valor_media END AS VE1,
                CASE WHEN cod_variable LIKE '%/AFT' THEN valor_acum END AS AFT,
                CASE WHEN cod_variable LIKE '%/AAT' THEN valor_acum END AS AAT,
                CASE WHEN cod_variable LIKE '%/AWT' THEN valor_acum END AS AWT
            FROM (
                select to_date(concat(LPAD(anyo::text, 4, '0'), LPAD(mes::text, 2, '0'), LPAD(dia::text, 2, '0')), 'YYYYMMDD') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_dias
                where cod_variable like '".$codEMB."/%' and data_medicion >=  to_timestamp(".$fechaIni.")  and data_medicion <= to_timestamp(".$fechaFin.")
            ) AS valores
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    static function getEvolucionEmbalseDetalleHorario($codEMB, $fechaIni, $fechaFin){
        
        $query = "SELECT
        extract(epoch from fecha) as timestamp,
        COALESCE(SUM(EV),null) AS \"EV\",
        COALESCE(SUM(PRA),null) AS \"PRA\",
        COALESCE(SUM(NE1),null) AS \"NE1\",
        COALESCE(SUM(VE1),null) AS \"VE1\",
        COALESCE(SUM(AFT),null) AS \"AFT\",
        COALESCE(SUM(AAT),null) AS \"AAT\",
        COALESCE(SUM(AWT),null) AS \"AWT\"
        FROM(
            SELECT
                fecha,
                CASE WHEN cod_variable LIKE '%/EV' THEN valor_acum END AS EV,
                CASE WHEN cod_variable LIKE '%/PRA' THEN valor_acum END AS PRA,
                CASE WHEN cod_variable LIKE '%/NE1' THEN valor_media END AS NE1,
                CASE WHEN cod_variable LIKE '%/VE1' THEN valor_media END AS VE1,
                CASE WHEN cod_variable LIKE '%/AFT' THEN valor_acum END AS AFT,
                CASE WHEN cod_variable LIKE '%/AAT' THEN valor_acum END AS AAT,
                CASE WHEN cod_variable LIKE '%/AWT' THEN valor_acum END AS AWT
            FROM (
                select to_timestamp(concat(LPAD(anyo::text, 4, '0'), LPAD(mes::text, 2, '0'), LPAD(dia::text, 2, '0'), LPAD(hora::text, 2, '0')), 'YYYYMMDDHH24') fecha, cod_variable, valor_media, valor_acum 
                from tbl_olap_medidas_horas
                where cod_variable like '".$codEMB."/%' and data_medicion >= to_timestamp(".$fechaIni.") and data_medicion <= to_timestamp(".$fechaFin.")
            ) AS valores
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";

        $dataVariables = ConexionBD::EjecutarConsulta($query);

        return $dataVariables;
    }

    // insertar coronación y aliviadero

    static function insertarCORALIV($datos, $codEMB){

        $query2 = "select cod_nivel, nivel, volumen, fecha_inicio, fecha_fin from tbl_nivel where cod_nivel like '".$codEMB."/ALIV' OR cod_nivel like '".$codEMB."/COR' OR cod_nivel like '".$codEMB."/RE1' OR cod_nivel like '".$codEMB."/RE2'";
        $dataNiveles = ConexionBD::EjecutarConsulta($query2);

        $niveles = Array("NCOR"=>"","VCOR"=>"");
        foreach ($dataNiveles as $lineaNiveles){
            switch($lineaNiveles['cod_nivel']){
                case $codEMB."/COR":
                    $niveles["NCOR"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VCOR"] = round($lineaNiveles['volumen'], 2);
                    break;
                case $codEMB."/ALIV":
                    $niveles["NALIV"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VALIV"] = round($lineaNiveles['volumen'], 2);
                    break;
                case $codEMB."/RE1":
                    $niveles["NRE1"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VRE1"] = round($lineaNiveles['volumen'], 2);
                    $niveles["fecha_inicio_RE1"] = $lineaNiveles['fecha_inicio'];
                    $niveles["fecha_fin_RE1"] = $lineaNiveles['fecha_fin'];
                    break;
                case $codEMB."/RE2":
                    $niveles["NRE2"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VRE2"] = round($lineaNiveles['volumen'], 2);
                    $niveles["fecha_inicio_RE2"] = $lineaNiveles['fecha_inicio'];
                    $niveles["fecha_fin_RE2"] = $lineaNiveles['fecha_fin'];
                    break;
            }
        }

        $dataRetorno = [];

        // Insertar los datos estaticos de la segunda query a los valores dinámicos por cada fecha
        foreach ($datos as $lineaDatos){

            // se reutiliza el bucle para convertir datos de str a int
            $lineaDatos["timestamp"] = intval($lineaDatos["timestamp"]);
            $lineaDatos["PRA"] = round($lineaDatos["PRA"], 1);
            $lineaDatos["NE1"] = round($lineaDatos["NE1"], 2);
            $lineaDatos["VE1"] = round($lineaDatos["VE1"], 1);
            $lineaDatos["AFT"] = round($lineaDatos["AFT"], 5);
            $lineaDatos["AAT"] = round($lineaDatos["AAT"], 5);
            $lineaDatos["AWT"] = round($lineaDatos["AWT"], 5);
            $lineaDatos["NCOR"] = $niveles["NCOR"];
            $lineaDatos["VCOR"] = $niveles["VCOR"];

            if(isset($niveles["NALIV"])) $lineaDatos["NALIV"] = $niveles["NALIV"];
            if(isset($niveles["NALIV"])) $lineaDatos["VALIV"] = $niveles["VALIV"];

            // aplicar la coronación si existe (RE1 o RE2 según la fecha) y la diferencia de volumen del embalse y de la coronación
            if(isset($niveles["fecha_inicio_RE1"]) && isset($niveles["fecha_fin_RE1"])){

                // obtener el año del timestamp 
                $valTimestamp = $lineaDatos['timestamp']; // timestamp en fecha unix
                $dt = new DateTime("@$valTimestamp");
                $anyo_dato = $dt->format('Y');

                $fechaInicioRE1Array = explode("-",$niveles["fecha_inicio_RE1"]);
                $fecha_inicio_RE1_epoch = strtotime($anyo_dato."-".$fechaInicioRE1Array[1]."-".$fechaInicioRE1Array[2]); 
                $fechaFinRE1Array = explode("-",$niveles["fecha_fin_RE1"]);
                $fecha_fin_RE1_epoch = strtotime($anyo_dato."-".$fechaFinRE1Array[1]."-".$fechaFinRE1Array[2]); 

                $fin_anyo = strtotime($anyo_dato."-12-31"); // final de año
                $inicio_anyo = strtotime($anyo_dato."-01-01"); // principios de año

                // si la fecha inicial es superior a la final significa que no existe un rango dentro del mismo año

                if($fecha_inicio_RE1_epoch > $fecha_fin_RE1_epoch){

                    if( 
                        ( $valTimestamp >= $fecha_inicio_RE1_epoch && $valTimestamp <= $fin_anyo ) || 
                        ( $valTimestamp >= $inicio_anyo && $valTimestamp <= $fecha_fin_RE1_epoch )
                    ){
                        $lineaDatos["NRE"] = $niveles["NRE1"];
                        $lineaDatos["VRED"] =  $niveles["VRE1"]  -  $lineaDatos["VE1"] ;
                    } 

                } else {

                    if($valTimestamp >= $fecha_inicio_RE1_epoch && $valTimestamp <= $fecha_fin_RE1_epoch){
                        $lineaDatos["NRE"] = $niveles["NRE1"];
                        $lineaDatos["VRED"] =  $niveles["VRE1"]  -  $lineaDatos["VE1"] ;
                    }

                }

            }

            if(isset($niveles["fecha_inicio_RE2"]) && isset($niveles["fecha_fin_RE2"])){

                // obtener el año del timestamp 
                $valTimestamp = $lineaDatos['timestamp']; // timestamp en fecha unix
                $dt = new DateTime("@$valTimestamp");
                $anyo_dato = $dt->format('Y');

                $fechaInicioRE2Array = explode("-",$niveles["fecha_inicio_RE2"]);
                $fecha_inicio_RE2_epoch = strtotime($anyo_dato."-".$fechaInicioRE2Array[1]."-".$fechaInicioRE2Array[2]); 
                $fechaFinRE2Array = explode("-",$niveles["fecha_fin_RE2"]);
                $fecha_fin_RE2_epoch = strtotime($anyo_dato."-".$fechaFinRE2Array[1]."-".$fechaFinRE2Array[2]); 

                $fin_anyo = strtotime($anyo_dato."-12-31"); // final de año
                $inicio_anyo = strtotime($anyo_dato."-01-01"); // principios de año

                // si la fecha inicial es superior a la final significa que no existe un rango dentro del mismo año
                if($fecha_inicio_RE2_epoch > $fecha_fin_RE2_epoch){

                    if( 
                        ( $valTimestamp >= $fecha_inicio_RE2_epoch && $valTimestamp <= $fin_anyo ) || 
                        ( $valTimestamp >= $inicio_anyo && $valTimestamp <= $fecha_fin_RE2_epoch )
                    ){
                        $lineaDatos["NRE"] = $niveles["NRE2"];
                        $lineaDatos["VRED"] = $niveles["VRE2"] - $lineaDatos["VE1"];
                    } 

                } else {

                    if($valTimestamp >= $fecha_inicio_RE2_epoch && $valTimestamp <= $fecha_fin_RE2_epoch){
                        $lineaDatos["NRE"] = $niveles["NRE2"];
                        $lineaDatos["VRED"] = $niveles["VRE2"] - $lineaDatos["VE1"];
                    }

                }

            }

            $dataRetorno[] = $lineaDatos;
        }

        return $dataRetorno;

    }

}