<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class PiezometrosManuales
{

	static function getPiezosManuales($DB)
	{
		$query = "select cod_estacion, nombre from tbl_estacion where tipo = 'PZM' order by cod_estacion asc";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);
		$datosRetorno = array("encabezado" => ["cod_estacion", "nombre"], "valores" => $data);
		return $datosRetorno;
	}

	static function getPiezosListForm()
	{
		$query = "select cod_estacion as cod_piezo, municipio from tbl_estacion where tipo = 'PZM' order by cod_estacion asc";
		$data = ConexionBD::EjecutarConsulta($query);
		return $data;
	}

	static function savePiezosManualesV2($datos)
	{
		foreach ($datos as $piezo) {
			PiezometrosManuales::addPiezoHist($piezo);
		}
		return $datos;
	}

	static function deletePiezosManualesV2($datos)
	{
		foreach ($datos as $piezo) {
			PiezometrosManuales::deletePiezoHist($piezo);
		}
		return $datos;
	}

	static function addPiezoHist($piezo)
	{
		$piezoCode = $piezo['cod_piezo'] ?? [];
		$new = $piezo['newValues'];
		$existing = $piezo['existingValues'];

		if (count($new) > 0) {
			foreach ($new as &$datoNew) {
				if (isset($datoNew["fecha"])) {
					$datoNew["timestamp"] = $datoNew["fecha"];
					unset($datoNew["fecha"]);
				}
				if (isset($datoNew["nivel"])) {
					if(strpos($datoNew["nivel"], ',')) {
						$datoNew["valor"] = str_replace(",", ".", $datoNew["nivel"]);
					} else {
						$datoNew["valor"] = $datoNew["nivel"];
					}
					unset($datoNew["nivel"]);
				}
				$datoNew["cod_variable"] = $piezoCode . "/CP";
			}
			foreach($new as $value) {
				PiezometrosManuales::insertPiezoManual($value["cod_variable"], $value["timestamp"], $value["valor"], $value["observaciones"]);
			}
		}

		if(count($existing) > 0) {
			foreach ($existing as &$datoExisting) {
				if (isset($datoExisting["fecha"])) {
					$datoExisting["timestamp"] = $datoExisting["fecha"];
					unset($datoExisting["fecha"]);
				}
				if (isset($datoExisting["nivel"])) {
					if(strpos($datoExisting["nivel"], ',')) {
						$datoExisting["valor"] = str_replace(",", ".", $datoExisting["nivel"]);
					} else {
						$datoExisting["valor"] = $datoExisting["nivel"];
					}
					unset($datoExisting["nivel"]);
				}
				$datoExisting["cod_variable"] = $piezoCode . "/CP";
			}
			foreach($existing as $value) {
				PiezometrosManuales::updatePiezoManual($value["cod_variable"], $value["timestamp"], $value["valor"], $value["observaciones"]);
			}
		}
		return null;
	}

	static function deletePiezoHist($piezo)
	{
		$piezoCode = $piezo['cod_piezo'] ?? [];
		$existing = $piezo['existingValues'] ?? [];

		if (count($existing) > 0) {
			foreach ($existing as &$datoExisting) {
				if (isset($datoExisting["fecha"])) {
					$datoExisting["timestamp"] = $datoExisting["fecha"];
					unset($datoExisting["fecha"]);
				}
				if (isset($datoExisting["nivel"])) {
					if(strpos($datoExisting["nivel"], ',')) {
						$datoExisting["valor"] = str_replace(",", ".", $datoExisting["nivel"]);
					} else {
						$datoExisting["valor"] = $datoExisting["nivel"];
					}
					unset($datoExisting["nivel"]);
				}
				$datoExisting["cod_variable"] = $piezoCode . "/CP";
				PiezometrosManuales::deletePiezoManual($datoExisting["cod_variable"], $datoExisting["timestamp"], $datoExisting["valor"]);
			}
		}
		return null;
	}

	static function insertPiezoManual($piezoCode, $fecha, $valor, $observaciones) {
		$query = "INSERT INTO tbl_dato_piezo_manual (cod_variable, timestamp, valor, observaciones) VALUES ('$piezoCode', '$fecha', $valor, '$observaciones')";
		return ConexionBD::EjecutarConsulta($query, false);
	}

	static function updatePiezoManual($piezoCode, $fecha, $valor, $observaciones) {
		$query = "UPDATE tbl_dato_piezo_manual SET valor = $valor, observaciones = '$observaciones' WHERE cod_variable = '$piezoCode' AND timestamp = '$fecha'";
		return ConexionBD::EjecutarConsulta($query, false);
	}

	static function deletePiezoManual($piezoCode, $fecha, $valor) {
		$query = "DELETE FROM tbl_dato_piezo_manual WHERE cod_variable LIKE '" . $piezoCode . "%' AND timestamp = '$fecha' AND valor= $valor";
		return ConexionBD::EjecutarConsulta($query, false);
	}

	static function deleteRecentPiezoHist($piezoCode)
	{
		$query = "delete from tbl_dato_piezo_manual where cod_variable like '" . $piezoCode . "%' and timestamp > NOW() - INTERVAL '2 YEAR'";
		return ConexionBD::EjecutarConsulta($query, false);
	}

	static function getPiezosManualesHist($idPiezoManual)
	{
		$query = "select timestamp, valor as \"CP\", observaciones from tbl_dato_piezo_manual where cod_variable like '" . $idPiezoManual . "%' and timestamp > NOW() - INTERVAL '2 YEAR' order by timestamp desc limit 10";
		$data = ConexionBD::EjecutarConsulta($query);
		$datosRetorno = array("encabezado" => ["timestamp", "CP", "observaciones"], "valores" => $data);
		return $datosRetorno;
	}

}
