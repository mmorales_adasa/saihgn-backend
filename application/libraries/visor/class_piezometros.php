<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Piezometros
{

    static function getEvolucionPiezometro($ids, $fechaIni, $fechaFin)
    {

        $idsDiezminutal = [];
        $periodo = 600;

        $idsDBCPDiezminutal = ConexionBD::EjecutarConsulta("select cod_estacion from tbl_estacion where tipo like 'PZ' and disponible = true AND cod_estacion not like '04.%'");
        foreach($idsDBCPDiezminutal as $id){
            $idsDiezminutal[] = str_replace(" ", "", $id["cod_estacion"]);
        }

        $idsStringDatosPiezo = "";
        $idsString = "";
        $idsStringTr = "";

        $useTblDatePiezo = false;
        $useDiezminutales = false;

        foreach ($ids as $id) {
            $id = str_replace(" ", "", $id);

            if ($idsString != "") $idsString .= " OR ";
            $idsString .= "cod_variable like '" . $id . "/CP'";

            if (!in_array($id, $idsDiezminutal)) {
                $useTblDatePiezo = true;
                if ($idsStringDatosPiezo != "") $idsStringDatosPiezo .= " OR ";
                $idsStringDatosPiezo .= "cod_variable like '" . $id . "/CP'";
            }
            if (in_array($id, $idsDiezminutal)) {
                $useDiezminutales = true;
                $id = str_replace(" ", "", $id);
                if ($idsStringTr != "") $idsStringTr .= " OR ";
                $idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/CP'";
            }
        }

        $query = "with ";

            if ($useDiezminutales) {

                $query .= "medidas AS (
                        select cod_variable, valor, timestamp_medicion as timestamp 
                        from tbl_hist_medidas 
                        where timestamp_medicion >= '" . $fechaIni . "' 
                        and timestamp_medicion <= '" . $fechaFin . "' 
                        and (" . $idsString . ")
                        and ind_validacion = 'S'
                        and (extract(epoch from DATE_TRUNC('minute',timestamp_medicion))/" . $periodo . " - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp_medicion))/" . $periodo . "))=0
                    ),
                    max_medidas AS (
                        select cod_variable, max(timestamp) from medidas group by cod_variable
                    ),";
            }

            $query .= "datostrymedidas AS (";

            if ($useTblDatePiezo) {

                $query .= "(select
                    timestamp,
                    cod_variable,
                    case when cod_variable like '%/CP' AND validez = 1 then valor ELSE NULL end valor
                    from tbl_dato_piezo
                    where timestamp >= '" . $fechaIni . "' and timestamp <= '" . $fechaFin . "'
                    and cod_variable like '%CP'
                    and (" . $idsStringDatosPiezo . ")
                    and validez = 1
                    and (extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . " - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . "))=0)";

            }

            if ($useDiezminutales) {

                if ($useTblDatePiezo) $query .= " UNION ";

                $query .= "(select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                    left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                    where timestamp >= '" . $fechaIni . "' 
                    and timestamp <= '" . $fechaFin . "'
                    and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                    and (" . $idsStringTr . ")
                    and (extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . " - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/" . $periodo . "))=0
                    union
                    select cod_variable, valor, timestamp from medidas)";
            }

            $query .= ")
        
            select
                timestamp,
                estacion,
                coalesce(SUM(CP),null) AS \"CP\"
                from(
                    select
                    timestamp,
                    split_part(cod_variable, '/', 1) as estacion,
                    case when cod_variable like '%/CP' then valor end as CP
                    from datostrymedidas
                ) as valores
                group by estacion, timestamp
                order by timestamp asc";

        $data = ConexionBD::EjecutarConsulta($query);

        $return = array();

        if ($data) {

            // agrupar por timestamp los valores de estación y calcular variables
            foreach ($data as $i => $row) {
                $row['CP'] = (float)$row['CP'];

                $return[$row["estacion"]][] = array(
                    "timestamp" => $row["timestamp"],
                    "CP" => round($row["CP"], 2),
                );
            }
        }
        return $return;
    }
}
