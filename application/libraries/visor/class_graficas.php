<?php 
require_once APPPATH.'/libraries/visor/class_conexion.php';
 
class Graficas {
    static function getVolumenMaxMinDiarioEmbalse($idEmbalse,$fechaIni,$fechaFin){
        $query = "select days.day, minutalesIni.valor iniVal, days.minVal, days.maxVal, minutalesFin.valor finVal 
                    from (
                        select date_trunc('day', datos.timestamp) \"day\", max(datos.valor) maxVal, min(datos.valor) minVal, min(datos.id) idIni, max(datos.id) idFin
                        from (select id, valor,timestamp from tbl_dat_minutales WHERE cod_variable like '".$idEmbalse."/VE1' AND TIMESTAMP BETWEEN '".$fechaIni."' AND '".$fechaFin."') as datos 
                        group by 1
                    ) days
                    join tbl_dat_minutales minutalesIni on days.idini = minutalesIni.id
                    join tbl_dat_minutales minutalesFin on days.idfin = minutalesFin.id
                    order by day asc";
        $data = ConexionBD::EjecutarConsulta($query);
        return $data;
    }

    static function getVolumenMaxMinSemanalEmbalse($idEmbalse,$fechaIni,$fechaFin){
        $query = "select weeks.week, minutalesIni.valor iniVal, weeks.minVal, weeks.maxVal, minutalesFin.valor finVal 
                    from (
                        select date_trunc('week', datos.timestamp) \"week\", max(datos.valor) maxVal, min(datos.valor) minVal, min(datos.id) idIni, max(datos.id) idFin
                        from (select id, valor,timestamp from tbl_dat_minutales WHERE cod_variable like '".$idEmbalse."/VE1' AND TIMESTAMP BETWEEN '".$fechaIni."' AND '".$fechaFin."') as datos 
                        group by 1
                    ) weeks
                    join tbl_dat_minutales minutalesIni on weeks.idini = minutalesIni.id
                    join tbl_dat_minutales minutalesFin on weeks.idfin = minutalesFin.id
                    order by week asc";
        $data = ConexionBD::EjecutarConsulta($query);
        return $data;
    }

    /**
     * Volumen diario del embalse
     */
    static function getVolumenDiarioEmbalse($idEmbalse,$fechaIni,$fechaFin){
        $query = "select data_medicion as day, valor_media as finVal from tbl_olap_medidas_dias where cod_variable like '".$idEmbalse."/VE1' and data_medicion BETWEEN '".$fechaIni."' AND '".$fechaFin."' ORDER BY data_medicion ASC";
        $data = ConexionBD::EjecutarConsulta2($query);
        return $data;
    }

    /**
     *  Precipitación acumulada e intensidad (a partir de la acumulada) de precipitación de la estación
     */
    static function getPrecipitacionEstacion($idEstacion, $fechaIni){
        date_default_timezone_set("UTC");
        
        $ids = [];
        $ids[] = $idEstacion;
        $fechaFin = date('Y-m-d H:i');
        $periodo = 3600;

        $idsString = "";
        foreach ($ids as $id) {
            $id = str_replace(" ", "", $id);
            if ($idsString != "") $idsString .= " OR ";
            $idsString .= "cod_variable like '" . $id . "/%'";
        }

        $idsStringTr = "";
        foreach ($ids as $id) {
            $id = str_replace(" ", "", $id);
            if ($idsStringTr != "") $idsStringTr .= " OR ";
            $idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/%'";
        }
            
        $query = "
        with 
            medidas AS (
                select cod_variable, valor, timestamp_medicion as timestamp 
                from tbl_hist_medidas 
                where timestamp_medicion >= '" . $fechaIni . "' 
                and timestamp_medicion <= '" . $fechaFin . "' 
                and (" . $idsString . ")
                and ind_validacion = 'S'
                and (extract(epoch from timestamp_medicion)/" . $periodo . " - TRUNC(extract(epoch from timestamp_medicion)/" . $periodo . "))=0
            ),
            max_medidas AS (
                select cod_variable, max(timestamp) from medidas group by cod_variable
            ),
            datostrymedidas AS (
                select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                where timestamp >= '" . $fechaIni . "' 
                and timestamp <= '" . $fechaFin . "'
                and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                and (" . $idsStringTr . ")
                and (extract(epoch from timestamp)/" . $periodo . " - TRUNC(extract(epoch from timestamp)/" . $periodo . "))=0
                union
                select cod_variable, valor, timestamp from medidas
            )
        
            select
                timestamp,
                estacion,
                coalesce(SUM(PRA),null) AS \"PRA\"
                from(
                    select
                    timestamp,
                    split_part(cod_variable, '/', 1) as estacion,
                    case when cod_variable like '%/PRA' then valor end as PRA
                    from datostrymedidas
                ) as valores
                group by estacion, timestamp
                order by timestamp asc";

        $data = ConexionBD::EjecutarConsulta($query);

        $timestampAnterior = null;
        $PRAAnterior = null;

        $return = Array(); // array de retorno

        if(isset($data)){
            // por cada línea calcular la intensidad de precipitación
            foreach($data as $row){
                // Retorno de timestamp con hora de España
                $dateTime = new DateTime($row['timestamp']);
                //$dateTime->setTimezone(new DateTimeZone("Europe/Madrid"));
                $PRA = (float)$row['PRA'];

                if (isset($timestampAnterior) && $timestampAnterior->format("Y-m-d") == $dateTime->format("Y-m-d") && isset($PRAAnterior)){
                    // Cálculo de la intensidad horaria de precipitación (no se usa el PI de db)
                    $PI = max($PRA - $PRAAnterior, 0);
                    // Cálculos si la fecha anterior es distínto día pero existe PRA positivo 
                } else if(isset($timestampAnterior) && $timestampAnterior->format("Y-m-d") != $dateTime->format("Y-m-d")) {
                    //$PI = $PRA;
                    $PI = 0;
                } else {
                    $PI = 0;
                }

                $timestampAnterior = $dateTime;
                $PRAAnterior = $PRA;

                $return[] = Array(
                    "timestamp" => $dateTime->format("Y-m-d H:i:s"),
                    "PRA" => round($PRA,2),
                    "PI" => round($PI,2)
                );
            }
        }
        return $return;
    }

    // nivel y volumen horario de estacion
    static function getNivelVolumenEstacion($idEstacion,$fechaIni){
        $ids = [];
        $ids[] = $idEstacion;
        $fechaFin = date('Y-m-d H:i');
        $periodo = 3600;

        $idsString = "";
        foreach ($ids as $id) {
            $id = str_replace(" ", "", $id);
            if ($idsString != "") $idsString .= " OR ";
            $idsString .= "cod_variable like '" . $id . "/%'";
        }

        $idsStringTr = "";
        foreach ($ids as $id) {
            $id = str_replace(" ", "", $id);
            if ($idsStringTr != "") $idsStringTr .= " OR ";
            $idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/%'";
        }
            
        $query = "
        with 
            medidas AS (
                select cod_variable, valor, timestamp_medicion as timestamp 
                from tbl_hist_medidas 
                where timestamp_medicion >= '" . $fechaIni . "' 
                and timestamp_medicion <= '" . $fechaFin . "' 
                and (" . $idsString . ")
                and ind_validacion = 'S'
                and (extract(epoch from timestamp_medicion)/" . $periodo . " - TRUNC(extract(epoch from timestamp_medicion)/" . $periodo . "))=0
            ),
            max_medidas AS (
                select cod_variable, max(timestamp) from medidas group by cod_variable
            ),
            datostrymedidas AS (
                select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
                left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
                where timestamp >= '" . $fechaIni . "' 
                and timestamp <= '" . $fechaFin . "'
                and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
                and (" . $idsStringTr . ")
                and (extract(epoch from timestamp)/" . $periodo . " - TRUNC(extract(epoch from timestamp)/" . $periodo . "))=0
                union
                select cod_variable, valor, timestamp from medidas
            )
        
            select
                timestamp,
                estacion,
                coalesce(SUM(NE1),null) AS \"NE1\",
                coalesce(SUM(VE1),null) AS \"VE1\"
                from(
                    select
                    timestamp,
                    split_part(cod_variable, '/', 1) as estacion,
                    case when cod_variable like '%/NE1' then valor end as NE1,
                    case when cod_variable like '%/VE1' then valor end as VE1
                    from datostrymedidas
                ) as valores
                group by estacion, timestamp
                order by timestamp asc";

        $data = ConexionBD::EjecutarConsulta($query);

        $return = [];

        if(isset($data)){
            // por cada línea calcular la intensidad de precipitación
            foreach($data as $row){
                $return[] = Array(
                    "timestamp" => $row["timestamp"],
                    "estacion" => $row["estacion"],
                    "NE1" => round($row["NE1"],2),
                    "VE1" => round($row["VE1"],2)
                );
            }
        }

        return $return;
    }

    // media datos diarios de embalse
    static function getValoresDiariosEMB($codEMB,$fechaIni,$fechaFin){
        $query = "SELECT
        timestamp,
        COALESCE(SUM(EV),null) AS \"EV\",
        COALESCE(SUM(PRA),null) AS \"PRA\",
        COALESCE(SUM(NE1),null) AS \"NE1\",
        COALESCE(SUM(VE1),null) AS \"VE1\",
        COALESCE(SUM(AFT),null) AS \"AFT\",
        COALESCE(SUM(AAT),null) AS \"AAT\",
        COALESCE(SUM(AWT),null) AS \"AWT\"
        FROM(
            SELECT
                data_medicion AS timestamp,
                CASE WHEN cod_variable LIKE '".$codEMB."/EV' THEN valor_acum END AS EV,
                CASE WHEN cod_variable LIKE '".$codEMB."/PRA' THEN valor_acum END AS PRA,
                CASE WHEN cod_variable LIKE '".$codEMB."/NE1' THEN valor_media END AS NE1,
                CASE WHEN cod_variable LIKE '".$codEMB."/VE1' THEN valor_media END AS VE1,
                CASE WHEN cod_variable LIKE '".$codEMB."/AFT' THEN valor_acum END AS AFT,
                CASE WHEN cod_variable LIKE '".$codEMB."/AAT' THEN valor_acum END AS AAT,
                CASE WHEN cod_variable LIKE '".$codEMB."/AWT' THEN valor_acum END AS AWT
            FROM (
                select data_medicion, cod_variable, valor_media, valor_acum from tbl_olap_medidas_dias where 
                    (cod_variable like '".$codEMB."/EV' 
                    OR cod_variable like '".$codEMB."/PRA' 
                    OR cod_variable like '".$codEMB."/NE1' 
                    OR cod_variable like '".$codEMB."/VE1' 
                    OR cod_variable like '".$codEMB."/AFT' 
                    OR cod_variable like '".$codEMB."/AAT' 
                    OR cod_variable like '".$codEMB."/AWT') 
                and data_medicion >= '".$fechaIni."'
            ) AS valores
        ) AS valores_matriz
        GROUP BY timestamp
        ORDER BY timestamp ASC";
        $dataVariables = ConexionBD::EjecutarConsulta2($query);

        $query2 = "select cod_nivel, nivel, volumen, fecha_inicio, fecha_fin from tbl_nivel where cod_nivel like '".$codEMB."/ALIV' OR cod_nivel like '".$codEMB."/COR' OR cod_nivel like '".$codEMB."/RE1' OR cod_nivel like '".$codEMB."/RE2'";
        $dataNiveles = ConexionBD::EjecutarConsulta($query2);

        $niveles = Array("NCOR"=>"","VCOR"=>"");
        foreach ($dataNiveles as $lineaNiveles){
            switch($lineaNiveles['cod_nivel']){
                case $codEMB."/COR":
                    $niveles["NCOR"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VCOR"] = round($lineaNiveles['volumen'], 2);
                    break;
                case $codEMB."/ALIV":
                    $niveles["NALIV"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VALIV"] = round($lineaNiveles['volumen'], 2);
                    break;
                case $codEMB."/RE1":
                    $niveles["NRE1"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VRE1"] = round($lineaNiveles['volumen'], 2);
                    $niveles["fecha_inicio_RE1"] = $lineaNiveles['fecha_inicio'];
                    $niveles["fecha_fin_RE1"] = $lineaNiveles['fecha_fin'];
                    break;
                case $codEMB."/RE2":
                    $niveles["NRE2"] = round($lineaNiveles['nivel'], 2);
                    $niveles["VRE2"] = round($lineaNiveles['volumen'], 2);
                    $niveles["fecha_inicio_RE2"] = $lineaNiveles['fecha_inicio'];
                    $niveles["fecha_fin_RE2"] = $lineaNiveles['fecha_fin'];
                    break;
            }
        }

        $dataRetorno = [];


        // Insertar los datos estaticos de la segunda query a los valores dinámicos por cada fecha
        foreach ($dataVariables as $lineaVariables){

            // se reutiliza el bucle para realizar un redondeo, no es para insertar los datos estáticos
            $lineaVariables["PRA"] = round($lineaVariables["PRA"], 1);
            $lineaVariables["NE1"] = round($lineaVariables["NE1"], 2);
            $lineaVariables["VE1"] = round($lineaVariables["VE1"], 1);
            $lineaVariables["AFT"] = round($lineaVariables["AFT"], 5);
            $lineaVariables["AAT"] = round($lineaVariables["AAT"], 5);
            $lineaVariables["AWT"] = round($lineaVariables["AWT"], 5);

            $lineaVariables["NCOR"] = $niveles["NCOR"];
            $lineaVariables["VCOR"] = $niveles["VCOR"];
            if(isset($niveles["NALIV"])) $lineaVariables["NALIV"] = $niveles["NALIV"];
            if(isset($niveles["NALIV"])) $lineaVariables["VALIV"] = $niveles["VALIV"];

            // para aplicar VRE1/NRE1 o VRE2/NRE2, obtener la fecha de la línea primero y aplicar los niveles y volumenes correspondientes de los resguardos

            $timestamp = $lineaVariables["timestamp"]; // ex: '2020-01-03'
            $year = substr($timestamp,0,4); // obtener año, ex '2020'
            $utimestamp = strtotime($lineaVariables['timestamp']); // timestamp en fecha unix

            if(isset($niveles["fecha_inicio_RE1"]) && isset($niveles["fecha_fin_RE1"])){
                $fecha_inicio_RE1 = strtotime(substr_replace($niveles["fecha_inicio_RE1"], $year, 0, 4)); // ex: '0001-06-01' -> '2020-06-01' -> 1578038400
                $fecha_fin_RE1 = strtotime(substr_replace($niveles["fecha_fin_RE1"], $year, 0, 4)); // ex: '0001-09-30' -> '2020-09-30' -> 1601449200

                if($fecha_inicio_RE1 > $fecha_fin_RE1){
                    // si la fecha final es inferior a la inicial se aplican unos rangos que vienen a ser el inicio del año y final del año para realizar los condicionales
                    
                    $rango_inicio_fecha_inicio_RE1 = $fecha_inicio_RE1;
                    $rango_fin_fecha_inicio_RE1 = strtotime($year."-12-31"); // final de año

                    $rango_inicio_fecha_fin_RE1 = strtotime($year."-01-01"); // principios de año
                    $rango_fin_fecha_fin_RE1 = $fecha_fin_RE1;

                    if( ($utimestamp >= $rango_inicio_fecha_inicio_RE1 && $utimestamp <= $rango_fin_fecha_inicio_RE1) || ( 
                        $utimestamp >= $rango_inicio_fecha_fin_RE1 && $utimestamp <= $rango_fin_fecha_fin_RE1)){
                            $lineaVariables["NRE"] = $niveles["NRE1"];
                            $lineaVariables["VRE"] = $niveles["VRE1"];
                            $lineaVariables["VRED"] = round(floatval($niveles["VRE1"]) - floatval($lineaVariables["VE1"]),2);
                    }

                } else {

                    if($utimestamp >= $fecha_inicio_RE1 && $utimestamp <= $fecha_fin_RE1){
                        $lineaVariables["NRE"] = $niveles["NRE1"];
                        $lineaVariables["VRE"] = $niveles["VRE1"];
                        $lineaVariables["VRED"] = round(floatval($niveles["VRE1"]) - floatval($lineaVariables["VE1"]),2);
                    }

                }

            }

            if(isset($niveles["fecha_inicio_RE2"]) && isset($niveles["fecha_fin_RE2"])){
                $fecha_inicio_RE2 = strtotime(substr_replace($niveles["fecha_inicio_RE2"], $year, 0, 4)); // ex: '0001-06-01' -> '2020-06-01'
                $fecha_fin_RE2 = strtotime(substr_replace($niveles["fecha_fin_RE2"], $year, 0, 4)); // ex: '0001-09-30' -> '2020-09-30'

                if($fecha_inicio_RE2 > $fecha_fin_RE2){
                    // si la fecha final es inferior a la inicial se aplican unos rangos que vienen a ser el inicio del año y final del año para realizar los condicionales
                    
                    $rango_inicio_fecha_inicio_RE2 = $fecha_inicio_RE2;
                    $rango_fin_fecha_inicio_RE2 = strtotime($year."-12-31"); // final de año

                    $rango_inicio_fecha_fin_RE2 = strtotime($year."-01-01"); // principios de año
                    $rango_fin_fecha_fin_RE2 = $fecha_fin_RE2;

                    if( ($utimestamp >= $rango_inicio_fecha_inicio_RE2 && $utimestamp <= $rango_fin_fecha_inicio_RE2) || ( 
                        $utimestamp >= $rango_inicio_fecha_fin_RE2 && $utimestamp <= $rango_fin_fecha_fin_RE2)){
                            $lineaVariables["NRE"] = $niveles["NRE2"];
                            $lineaVariables["VRE"] = $niveles["VRE2"];
                            $lineaVariables["VRED"] = round(floatval($niveles["VRE2"]) - floatval($lineaVariables["VE1"]),2);
                    }

                } else {

                    if($utimestamp >= $fecha_inicio_RE2 && $utimestamp <= $fecha_fin_RE2){
                        $lineaVariables["NRE"] = $niveles["NRE2"];
                        $lineaVariables["VRE"] = $niveles["VRE2"];
                        $lineaVariables["VRED"] = round(floatval($niveles["VRE2"]) - floatval($lineaVariables["VE1"]),2);
                    }

                }

            }

            $dataRetorno[] = $lineaVariables;
        }

        return $dataRetorno;
    }

}