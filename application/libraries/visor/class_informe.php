<?php 
require_once APPPATH.'/libraries/visor/class_operaciones.php';
require_once APPPATH.'/libraries/visor/class_conexion.php';

class Informe {

   /**
     * @nombre InformeSeguimiento
     * @descripcion Hace una tabla con la información de tbl_ultimo_dato_medido
	 *				Se compone la variable $encabezado a partir de la de $camposF más la de $campos_variables
	 * @param $nombre_tabla string Nombre de la tabla
	 * @param $camposF array relación de los campos fijos del encabezado
	 * @param $campos_variables array relación de los campos variables del encabezado
     */
	/*function InformeSeguimiento($tipo,$id,$formato){	///ARREGLAR
		require './php/config.php';
		
		//Default, arreglar
		$rango=$menu[$id[0]][2];
		$informe="";
		$periodo='agregado';
		
		if(isset($_POST["enviar"])){// detecta si se manda por post un valor 
			$informe=$_POST['informe'];
			$periodo=$_POST['periodo'];
			$fecha_i=intval(strtotime($_POST['fecha_i'])/900)*900;	//Fecha redondeada a los 15 minutos inferiores
			$fecha_f=intval(strtotime($_POST['fecha_f'])/900)*900;	//Fecha redondeada a los 15 minutos inferiores
		}else{
			//$estacion="";
			//$fecha_i=1585699200;				//Fecha de referencia: 01.04.2020 0:00 ARREGLAR sustituir por una variable configurada en config
			$informe='RIEGO';
			$periodo='agregado';
			$fecha_f=intval(time()/3600)*3600;	//Fecha actual redondeada a la hora en punto
			//$fecha_i=intval(($fecha_f-20*86400)/86400)*86400;	//20 días antes
			$fecha_i=strtotime('2020-10-01');	//inicio de la campaña de riego o del año hidrológico, según convenga
		}
		
		//FORMULARIO
		$elec_informe=array('RIEGO','ABAST.','Z3-SEGUIM.','Z5-SEGUIM.','Z67-SEGUIM.','SISTEMA-GUADIANA','SISTEMA-ZÚJAR','GUADIANA','ZRO','MON','TROZO','VIL-AVE','VIL-ABA','TOMAS DIRECTAS','Z12-COTAS','Z12-VOLUMEN','Z3-COTAS-VOL','Z5-COTAS','Z5-VOLUMEN','Z67-COTAS','Z67-VOLUMEN'); //,'ZRO-CAMPAÑA','ZRT-CAMPAÑA'
		
		//$elec_periodo=array('diario','semanal');
		$elec_periodo=array('agregado');
		//$elec_periodo=array('minutal','horario','diario','agregado');
		
		echo '<h2>INFORME DE ESTACIONES SUMARIZADAS</h2>';
		echo '<form class="oculto" method="post" title="introducir fecha y pulsar aceptar">';
			echo '<table class="display">';
				echo '<thead>';
					echo '<tr>';
						echo '<td>';
						
							echo '<a>informe: </a><select style="height:25px" name="informe" size="1" simple>';
								for($j = 0; $j < count($elec_informe); $j++){
									echo '<option value="'.$elec_informe[$j].'"'.(($informe==$elec_informe[$j])?" selected='selected'":"").'><a>'.$elec_informe[$j].'</a></option>';
								}
							echo '</select>';
							
							echo '<a> periodo: </a><select style="height:25px" name="periodo" size="1" simple>';
								for($j = 0; $j < count($elec_periodo); $j++){
									echo '<option value="'.$elec_periodo[$j].'"'.(($periodo==$elec_periodo[$j])?" selected='selected'":"").'><a>'.$elec_periodo[$j].'</a></option>';
								}
							echo '</select>';
							echo '<a> f. inicial: </a><input style="height:25px" type="datetime-local" name="fecha_i" value="'.date('Y-m-d', $fecha_i).'T'.date('H:i', $fecha_i).'" color="gray">';
							echo '<a> f. final: </a>  <input style="height:25px" type="datetime-local" name="fecha_f" value="'.date('Y-m-d', $fecha_f).'T'.date('H:i', $fecha_f).'" color="gray"> ';
							echo '<input  style="height:25px" type="Submit" name="enviar" value="ACEPTAR">';
						
					echo '</td></tr>';
				echo '</thead>';
			echo '</table>';
		echo '</form>';
		*/
		/*
		echo '<form class="oculto" method="post" title="introducir fecha y pulsar aceptar">';
			echo '<table width=100%>';
				echo '<thead>';
					echo '<tr>';
						echo '<td rowspan=2>';
							echo '<a>informe: </a><select style="height:25px" name="informe" size="1" simple>';
								for($j = 0; $j < count($elec_informe); $j++){
									echo '<option value="'.$elec_informe[$j].'"'.(($informe==$elec_informe[$j])?" selected='selected'":"").'><a>'.$elec_informe[$j].'</a></option>';
								}
							echo '</select>';
						
						echo '<td rowspan=2>';
							echo '<a> periodo: </a><select style="height:25px" name="periodo" size="1" simple>';
								for($j = 0; $j < count($elec_periodo); $j++){
									echo '<option value="'.$elec_periodo[$j].'"'.(($periodo==$elec_periodo[$j])?" selected='selected'":"").'><a>'.$elec_periodo[$j].'</a></option>';
								}
							echo '</select>';
						echo '</td>';
						
						echo '<td>';
							echo '<a> f. inicial: </a><input style="height:25px" type="datetime-local" name="fecha_i" value="'.date('Y-m-d', $fecha_i).'T'.date('H:i', $fecha_i).'" color="gray">';
						echo '</td>';

						echo '<td rowspan=2>';
							echo '<input  style="height:25px" type="Submit" name="enviar" value="ACEPTAR">';
						echo '</td>';
					echo '</tr>';
					
					echo '<tr>';
						echo '<td>';
							echo '<a> f. final: </a>  <input style="height:25px" type="datetime-local" name="fecha_f" value="'.date('Y-m-d', $fecha_f).'T'.date('H:i', $fecha_f).'" color="gray"> ';
						echo '</td>';
					echo '</tr>';
					
				echo '</thead>';
			echo '</table>';
		echo '</form>';
		
		*/
		
	/*	switch($periodo){
			case ($periodo=='minutal'):	
				$tablaBD = "tbl_olap_medidas_horas d";	//esta tabla no tiene datos medios y acumulados de todas las variables, sólo PRA y PA
				$camposF = array("data_medicion");
				$codigoF = 'data_medicion';
				$datosF  = Operaciones::ObtenerRangoHoras($fecha_i,$fecha_f);
				break;
			case ($periodo=='horario'):	
				$tablaBD = "tbl_olap_medidas_horas d";	//esta tabla no tiene datos medios y acumulados de todas las variables, sólo PRA y PA
				$camposF = array("data_medicion");
				$codigoF = 'data_medicion';
				$datosF  = Operaciones::ObtenerRangoHoras($fecha_i,$fecha_f);
				break;
			case ($periodo=='agregado'):
				$tablaBD = "tbl_olap_medidas_dias d";
				$camposF = array('semana',"data_medicion","dia_semana");
				$codigoF = 'data_medicion';
				$datosF  = Operaciones::ObtenerRangoFechas($fecha_i,$fecha_f);
				break;
			default: //semanal
				$tablaBD = "tbl_olap_medidas_dias d";
				$camposF = array("semana");
				$codigoF = 'semana';
				$datosF  = Operaciones::ObtenerRangoSemanas($fecha_i,$fecha_f);
		}


		$nombre_tabla=utf8_decode("INFORME: ".$informe.". DATOS DIARIOS DEL ".date('d.m.Y',$fecha_i)." AL ".date('d.m.Y',$fecha_f));
		
		switch ($informe){	//Elección del rango de estaciones según la elección
			
			case ($informe=="ZRO"):
				$camposV=array('E2-04/NE1','E2-04/VE1','E2-04/QC1','E2-04/AC1','E2-04/QC2','E2-04/QWC','E2-11/NE1','E2-11/VE1','E2-11/AC2','E2-11/AC3');
				break;
				//$camposV=array('E2-04/NE1','E2-10/NE1','E2-11/NE1','E2-04/VE1','E2-04/QWC','E2-04/QWR','E2-04/AWC','E2-07/AWR');
			
			case ($informe=="RIEGO"):
				$camposV=array('E1-01/AC1','E1-05/AC1','E1-06/AT1','E1-06/AT2','E1-09/AT2','E2-03/AC1','E2-04/AC1','E2-11/AC2','E2-07/AC1','E2-19/AC1','E2-19/AC2');
				break;
			
			case ($informe=="Z3-SEGUIM."):
				$camposV=array('E2-03/AC1','E2-04/AWR','E2-04/AWC','E2-04/AC1','E2-04/ATR','E2-07/AWR','E2-07/AWC','E2-07/AC1','E2-07/ATR');
				break;
			
			case ($informe=="Z5-SEGUIM."):
				$camposV=array('E2-12/AFT','E2-12/AC1','E2-13/AFT','E2-13/AC1','E2-10/AAT','E2-10/AFT','E2-10/AC1','E2-10/AC2','E2-11/AFT','E2-11/AC2','E2-11/AC3','E2-33/AFT','E2-34/AFT','E2-34/AC1');
				break;
			
			case ($informe=="Z67-SEGUIM."):
				$camposV=array('E2-16/AFT','E2-16/AWT','E2-32/AFT','E2-32/AT1','E2-24/AFT','E2-19/AAT','E2-19/AC1','E2-19/AC2','CR2-34/AR1');	//,'E2-24/AWR','E2-24/AWT'
				break;
			
			case ($informe=="SISTEMA-GUADIANA"):
				$camposV=array('E2-01/NE1','E2-01/VE1','E2-01/AWT','E2-03/NE1','E2-03/VE1','E2-03/AC1','E2-03/AWT','E2-04/NE1','E2-04/VE1','E2-04/ATR');
				break;
			
			case ($informe=="SISTEMA-ZÚJAR"):
				$camposV=array('E2-06/NE1','E2-06/VE1','E2-06/AWT','E2-07/NE1','E2-07/VE1','E2-07/ATR');
				break;
			
			case ($informe=="GUADIANA"):
				$camposV=array('CR2-06/AR1','CR2-13/AR1','CR2-51/NR1','CR2-53/NR1','CR2-25/NR1','CR2-25/AR1','CR2-34/AR1','E2-25/AR1','CR2-55/AR1');
				break;
			
			case ($informe=="ABAST."):
				$camposV=array("E1-01/AT1","E1-02/AT1","E1-02/AT2","E1-02/AT3","E1-05/AT2","E1-05/AT3","E2-07/AT1","E2-16/AT2","E2-22/AT1","E2-24/AT2");
				break;
				
			case ($informe=="TROZO"):
				$camposV=array('E2-04/NE1','E2-07/NE1','E2-06/NE1','E2-04/VE1','E2-07/VE1','E2-06/VE1','E2-04/QTR','E2-07/QTR','E2-04/ATR','E2-07/ATR','E2-06/QWT','E2-06/AWT');
				break;
			
			case ($informe=="MON"):
				$camposV=array('E2-19/NE1','E2-19/VE1','E2-19/QC1','E2-19/QC2','E2-19/AC1','E2-19/AC2','CR2-34/QR1','CR2-34/AR1');
				break;
			
			case ($informe=="VIL-AVE"):
				$camposV=array('E2-24/NE1','E2-24/VE1','CR2-43/QR1','CR2-43/AR1','CR2-44/QR1','CR2-44/AR1','CR2-45/QR1','CR2-45/AR1','E2-25/QR1','E2-25/AR1');
				break;
			
			case ($informe=="VIL-ABA"):
				$camposV=array('E2-24/NE1','E2-24/VE1','E2-24/QT2','E2-24/QT3','E2-24/QT4','E2-24/AT2','E2-24/AT3','E2-24/AT4');
				break;
				
			case ($informe=="TOMAS DIRECTAS"):
				$camposV=array('E2-04/QWR','E2-04/AWR','E2-07/QWR','E2-07/AWR','CR2-34/QR1','CR2-34/AR1','E2-25/QR1','E2-25/AR1');  //,'E2-24/QWR'
				break;
				
			case ($informe=="ZRO-CAMPAÑA"):
				$camposV=array('E2-04/AWC','E2-04/AC1','E2-11/AC3','ZRO/VTOTAL','ZRO/VACUM','ZRO/PREV'); //,'ZRO/VDIF'
				break;
			
			case ($informe=="Z12-COTAS"):
				$camposV=array('E1-01/NE1','E1-02/NE1','E1-03/NE1','E1-05/NE1','E1-06/NE1','E1-07/NE1','E1-08/NE1','E1-09/NE1','E1-10/NE1');
				break;
				
			case ($informe=="Z12-VOLUMEN"):
				$camposV=array('E1-01/VE1','E1-02/VE1','E1-03/VE1','E1-05/VE1','E1-06/VE1','E1-07/VE1','E1-08/VE1','E1-09/VE1','E1-10/VE1');
				break;
				
			case ($informe=="Z3-COTAS-VOL"):
				$camposV=array('E2-01/NE1','E2-03/NE1','E2-04/NE1','E2-06/NE1','E2-07/NE1','E2-01/VE1','E2-03/VE1','E2-04/VE1','E2-06/VE1','E2-07/VE1');
				break;
			
			case ($informe=="Z5-COTAS"):
				$camposV=array('E2-08/NE1','E2-09/NE1','E2-10/NE1','E2-11/NE1','E2-12/NE1','E2-13/NE1','E2-33/NE1','E2-34/NE1');
				break;
			
			case ($informe=="Z5-VOLUMEN"):
				$camposV=array('E2-08/VE1','E2-09/VE1','E2-10/VE1','E2-11/VE1','E2-12/VE1','E2-13/VE1','E2-33/VE1','E2-34/VE1');
				break;
			
			case ($informe=="Z67-COTAS"):
				$camposV=array('E2-16/NE1','E2-15/NE1','E2-32/NE1','E2-19/NE1','E2-18/NE1','E2-17/NE1','E2-24/NE1','E2-28/NE1','E2-22/NE1','E2-20/NE1','E2-21/NE1');
				break;
			
			case ($informe=="Z67-VOLUMEN"):
				$camposV=array('E2-16/VE1','E2-15/VE1','E2-32/VE1','E2-19/VE1','E2-18/VE1','E2-17/VE1','E2-24/VE1','E2-28/VE1','E2-22/VE1','E2-20/VE1','E2-21/VE1');
				break;
			
		}
		
		//Obtención de los datos de las variables
		$aux=array();
		$datosV=array();
		
		//Almacena en una matriz la consulta entre timestamps de todas las variables y le pone como key dentro de cada variable al timestamp
		foreach($camposV as $campoV){
			$query = "SELECT cod_variable, data_medicion, valor_media, valor_acum, num_semana as semana, anyo FROM ".$tablaBD." WHERE cod_variable = '".$campoV."' AND data_medicion between '".date('d/m/Y H:i' ,$fecha_i)."' and '".date('d/m/Y H:i',$fecha_f)."' ORDER by data_medicion asc";	
			$aux[$campoV] = ConexionBD::EjecutarConsulta2($query); //para reindexar la matriz con el timestamp como clave para después introducir el dato directamente, sin sentencias if
		}
		
		//Para particularizar la función
		$codigoF='data_medicion';
		$codigoV='data_medicion';
		$valor=array('valor_media','valor_acum'); //o valor_acum (hay que meterlo en el foreach)
		
		//Indexa la matriz $datosV con el $codigoF para que después se pueda montar la matriz fácilmente
		$datosV=Operaciones::AñadirIndexCamposV($codigoF,$aux);
		
		
		//Se inicializan las variables derivadas
		foreach($camposV as $campoV){
			//Casos particulares de datos sumarizados en horizontal
			
			if(explode('/',$campoV)[1]=='VTOTAL' or explode('/',$campoV)[1]=='VACUM'  or explode('/',$campoV)[1]=='VDIF'){
				foreach ($datosF as $claveF => $datoF){	
					if(array_key_exists($datoF[$codigoF],$datosV[$camposV[0]])){ //ARREGLAR un poco chapucero lo del [0]
						$datosV[$campoV][$datoF[$codigoF]]['cod_variable']=$campoV;
						$datosV[$campoV][$datoF[$codigoF]][$codigoV]=$datoF[$codigoF];
						$datosV[$campoV][$datoF[$codigoF]]['valor_acum']='';
					}
				}			
			}
		}
		
		//Casos de operaciones especiales tras indexar
		foreach($camposV as $campoV){
			$suma=0;
			//Casos particulares de datos sumarizados en horizontal
			if($campoV=='ZRO/VTOTAL'){
				foreach ($datosF as $claveF => $datoF){	
					if(array_key_exists($datoF[$codigoF],$datosV[$camposV[0]])){ //ARREGLAR un poco chapucero lo del [0]
						$datosV[$campoV][$datoF[$codigoF]]['valor_acum']=$datosV['E2-04/AWC'][$datoF[$codigoF]]['valor_acum']+$datosV['E2-04/AC1'][$datoF[$codigoF]]['valor_acum']+$datosV['E2-11/AC3'][$datoF[$codigoF]]['valor_acum'];
						$suma=$suma+$datosV[$campoV][$datoF[$codigoF]]['valor_acum'];
						$datosV['ZRO/VACUM'][$datoF[$codigoF]]['valor_acum']=$suma;
					}
				}			
			}
		}
		
		
		
		
		//var_dump($datosV);
		
		if(isset($_POST["enviar"])){
			$datos=ObtenerDatos::UnirDatosFijosVariablesAgregados($codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV); //ARREGLAR pendiente de meter en una función. el problema es que el return no puede devolver dos matrices $datos y resumen.
			$tabla=Tabla::DibujarTablaHorizontal($nombre_tabla,$datos['campos'],$datos['cuerpo'],$formato,$datos['resumen']); //ARREGLAR agrupar en datos todo el encabezado, campos, cuerpo, resumen... para enviarlo a la función de dibujar tabla
			$leyenda=Operaciones::ObtenerLeyendaInforme($camposV);
		}
		
		//Destruyo las matrices
		unset($valor);
		unset($camposF);
		unset($camposV);
		unset($datosF);
		unset($datosV);
	}*/

	static function getReacarData($fecha_i, $fecha_f, $codEstacion, $campoV) {
		$query = "with 
		medidas AS (
			select cod_variable, valor, timestamp
			from tbl_dato_reacar 
			where timestamp >= '".date('d/m/Y H:i:s' ,$fecha_i)."'
			and timestamp <= '".date('d/m/Y H:i:s' ,$fecha_f)."' 
			and cod_variable IN ('".$codEstacion."/".$campoV."')
			and validez = 1 
		),
		max_medidas AS (
			select cod_variable, max(timestamp) from medidas group by cod_variable
		),
		datostrymedidas AS (
			select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
			left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
			where timestamp >= '".date('d/m/Y H:i:s' ,$fecha_i)."' 
			and timestamp <= '".date('d/m/Y H:i:s' ,$fecha_f)."'
			and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
			and tbl_dato_tr.cod_variable IN ('".$codEstacion."/".$campoV."')
			union
			select cod_variable, valor, timestamp from medidas
		)
		select 
			DATE_TRUNC('minute',timestamp) as data_medicion,
			coalesce(SUM(\"".$codEstacion."/".$campoV."\"),null) AS valor
			from( 
				select timestamp, 
				substring(cod_variable,0,6) as estacion,
				case when cod_variable like '".$codEstacion."/".$campoV."' then valor end as \"".$codEstacion."/".$campoV."\"
				from datostrymedidas
				WHERE (extract(epoch from DATE_TRUNC('minute',timestamp))/600 - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/600))=0
			) as valores
			group by timestamp 
			order by timestamp asc";

		return ConexionBD::EjecutarConsulta($query); 
	}

    /**
     * @nombre InformeEstacion
     */
	static function InformeEstacion($codEstacion, $tipo, $periodo, $fecha_iRecibido, $fecha_fRecibido){	///$estacion es un array
        require APPPATH.'/libraries/visor/config.php';

		$segundos = 600;

		switch ($periodo){

			case ($periodo == 'minutal' && !in_array($tipo,["EAA","ECC","EXC"])):

				$segundos = 600;

				$fecha_i = intval(strtotime($fecha_iRecibido)/$segundos) * $segundos;	//Fecha redondeada a 10 minutos
        		$fecha_f = intval(strtotime($fecha_fRecibido)/$segundos) * $segundos;	//Fecha redondeada a 10 minutos

				$camposF = array("data_medicion");
				$datosF = Operaciones::ObtenerRangoMinutos($fecha_i, $fecha_f);
				$valor=array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
				break;

			case ($periodo == 'minutal' && in_array($tipo,["EAA","ECC","EXC"])):

				$segundos = 900;

				$fecha_i = intval(strtotime($fecha_iRecibido)/$segundos) * $segundos;	//Fecha redondeada a 1h
        		$fecha_f = intval(strtotime($fecha_fRecibido)/$segundos) * $segundos;	//Fecha redondeada a 1h

				$camposF = array("data_medicion");
				$datosF = Operaciones::ObtenerRangoMinutosV2($fecha_i, $fecha_f, $segundos);
				$valor=array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
				break;
		
			case ($periodo == 'horario'):

				$segundos = 3600;

				$fecha_i = intval(strtotime($fecha_iRecibido)/$segundos) * $segundos;	//Fecha redondeada a 1h
        		$fecha_f = intval(strtotime($fecha_fRecibido)/$segundos) * $segundos;	//Fecha redondeada a 1h

				$camposF = array("data_medicion");
				$datosF = Operaciones::ObtenerRangoHoras($fecha_i, $fecha_f);
				$valor = array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
				break;
		
			case ($periodo == 'diario'):

				$segundos = 86400;

				$fecha_i = intval(strtotime($fecha_iRecibido)/$segundos) * $segundos;	//Fecha redondeada a 1d
        		$fecha_f = intval(strtotime($fecha_fRecibido)/$segundos) * $segundos;	//Fecha redondeada a 1d

				$camposF = array("data_medicion");
				$datosF = Operaciones::ObtenerRangoDias($fecha_i, $fecha_f);
				$valor = array('valor_media','valor_acum'); //o valor_acum (hay que meterlo en el foreach)
				break;

			case ($periodo == 'agregado'):

				$fecha_i = intval(strtotime($fecha_iRecibido)/600) * 600;	//Fecha redondeada a 10 minutos
        		$fecha_f = intval(strtotime($fecha_fRecibido)/600) * 600;	//Fecha redondeada a 10 minutos

				$camposF = array("semana","data_medicion","dia_semana");
				$datosF = Operaciones::ObtenerRangoFechas($fecha_i, $fecha_f);
				break;
		}
		
		switch ($tipo){	//Elección del rango de estaciones según la elección
			
			case ($tipo == "E" or in_array(trim($codEstacion), array("E","Z12","Z3","Z5","Z67"))):
				// DATOS DIARIOS DE EMBALSES
				if($periodo == 'agregado'){

					$tablaBD = "tbl_olap_medidas_dias";
					$variables = array("NE1","VE1","PRA","PI","EV","AAT","AC1","AC2","AC3","AFT","AWC","AWR","AWT","ATR","AT1","AT2","AT3","AT4"); //,"EV" //ARREBLAR PARA TOMAR LAS VARIABLES QUE ESTÉN DISPONIBLES PARA ESA ESTACIÓN
					$valor=array('valor_media','valor_acum'); //o valor_acum (hay que meterlo en el foreach)
				
				}else{

					$tablaBD = "tbl_dat_minutales";
					$variables = array("NE1","VE1","PRA","PI","QWR","QTR","QC1","NC1","QC2","NC2","QT1","QT2","QT3","QT4"); //,"AT1","AT2","AT3" 
					$valor = array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
				
				}
				break;
	
	
			case ($tipo == "CR" or $codEstacion == "CR"):
				// DATOS DIARIOS DE LA ESTACIÓN DE AFORO
				if($periodo == 'agregado'){
					$tablaBD = "tbl_olap_medidas_dias";
					$variables = array("QR1","NR1","AR1","PRA","PI"); 
					$valor=array('valor_media','valor_acum'); //o valor_acum (hay que meterlo en el foreach)
				}else{
					$tablaBD = "tbl_dat_minutales";
					$variables = array("QR1","NR1","PRA","PI"); 
					$valor=array('valor','valor');
				}
				break;
				
			
			case ($tipo == "PZ" or $codEstacion == "PZ"):
				// DATOS DIARIOS DEL PIEZÓMETRO
				if($periodo== 'agregado'){
					$tablaBD = "tbl_olap_medidas_dias";
					$variables = array("CP"); 
					$valor=array('valor_media','valor_acum');
				}else{
					$tablaBD = "tbl_dat_minutales";
					$variables = array("CP"); 
					$valor=array('valor','valor');
				}
				break;
				
			case ( $tipo == "EM" or $codEstacion == "EM"):
				// DATOS DIARIOS DE LA ESTACIÓN METEOROLÓGICA
				if($periodo=='agregado'){
					$tablaBD = "tbl_olap_medidas_dias";
					$variables = array("PRA","PI","TA","VV","PA","RT"); //HR, DV no se calculan
					$valor=array('valor_media','valor_acum'); //o valor_acum (hay que meterlo en el foreach)
				}else{
					$tablaBD = "tbl_dat_minutales d";
					$variables = array("PRA","PI","TA","VV","PA","RT"); //HR, DV no se calculan
					$valor=array('valor','valor');
				}
				break;
	 
			case ($tipo == "EAA" or $codEstacion == "EAA"):
                // DATOS DIARIOS DE LA ESTACIÓN SAICA
				$tablaBD = "tbl_dato_saica"; //comprobar	
				$variables = array("TA", "TB", "PH", "OX", "CD", "NH", "NO", "PO", "SAC");
				$valor=array('valor','valor');
				break;	
	
			case ($tipo == "ECC" or $codEstacion == "ECC"):
				// DATOS DIARIOS DE LA ESTACIÓN REACAR		
				$tablaBD = "tbl_dato_reacar";	
				$variables = array("TA","TB","PH","OX","CD","RD","SAC");
				$valor=array('valor','valor');
				break;	

			case ( $tipo == "EXC" or $codEstacion == "EXC"):
				// DATOS DIARIOS DE LA ESTACIÓN INDUSTRIAL		
				$tablaBD = "tbl_dato_reacar";	
				$variables = array("QV","TA","PH","CD","NF","COT","SAC");
				$valor=array('valor','valor');
				break;	
		}
		
		//Añade el código de la estación a un listado de variables y elimina las que no existen para esa estación
		$camposV = Informe::FiltrarCodigosEstacion($codEstacion,$variables); // FALTA FILTRAR!

        //Obtención de los datos de las variables
		$aux=array();
		$datosV=array();
        


		//Almacena en una matriz la consulta entre timestamps de cada variable
		foreach($camposV as $campoV){
			switch ($periodo){
				
				case ($periodo == 'agregado'):
					$query = "SELECT split_part(cod_variable,'/', 2) as cod_variable, DATE(data_medicion) as data_medicion, valor_media, valor_acum, num_semana as semana FROM ".$tablaBD." WHERE cod_variable = '".$codEstacion."/".$campoV."' AND data_medicion BETWEEN '".date('d/m/Y' ,$fecha_i)."' and '".date('d/m/Y',$fecha_f)."' ORDER by data_medicion asc";
					$aux[$campoV] = ConexionBD::EjecutarConsulta($query); 
					break;

				case $tipo == "EAA":

					$query = "
						select cod_variable, valor, timestamp as data_medicion
						from tbl_dato_saica 
						where timestamp >= '".date('d/m/Y H:i:s' ,$fecha_i)."'
						and timestamp <= '".date('d/m/Y H:i:s' ,$fecha_f)."' 
						and cod_variable IN ('".$codEstacion."/".$campoV."')
						and (extract(epoch from DATE_TRUNC('minute',timestamp))/".$segundos." - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/".$segundos."))=0
						order by timestamp asc";

						$aux[$campoV] = ConexionBD::EjecutarConsulta($query);

					break;

				case ($tipo == "ECC" || $tipo == "EXC"):

					$query = "
						select cod_variable, valor, timestamp as data_medicion
						from tbl_dato_reacar
						where timestamp >= '".date('d/m/Y H:i:s' ,$fecha_i)."'
						and timestamp <= '".date('d/m/Y H:i:s' ,$fecha_f)."' 
						and cod_variable IN ('".$codEstacion."/".$campoV."')
						and (extract(epoch from DATE_TRUNC('minute',timestamp))/".$segundos." - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/".$segundos."))=0
						order by timestamp asc";

						$aux[$campoV] = ConexionBD::EjecutarConsulta($query);

					break;
				case ($tipo == "PZ" && (strpos( $codEstacion , '04.' ) === 0)):

					$query = "select 
							DATE_TRUNC('minute',timestamp) as data_medicion,
							case when cod_variable like '%/".$campoV."' AND validez = 1 then valor ELSE NULL end valor
							from tbl_dato_piezo
							where cod_variable like '".$codEstacion."/".$campoV."'";

						$aux[$campoV] = ConexionBD::EjecutarConsulta($query); 
					break;
					
				default:
					//$query = "SELECT split_part(cod_variable,'/', 2) as cod_variable, timestamp as data_medicion, valor FROM ".$tablaBD." WHERE cod_variable = '".$codEstacion."/".$campoV."' AND timestamp BETWEEN '".date('d/m/Y H:i:s' ,$fecha_i)."' and '".date('d/m/Y H:i:s',$fecha_f)."' ORDER by timestamp asc";
					
					$query = "with 
					medidas AS (
						select cod_variable, valor, timestamp_medicion as timestamp 
						from tbl_hist_medidas 
						where timestamp_medicion >= '".date('d/m/Y H:i:s' ,$fecha_i)."'
						and timestamp_medicion <= '".date('d/m/Y H:i:s' ,$fecha_f)."' 
						and cod_variable IN ('".$codEstacion."/".$campoV."')
						and ind_validacion = 'S'
					),
					max_medidas AS (
						select cod_variable, max(timestamp) from medidas group by cod_variable
					),
					datostrymedidas AS (
						select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
						left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
						where timestamp >= '".date('d/m/Y H:i:s' ,$fecha_i)."' 
						and timestamp <= '".date('d/m/Y H:i:s' ,$fecha_f)."'
						and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
						and tbl_dato_tr.cod_variable IN ('".$codEstacion."/".$campoV."')
						union
						select cod_variable, valor, timestamp from medidas
					)
					
					select 
						DATE_TRUNC('minute',timestamp) as data_medicion,
						coalesce(SUM(\"".$codEstacion."/".$campoV."\"),null) AS valor
						from( 
							select timestamp, 
							substring(cod_variable,0,6) as estacion,
							case when cod_variable like '".$codEstacion."/".$campoV."' then valor end as \"".$codEstacion."/".$campoV."\"
							from datostrymedidas
							WHERE (extract(epoch from DATE_TRUNC('minute',timestamp))/600 - TRUNC(extract(epoch from DATE_TRUNC('minute',timestamp))/600))=0
						) as valores
						group by timestamp 
						order by timestamp asc";

					$aux[$campoV] = ConexionBD::EjecutarConsulta($query); 




			}
        }

		//Para particularizar la función
		$codigoF='data_medicion';
		$codigoV='data_medicion';
		
		//Indexa la matriz $datosV con el $codigoF para que después se pueda montar la matriz fácilmente
		$datosV = Operaciones::AñadirIndexCamposV($codigoF, $aux);

        switch ($periodo){
            case ($periodo=='agregado'):
                $valor = array('valor_media','valor_acum'); //o valor_acum (hay que meterlo en el foreach)
                $datosTemp = ObtenerDatos::UnirDatosFijosVariablesAgregados($codEstacion,$codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV); 
                break;
            default:
                $valor = array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
                $datosTemp = ObtenerDatos::UnirDatosFijosVariablesSinTimestampNiResumen($codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV); 
        }


        // encabezado con la información de los campos
		$encabezados = $datosTemp['campos'];


		// crear array de retorno
		/*$datos = Array( "campos" => Array() , "cabecera" => Array() , "cuerpo" => Array() , "resumen" => Array() , "leyenda" => Array());

		// el formato de la leyenda se realiza en el lado cliente
		$datos["leyenda"] = Operaciones::ObtenerLeyendaInforme($camposV);

		// campos
		$keys = array_keys( $encabezado ); 
		foreach ($datosTemp['campos'] as $clave => $campo) {
			$datos["campos"][] = array("nombre" => $keys[$clave] , "type" => $encabezado[$keys[$clave]]['formato']);
		}

		// aplicar nombre de cabecera
		foreach ($encabezado as $i => $fila){

			if(in_array($i,Array("semana", "dia_semana"))){
				$datos['cabecera'][] = array("cabecera" => $fila['campo'] );

			} else if(in_array($i,Array("data_medicion"))){
				$datos['cabecera'][] = array("cabecera" => $fila['campo'] . " [" . $fila['unidad'] . "]");
				
			} else {
				if($fila['unidad']<>""){
					$datos['cabecera'][] = array("cabecera" => $i . " [" . $fila['unidad'] . "]");
				} else {
					$datos['cabecera'][] = array("cabecera" => $i);
				}
			}
		}


		// formatear cuerpo
        foreach ($datosTemp['cuerpo'] as $clave => $fila) {
			$temp = array();
            foreach ($fila as $campo => $valor) {
				$temp[$campo] = Operaciones::AplicarFormatoValor($valor, $encabezado[$campo]['formato'], $campo);
			}
			$datos["cuerpo"][] = $temp;
        }

		// formatear resumen
		if(isset($datosTemp['resumen'])){
			foreach ($datosTemp['resumen'] as $claveL => $linea){
				if($claveL<>'inicio' and $claveL<>'tipo' and $claveL<>'diferencia'){ //ARREGLAR tipo
					$temp = array();
					foreach ($datosTemp['campos'] as $clave => $valor){
						$temp[$valor] = Operaciones::AplicarFormatoValor($datosTemp['resumen'][$claveL][$valor],  $encabezado[$valor]['formato'],  $valor);
					}
					$datos["resumen"][] = $temp;
				}
			}
		}*/

		// generar los encabezados
		$datosRetorno = Array( "principal" => Array("encabezado"=> Array(),"valores"=> Array()), "resumen" => Array("encabezado"=> Array(),"valores"=> Array()) , "leyenda" => Array());
		
		// generar los encabezados
		foreach($encabezados as $campo){

			//$encabezado["cod_campo_completo"] = ObtenerDatos::ObtenerCodCampoCompleto($campo);

			/*if(!in_array($encabezado["cod_campo"], Array("semana", "dia_semana","data_medicion"))){
				$encabezado["campo"] = $campo." ".$encabezado["campo"];
			}*/

			$datosRetorno["principal"]["encabezado"][] = $campo;

			if(isset($datosTemp['resumen'])){
				$datosRetorno["resumen"]["encabezado"][] = $campo;
			}
		}

		// formatear cuerpo
		foreach ($datosTemp['cuerpo'] as $clave => $fila) {
			foreach ($fila as $campo => $valor) {
				$datosRetorno["principal"]["valores"][$clave][$campo] = $valor;
			}
		}


		// resumen
		if(isset($datosTemp['resumen'])){
		

			foreach ($datosTemp['resumen'] as $clave => $fila) {
				foreach ($fila as $campo => $valor) {
					//if($claveL<>'inicio' and $claveL<>'tipo' and $claveL<>'diferencia'){ //ARREGLAR tipo
						$datosRetorno["resumen"]["valores"][$clave][$campo] = $valor;
					//}
				}
			}

		}

		// el formato de la leyenda se realiza en el lado cliente
		$datosRetorno["leyenda"] = Operaciones::ObtenerLeyendaInforme($codEstacion,$camposV);

		$datos = $datosRetorno;

        return $datos;
		
	}

	   /**
     * @nombre InformeBalance
     * @descripcion Hace una tabla con la información del balance de una presa
	 *				Se compone la variable $encabezado a partir de la de $camposF más la de $campos_variables
	 * @param $nombre_tabla string Nombre de la tabla
	 * @param $camposF array relación de los campos fijos del encabezado
	 * @param $campos_variables array relación de los campos variables del encabezado
     */
	/*function InformeBalance($tipo,$id,$formato){	///ARREGLAR
		require './php/config.php';
		
		$rango="E";
		$estacion=$menu[$id[0]][2];
		$periodo='agregado';
		$elec_periodo=array('agregado');

		if(isset($_POST["enviar"])){// detecta si se manda por post un valor 
			//$periodo=$_POST['periodo'];
			$estacion=$_POST['estacion'];
			$fecha_i=intval(strtotime($_POST['fecha_i'])/900)*900;	//Fecha redondeada a los 15 minutos inferiores
			$fecha_f=intval(strtotime($_POST['fecha_f'])/900)*900;	//Fecha redondeada a los 15 minutos inferiores
		}else{
			//$periodo='agregado';
			$fecha_f=intval(time()/3600)*3600;	//Fecha actual redondeada a la hora en punto
			$fecha_i=intval(($fecha_f-20*86400)/86400)*86400;	//x días antes según el tipo de espacio temporal
		}
		
		
		//FORMULARIO
		$estaciones=Operaciones::ObtenerRangoEstaciones($rango);
		
		echo '<h2>FORMULARIO DE ENTRADA DE DATOS MANUALES DE PRESAS</h2>';
		echo '<form class="oculto" method="post" title="introducir fecha y pulsar aceptar">';
			echo '<table class="display">';
				echo '<thead>';
					echo '<tr>';
						echo '<td>';
						
							echo '<select style="height:25px" name="estacion" size="1" simple>';
								for($j = 0; $j < count($estaciones); $j++){
									echo '<option value="'.trim($estaciones[$j]['cod_estacion']).'"'.(($estacion==trim($estaciones[$j]['cod_estacion']))?" selected='selected'":"").'><a>'.trim($estaciones[$j]['cod_estacion']).': '.$estaciones[$j]['nombre'].'</a></option>';
								}
							echo '</select>';
							*/
							/*
							echo '<a> periodo: </a><select style="height:25px" name="periodo" size="1" simple>';
								for($j = 0; $j < count($elec_periodo); $j++){
									echo '<option value="'.$elec_periodo[$j].'"'.(($periodo==$elec_periodo[$j])?" selected='selected'":"").'><a>'.$elec_periodo[$j].'</a></option>';
								}
							echo '</select>';
							*/
							/*echo '<a> f. inicial: </a><input style="height:25px" type="datetime-local" name="fecha_i" value="'.date('Y-m-d', $fecha_i).'T'.date('H:i', $fecha_i).'" color="gray">';
							echo '<a> f. final: </a>  <input style="height:25px" type="datetime-local" name="fecha_f" value="'.date('Y-m-d', $fecha_f).'T'.date('H:i', $fecha_f).'" color="gray"> ';
							echo '<input  style="height:25px" type="Submit" name="enviar" value="ACEPTAR">';
						
					echo '</td></tr>';
				echo '</thead>';
			echo '</table>';
		echo '</form>';

		
		switch ($periodo){
			
			case ($periodo=='agregado'):
				$tiempo  = 20; //20 días
				$camposF = array("semana","data_medicion","dia_semana");
				//$codigoF = 'data_medicion';
				$datosF = Operaciones::ObtenerRangoFechas($fecha_i,$fecha_f);
				break;
		}
		
	
		
		$nombre_tabla=$estacion.utf8_decode(". DATOS DIARIOS DE EMBALSES DEL ".date('d.m.Y',$fecha_i)." AL ".date('d.m.Y',$fecha_f));
		
		if($periodo=='agregado'){
			$tablaBD = "tbl_dato_oficial_dia";
			$variables = array("NE1","PR8","EV","LT1","LT2","LT3","LT4","LWT","TMAX","TMIN","VE1","AZ","AT1","AT2","AT3","AT4","AWT","AAT","AFT","AEV","APE"); //,"EV","AAT","AC1","AC2","AC3","AFT","AWC","AWR","AWT","ATR","AT1","AT2","AT3","AT4" //ARREBLAR PARA TOMAR LAS VARIABLES QUE ESTÉN DISPONIBLES PARA ESA ESTACIÓN
			$valor=array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
		}else{
			//NO SE USA DE MOMENTO
			//$tablaBD = "tbl_dat_minutales";
			//$variables = array("NE1","VE1","PI","QWR","QTR","QC1","NC1","QC2","NC2","QT1","QT2","QT3","QT4"); //,"AT1","AT2","AT3" 
			//$valor=array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
		}
		
		//Añade el código de la estación a un listado de variables y elimina las que no existen para esa estación
		$estacion=str_replace('E','EX',$estacion);
		$camposV = Operaciones::AñadirCodigoEstacion($estacion,$variables);
		
		//Obtención de los datos de las variables
		$aux=array();
		$datosV=array();
		
		//Almacena en una matriz la consulta entre timestamps de cada variable
		foreach($camposV as $campoV){
			switch ($periodo){
				case ($periodo=='agregado'):
					$query = "SELECT cod_variable, fecha as data_medicion, valor, origen_dato  FROM ".$tablaBD." WHERE cod_variable = '".$campoV."' AND fecha BETWEEN '".date('d/m/Y' ,$fecha_i)."' and '".date('d/m/Y',$fecha_f)."' ORDER by fecha asc";
					$aux[$campoV] = ConexionBD::EjecutarConsulta($query); 
					break;
					
				default:
					$query = "SELECT cod_variable, timestamp as data_medicion, valor FROM ".$tablaBD." WHERE cod_variable = '".$campoV."' AND timestamp BETWEEN '".date('d/m/Y H:i:s' ,$fecha_i)."' and '".date('d/m/Y H:i:s',$fecha_f)."' ORDER by timestamp asc";
					$aux[$campoV] = ConexionBD::EjecutarConsulta($query); 
			}
		}
		
		//Para particularizar la función
		$codigoF='data_medicion';
		$codigoV='data_medicion';
		
		//Indexa la matriz $datosV con el $codigoF para que después se pueda montar la matriz fácilmente
		$datosV=Operaciones::AñadirIndexCamposV($codigoF,$aux);
		
		//var_dump($datosV);
		
		if(isset($_POST["enviar"]) or $id[0]<=41){ //lo de <=41 es para que cuando se trate de una presa salga directamente el informe relleno para esa presa
			switch ($periodo){
				case ($periodo=='agregado'):
					$valor=array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
					$datos=ObtenerDatos::UnirDatosFijosVariablesAgregados($codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV); 
					$tabla=Tabla::DibujarTablaHorizontal($nombre_tabla,$datos['campos'],$datos['cuerpo'],$formato,$datos['resumen']); //ARREGLAR agrupar en datos todo el encabezado, campos, cuerpo, resumen... para enviarlo a la función de dibujar tabla
					break;
				default:
					$valor=array('valor','valor'); //o valor_acum (hay que meterlo en el foreach)
					$datos=ObtenerDatos::UnirDatosFijosVariablesSinTimestampNiResumen($codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV); 
					$tabla=Tabla::DibujarTablaHorizontal($nombre_tabla,$datos['campos'],$datos['cuerpo'],'CONFORMATO',''); //ARREGLAR agrupar en datos todo el encabezado, campos, cuerpo, resumen... para enviarlo a la función de dibujar tabla
			}
			$leyenda=Operaciones::ObtenerLeyendaInforme($camposV);
		}
		
		//var_dump($datos['cuerpo']);
		
		//Destruyo las matrices
		unset($aux);
		unset($valor);
		unset($camposF);
		unset($camposV);
		unset($datosF);
		unset($datosV);
	}*/


	
	
	   /**
     * @nombre InformeRiego
     * @descripcion Hace una tabla con la información de una estación
	 *				Se compone la variable $encabezado a partir de la de $camposF más la de $campos_variables
	 * @param $nombre_tabla string Nombre de la tabla
	 * @param $camposF array relación de los campos fijos del encabezado
	 * @param $campos_variables array relación de los campos variables del encabezado
     */
	/*function InformeRiego($tipo,$id,$formato){	///ARREGLAR
		require './php/config.php';
		
		//Default, arreglar
		$rango=$menu[$id[0]][2];
		//$informe="";
		$periodo='agregado';
		
		if(isset($_POST["enviar_riego"])){// detecta si se manda por post un valor 
			$informe=$_POST['informe'];
			$periodo=$_POST['periodo'];
			$fecha_i=intval(strtotime($_POST['fecha_i'])/900)*900;	//Fecha redondeada a los 15 minutos inferiores
			$fecha_f=intval(strtotime($_POST['fecha_f'])/900)*900;	//Fecha redondeada a los 15 minutos inferiores
		}else{
			//$estacion="";
			//$fecha_i=1585699200;				//Fecha de referencia: 01.04.2020 0:00 ARREGLAR sustituir por una variable configurada en config
			$informe='ZRO-CONSUMOS';
			$periodo='semanal';
			//$fecha_f=intval(time()/3600)*3600;	//Fecha actual redondeada a la hora en punto
			$fecha_f=strtotime('2020-11-01');	//Fecha actual redondeada a la hora en punto
			//$fecha_i=intval(($fecha_f-20*86400)/86400)*86400;	//20 días antes
			$fecha_i=strtotime('2020-04-01');	//inicio de la campaña de riego
		}
		
		//FORMULARIO
		$elec_informe=array('ZRO-CONSUMOS','ZRM-CONSUMOS','ZRL-CONSUMOS','TOTAL-CONSUMOS');
		
		$elec_periodo=array('2011','2012','2013','2014','2015','2016','2017','2018','2019','2020');	// ARREGLAR: poner un generador de las 10 últimas campañas
		
		echo '<h2>INFORME DE SEGUIMIENTO DE CAMPAÑA DE RIEGO</h2>';
		echo '<form class="oculto" method="post" title="introducir fecha y pulsar aceptar">';
			echo '<table class="display">';
				echo '<thead>';
					echo '<tr>';
						echo '<td>';
							echo '<a>informe: </a><select style="height:25px" name="informe" size="1" simple>';
								for($j = 0; $j < count($elec_informe); $j++){
									echo '<option value="'.$elec_informe[$j].'"'.(($informe==$elec_informe[$j])?" selected='selected'":"").'><a>'.$elec_informe[$j].'</a></option>';
								}
							echo '</select>';
							
							echo '<a> campaña: </a><select style="height:25px" name="periodo" size="1" simple>';
								for($j = 0; $j < count($elec_periodo); $j++){
									echo '<option value="'.$elec_periodo[$j].'"'.(($periodo==$elec_periodo[$j])?" selected='selected'":"").'><a>'.$elec_periodo[$j].'</a></option>';
								}
							echo '</select>';
							echo '<a> f. inicial: </a><input style="height:25px" type="datetime-local" name="fecha_i" value="'.date('Y-m-d', $fecha_i).'T'.date('H:i', $fecha_i).'" color="gray">';
							echo '<a> f. final: </a>  <input style="height:25px" type="datetime-local" name="fecha_f" value="'.date('Y-m-d', $fecha_f).'T'.date('H:i', $fecha_f).'" color="gray"> ';
							echo '<input style="height:25px" type="Submit" name="enviar_riego" value="ACEPTAR">';
						
					echo '</td></tr>';
				echo '</thead>';
			echo '</table>';
		echo '</form>';
		
		
		switch($periodo){
			case ($periodo=='agregado'):
				$tablaBD = "tbl_riego_prevision";
				$camposF = array('semana',"data_medicion","dia_semana");
				$codigoF = 'data_medicion';
				$datosF  = Operaciones::ObtenerRangoFechas($fecha_i,$fecha_f);
				break;
			default: //semanal
				$tablaBD = "tbl_riego_prevision";
				$camposF = array("semana");
				$codigoF = 'semana';
				$datosF  = Operaciones::ObtenerRangoSemanas($fecha_i,$fecha_f);
				
		}


		//$nombre_tabla="INFORME: ".$informe.". DATOS DIARIOS DEL ".date('d.m.Y',$fecha_i)." AL ".date('d.m.Y',$fecha_f);
		
		switch ($informe){	//Elección del rango de estaciones según la elección
			
			case ($informe=="ZRO-CONSUMOS"):
				$nombre_tabla="INFORME: ".$informe.utf8_decode(": CONSUMOS DE LAS 10 ÚLTIMAS CAMPAÑAS DE RIEGO");
				$camposV=array('2011/ZRO','2012/ZRO','2013/ZRO','2014/ZRO','2015/ZRO','2016/ZRO','2017/ZRO','2018/ZRO','2019/ZRO','2020/ZRO');
				break;
			
			case ($informe=="ZRM-CONSUMOS"):
				$nombre_tabla="INFORME: ".$informe.utf8_decode(": CONSUMOS DE LAS 10 ÚLTIMAS CAMPAÑAS DE RIEGO");
				$camposV=array('2011/ZRM','2012/ZRM','2013/ZRM','2014/ZRM','2015/ZRM','2016/ZRM','2017/ZRM','2018/ZRM','2019/ZRM','2020/ZRM');
				break;
			
			case ($informe=="ZRL-CONSUMOS"):
				$nombre_tabla="INFORME: ".$informe.utf8_decode(": CONSUMOS DE LAS 10 ÚLTIMAS CAMPAÑAS DE RIEGO");
				$camposV=array('2011/ZRL','2012/ZRL','2013/ZRL','2014/ZRL','2015/ZRL','2016/ZRL','2017/ZRL','2018/ZRL','2019/ZRL','2020/ZRL');
				break;
				
			case ($informe=="TOTAL-CONSUMOS"):
				$nombre_tabla="INFORME: ".$informe.utf8_decode(": COMPARATIVO DE CONSUMOS ENTRE ZONAS REGABLES EN LA CAMPAÑA DE ").$periodo;
				$variables=array('ZRP','ZRT','ZRV','ZRG','ZRCE','ZRO','ZRZ','ZRM','ZRL','TOTAL_ZR');
				//$anyo = date("Y", $fecha_i);
				$camposV = Operaciones::AñadirCodigoAnyo($periodo,$variables);
				break;
			
			case ($informe=="ZRO-SEGUIM."):
				$nombre_tabla="INFORME: ".$informe.utf8_decode(": SEGUIMIENTO DE LA CAMPAÑA DE RIEGO ").$periodo;
				$variables=array('ZRO','TOTAL_ZR');
				//$anyo = date("Y", $fecha_i);
				$camposV = Operaciones::AñadirCodigoAnyo($periodo,$variables);
				break;
		}
		
		//Obtención de los datos de las variables
		$aux=array();
		$datosV=array();
		
		//Almacena en una matriz la consulta entre timestamps de todas las variables y le pone como key dentro de cada variable al timestamp
		foreach($camposV as $campoV){
			$query = "SELECT * FROM ".$tablaBD." WHERE cod_zr = split_part('$campoV','/',2) AND anyo = split_part('$campoV','/',1)::integer AND num_semana between '".date('W' ,$fecha_i)."' and '".date('W',$fecha_f)."' ORDER by num_semana asc";
			$aux[$campoV] = ConexionBD::EjecutarConsulta($query); //para reindexar la matriz con el timestamp como clave para después introducir el dato directamente, sin sentencias if
			
			foreach($aux as $datoV){ //pongo como clave la fecha
				foreach($datoV as $clave => $var){
					$datosV[$campoV][$datoV[$clave]['num_semana']]=$datoV[$clave];
				}
			}
			unset($aux);
		}
		
		
		
		//Para particularizar la función
		$codigoF='semana';
		$codigoV='num_semana';
		$valor=array('vol_peticion','vol_servido'); //o valor_acum (hay que meterlo en el foreach)
		
		if(isset($_POST["enviar_riego"])){
			
			
			//$datos=ObtenerDatos::UnirDatosFijosVariablesAgregados($codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV); //ARREGLAR pendiente de meter en una función. el problema es que el return no puede devolver dos matrices $datos y resumen.
			$datos=ObtenerDatos::UnirDatosFijosVariablesRiego($codigoF,$codigoV,$valor,$camposF,$camposV,$datosF,$datosV); //ARREGLAR pendiente de meter en una función. el problema es que el return no puede devolver dos matrices $datos y resumen.
				?>
            <!--
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.21/js/dataTables.jqueryui.min.js"></script>

		<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

		
		
		
		
		
		
		<link rel="stylesheet" href="../DataTables/datatables.css" />
		<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" />
		<link href="https://cdn.datatables.net/1.10.21/css/dataTables.jqueryui.min.css" rel="stylesheet" />

		<link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" rel="stylesheet" />-->
		
	
	    <?php
			$tabla=Tabla::DibujarTablaHorizontal($nombre_tabla,$datos['campos'],$datos['cuerpo'],$formato,$datos['resumen']); //ARREGLAR agrupar en datos todo el encabezado, campos, cuerpo, resumen... para enviarlo a la función de dibujar tabla
			//$leyenda=Operaciones::ObtenerLeyendaInforme($camposV);
		}
		
		//var_dump($datos['cuerpo']);
		
		//Destruyo las matrices
		unset($valor);
		unset($camposF);
		unset($camposV);
		unset($datosF);
		unset($datosV);
	}*/

	// Datos semanales con resumen al final
	/*function ConsumoAguaAlta($semana) {
		
		
		
		
		
		//Configuración de las consultas SELECT para cada tipo de tabla de datos
		$estacion1=array("E1-01/QC1","E2-03/QC1","E2-04/QC1","E2-07/QC1","E2-19/QC1","E2-19/QC2","E1-01/QT1","E2-07/QT1","E2-16/QT2","E2-22/QT1","E2-24/QT2");		
		$estacion2=array("E1-01/AC1","E2-03/AC1","E2-04/AC1","E2-07/AC1","E2-19/AC1","E2-19/AC2","E1-01/AT1","E2-07/AT1","E2-16/AT2","E2-22/AT1","E2-24/AT2");		
		$valores=array("QC1","AC1");
		/*AC1,valor_acum
		QC1,valor_media*/
		/*$conection = "host=172.21.0.6 dbname=SAIHG user=postgres password=postgres port=5432";	
		$t_inicio = 1585526400; //1585526400 es el 2020.03.30 00:00
		
		$fecha_actual = "16-03-2020";
		
		echo date( "d-m-Y", strtotime( $fecha_actual." +1 day" ) );
		echo 
		//sumo 1 día
		///echo date("d/m/Y",strtotime($fecha_actual."+ 1 days")); 
		
		$nombre_tabla="INFORME DE CONSUMO EN ALTA";
		$campos=array("pto.control","Q1","A1","Q2","A2","Q3","A3","Q4","A4","Q5","A5","Q6","A6","Q7","A7");
		
		for ($i=0; $i<count($estacion2); $i++){
			
			$datos[$i][0]=$estacion2[$i];
			
			for ($j=0; $j<7; $j++){	//7 días de la semana
				//Consulta para el volumen
				$query= "select * from tbl_olap_medidas_dias WHERE cod_variable = '".$estacion2[$i]."' and data_medicion = to_timestamp('".date( "d-m-Y", strtotime( $fecha_actual."+".$j." day" ))."','DD-MM-YYYY')";
				
				$dbconn = pg_connect($conection) or die('No se ha podido conectar: ' . pg_last_error());
				$consulta = pg_query($query) or die('Error: ' . pg_last_error());
				
				//echo $query;
				
				$resultado = array();
				$resultado = pg_fetch_assoc($consulta);
				
				$datos[$i][2+2*$j]=$resultado['valor_acum'];
				$datos[$i][1+2*$j]=$resultado['valor_acum']*1000000/86400; //Cálculo del caudal
				
				pg_free_result($consulta);	// Liberando el conjunto de resultados
				pg_close($dbconn);			// Cerrando la conexión
				
				///$fecha_actual=date("d/m/Y",strtotime($fecha_actual."+ 1 days"))
			}
		}
		///return $datos;
		$tablas = new Tabla();
		//$tablas->DibujarTablaParticular($nombre_tabla,$campos,$datos);
		$tabla=Tabla::DibujarTablaHorizontal($nombre_tabla,$campos,$datos,"PRUEBA");
    }
    */

    //Añade el código de la estación a un listado de variables y elimina las que no existen para esa estación
    static function FiltrarCodigosEstacion($codEstacion, $variables){
        $estacionVariable = Array();
        $data = Array();
        // set cod estacion
        foreach($variables as $variable){
            $estacionVariable[] = $codEstacion."/".$variable;
        }
        if(count($estacionVariable)>0){
            $estacionVariableString = implode("','",$estacionVariable);
            $query = "SELECT split_part(cod_variable,'/', 2) variable FROM tbl_variable WHERE cod_variable IN ('" . $estacionVariableString . "')";
			
			$variablesEncontradas = ConexionBD::EjecutarConsulta($query);

            if(count($variablesEncontradas)>0){
                // formatear array
                $variablesEncontradasForm = Array();
                foreach($variablesEncontradas as $variable){
                    $variablesEncontradasForm[] = $variable['variable'];
                }
                // filtrar las variables recibidas usando las encontradas
                foreach($variables as $variable){
                    if(FALSE!==array_search($variable, $variablesEncontradasForm)) $data[] = $variable;
                }
            }
        }
        return $data;
    }
}