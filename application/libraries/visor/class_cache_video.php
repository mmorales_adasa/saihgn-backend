<?php 

class CacheVideo { 

    static function CachearVideo(){
        $cambiosGlobales = 0;
        $videoPath = APPPATH . "libraries/visor/video/";
        $videoCachePath = APPPATH . "libraries/visor/video_cache/";

        if (!file_exists($videoCachePath)) {
            mkdir($videoCachePath, 0777, true);
        }
        $files = scandir($videoPath);
        foreach($files as $file){
            if(is_dir($videoPath . $file) and substr( $file, 0, 1 ) !== "."){
                $embalseDirPath = $videoPath . $file . '/';
                $embalseDirCachePath = $videoCachePath . $file . '/';
                if (!file_exists($embalseDirCachePath)) {
                    chdir($videoCachePath);
                    mkdir($file, 0777, true);
                }

                $cambios = CacheVideo::copyFilesVideo($embalseDirPath, $embalseDirCachePath, 1);
                $cambiosGlobales = $cambiosGlobales + $cambios;
            }
        }
        return $cambiosGlobales;
    }

    static function CachearVideoTimelapse(){

        $cambiosGlobales = 0;

        $timelapsePath = APPPATH . "libraries/visor/timelapse/";
        $timelapseCachePath = APPPATH . "libraries/visor/timelapse_cache/";

        // se crea el directorio de cache si este no existe
        if (!file_exists($timelapseCachePath)) {
            mkdir($timelapseCachePath, 0777, true);
        }

        $files = scandir($timelapsePath);

        // por cada fichero del directorio origen
        foreach($files as $file){
            
            // revisamos que sea un directorio
            if(is_dir($timelapsePath . $file) and substr( $file, 0, 1 ) !== "."){

                $embalseDirPath = $timelapsePath . $file;
                $embalseDirCachePath = $timelapseCachePath . $file;

                // se crea directorio de embalse de destino si no existe
                if (!file_exists($embalseDirCachePath)) {
                    chdir($timelapseCachePath);
                    mkdir($file, 0777, true);
                }

                // se escanea el subdirectorio de origen
                $embalseFiles = scandir($embalseDirPath);


                // por cada fichero del subdirectorio de origen
                foreach($embalseFiles as $camFile){

                    // revisamos que sea un directorio
                    if(is_dir($embalseDirPath . "/" . $camFile) and substr( $file, 0, 1 ) !== "."){

                        $videoSubDirPath = $embalseDirPath . "/" . $camFile . "/";
                        $videoSubDirCachePath = $embalseDirCachePath . "/" . $camFile . "/";

                        // se crea directorio de timelapse de camara de destino si no existe
                        if (!file_exists($videoSubDirCachePath)) {
                            chdir($embalseDirCachePath);
                            mkdir($camFile, 0777, true);
                        }

                        $cambios = CacheVideo::copyFiles($videoSubDirPath, $videoSubDirCachePath, 144, 4);
                        $cambiosGlobales = $cambiosGlobales + $cambios;
                    }
                }
            }
        }

        return $cambiosGlobales;

    }

    private static function copyFiles($path, $pathCache, $length, $selectFileModulus){
       
        $cambios = 0;
        chdir($path);

        // obtener ficheros por orden de modificación en directorio de origen
        $allfiles = array_reverse(glob("*.*"));

        $files = [];
        $i = 0;
        foreach($allfiles as $file) {
            if($i==$selectFileModulus) $i=0;
            if($i==0) $files[] = $file;
            $i++;
        }

        // cortar el array al número de ficheros recibido
        $files = array_slice($files, 0, $length);
        chdir($pathCache);
        
        // obtener ficheros por orden de modificación en directorio de destino
        $files2 = array_reverse(glob("*.*"));

        // borrar ficheros de destino que no existan en origen
        foreach($files2 as $file2){
            if(!in_array($file2, $files)){
                $cambios++;
                unlink($file2);
            }
        }

        // realizar una copia de los ficheros existentes en origen que no existen en destino
        foreach($files as $file){
            if(!in_array($file, $files2)){
                $cambios++;
                if (is_dir($path.$file)) {
                    break;
                }
                copy($path.$file, $pathCache.$file);
            }
        }

        // revisar de nuevo el directorio de destino
        $files2 = array_reverse(glob("*.*"));

        // eliminar los ficheros restantes
        $i = 1;
        foreach($files2 as $file2){
            if($i>$length){
                $cambios++;
                unlink($file2);
            }
            $i++;
        }
        return $cambios;
    }

    private static function copyFilesVideo($path, $pathCache, $selectFileModulus){
       
        $cambios = 0;
        chdir($path);

        // obtener ficheros 
        $allfiles = glob("*.*");

        $files = [];
        foreach($allfiles as $file) {
            $files[] = $file;
        }

        chdir($pathCache);
        
        // obtener ficheros por orden de modificación en directorio de destino
        $files2 = array_reverse(glob("*.*"));

        $files2MD5 = [];
        foreach($files2 as $file2){
            $files2MD5[] = md5_file($pathCache.$file2);
        }

        // realizar una copia de los ficheros existentes
        foreach($files as $file){
            
            $fileMD5 = md5_file($path.$file);

            if (is_dir($path.$file)) {
                break;
            }

            if(!in_array($fileMD5, $files2MD5)){
                $content = file_get_contents($path.$file);
                file_put_contents($pathCache.$file,$content);
                $cambios++;
            }

        }

        // borrar ficheros de destino que no existan en origen
        foreach($files2 as $file2){
            if(!in_array($file2, $files)){
                //$cambios++;
                unlink($file2);
            }
        }

        return $cambios;
    }

}
?>