<?php
require_once APPPATH.'/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class IndustrialExternos {

    /**
     * Fecha inicial y final en UTC
     */
    static function getIndustrialExternos($ids, $periodo, $fechaIni, $fechaFin){
        require_once APPPATH.'/libraries/visor/config.php';

        $periodo = 900;

        $idsString = "";
        foreach($ids as $id){
            if($idsString != "") $idsString .= " OR ";
            $idsString .= "cod_variable like '".$id."/%'";
        }

        $query = "select
            timestamp,
            estacion,
            coalesce(SUM(CD),null) AS \"CD\",
            coalesce(SUM(PH),null) AS \"PH\",
            coalesce(SUM(QT),null) AS \"QT\",
            coalesce(SUM(TA),null) AS \"TA\",
            coalesce(SUM(NF),null) AS \"NF\",
            coalesce(SUM(COT),null) AS \"COT\"
            from(
                select
                timestamp,
                substring(cod_variable,0,7) as estacion,
                case when cod_variable like '%/CD' then valor end as CD,
                case when cod_variable like '%/PH' then valor end as PH,
                case when cod_variable like '%/QT' then valor end as QT,
                case when cod_variable like '%/TA' then valor end as TA,
                case when cod_variable like '%/NF' then valor end as NF,
                case when cod_variable like '%/COT' then valor end as COT
                from tbl_dato_reacar
                where timestamp >= '".$fechaIni."' and timestamp <= '".$fechaFin."'
                and (".$idsString.")
                and (extract(epoch from timestamp)/".$periodo." - TRUNC(extract(epoch from timestamp)/".$periodo."))=0
            ) as valores
            group by estacion, timestamp
            order by timestamp asc";

        $data = ConexionBD::EjecutarConsulta($query);

        $return = Array(); // array de retorno
        
        if($data){
            // agrupar por timestamp los valores de estación y calcular variables
            foreach($data as $i => $row){                
                // conversión de datos manual
                $row['CD'] = (float)$row['CD']; 
                $row['PH'] = (float)$row['PH']; 
                $row['QT'] = (float)$row['QT']; 
                $row['TA'] = (float)$row['TA']; 
                $row['NF'] = (float)$row['NF']; 
                $row['COT'] = (float)$row['COT']; 

                $return[$row["estacion"]][] = Array(
                    "timestamp" => $row["timestamp"],
                    "CD" => $row["CD"],
                    "PH" => $row["PH"],
                    "QV" => $row["QV"],
                    "TA" => $row["TA"],
                    "NF" => $row["NF"],
                    "COT" => $row["COT"]
                ); 
            }
        }
        return $return;
    }

    static function getValoresEXC($codigo, $tiempo, $fInicial, $fFinal)
    {
        $tiempo = intval($tiempo);
        if ($tiempo < 0) {
            $filtro = " timestamp between '$fInicial' and '$fFinal' ";
        } else {
            $filtro = " timestamp > current_date-$tiempo ";
        }

        $array_datos = array();

        $DB = ConexionBD::Conexion();

        // conocer las variables disponibles para la estación saica, el apartado ya tiene una serie de variables a usar
        $variablesDisponibles = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/', 2) variable_disponible from tbl_variable where cod_variable like '".$codigo."%' and disponible = 1");
        $variablesApartado = ['CD','COT','NF','PH','QT','TA','SAC','TB','QV'];
        $columnas = ['timestamp'];

        $variablesDisponiblesArray = [];

        if($variablesDisponibles){
            foreach($variablesDisponibles as $variableDisponible){
                $variablesDisponiblesArray[] = $variableDisponible['variable_disponible']; 
            }
            foreach($variablesApartado as $variableApartado){
                if(in_array($variableApartado, $variablesDisponiblesArray)) {
                    $columnas[] = $variableApartado;
                }
            }
        }

        if ($tiempo <= 30) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro  order by timestamp ");
        }
        if ($tiempo > 30 && $tiempo <= 60) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro and (to_char(timestamp, 'MI')='00') order by timestamp ");
        }
        if ($tiempo > 60 && $tiempo <= 120) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro  and (to_char(timestamp, 'hh:MI:SS')='00:00:00' OR to_char(timestamp, 'hh:MI:SS')='06:00:00' OR to_char(timestamp, 'hh:MI:SS')='12:00:00' OR to_char(timestamp, 'hh:MI:SS')='18:00:00') order by timestamp ");
        }
        if ($tiempo > 120) {
            $datos = ConexionBD::EjecutarConsultaConDB($DB,"select split_part(cod_variable,'/',1)as estacion,split_part(cod_variable,'/',2) as variable, cod_variable,timestamp ,valor,validez from tbl_dato_reacar tdr  where split_part(cod_variable,'/',1)='$codigo' and $filtro  and ( to_char(timestamp, 'hh:MI:SS')='12:00:00') order by timestamp ");
        }

        ConexionBD::CerrarConexion($DB);

        $filas = array_unique(Operaciones::sacarDatos($datos, 'timestamp'));
        $filas = Operaciones::arrayEstructura($filas);
        $registros = array();
        for ($i = 0; $i < count($filas); $i++) {
            $array_aux = array();
            for ($j = 0; $j < count($columnas); $j++) {
                $array_aux = $array_aux + array($columnas[$j] => "");
            }

            $array_aux['timestamp'] = $filas[$i];
            $array_aux['cod_estacion'] = $codigo;
            array_push($registros, $array_aux);
        }
        for ($i = 0; $i < count($datos); $i++) {
            $timestamp = $datos[$i]['timestamp'];
            $posicion = array_search($timestamp, array_column($registros, 'timestamp'));

            if (intval($datos[$i]['validez']) < 2) {
                $registros[$posicion][$datos[$i]['variable']] = $datos[$i]['valor'];
            } else {
                $registros[$posicion][$datos[$i]['variable']] = 'No válido';
            }

            $registros[$posicion]['timestamp'] = $datos[$i]['timestamp'];
        }

        $registro = $registros;

        $array_datos = $array_datos + array("datos" => $registro, "cabecera" => $columnas);

        return $array_datos;
    }

}