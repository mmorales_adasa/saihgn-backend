<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
date_default_timezone_set("UTC");

class Aforos
{

	/**
	 * Fecha inicial y final en UTC
	 */
	static function getEvolucionAforos($ids, $periodo, $fechaIni, $fechaFin)
	{
		require_once APPPATH . '/libraries/visor/config.php';

		$idsString = "";
		foreach ($ids as $id) {
			$id = str_replace(" ", "", $id);
			if ($idsString != "") $idsString .= " OR ";
			$idsString .= "cod_variable like '" . $id . "/%'";
		}

		$idsStringTr = "";
		foreach ($ids as $id) {
			$id = str_replace(" ", "", $id);
			if ($idsStringTr != "") $idsStringTr .= " OR ";
			$idsStringTr .= "tbl_dato_tr.cod_variable like '" . $id . "/%'";
		}

		$query = "
		with 
			medidas AS (
				select cod_variable, valor, timestamp_medicion as timestamp 
				from tbl_hist_medidas 
				where timestamp_medicion >= '" . $fechaIni . "' 
				and timestamp_medicion <= '" . $fechaFin . "'
				and (" . $idsString . ")
				and ind_validacion = 'S'
				and (extract(epoch from timestamp_medicion)/" . $periodo . " - TRUNC(extract(epoch from timestamp_medicion)/" . $periodo . "))=0
			),
			max_medidas AS (
				select cod_variable, max(timestamp) from medidas group by cod_variable
			),
			datostrymedidas AS (
				select tbl_dato_tr.cod_variable, valor, timestamp from tbl_dato_tr
				left join max_medidas on tbl_dato_tr.cod_variable = max_medidas.cod_variable
				where timestamp >= '" . $fechaIni . "' 
				and timestamp <= '" . $fechaFin . "'
                and ((max_medidas.max is not null and max_medidas.max < tbl_dato_tr.timestamp) OR (max_medidas.max is null))
				and (" . $idsStringTr . ")
				and (extract(epoch from timestamp)/" . $periodo . " - TRUNC(extract(epoch from timestamp)/" . $periodo . "))=0
				union
				select cod_variable, valor, timestamp from medidas
			)
		
			select
				timestamp,
				estacion,
				coalesce(SUM(NR1),null) AS \"NR1\",
				coalesce(SUM(QR1),null) AS \"QR1\",
				coalesce(SUM(PRA),null) AS \"PRA\",
				coalesce(SUM(ET),null) AS \"ET\"
				from(
					select
					timestamp,
					split_part(cod_variable, '/', 1) as estacion,
					case when cod_variable like '%/NR1' then valor end as NR1,
					case when cod_variable like '%/QR1' then valor end as QR1,
					case when cod_variable like '%/PRA' then valor end as PRA,
					case when cod_variable like '%/ET' then valor end as ET
					from datostrymedidas
				) as valores
				group by estacion, timestamp
				order by timestamp asc";

		$data = ConexionBD::EjecutarConsulta($query);

		$return = array();

		if ($data) {

			// variable para almacenar el último dato para cada estación, necesario para calcular las variables que requieren datos anteriores
			$timestampAnterior = Array();
			$VE1Anterior = Array();
			$NRAnterior = Array();
			$PRAAnterior = Array();

			// agrupar por timestamp los valores de estación y calcular variables
			foreach ($data as $i => $row) {

				// conversión de datos, no es posible acceder a configurar correctamente codeigniter para conseguir retornar los valores en su formato correcto
				$row['NR1'] = (float)$row['NR1'];
				$row['QR1'] = (float)$row['QR1'];
				$row['PRA'] = (float)$row['PRA'];

				// Retorno de timestamp con hora de España
				/*$dateTime = new DateTime($row['timestamp']);
				$dateTime->setTimezone(new DateTimeZone("Europe/Madrid"));
				$row['timestamp'] = $dateTime->format("Y/m/d H:i:s");*/

				// Cálculos si existe timestamp anterior para esta estación
				if (isset($timestampAnterior[$row["estacion"]]) && $timestampAnterior[$row["estacion"]] != $row["timestamp"]){

					// Cálculo de la intensidad horaria de precipitación (no se usa el PI de db)
					$row['PI'] = max(($row['PRA'] - $PRAAnterior[$row["estacion"]]) / ($periodo/3600),0);

					// Cálculo de la tendencia de nivel
					$delta = $row['NR1'] - $NRAnterior[$row["estacion"]];
					if ($delta > 0){
						$row['NTENDENCIA'] = "sube";
					} else if ($delta < 0){
						$row['NTENDENCIA'] = "baja";
					} else {
						$row['NTENDENCIA'] = "igual";
					}

				} else {
					$row['PI'] = null;
					$row['NTENDENCIA'] = null;
				}

				$timestampAnterior[$row["estacion"]] = $row["timestamp"];
				$NRAnterior[$row["estacion"]] = $row["NR1"];
				$PRAAnterior[$row["estacion"]] = $row["PRA"];

				$return[$row["estacion"]][] = array(
					"timestamp" => $row["timestamp"],
					"NR1" => round($row["NR1"], 2),
					"QR1" => round($row["QR1"], 2),
					"PRA" => round($row["PRA"], 2),
					"PI" => round($row["PI"], 2),
					"NTENDENCIA" => $row["NTENDENCIA"],
					"ESTADOCOM" => $row["ET"]
				);

			}
		}
		return $return;
	}
}
