<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
class CurvaGasto extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method,Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('http_helper');        
        $this->load->library('session');
        $this->load->library('common/user');      
        $this->load->library('common/termFormula');  
        $this->load->library('common/node');      
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->model('curvaGastoDao');
        $this->load->library('sgi/VariableModel');
        $this->load->library('sgi/EstacionModel');
    }    
    function getInterpolacion_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_curva      = $parameters['data']['cod_curva'];           
        $this->response( $this->curvaGastoDao->getInterpolacion($cod_curva), 200);            
    }  
    function calcula_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_curva      = $parameters['data']['cod_curva'];           
        $cod_estacion      = $parameters['data']['cod_estacion'];           
        $cod_variable_caudal      = $parameters['data']['cod_variable_caudal'];           
        $cod_variable_nivel      = $parameters['data']['cod_variable_nivel'];           
        $fecha_ini      = $parameters['data']['fecha_ini'];           
        $fecha_fin      = $parameters['data']['fecha_fin'];           
        $this->response( $this->curvaGastoDao->calcula($cod_curva, $cod_estacion,  $cod_variable_caudal,$cod_variable_nivel, $fecha_ini, $fecha_fin), 200);            
    }     
    function importXMLData_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data      = $parameters['data'];           
        $this->response( $this->curvaGastoDao->importXMLData($data), 200);            
    }     
}
