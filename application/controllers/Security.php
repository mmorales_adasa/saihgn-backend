<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
require_once APPPATH . '/libraries/visor/class_jasper_reports.php';
class Security extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, AUTHJWT, Authorization, Access-Control-Request-Method, Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('auth2_helper');
        $this->load->library('session');        
        $this->load->helper('jwt_helper');        
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->library('common/user');           
        $this->load->model('BasicCrudDao');
        $this->load->model('SecurityDao');
        $this->load->library('email');
        
    } 
    /*function getSessionInfo_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $sessionId        = $parameters['SESSIONID']; 
        if ($this->SecurityDao->getSessionInfo($sessionId, $profile)){
            $this->response( $profile, 200);
        }else{
            $this->response( null, 401);    
        }
    }*/       
    function login_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $user        = $parameters['login']; 
        $pwd         = $parameters['password']; 
        if ($this->SecurityDao->login($user, $pwd)){
            $this->SecurityDao->getProfile($user,  "db", "VISOR", $profile);
            $this->response( $profile, 200);
        }else{            
            $ad = ldap_connect("ldap://" .  Constants::LDAP_SERVER) or die('Could not connect to LDAP server.');
            $dn = "CN=Users,DC=chguadiana,DC=es";
            $authenticated = @ldap_bind($ad, $user  .  "@chguadiana.es",  $pwd );
            if ($authenticated)
            {
                $isITuser = ldap_search($ad,$dn,'(&(objectClass=User)(sAMAccountName=' .  $user . '))');
                if ($isITuser) {
                    $this->SecurityDao->getProfile($user,  "db", "VISOR", $profile);
                    $this->response( $profile, 200);                                           
                } else {                
                    $this->response( null, 500);                        
                }
            }                   
            else{
                $this->response('Bad authentication', 401);                        
            }
        }
    }
    function recover_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $username  = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->recover($username, 200));
    }   

    function getPermisosNivel_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_nivel_acceso  = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getPermisosNivel($cod_nivel_acceso, 200));
    }   
    function getVideosNivel_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_nivel_acceso  = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getVideosNivel($cod_nivel_acceso, 200));
    }    
    function getInformesNivel_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_nivel_acceso  = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getInformesNivel($cod_nivel_acceso, 200));
    }      
    function getPermisosDataNivel_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_nivel_acceso  = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getPermisosDataNivel($cod_nivel_acceso, 200));
    }  
    function getPermisosDataUsuario_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_usuario  = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getPermisosDataUsuario($cod_usuario, 200));
    }            
    function getPermisosUsuario_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_usuario = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getPermisosUsuario($cod_usuario, 200));
    }  
    function getVideosUsuario_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_usuario = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getVideosUsuario($cod_usuario, 200));
    }
    function getInformesUsuario_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_usuario = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getInformesUsuario($cod_usuario, 200));
    }    
    function getUbicacionesUsuario_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_usuario = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getUbicacionesUsuario($cod_usuario, 200));
    }               
    function getPermisosDataUsuario2_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_usuario  = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getPermisosDataUsuario2($cod_usuario, 200));
    }      
    function getPermisosElemento_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_elemento = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getPermisosElemento($cod_elemento, 200));
    }     
    function getPermisosVideo_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria =  $this->filter->readCriteria($filters);     
        $cod_video = $criteria[0]->getValues()[0];        
        $this->response( $this->SecurityDao->getPermisosVideo($cod_video, 200));
    }   
         
    function savePermisosElementoNivel_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->SecurityDao->savePermisosElementoNivel($data), 200);    
    }      
    function savePermisosUsuario_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->SecurityDao->savePermisosUsuario($data['permisos'], $data['estaciones'], $data['videos'],$data['informes'],  $data['cod_usuario']), 200);    
    }  
    function savePermisosNivel_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->SecurityDao->savePermisosNivel($data['permisos'], $data['estaciones'], $data['videos'], $data['informes'],  $data['cod_nivel_acceso']), 200);    
    }     
    function savePermisosVideo_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->SecurityDao->savePermisosVideo($data['niveles'],  $data['cod_video']), 200);    
    }       
    function savePermisosUbicaciones_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->SecurityDao->savePermisosUbicaciones($data['ubicaciones'],  $data['cod_usuario']), 200);    
    }      
    function duplicateUser_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->SecurityDao->duplicateUser($data['cod_usuario']), 200);    
    }   
    function duplicateNivelAcceso_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->SecurityDao->duplicateNivelAcceso($data['cod_nivel_acceso']), 200);    
    }               
    function getProfile_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $user        = $parameters['COD_USUARIO']; 
        $app         = $parameters['APP']; 
        $ticketCAS   = $parameters['CASTICKET'];     
        $origin      = $parameters['ORIGIN'];       // ldap or db
        if ($this->SecurityDao->getProfile($user, $origin, $app, $profile)){
            $this->response( $profile, 200);
        }else{
            $this->response( null, 401);    
        }
    }
    function changePassword_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];         
        $username         = $data['username']; 
        $password         = $data['password']; 
        $this->response( $this->SecurityDao->changePassword($data['username'], $data['password']), 200);    
    }    

    function changePasswordVisor_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters  = $this->input->post();

        $usuario = $parameters['usuario'];
        $pwdActual = $parameters['pwdActual'];
        $pwdNuevo = $parameters['pwdNuevo'];

        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado) {
            if(isset($usuario) && isset($pwdActual) && isset($pwdNuevo)){
                // validar usuario
                $encontrado = $this->SecurityDao->getPasswordByUsername($usuario);

                if($encontrado!=null){

                    if($encontrado==$pwdActual){

                        // cambio de contraseña
                        $respuesta = $this->SecurityDao->changePasswordVisor($usuario, $pwdNuevo);
                        $this->response($respuesta, 200);
                        
                    } else {
                        $this->response(["error"=>"Contraseña actual no válida."], 200);
                    }
                } else {
                    $this->response(["error"=>"Usuario no encontrado."], 200);
                }
            } else {
                $this->response(null, 400);
            }
        } else {
            $this->response(null, 401);
        }

    }  

    function decodeJWT_post() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $jwt     = $parameters['data']['jwt'];        
        $token = AUTHORIZATION::validateToken($jwt);                
        $this->response( $token, 200);        
    }

    function testToken_get($username) {
        
        if ($this->SecurityDao->getProfile($username, "db", Constants::APP_VISOR , $profile)){            
             $token = AUTHORIZATION::validateToken($profile);
            //$this->response($token, 200);
            $this->response('kk', 200);
        }


    }

}
