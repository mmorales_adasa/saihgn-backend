<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');

require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
require_once APPPATH . '/libraries/visor/class_informe_variable.php';
require_once APPPATH . '/libraries/visor/class_explotacionV2.php';

class Telegram extends REST_Controller
{
    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, AUTHJWT, Authorization, Access-Control-Request-Method, Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('jwt_helper');
        $this->load->helper('date_helper');
        $this->load->helper('auth_helper');
        $this->load->helper('auth2_helper');
        $this->load->helper('log_helper');
        $this->load->helper('http_helper');
        $this->load->library('session'); 
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->library('common/ImagenBase64');
        $this->load->library('common/ImagenesBase64');
        $this->load->library('common/EstacionTelegram');
        $this->load->library('common/user');
        $this->load->library('common/ValoresEmbalse');
        $this->load->model('TelegramDao');
        $this->load->model('VisorDao');
        $this->load->model('SecurityDao');
        $this->load->model('BasicCrudDao');
        
    }

    function start_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();

        if (!isset($parameters['id_usuario'])) {
            $this->response(null, 200);
        }
        
        LOG::insertLog($parameters['id_usuario'], Constants::APP_TELEGRAM, "POST - Start", null, null);

        $cod_telegram = $parameters['id_usuario'];
        $user = $this->TelegramDao->getUserByTelegram($cod_telegram);

        if ($user) {
            $this->SecurityDao->getProfile($user["username"], "db", "VISOR", $profile);
            $this->response($profile, 200);
        } else {
            $this->response(null, 200);
        }
        
    }

    function estaciones_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $telegramID = (!is_null($this->input->get_request_header('telegramID'))) ? $this->input->get_request_header('telegramID') : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $parameters = $this->input->get();

        $nombreEstacion = isset($parameters['nombreEstacion']) ? $parameters['nombreEstacion'] : null;
        $codEstacion = isset($parameters['codEstacion']) ? $parameters['codEstacion'] : null;
        $tipoEstacion = isset($parameters['tipoEstacion']) ? $parameters['tipoEstacion'] : null;
        $zona = isset($parameters['zona']) ? $parameters['zona'] : null;
        LOG::insertLog($telegramID, Constants::APP_TELEGRAM, "GET - Obtener estaciones", $tipoEstacion, $codEstacion);

        $botPermissions = [
            "E" => sE::REDES_CONTROL_EMBALSES,
            "CR" => sE::REDES_CONTROL_AFOROS,
            "PZ" => sE::REDES_CONTROL_PIEZOMETROS,
            "PZM" => sE::REDES_CONTROL_PIEZOMETROS_MANUALES,
            "ECC" => sE::REDES_CONTROL_CALIDAD_REACAR,
            "R" => sE::REDES_CONTROL_REPETIDORES,
            "EM" => sE::REDES_CONTROL_METEO,
            "EAA" => sE::REDES_CONTROL_CALIDAD_SAICA,
            "S" => sE::REDES_CONTROL_CALIDAD_SMULTIPARAM,
            "EXC" => sE::REDES_CONTROL_CALIDAD_INDUSTRIAL,
            "NR" => sE::REDES_CONTROL_NIVELRIO,
            "CC" => sE::USOS_AGUA_CONCESIONES,
            "ACA" => sE::USOS_AGUA_CONCESIONES,
        ];
        
        if (isset($tipoEstacion) && $tokenValidado && $tokenValidado->payload && isset($botPermissions[$tipoEstacion]) && in_array($botPermissions[$tipoEstacion], $tokenValidado->payload->elementos)) {
            $response = $this->TelegramDao->getEstaciones($nombreEstacion, $codEstacion, $tipoEstacion, $zona, getGrantedStations($tokenValidado->payload->estaciones));
        } else {
            $response = $this->TelegramDao->getEstaciones($nombreEstacion, $codEstacion, $tipoEstacion, $zona);
        }
        $this->response($response, 200);
    }

    function valores_cache_estacion_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $telegramID = (!is_null($this->input->get_request_header('telegramID'))) ? $this->input->get_request_header('telegramID') : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $parameters = $this->input->get();

        $idEstacion = isset($parameters['idEstacion']) ? $parameters['idEstacion'] : null;
        $tipoEstacion = isset($idEstacion) ? $this->TelegramDao->getTipoEstacion($idEstacion) : null;
        LOG::insertLog($telegramID, Constants::APP_TELEGRAM, "GET - Instantaneo estacion", $tipoEstacion, $idEstacion);

        $botPermissions = [
            "E" => sE::REDES_CONTROL_EMBALSES,
            "CR" => sE::REDES_CONTROL_AFOROS,
            "PZ" => sE::REDES_CONTROL_PIEZOMETROS,
            "PZM" => sE::REDES_CONTROL_PIEZOMETROS_MANUALES,
            "ECC" => sE::REDES_CONTROL_CALIDAD_REACAR,
            "R" => sE::REDES_CONTROL_REPETIDORES,
            "EM" => sE::REDES_CONTROL_METEO,
            "EAA" => sE::REDES_CONTROL_CALIDAD_SAICA,
            "S" => sE::REDES_CONTROL_CALIDAD_SMULTIPARAM,
            "EXC" => sE::REDES_CONTROL_CALIDAD_INDUSTRIAL,
            "NR" => sE::REDES_CONTROL_NIVELRIO,
            "CC" => sE::USOS_AGUA_CONCESIONES,
            "ACA" => sE::USOS_AGUA_CONCESIONES,
        ];

        if ($tokenValidado && $tokenValidado->payload && isset($idEstacion) && isset($tipoEstacion) && isset($botPermissions[$tipoEstacion]) && in_array($botPermissions[$tipoEstacion], $tokenValidado->payload->elementos)) {
            $response = $this->TelegramDao->getValoresEstacionCache($idEstacion, getGrantedStations($tokenValidado->payload->estaciones));
            
        } else {
            $response = $this->TelegramDao->getValoresEstacionCache($idEstacion);  
        }

        $this->response($response, 200); 
    }

    function evolucionEstacion_post() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $telegramID = (!is_null($this->input->get_request_header('telegramID'))) ? $this->input->get_request_header('telegramID') : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();

        if (!isset($parameters['cod_estacion'])) {
            $this->response(null, 200);
        }

        $variables = $this->TelegramDao->getCodEstaciones($parameters['cod_estacion']);
        $zoom = (isset($parameters['zoom'])) ? $parameters['zoom'] : null;
        $fecha_i = (isset($parameters['fecha_i'])) ? $parameters['fecha_i'] : null;
        $fecha_f = (isset($parameters['fecha_f'])) ? $parameters['fecha_f'] : null;
        $tiempo = (isset($parameters['tiempo'])) ? $parameters['tiempo'] : null;
        $antiguedad = (isset($parameters['antiguedad'])) ? $parameters['antiguedad'] : null;
        $encabezado = (isset($parameters['encabezado'])) ? $parameters['encabezado'] : false;
        $tipoEstacion = isset($parameters['cod_estacion']) ? $this->TelegramDao->getTipoEstacion($parameters['cod_estacion']) : null;
        LOG::insertLog($telegramID, Constants::APP_TELEGRAM, "POST - Historico estacion", $tipoEstacion, $parameters['cod_estacion']);

        // REACAR
        if ($tokenValidado && $tokenValidado->payload && in_array(sE::REDES_CONTROL_CALIDAD_REACAR, $tokenValidado->payload->elementos) && strpos($parameters['cod_estacion'], "ECC") !== false) {
            $val = $this->VisorDao->getValoresReacar($parameters['cod_estacion'], $tiempo, $fecha_i, $fecha_f);
            $this->response($val, 200);
        }

        // Ind. Externos
        if ($tokenValidado && in_array(sE::REDES_CONTROL_CALIDAD_INDUSTRIAL, $tokenValidado->payload->elementos) &&
            strpos($parameters['cod_estacion'], "EXC") !== false) {
            $val = $this->VisorDao->getValoresEXC($parameters['cod_estacion'], $tiempo, $fecha_i, $fecha_f);
            $this->response($val, 200);
        }

        // Concesiones
        if($tokenValidado && $tokenValidado->payload && in_array(sE::USOS_AGUA_CONCESIONES, $tokenValidado->payload->elementos) && substr($parameters['cod_estacion'], 0, 2) === "CC") {
            $estacionesPorTipo = (array) $tokenValidado->payload->estaciones;
            foreach ($estacionesPorTipo as $tipo) {
                foreach ($tipo as $idEst) {
                    $estaciones_usuario[] = trim($idEst);
                }
            }
            if (isset($variables)) {
                $val = $this->TelegramDao->getEvolucionEstacion($zoom, $variables, $fecha_i, $fecha_f, $encabezado);
                $this->response($val, 200);
            }
        }

        // si el cod_estacion es de tipo PZ y la fecha inicial y final son igual, le sumamos 1 dia a la fecha final
        if (strpos($parameters['cod_estacion'], "04.") !== false && $fecha_i === $fecha_f) {
            $fecha_f = $fecha_i + 86400;
        }

        if (substr($parameters['cod_estacion'], 0, 1) === "S") {
            $val = $this->TelegramDao->getHistoricoSondasMulti($parameters['cod_estacion'], $antiguedad);
            $this->response($val, 200);
        }
        
        if (strpos($parameters['cod_estacion'], "EAA") !== false) {
            $val = $this->VisorDao->getValoresSAICA($parameters['cod_estacion'], $tiempo, $fecha_i, $fecha_f);
            $this->response($val, 200);
        }

        $response = $this->TelegramDao->getEvolucionEstacion($zoom, $variables, $fecha_i, $fecha_f, $encabezado);
        $this->response($response, 200);
    }

    function imagen_estacion_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $telegramID = (!is_null($this->input->get_request_header('telegramID'))) ? $this->input->get_request_header('telegramID') : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $parameters = $this->input->get();

        $cod_estacion = isset($parameters['cod_estacion']) ? $parameters['cod_estacion'] : null;
        $tipoEstacion = isset($cod_estacion) ? $this->TelegramDao->getTipoEstacion($cod_estacion) : null;
        LOG::insertLog($telegramID, Constants::APP_TELEGRAM, "GET - Obtener imagenes", $tipoEstacion, $cod_estacion);

        if (!isset($cod_estacion)) {
            $this->response(null, 200);
        } else {
            if ($tokenValidado) {
                $videos = $this->TelegramDao->getImagen($cod_estacion, $tokenValidado->payload->videos, getGrantedStations($tokenValidado->payload->estaciones));
            } else {
                $videos = $this->TelegramDao->getImagen($cod_estacion);
            }
            $this->response($videos, 200);
        }
    }

    function zonas_get() {
        $response = $this->TelegramDao->getZonas();
        $this->response($response, 200);
    }

    function evolucion_embalses_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $telegramID = (!is_null($this->input->get_request_header('telegramID'))) ? $this->input->get_request_header('telegramID') : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $parameters = $this->input->post();
        
        $ids = (isset($parameters['ids'])) ? $parameters['ids'] : null;
        $fecha_i = (isset($parameters['fecha_i'])) ? $parameters['fecha_i'] : null;
        $fecha_f = (isset($parameters['fecha_f'])) ? $parameters['fecha_f'] : null;
        $periodo = (isset($parameters['periodo'])) ? $parameters['periodo'] : null;

        LOG::insertLog($telegramID, Constants::APP_TELEGRAM, "POST - Evolucion embalses", "E", count($ids) > 0 ? $ids[0] : null);

        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_EVOLUCION_EMBALSES, $accesos)) {
                if (isset($ids) && isset($fecha_i) && isset($fecha_f) && isset($periodo)) {
                    $val = $this->VisorDao->EvolucionEMB($ids, $periodo, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function buscador_estaciones_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $parameters = $this->input->get();

        $filtro = isset($parameters['filtro']) ? $parameters['filtro'] : null;

        if ($tokenValidado) {
            $estaciones = $this->TelegramDao->getBuscadorEstaciones($filtro, getGrantedStations($tokenValidado->payload->estaciones));
        } else {
            $estaciones = $this->TelegramDao->getBuscadorEstaciones($filtro); 
        }
        $this->response($estaciones, 200);
    }

    function estaciones_usuario_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);


        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->estaciones) {
            $tiposEstaciones = array();
            foreach ($tokenValidado->payload->estaciones as $key => $val) {
                array_push($tiposEstaciones, $key);
            }
            $this->response($tiposEstaciones, 200);
        }
        $this->response(null, 401);
    }

    function valores_zonas_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $telegramID = (!is_null($this->input->get_request_header('telegramID'))) ? $this->input->get_request_header('telegramID') : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $parameters = $this->input->get();

        $idZona = isset($parameters['idZona']) ? $parameters['idZona'] : null;
        $tipo = isset($parameters['tipo']) ? $parameters['tipo'] : null;
        LOG::insertLog($telegramID, Constants::APP_TELEGRAM, "GET - Valores por zonas", null, $idZona);

        if ($tokenValidado) {
            $response = $this->TelegramDao->getValoresEmbalsesZonaAndDate($idZona, $parameters['fecha'], $tipo);
            $this->response($response, 200);
        }
        $this->response(null, 401);
    }

    function explotacion_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $tokenValidado = AUTHORIZATION::validateToken($token);

        $variables = $parameters['variables'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];

        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos && in_array(sE::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION, $tokenValidado->payload->elementos)) {
            if (isset($parameters) && isset($variables) && isset($periodo) && isset($fecha_i) && isset($fecha_f)) {
                $val["valores"] = ExplotacionV2::getExplotacion($variables, $periodo, $fecha_i, $fecha_f);
                $this->response($val, 200);
            } else {
                $this->response(null, 400);
            }
        } else {
            $this->response(null, 401);
        }
    }

}
