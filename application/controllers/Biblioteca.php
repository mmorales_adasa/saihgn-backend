<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');

require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/libraries/visor/security_elements.php");

class Biblioteca extends REST_Controller
{
    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, AUTHJWT, Authorization, Access-Control-Request-Method, Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('jwt_helper');
        $this->load->helper('date_helper');
        $this->load->helper('auth2_helper');
        $this->load->helper('http_helper');
        $this->load->model('bibliotecaDao');
        $this->load->library('common/fichaBiblioteca');
        $this->load->library('email');
    }

    function mailingReportePendientes_get(){
        $this->bibliotecaDao->mailRecordatorioPendientes();
    }

    function hasPermission($isAdmin) {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if (!$tokenValidado) {
            return false;
        }
        $accesos = $tokenValidado->payload->elementos;
        return in_array(sE::GESTION_DOCUMENTAL_BIBLIOTECA, $accesos) &&
            ($isAdmin ? in_array(sE::GESTION_DOCUMENTAL_BIBLIOTECA, $accesos) : true);
    }

    function datos_iniciales_get() 
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        
        $zonas = $this->bibliotecaDao->listaZonas();
        $provincias = $this->bibliotecaDao->listaProvincias();
        $tipos = $this->bibliotecaDao->listaTipos();
        $categorias = $this->bibliotecaDao->listaCategorias();
        $clases = $this->bibliotecaDao->listaClases();
        $municipios = $this->bibliotecaDao->listaMunicipios();
        $subcategorias = $this->bibliotecaDao->listaSubcategoria();
        $subtipos = $this->bibliotecaDao->listaSubtipos();
        $numeros = $this->bibliotecaDao->listaNumeros();
        $claves = $this->bibliotecaDao->listaClaves();
        $toponimos = $this->bibliotecaDao->listaToponimos();
        $notas = $this->bibliotecaDao->listaNotas();
        $contratistas = $this->bibliotecaDao->listaContratistas();
        $autores = $this->bibliotecaDao->listaAutores();
        $refletras = $this->bibliotecaDao->listaRefletras();
        $ubicaciones = $this->bibliotecaDao->listaUbicaciones();
        $ultimoRefNumero = $this->bibliotecaDao->getUltimoRefNumero();
        $archivos = $this->bibliotecaDao->listaArchivos();

        $retorno = [$zonas, $provincias, $tipos, $categorias, $clases, $municipios, $subcategorias, $subtipos, $numeros, 
                    $claves, $toponimos, $notas, $contratistas, $autores, $refletras, $ubicaciones, $ultimoRefNumero, $archivos];
        $this->response($retorno, 200);
    }

    function categorias_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaCategorias();
        $this->response($retorno, 200);
    }

    function municipios_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaMunicipios();
        $this->response($retorno, 200);
    }

    function zonas_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaZonas();
        $this->response($retorno, 200);
    }

    function clases_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaClases();
        $this->response($retorno, 200);
    }

    function provincias_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaProvincias();
        $this->response($retorno, 200);
    }

    function tipos_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaTipos();
        $this->response($retorno, 200);
    }

    function toponimos_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaToponimos();
        $this->response($retorno, 200);
    }

    function notas_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaNotas();
        $this->response($retorno, 200);
    }

    function contratistas_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaContratistas();
        $this->response($retorno, 200);
    }

    function autores_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaAutores();
        $this->response($retorno, 200);
    }

    
    function refletras_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaRefletras();
        $this->response($retorno, 200);
    }

    function proyectos_post()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $_POST = json_decode(file_get_contents('php://input'), true);
        $filters = $this->input->post();
        $retorno = $this->bibliotecaDao->listaProyectos($filters);
        $this->response($retorno, 200);
    }

    function usuarios_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaUsuarios();
        $this->response($retorno, 200);
    }

    function baldas_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaBaldas();
        $this->response($retorno, 200);
    }

    function numeros_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaNumeros();
        $this->response($retorno, 200);
    }

    function claves_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
        $retorno = $this->bibliotecaDao->listaClaves();
        $this->response($retorno, 200);
    }

    function altaProyecto_post()
    {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }

        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();

        $propiedades = [

            'zona' => $parameters['zona'],
            'provincia' => $parameters['provincia'],
            'categoria' => $parameters['categoria'],
            'tipo' => $parameters['tipo'],
            'clase' => $parameters['clase'],
            'municipio' => $parameters['municipio'],
            'subcategoria' => $parameters['subcategoria'],
            'subtipo' => $parameters['subtipo'],
            'numero' => $parameters['numero'],
            'titulo' => $parameters['titulo'],
            'autor' => $parameters['autor'],
            'fecha' => $parameters['fecha'],
            'clave' => $parameters['clave'],
            'toponimo' => $parameters['toponimo'],
            'notas' => $parameters['notas'],
            'contratista' => $parameters['contratista'],
            'ref_letra' => $parameters['ref_letra'],
            'ubicacion' => $parameters['ubicacion'],
            'balda' => $parameters['balda'],
            'mueble' => $parameters['mueble']
        ];
        $this->bibliotecaDao->altaDocumento($propiedades);
    }

    function modificarProyecto_put()
    {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }

        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();

        $propiedades = [
            'id_proyecto' => $parameters['id_proyecto'],
            'zona' => $parameters['zona'],
            'provincia' => $parameters['provincia'],
            'categoria' => $parameters['categoria'],
            'tipo' => $parameters['tipo'],
            'clase' => $parameters['clase'],
            'municipio' => $parameters['municipio'],
            'subcategoria' => $parameters['subcategoria'],
            'subtipo' => $parameters['subtipo'],
            'numero' => $parameters['numero'],
            'titulo' => $parameters['titulo'],
            'autor' => $parameters['autor'],
            'fecha' => $parameters['fecha'],
            'clave' => $parameters['clave'],
            'toponimo' => $parameters['toponimo'],
            'notas' => $parameters['notas'],
            'contratista' => $parameters['contratista'],
            'ref_letra' => $parameters['ref_letra'],
            'ubicacion' => $parameters['ubicacion'],
            'balda' => $parameters['balda'],
            'mueble' => $parameters['mueble']
        ];

        $this->bibliotecaDao->updateDocumento($propiedades);
    }

    function deleteProyecto_delete()
    {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }

        $parameters = $this->input->get();
        $id_proyecto = $parameters['id_proyecto'];

        if(isset($id_proyecto)){
            $resultado = $this->bibliotecaDao->deleteDocumento($id_proyecto);
            $this->response($resultado,200);
        } else {
            $this->response(null, 500);
        }
    }

    function subtipos_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $retorno = $this->bibliotecaDao->listaSubtipos();
        $this->response($retorno, 200);
    }

    function tipos_subtipos_get()
    {
        $parameters = $this->input->get();

        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $retorno = $this->bibliotecaDao->listaTipoSubtipos($parameters["tipo"]);
        $this->response($retorno, 200);
    }

    function categoria_subcategoria_get()
    {
        $parameters = $this->input->get();

        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

       $retorno = $this->bibliotecaDao->listaCategoriaSubcategoria($parameters["categoria"]);
        $this->response($retorno, 200);
    }

    function categoria_clase_get()
    {
        $parameters = $this->input->get();

        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

       $retorno = $this->bibliotecaDao->listaCategoriaClase($parameters["categoria"]);
        $this->response($retorno, 200);
    }

    function subcategoria_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $retorno = $this->bibliotecaDao->listaSubcategoria();
        $this->response($retorno, 200);
    }

    /*function prestadoById_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $parameters = $this->input->get();
        if ($parameters['id_proyecto']) {
            $retorno["detallePrestado"] = $this->bibliotecaDao->findPrestadoByIdProyecto($parameters['id_proyecto']);
            $this->response($retorno, 200);
        } else {
            $this->response(null, 400);
        }
    }*/

    function zonaById_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $parameters = $this->input->get();
        if ($parameters['idZona']) {
            $retorno["zona"] = $this->bibliotecaDao->findClaseById($parameters['idZona']);
            $this->response($retorno, 200);
        } else {
            $this->response(null, 400);
        }
    }

    function balda_mueble_get()
    {
        $parameters = $this->input->get();

        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $retorno = $this->bibliotecaDao->listaBaldaMueble(
            $parameters["ubicacion"], 
            isset($parameters["mueble"]) ? $parameters["mueble"]:null, 
            isset($parameters["balda"]) ? $parameters["balda"]:null
        );
        $this->response($retorno, 200);
    }

    function prestados_post()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }
    
        $_POST = json_decode(file_get_contents('php://input'), true);
        $filters = $this->input->post();
        $solicitudes = $this->bibliotecaDao->listaPrestados($filters);
        $this->response($solicitudes, 200);

    }

    function proyectoById_get()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $parameters = $this->input->get();
        if ($parameters['id_proyecto']) {
            $retorno["proyecto"] = $this->bibliotecaDao->findProyectoById($parameters['id_proyecto']);
            $retorno["historico"] = $this->bibliotecaDao->findPrestadoByIdProyecto($parameters['id_proyecto']);
            $this->response($retorno, 200);
        } else {
            $this->response(null, 400);
        }
    }

    function altaSolicitud_post()
    {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }

        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();

        $username = $parameters['username'];
        $id_proyecto = $parameters['id_proyecto'];
        $fecha_solicitud = $parameters['fecha_solicitud'];

        $resultado = $this->bibliotecaDao->altaSolicitud($username, $id_proyecto, $fecha_solicitud);

        // envío de correo si la solicitud ha sido registrada
        if($resultado == true && !in_array($username,["adasa","admin"])){ // mandar aviso de mail si la inserción ha sido correcta, no mandar si la solicitud es de usuarios de desarrollo
            $this->bibliotecaDao->mailSolicitud($username, $id_proyecto);
        }

        $this->response($resultado, 200);
    }

    function solicitudes_post()
    {
        if (!$this->hasPermission(false)) {
            $this->response(null, 400);
            return;
        }

        $_POST = json_decode(file_get_contents('php://input'), true);
        $filters = $this->input->post();
        $solicitudes = $this->bibliotecaDao->listaSolicitudes($filters);
        $this->response($solicitudes, 200);
    }

    function deleteSolicitud_delete()
    {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }

        $parameters = $this->input->get();

        $fecha_solicitud = $parameters['fecha_solicitud'];
        $id_proyecto = $parameters['id_proyecto'];
        $username = $parameters['username'];

        if(isset($fecha_solicitud) && isset($id_proyecto) && isset($username)){
            $resultado = $this->bibliotecaDao->borrarSolicitud($id_proyecto,$fecha_solicitud,$username);
            $this->response($resultado,200);
        } else {
            $this->response(null, 500);
        }
    }

    function aceptarSolicitud_post()
    {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }

        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();

        $id_proyecto = $parameters['id_proyecto'];
        $fecha_prestamo = $parameters['fecha_prestamo'];
        $username = $parameters['username'];

        $resultado = $this->bibliotecaDao->aceptarSolicitudDAO($fecha_prestamo, $id_proyecto, $username);

        $this->response($resultado, 200);
    }

    function devolverPrestamo_post()
    {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }

        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $id_proyecto = $parameters['id_proyecto'];
        $fecha_devolucion = $parameters['fecha_devolucion'];
        $username = $parameters['username'];
        $resultado = $this->bibliotecaDao->devolverPrestamo($id_proyecto, $fecha_devolucion, $username);

        $this->response($resultado, 200);
    }

    function ubicacionAdmin_post() {
        if (!$this->hasPermission(true)) {
            $this->response(null, 400);
            return;
        }
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        if ($parameters['cod_user']) {
            $retorno = $this->bibliotecaDao->findUbicacionesUser($parameters['cod_user']);
            $this->response($retorno, 200);
        } else {
            $this->response(null, 400);
        }
    }
}
