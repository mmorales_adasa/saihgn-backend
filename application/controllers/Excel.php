<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');

require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");

class Excel extends REST_Controller
{
    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, AUTHJWT, Authorization, Access-Control-Request-Method, Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('jwt_helper');
        $this->load->helper('date_helper');
        $this->load->helper('auth_helper');
        $this->load->helper('auth2_helper');
        $this->load->helper('http_helper');
        $this->load->library('session'); 
        $this->load->library('common/filter');
        $this->load->library('common/user');
        $this->load->model('ExcelDao');
        $this->load->model('visorDao');
    }

    function genera_historico_get() {
        $parameters = $this->input->get();
        $variables = $this->ExcelDao->getVariablesEstaciones();
        $fecha_i = 1664582400;
        $fecha_f = date_timestamp_get(date_create());
    
        $response = $this->ExcelDao->getEvolucionEstacionDiezMinutal("i", $variables, $fecha_i, $fecha_f, false);
        $this->response($response['valores'], 200);
    
    }

}
