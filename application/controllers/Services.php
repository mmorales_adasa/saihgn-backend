<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
class Services extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method,Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('http_helper');        
        $this->load->library('session');
        //$this->load->library('common/user');           
        //$this->load->model('userDAO');
    }    
    function test_get() {
        //$this->userDAO->getUsers();     
        $this->response( "ok!", 200);    
    }
}
