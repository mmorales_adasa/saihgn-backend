<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
class BasicCrud extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method,Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('http_helper');        
        $this->load->library('session');
        $this->load->library('common/user');      
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->model('BasicCrudDao');
        $this->load->model('UserDao');
    }    
    function getData_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $entity      = $parameters['data']['entity'];               
        $sort        = isset($parameters['data']['sort']) ? $parameters['data']['sort'] : null;             
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $this->response( $this->BasicCrudDao->getData($entity, $criteria,$sort), 200);    
    }
    function save_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters   = $this->input->post();
        $entity       = $parameters['data']['entity'];       
        $mode         = $parameters['data']['mode'];
        $model        = $parameters['data']['model'];

        if ($mode==Constants::DATABASE_OPERATION_INSERT){
            if ($this->BasicCrudDao->save($entity, $mode, $model)){
                $lastId = $this->BasicCrudDao->getLastId($entity);
                if (isset($lastId)) {
                        $this->response($lastId);
                }else{
                    $this->response(true);
                }
            }            
        }
        else{
            $this->response( $this->BasicCrudDao->save($entity, $mode, $model), 200);                
        }        
    }
    function getDataPagination_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $entity      = $parameters['data']['entity']; 
        $limit        = $parameters['data']['limit']; 
        $offset      = $parameters['data']['offset']; 
        $filters     = $parameters['data']['filters'];       
        $sort        = $parameters['data']['sort'];    
        
        $criteria = $sql= $this->filter->readCriteria($parameters['data']['filters']);
        $this->response( $this->BasicCrudDao->getDataPagination($entity, $limit, $offset,$criteria,$sort), 200);  
    } 
 
    function getDataPaginationCount_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $entity      = $parameters['data']['entity']; 
        $filters     = $parameters['data']['filters'];               
        $criteria = $sql= $this->filter->readCriteria($parameters['data']['filters']);
        $this->response( $this->BasicCrudDao->getDataPaginationCount($entity,$criteria), 200);  
    }     
}
