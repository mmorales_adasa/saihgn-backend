<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
class DatoBruto extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method,Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('http_helper');        
        $this->load->library('session');
        $this->load->library('common/user');      
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->model('datosBrutosDao');
        $this->load->library('sgi/VariableModel');
        $this->load->library('sgi/EstacionModel');
        $this->load->library('sgi/DatoBrutoModel');
        $this->load->library('sgi/MotivoInvalidacionModel');
    }    
    function findVariables_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $this->response( $this->datosBrutosDao->findVariables($criteria), 200);    
        
    }
    function findVariablesPorRed_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);      
        $red = $parameters['data']['red'];  
        $this->response( $this->datosBrutosDao->findVariablesPorRed($criteria, $red), 200);    
        
    }    
    function findData_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $this->response( $this->datosBrutosDao->findData($criteria), 200);    
    }     
    function findDataPorRed_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $red = $parameters['data']['red'];  
        $this->response( $this->datosBrutosDao->findDataPorRed($criteria, $red), 200);    
    }        
    function saveData_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->datosBrutosDao->saveData($data), 200);    
    }  
    function saveDataPorRed_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $measures = $parameters['data']['measures'];  
        $red      = $parameters['data']['red'];  
        $this->response( $this->datosBrutosDao->saveDataPorRed($measures, $red), 200);    
    }     
      
    function createData_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->datosBrutosDao->createData($data), 200);    
    }     
    /********************************************* */
    /****************AQUADAM********************** */
    /********************************************* */      
    function findDataAquadam_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $this->response( $this->datosBrutosDao->findDataAquadam($criteria), 200);  
    }
        function saveDataAquadam_post() {                   
            $_POST = json_decode(file_get_contents('php://input'), true);  
            $parameters  = $this->input->post();
            $data     = isset($parameters['data']) ? $parameters['data'] : [];  
            $this->response( $this->datosBrutosDao->saveDataAquadam($data), 200);         
    }
    function saveDataAquadamMasivo_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $data        = isset($parameters['data']) ? $parameters['data'] : [];  
        $this->response( $this->datosBrutosDao->saveDataAquadamMasivo($criteria, $data), 200);         
        
    }               
    function getReferenceDays_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];  
        $this->response( $this->datosBrutosDao->getReferenceDays($data), 200);    
    }         
    function getDatosNivel_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];  
        $this->response( $this->datosBrutosDao->getDatosNivel($data), 200);    
    }  
    function getDatosCotas_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];  
        $this->response( $this->datosBrutosDao->getDatosCotas($data), 200);    
    }          
    /********************************************* */
    /****************IMPORT********************** */
    /********************************************* */ 

    function importData_post() {                 
          
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        //$data     = isset($parameters['data']) ? $parameters['data'] : [];  
        $conf = $parameters['data']['conf'];  
        $vars = $parameters['data']['vars'];  
        $datos      = $parameters['data']['datos'];          
        $this->response( $this->datosBrutosDao->importData($conf, $vars,$datos), 200);    
    }  
    
    function fillHoles_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        //$data     = isset($parameters['data']) ? $parameters['data'] : [];  
        $frecuencia = $parameters['data']['frecuencia'];  
        $fechaIni = $parameters['data']['fechaIni'];  
        $fechaFin = $parameters['data']['fechaFin'];  
        $codVariable = $parameters['data']['codVariable'];  

        $this->response( $this->datosBrutosDao->fillHoles($frecuencia,$fechaIni, $fechaFin, $codVariable), 200);    
    }   
    function showHistory_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable = $parameters['data']['cod_variable'];  
        $timestamp_medicion = $parameters['data']['timestamp_medicion'];  
        $this->response( $this->datosBrutosDao->showHistory($cod_variable, $timestamp_medicion, 200));
    }  

    function forceSumarizacion_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $fechaIni = $parameters['data']['fechaIni'];  
        $fechaFin = $parameters['data']['fechaFin'];  
        $codVariable = $parameters['data']['codVariable'];  

        $this->response( $this->datosBrutosDao->forceSumarizacion($fechaIni, $fechaFin, $codVariable), 200);    
    }    
        
}
