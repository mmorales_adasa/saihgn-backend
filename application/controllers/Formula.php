<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
class Formula extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method,Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('http_helper');        
        $this->load->library('session');
        $this->load->library('common/user');      
        $this->load->library('common/termFormula');  
        $this->load->library('common/node');      
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->model('formulaDao');
        $this->load->library('sgi/VariableModel');
        $this->load->library('sgi/EstacionModel');
        

    }    
    function getNextValueSequence_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $this->response( $this->formulaDao->getNextValueSequence(), 200);    
    }
    function getEstacionesVariablesYConstantes_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $text      = $parameters['data']['text']; 
        $this->response( $this->formulaDao->getEstacionesVariablesYConstantes($text), 200);    
    }  
    function setRecalculo_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable      = $parameters['data']['cod_variable']; 
        $fecha_ini         = $parameters['data']['fecha_ini']; 
        $fecha_fin         = $parameters['data']['fecha_fin']; 
        $this->response( $this->formulaDao->setRecalculo($cod_variable, $fecha_ini, $fecha_fin), 200);    
    }        
    function getTokensFormula_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable      = $parameters['data']['cod_variable'];    
        $this->response( $this->formulaDao->getTokensFormula($cod_variable), 200);            
    }
    /*function obtenerNodoPorCodVariable_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable      = $parameters['data']['cod_variable'];    
        $this->response( $this->formulaDao->obtenerNodoPorCodVariable($cod_variable), 200);            
    }  
    function obtenerHijosPorNodo_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable      = $parameters['data']['cod_variable'];    
        $this->response( $this->formulaDao->obtenerHijosPorNodo($cod_variable), 200);            
    }  
    function updateNivellCalculNode_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable      = $parameters['data']['cod_variable'];    
        $this->response( $this->formulaDao->updateNivellCalculNode($cod_variable), 200);            
    }*/ 
    function test_get($cod_variable){
        
        //echo $cod_variable;
        $tokens = $this->formulaDao->getTokensFormula($cod_variable);
        /*foreach ($tokens as $token) {
            echo $token->cod_tipo_termino;
        }*/
        
        $this->formulaDao->upgradeTokensFormula($cod_variable,$tokens);
        $this->response("ok!", 200);            
    }
    
    function tractarVarsAillades_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable      = $parameters['data']['cod_variable'];           
        $cap = new Node();
        $cap->cod_variable = $cod_variable;
        $fills_anteriors = $this->formulaDao->getFills($cap);
        $this->response( true, 200);            
    }  
    
    function upgradeTokensFormula_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $cod_variable      = $parameters['data']['cod_variable'];    
        $tokens      = $parameters['data']['tokens'];
        $node =  new Node();
        //echo(gettype($tokens));
        $nodes = array();
        foreach ($tokens as $token) {
            $node =  new Node();
            $node->cod_tipo_termino = $token["cod_tipo_termino"];
            $node->cod_variable     = $token["cod_variable"];
            $node->variable        = $token["variable"];
            $node->num_termino     = $token["num_termino"];
            $node->tipo_valor     = $token["tipo_valor"];
            $node->constante     = $token["constante"];
            $node->constante_num     = $token["constante_num"];
            $node->calculada     = $token["calculada"];
            $node->operador     = $token["operador"];        
            array_push($nodes,$node); 
        }
        //$this->response("ok!", 200);    
        $this->response( $this->formulaDao->upgradeTokensFormula($cod_variable,$nodes), 200);            
    }    
    function checkTokensFormula_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $operadors      = $parameters['data'];    
        $this->response( $this->formulaDao->checkTokensFormula($operadors), 200);    
    }       
}
