<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');


require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/libraries/visor/security_elements.php");
class Visor extends REST_Controller
{
    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, AUTHJWT, Authorization, Access-Control-Request-Method, Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->helper('jwt_helper');
        $this->load->helper('date_helper');
        $this->load->helper('auth2_helper');
        $this->load->helper('http_helper');
        $this->load->model('visorDao');
    }

    function generar_datos_esquema_aex($DB, $datosRTAP)
    {

        // cargar resultado reciente y guardarlo como anterior para conseguir las tendencias
        $rtap_reciente_esquema_aex = $this->visorDao->CargarRecursoCache($DB, 'rtap_reciente_esquema_aex');

        if (isset($rtap_reciente_esquema_aex)) {
            $this->visorDao->GuardarRecursoCache($DB, 'rtap_anterior_esquema_aex', $rtap_reciente_esquema_aex);
        }

        $seleccionDatosRTAP = [];
        if (isset($datosRTAP["E"])) $seleccionDatosRTAP["E"] = $datosRTAP["E"];
        if (isset($datosRTAP["CR"])) $seleccionDatosRTAP["CR"] = $datosRTAP["CR"];

        // encode y guardado
        $json = json_encode($seleccionDatosRTAP, JSON_HEX_APOS); // escapar apostrofes
        $this->visorDao->GuardarRecursoCache($DB, 'rtap_reciente_esquema_aex', $json);
    }

    function generateVideoTimelapse_get()
    {
        require_once APPPATH . '/libraries/visor/class_cache_video.php';
        echo CacheVideo::CachearVideoTimelapse();
    }

    function generateVideoTimelapseCacheTest_get() {
        require_once APPPATH . '/libraries/visor/class_cache_videotimelapse_test.php';
        $val = CacheVideoTimelapseTest::CachearVideoTimelapse();
        var_dump($val);
    }

    function generateVideoCache_get() {
        require_once APPPATH . '/libraries/visor/class_cache_video.php';
        echo CacheVideo::CachearVideo();
    }

    function generateVideoCacheTest_get() {
        require_once APPPATH . '/libraries/visor/class_cache_video_test.php';
        echo CacheVideoTest::CachearVideo();
    }

    function testVideo_get()
    {
        $timelapsePath = APPPATH . "libraries/visor/video/";
        $files = scandir($timelapsePath);
        var_dump($files);
    }

    function testVideoReacar_get()
    {
        $timelapsePath = APPPATH . "libraries/visor/videoReacar/";
        $files = scandir($timelapsePath);
        var_dump($files);
    }

    function testVideoTimelapse_get()
    {
        $timelapsePath = APPPATH . "libraries/visor/timelapse/";
        $files = scandir($timelapsePath);
        var_dump($files);
    }

    function generarVideoReacar_get()
    {
        require_once APPPATH . '/libraries/visor/class_cache_video_reacar.php';
        echo CacheVideoReacar::CachearVideoReacarTimelapse();
    }

    // almacenar datos de rtap
    function guardar_RTAP_data($DB, $datosRTAP)
    {
        $json = json_encode($datosRTAP);
        $this->visorDao->GuardarRecursoCache($DB, 'rtap', $json);
    }

    // función para almacenamiento y cacheo de datos de tiempo real
    function generate_get()
    {
        date_default_timezone_set("UTC");
        $minuto = date("i");

        if(($minuto % 5) != 0 && ($minuto % 10) != 0) return;
        
        $datosRTAP = $this->visorDao->ObtenerDatosRTAP();

        $DB = ConexionBD::Conexion();

        // insertar en tbl_dato_tr los PI de forma cincominutal
        if(($minuto % 5) == 0 && isset($datosRTAP) && count($datosRTAP) > 0){
            $this->visorDao->setCincoMinutalFromRTAP($DB, $datosRTAP);
            $this->visorDao->setCincoMinutalFromRTAPTest($DB, $datosRTAP);
        }

        // insertar en tbl_dato_tr todas las variables menos PI de forma diezminutal
        if(($minuto % 10) == 0 && isset($datosRTAP) && count($datosRTAP) > 0){
            $this->visorDao->setDiezMinutalFromRTAP($DB, $datosRTAP);
            $this->visorDao->setDiezMinutalFromRTAPTest($DB, $datosRTAP);
            $this->visorDao->generar_datos_pluviometria($DB);
            $this->guardar_RTAP_data($DB, $datosRTAP);
            $this->generar_datos_esquema_aex($DB, $datosRTAP);
        }

        // cacheado de datos, no importa si no llegan datos de rtap
        if(($minuto % 10) == 0){
            $ultimosDatos = ObtenerDatos::UltimosDatos($DB);
            $this->visorDao->generar_master_ultres($DB, $ultimosDatos);
            $this->visorDao->generar_esquemas($DB, $ultimosDatos);
            $this->visorDao->GuardarUltimosDatos($DB, $ultimosDatos);
        }

        ConexionBD::CerrarConexion($DB);

    }

    // función para generar datos históricos en el fichero hist_embalses.json e históricos de precipitación/niveles de cada estación en ficheros separados, 
    // este fichero master es usado para otras funciones (generate por ejemplo)
    // recomendable llamarla cada 24h por cron dado el alto tiempo de ejecución
    function generate_diario_get()
    {
        $DB = ConexionBD::Conexion();
        // $this->generate_hist_embalses($DB);
        ConexionBD::CerrarConexion($DB);
    }

    /*function setCincoMinutal_get()
    {
        $this->visorDao->setCincoMinutalFromRTAP();
        $this->visorDao->setCincoMinutalFromRTAPTest();
    }*/

    /*function setDiezMinutal_get()
    {
        $this->visorDao->setDiezMinutalFromRTAP();
    }*/

    function testpermestaciones_get(){
        require_once APPPATH . '/libraries/visor/class_permisos_estacion.php';
        $DB = ConexionBD::Conexion();
        PermisosEstacion::getPermisoUsuariosTipoEstacion($DB,'ECC');
        ConexionBD::CerrarConexion($DB);
    }

    function testinsert_get()
    {
        $datosRTAP = $this->visorDao->ObtenerDatosRTAP();
        $DB = ConexionBD::Conexion();
        $this->visorDao->setCincoMinutalFromRTAPTest($DB, $datosRTAP);
        ConexionBD::CerrarConexion($DB);
    }

    function zonas_get()
    {

        $retorno = array();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado) {

            // se filtra la lista de estaciones recibida con la base de datos para recibir datos extra de las estaciones
            $estacionesPorTipo = (array) $tokenValidado->payload->estaciones;
            $idEstaciones = [];
            foreach ($estacionesPorTipo as $tipo) {
                foreach ($tipo as $idEstacion) {
                    $idEstaciones[] = trim($idEstacion);
                }
            }
            $estacionesDisponibles = $this->visorDao->Estaciones($idEstaciones);

            // obtener los accesos
            $accesos = $tokenValidado->payload->elementos;

            foreach ($estacionesDisponibles as $estacion) {
                // por cada estación disponible se añadie en el apartado de zonas si tiene el permiso para esa zona y es de tipo Embalse
                if (trim($estacion["zona"]) == "Z12" && $estacion["tipo"] == "E" && in_array(sE::ZONAS_Z12, $accesos)) $retorno["zonas"]["Z12"][] = $estacion;
                if (trim($estacion["zona"]) == "Z3" && $estacion["tipo"] == "E" && in_array(sE::ZONAS_Z3, $accesos)) $retorno["zonas"]["Z3"][] = $estacion;
                if (trim($estacion["zona"]) == "Z5" && $estacion["tipo"] == "E" && in_array(sE::ZONAS_Z5, $accesos)) $retorno["zonas"]["Z5"][] = $estacion;
                if (trim($estacion["zona"]) == "Z67" && $estacion["tipo"] == "E" && in_array(sE::ZONAS_Z67, $accesos)) $retorno["zonas"]["Z67"][] = $estacion;
                // en redes de control solo se agrupan por zona
                if (trim($estacion["zona"]) == "Z12") $retorno["redes_de_control"]["Z12"][] = $estacion;
                if (trim($estacion["zona"]) == "Z3") $retorno["redes_de_control"]["Z3"][] = $estacion;
                if (trim($estacion["zona"]) == "Z5") $retorno["redes_de_control"]["Z5"][] = $estacion;
                if (trim($estacion["zona"]) == "Z67") $retorno["redes_de_control"]["Z67"][] = $estacion;
                if (trim($estacion["zona"]) == "OCC") $retorno["redes_de_control"]["OCC"][] = $estacion;
                if (trim($estacion["zona"]) == "ORI") $retorno["redes_de_control"]["ORI"][] = $estacion;
            }
        }

        $this->response($retorno, 200);
    }

    function estaciones_get()
    {

        $retorno = array();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado) {

            // se filtra la lista de estaciones recibida con la base de datos para recibir datos extra de las estaciones
            $estacionesPorTipo = (array) $tokenValidado->payload->estaciones;
            $idEstaciones = [];
            foreach ($estacionesPorTipo as $tipo) {
                foreach ($tipo as $idEstacion) {
                    $idEstaciones[] = trim($idEstacion);
                }
            }

            $DB = ConexionBD::Conexion();
            $retorno["estaciones"] = $this->visorDao->EstacionesV2($DB, $idEstaciones);
            $retorno["masas"] = $this->visorDao->Masas($DB);
            $retorno["zonasRegables"] = $this->visorDao->ZonasRegables($DB);
            $retorno["alarmas_tr"] = $this->visorDao->ConfAlarmas_tr($DB);
            $retorno["tiposEstacion"] = $this->visorDao->TiposEstacion($DB); 
            ConexionBD::CerrarConexion($DB);
            
            $this->response($retorno, 200);
        } else {
            $this->response($retorno, 401);
        }
    }

    function cabeceras_get()
    {
        $cabeceras = $this->visorDao->Cabeceras();
        $this->response($cabeceras, 200);
    }

    function cabecerasV2_get()
    {
        $cabeceras = $this->visorDao->CabecerasV2();
        $this->response($cabeceras, 200);
    }    
    
    function generartest_get() {
        $url = APPPATH . "/esquemas/";
		$esquemaHTML = @file_get_contents($url . 'E2-16.htm');
		$esquemaHTML = iconv(mb_detect_encoding($esquemaHTML, mb_detect_order(), true), "UTF-8//IGNORE", $esquemaHTML);
		if ($esquemaHTML == false) {
			echo false;
		} else {
            var_dump($esquemaHTML);
        }
    }

    function generarEsquemas_get() {
        $DB = ConexionBD::Conexion();
        $this->visorDao->generar_esquemas($DB);
        ConexionBD::CerrarConexion($DB);
    }    

    function getUltimosvalores_get(){
        $DB = ConexionBD::Conexion();
        var_dump($this->visorDao->CargarUltimosDatos($DB));
        ConexionBD::CerrarConexion($DB);
    }
    
    function resourceByID_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $id = (isset($parameters['id'])) ? $parameters['id'] : null;
        $idEstacion = (isset($parameters['idEstacion'])) ? $parameters['idEstacion'] : null;
        $type = $parameters['type'];

        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado) {

            $estacionesPorTipo = (array) $tokenValidado->payload->estaciones;
            $estaciones_usuario = [];
            $video_usuario = [];
            foreach ($estacionesPorTipo as $tipo) {
                foreach ($tipo as $idEst) {
                    $estaciones_usuario[] = trim($idEst);
                }
            }

            if (isset($tokenValidado->payload->videos)) {
                $video_usuario = $tokenValidado->payload->videos;
            }

            // obtener los accesos
            $accesos = $tokenValidado->payload->elementos;

            // listado entero de estaciones con sus datos de rtap
            switch ($type) {
                case 'ficha_sira':
                    $json = array();
                    $DB = ConexionBD::Conexion();
                    $json['info'] = $this->visorDao->GetInfo($DB, 'SIRA');
                    ConexionBD::CerrarConexion($DB);
                    $this->response($json, 200);

                    break;
                case 'mapa_estaciones':
                    $json = array();

                    // filtrar por permiso y luego por estaciones
                    $permList["E"] =    [sE::REDES_CONTROL_EMBALSES, sE::REDES_CONTROL_EMBALSES_MAPA];
                    $permList["CR"] =   [sE::REDES_CONTROL_AFOROS, sE::REDES_CONTROL_AFOROS_MAPA];
                    $permList["PZ"] =   [sE::REDES_CONTROL_PIEZOMETROS, sE::REDES_CONTROL_PIEZOMETROS_MAPA];
                    $permList["PZM"] =  [sE::REDES_CONTROL_PIEZOMETROS_MANUALES, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_MAPA];
                    $permList["R"] =    [sE::REDES_CONTROL_REPETIDORES, sE::REDES_CONTROL_REPETIDORES_MAPA];
                    $permList["EM"] =   [sE::REDES_CONTROL_METEO, sE::REDES_CONTROL_METEO_MAPA];
                    $permList["EMAEMET"] =   [sE::REDES_CONTROL_METEO, sE::REDES_CONTROL_METEO_AEMET_MAPA];
                    $permList["CAL"] =  [sE::REDES_CONTROL_CALIDAD_RESUMEN, sE::REDES_CONTROL_CALIDAD_RESUMEN_MAPA];
                    $permList["EAA"] =  [sE::REDES_CONTROL_CALIDAD_SAICA, sE::REDES_CONTROL_CALIDAD_SAICA_MAPA];
                    $permList["ECC"] =  [sE::REDES_CONTROL_CALIDAD_REACAR, sE::REDES_CONTROL_CALIDAD_REACAR_MAPA];
                    $permList["EXC"] =  [sE::REDES_CONTROL_CALIDAD_INDUSTRIAL, sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_MAPA];
                    $permList["NR"] =   [sE::REDES_CONTROL_NIVELRIO, sE::REDES_CONTROL_NIVELRIO_MAPA];
                    $permList["S"] =   [sE::REDES_CONTROL_CALIDAD_SMULTIPARAM, sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_MAPA];
                    $permList["D"] =   [sE::REDES_CONTROL_ENPROYECTO, sE::REDES_CONTROL_ENPROYECTO_MAPA];
                    $permList["CC"] =   [sE::USOS_AGUA_CONCESIONES, sE::USOS_AGUA_CONCESIONES_CONDUCCION_MAPA];
                    $permList["ACA"] =   [sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL, sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_MAPA];

                    $DB = ConexionBD::Conexion();
                    $json_cache = json_decode($this->visorDao->CargarRecursoCache($DB, "master_ultres_categorias"), true);
                    ConexionBD::CerrarConexion($DB);

                    if (isset($json_cache)) {
                        foreach ($json_cache as $key => $datos) {
                            if ((isset($permList[$key]) && in_array($permList[$key][0], $accesos) && in_array($permList[$key][1], $accesos))) {
                                // s.multi o en proyecto no se necesita que el usuario tenga la estación, el resto se revisa
                                if($key == "D"){
                                    $json[$key]['ult_res'] = $datos;
                                } else {
                                    $json[$key]['ult_res'] = $this->filtrarULT_RES($datos, $estaciones_usuario, $key, $accesos);
                                }
                            }
                        }
                    }

                    $this->response($json, 200);

                    break;
                case 'ficha_redes_control':

                    $json = array();

                    $perm = null;
                    $permList["E"] =    [sE::REDES_CONTROL_EMBALSES, sE::REDES_CONTROL_EMBALSES_INFO, sE::REDES_CONTROL_EMBALSES_DATOS, sE::REDES_CONTROL_EMBALSES_NOTAS, sE::REDES_CONTROL_EMBALSES_LLUVIAS];
                    $permList["CR"] =   [sE::REDES_CONTROL_AFOROS, sE::REDES_CONTROL_AFOROS_INFO, sE::REDES_CONTROL_AFOROS_DATOS, sE::REDES_CONTROL_AFOROS_NOTAS];
                    $permList["PZ"] =   [sE::REDES_CONTROL_PIEZOMETROS, sE::REDES_CONTROL_PIEZOMETROS_INFO, sE::REDES_CONTROL_PIEZOMETROS_DATOS, sE::REDES_CONTROL_PIEZOMETROS_NOTAS];
                    $permList["PZM"] =  [sE::REDES_CONTROL_PIEZOMETROS_MANUALES, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_INFO, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DATOS, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_NOTAS];
                    $permList["R"] =    [sE::REDES_CONTROL_REPETIDORES, sE::REDES_CONTROL_REPETIDORES_INFO, sE::REDES_CONTROL_REPETIDORES_DATOS, sE::REDES_CONTROL_REPETIDORES_NOTAS];
                    $permList["EM"] =   [sE::REDES_CONTROL_METEO, sE::REDES_CONTROL_METEO_INFO, sE::REDES_CONTROL_METEO_DATOS, sE::REDES_CONTROL_METEO_NOTAS];
                    $permList["CAL"] =  [sE::REDES_CONTROL_CALIDAD_RESUMEN, sE::REDES_CONTROL_CALIDAD_RESUMEN_INFO, sE::REDES_CONTROL_CALIDAD_RESUMEN_DATOS, sE::REDES_CONTROL_CALIDAD_RESUMEN_NOTAS];
                    $permList["EAA"] =  [sE::REDES_CONTROL_CALIDAD_SAICA, sE::REDES_CONTROL_CALIDAD_SAICA_INFO, sE::REDES_CONTROL_CALIDAD_SAICA_DATOS, sE::REDES_CONTROL_CALIDAD_SAICA_NOTAS];
                    $permList["ECC"] =  [sE::REDES_CONTROL_CALIDAD_REACAR, sE::REDES_CONTROL_CALIDAD_REACAR_INFO, sE::REDES_CONTROL_CALIDAD_REACAR_DATOS, sE::REDES_CONTROL_CALIDAD_REACAR_NOTAS];
                    $permList["S"] =  [sE::REDES_CONTROL_CALIDAD_SMULTIPARAM, sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_INFO, sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DATOS, sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_NOTAS];
                    $permList["EXC"] =  [sE::REDES_CONTROL_CALIDAD_INDUSTRIAL, sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_INFO, sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_DATOS, sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_NOTAS];
                    $permList["NR"] =   [sE::REDES_CONTROL_NIVELRIO, sE::REDES_CONTROL_NIVELRIO_INFO, sE::REDES_CONTROL_NIVELRIO_DATOS, sE::REDES_CONTROL_NIVELRIO_NOTAS];
                    $permList["D"] =    [sE::REDES_CONTROL_ENPROYECTO, sE::REDES_CONTROL_ENPROYECTO_INFO, sE::REDES_CONTROL_ENPROYECTO_DATOS, sE::REDES_CONTROL_ENPROYECTO_DATOS];
                    $permList["CC"] =    [sE::USOS_AGUA_CONCESIONES, sE::USOS_AGUA_CONCESIONES_CONDUCCION_INFO, sE::USOS_AGUA_CONCESIONES_CONDUCCION_DATOS, sE::USOS_AGUA_CONCESIONES_CONDUCCION_NOTAS];
                    $permList["ACA"] =    [sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL, sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_INFO, sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_DATOS, sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_NOTAS];

                    $esquemaPermissions = [
                        "E" => sE::REDES_CONTROL_EMBALSES_ESQUEMAS,
                        "CR" => sE::REDES_CONTROL_AFOROS_ESQUEMAS,
                        "PZ" => sE::REDES_CONTROL_PIEZOMETROS_ESQUEMAS,
                        "PZM" => sE::REDES_CONTROL_PIEZOMETROS_MANUALES_ESQUEMAS,
                        "R" => sE::REDES_CONTROL_REPETIDORES_ESQUEMAS,
                        "EM" => sE::REDES_CONTROL_METEO_ESQUEMAS,
                        "CAL" => sE::REDES_CONTROL_CALIDAD_RESUMEN_ESQUEMAS,
                        "EAA" => sE::REDES_CONTROL_CALIDAD_SAICA_ESQUEMAS,
                        "ECC" => sE::REDES_CONTROL_CALIDAD_REACAR_ESQUEMAS,
                        "S" => sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_ESQUEMAS,
                        "EXC" => sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_ESQUEMAS,
                        "NR" => sE::REDES_CONTROL_NIVELRIO_ESQUEMAS,
                        "D" => sE::REDES_CONTROL_ENPROYECTO_ESQUEMAS,
                        "CC" => sE::USOS_AGUA_CONCESIONES_CONDUCCION_ESQUEMAS,
                        "ACA" => sE::USOS_AGUA_CONCESIONES_CONDUCCION_ESQUEMAS,
                    ];

                    $videoPermissions = [
                        "E" => sE::REDES_CONTROL_EMBALSES_VIDEO,
                        "CR" => sE::REDES_CONTROL_AFOROS_VIDEO,
                        "PZ" => sE::REDES_CONTROL_PIEZOMETROS_VIDEO,
                        "PZM" => sE::REDES_CONTROL_PIEZOMETROS_MANUALES_VIDEO,
                        "R" => sE::REDES_CONTROL_REPETIDORES_VIDEO,
                        "EM" => sE::REDES_CONTROL_METEO_VIDEO,
                        "CAL" => sE::REDES_CONTROL_CALIDAD_RESUMEN_VIDEO,
                        "EAA" => sE::REDES_CONTROL_CALIDAD_SAICA_VIDEO,
                        "ECC" => sE::REDES_CONTROL_CALIDAD_REACAR_VIDEO,
                        "S" => sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_VIDEO,
                        "EXC" => sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_VIDEO,
                        "NR" => sE::REDES_CONTROL_NIVELRIO_VIDEO,
                        "D" => sE::REDES_CONTROL_ENPROYECTO_VIDEO,
                        "CC" => sE::USOS_AGUA_CONCESIONES_CONDUCCION_VIDEO,
                        "ACA" => sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_VIDEO,
                    ];

                    if (isset($permList[$id]) && in_array($permList[$id][0], $accesos) && (in_array($permList[$id][2], $accesos) || in_array($permList[$id][3], $accesos))) {

                        $DB = ConexionBD::Conexion();
                        $datosRTAP = (array) json_decode($this->visorDao->CargarRecursoCache($DB, 'rtap'), true);
                        $json_cache = json_decode($this->visorDao->CargarRecursoCache($DB, "master_ultres_categorias"), true);

                        $datos_estacion = array();

                        $json['info'] = (isset($permList[$id][1]) && in_array($permList[$id][1], $accesos)) ? $this->visorDao->GetInfo($DB, $id) : [];


                        if ($id == "CAL") {
                
                            $datos_estacion['ult_res']['EAA'] = $json_cache["EAA"];
                            $datos_estacion['ult_res']['ECC'] = $json_cache["ECC"];
                            $datos_estacion['ult_res']['EXC'] = $json_cache["EXC"];

                            $datos_estacion['est_res']['EAA'] = $this->visorDao->UltimosDatosMedidos("EST/RES", 'EAA', null, null, $DB);
                            $datos_estacion['est_res']['ECC'] = $this->visorDao->UltimosDatosMedidos("EST/RES", 'ECC', null, null, $DB);
                            $datos_estacion['est_res']['EXC'] = $this->visorDao->UltimosDatosMedidos("EST/RES", 'EXC', null, null, $DB);
                        
                        } else {
                
                            $datos_estacion['ult_res'] = $json_cache[$id] ?? null;
                            $datos_estacion['est_res'] = $this->visorDao->UltimosDatosMedidos("EST/RES", $id, null, null, $DB);
                        }

                        if($id=="E"){

                            // cálculo de volumen/capacidad/porcentaje según los embalses de la zona
                            if (isset($datos_estacion['ult_res']) && isset($datos_estacion['ult_res']['valores'])) {
                                $zonaVolumen = 0;
                                $zonaCapacidad = 0;
                                $zonaPorcentajeVol = 0;
                                $valores = $datos_estacion['ult_res']['valores'];

                                foreach ($valores as $ult_res_embalse) {
                                    if (
                                        isset($ult_res_embalse) 
                                        && isset($ult_res_embalse["VE1"]) 
                                        && $ult_res_embalse["VE1"] != 0 
                                        && $ult_res_embalse["VE1"] != null 
                                        && isset($ult_res_embalse["PV1"]) 
                                        && $ult_res_embalse["PV1"] != 0 
                                        && $ult_res_embalse["PV1"] != null) {

                                        // calcular los valores de zona a partir de los embalses que incorpora
                                        $embalseVolumen = $ult_res_embalse["VE1"];
                                        $embalsePorcentaje = $ult_res_embalse["PV1"];
                                        $zonaCapacidad += ($embalseVolumen / $embalsePorcentaje) * 100;
                                        $zonaVolumen += $embalseVolumen;
                                        $zonaPorcentajeVol = ($zonaVolumen / $zonaCapacidad) * 100;
                                    }
                                }
                                $json['embalses_volcap'] = ["volumen" => $zonaVolumen, "capacidad" => $zonaCapacidad, "porcentajeVol" => $zonaPorcentajeVol];
                            }

                            if(in_array($permList[$id][4], $accesos)){
                                $json['embalses_aemet'] = $this->visorDao->UltimosDatosEmbalsesAemet($estaciones_usuario, $DB);
                                $json['embalses_lluviaSAIH'] = $this->visorDao->UltimosDatosEmbalsesLluviaSAIH($estaciones_usuario, $DB);
                                $json['embalses_lluviaPonderadaSAIH'] = $this->visorDao->UltimosDatosEmbalsesLluviaPonderadaSAIH($estaciones_usuario, $DB);
                            }

                            $json['video'] = $this->visorDao->CamarasEmbalses();
                            $json['video'] = (in_array(sE::REDES_CONTROL_EMBALSES_VIDEO, $accesos) && isset($json['video'])) ? $this->filtrarVIDEO($json['video'], $video_usuario) : [];
                        } else {
                            if (in_array($videoPermissions[$id], $accesos)) {
                                $json['video'] = $this->visorDao->CamarasRedesGeneral($id);
                                $json['video'] = $json['video'] ? $this->filtrarVIDEO($json['video'], $video_usuario) : [];
                            }
                        }

                        if (in_array($esquemaPermissions[$id], $accesos)) {
                            $json['esquemaV2'] = $this->visorDao->getEsquemaV2($DB, $id);
                        }

                        ConexionBD::CerrarConexion($DB);

                        // filtrar datos usando los permisos del usuario
                        if ($permList[$id][2] && $id != "CAL" && isset($datos_estacion['ult_res'])) {

                            // si el tipo es 'en desarrollo' no se filtra
                            if($id == "D"){
                                $json['ult_res'] = $datos_estacion['ult_res'];
                            } else {
                                $json['ult_res'] = $this->filtrarULT_RES($datos_estacion['ult_res'], $estaciones_usuario, $id, $accesos);
                            }

                        }

                        // CAL (debe contiene los datos de EAA, ECC y EXC)
                        if ($permList[$id][2] && $id == "CAL" && isset($datos_estacion['ult_res'])) {

                            if (in_array($permList["EAA"][2], $accesos)) $json['ult_res']['EAA'] = $this->filtrarULT_RES($datos_estacion['ult_res']['EAA'], $estaciones_usuario);
                            if (in_array($permList["ECC"][2], $accesos)) $json['ult_res']['ECC'] = $this->filtrarULT_RES($datos_estacion['ult_res']['ECC'], $estaciones_usuario);
                            if (in_array($permList["EXC"][2], $accesos)) $json['ult_res']['EXC'] = $this->filtrarULT_RES($datos_estacion['ult_res']['EXC'], $estaciones_usuario);
                        
                        }

                        if ($permList[$id][3] && isset($datos_estacion['est_res'])) {
                            $json['est_res'] = $this->filtrarEST_RES($datos_estacion['est_res'], $estaciones_usuario);
                        }
                    }

                    $this->response($json, 200);
                    break;

                case 'ficha_redes_control_detalle':
                    $json = array();

                    $permList["EAA"] = [sE::REDES_CONTROL_CALIDAD_SAICA_DETALLE, sE::REDES_CONTROL_CALIDAD_SAICA_DETALLE_INFO, sE::REDES_CONTROL_CALIDAD_SAICA_DETALLE_DATOS, sE::REDES_CONTROL_CALIDAD_SAICA_DETALLE_NOTAS];
                    $permList["ECC"] = [sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE, sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_INFO, sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_DATOS, sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_NOTAS];
                    $permList["S"] = [sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE, sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE_INFO, sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE_DATOS, sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE_NOTAS];
                    $permList["EXC"] = [sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_DETALLE, sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_DETALLE_INFO, sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_DETALLE_DATOS, sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_DETALLE_NOTAS];
                    $permList["NR"] =  [sE::REDES_CONTROL_NIVELRIO_DETALLE, sE::REDES_CONTROL_NIVELRIO_DETALLE_INFO, sE::REDES_CONTROL_NIVELRIO_DETALLE_DATOS, sE::REDES_CONTROL_NIVELRIO_DETALLE_NOTAS, sE::REDES_CONTROL_NIVELRIO_DETALLE_VARIABLES];
                    $permList["PZ"] =  [sE::REDES_CONTROL_PIEZOMETROS_DETALLE, sE::REDES_CONTROL_PIEZOMETROS_DETALLE_INFO, sE::REDES_CONTROL_PIEZOMETROS_DETALLE_DATOS, sE::REDES_CONTROL_PIEZOMETROS_DETALLE_NOTAS, sE::REDES_CONTROL_PIEZOMETROS_DETALLE_VARIABLES];
                    $permList["PZM"] =  [sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DETALLE, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DETALLE_INFO, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DETALLE_DATOS, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DETALLE_NOTAS, sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DETALLE_VARIABLES];
                    $permList["CR"] =  [sE::REDES_CONTROL_AFOROS_DETALLE, sE::REDES_CONTROL_AFOROS_DETALLE_INFO, sE::REDES_CONTROL_AFOROS_DETALLE_DATOS, sE::REDES_CONTROL_AFOROS_DETALLE_NOTAS, sE::REDES_CONTROL_AFOROS_DETALLE_VARIABLES];
                    $permList["R"] =  [sE::REDES_CONTROL_REPETIDORES_DETALLE, sE::REDES_CONTROL_REPETIDORES_DETALLE_INFO, sE::REDES_CONTROL_REPETIDORES_DETALLE_DATOS, sE::REDES_CONTROL_REPETIDORES_DETALLE_NOTAS, sE::REDES_CONTROL_REPETIDORES_DETALLE_VARIABLES];
                    $permList["EM"] =  [sE::REDES_CONTROL_METEO_DETALLE, sE::REDES_CONTROL_METEO_DETALLE_INFO, sE::REDES_CONTROL_METEO_DETALLE_DATOS, sE::REDES_CONTROL_METEO_DETALLE_NOTAS, sE::REDES_CONTROL_METEO_DETALLE_VARIABLES];
                    $permList["CC"] =  [sE::USOS_AGUA_CONCESIONES_CONDUCCION_DETALLE, sE::USOS_AGUA_CONCESIONES_CONDUCCION_DETALLE_INFO, sE::USOS_AGUA_CONCESIONES_CONDUCCION_DETALLE_DATOS, sE::USOS_AGUA_CONCESIONES_CONDUCCION_DETALLE_NOTAS, sE::USOS_AGUA_CONCESIONES_CONDUCCION_DETALLE_VARIABLES];
                    $permList["ACA"] =    [sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_DETALLE, sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_DETALLE_INFO, sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_DETALLE_DATOS, sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_DETALLE_NOTAS];

                    $esquemaPermissions = [
                        "EAA" => sE::REDES_CONTROL_CALIDAD_SAICA_DETALLE_ESQUEMAS,
                        "ECC" => sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_ESQUEMAS,
                        "S" => sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE_ESQUEMAS,
                        "EXC" => sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_DETALLE_ESQUEMAS,
                        "NR" => sE::REDES_CONTROL_NIVELRIO_DETALLE_ESQUEMAS,
                        "PZ" => sE::REDES_CONTROL_PIEZOMETROS_DETALLE_ESQUEMAS,
                        "PZM" => sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DETALLE_ESQUEMAS,
                        "CR" => sE::REDES_CONTROL_AFOROS_DETALLE_ESQUEMAS,
                        "R" => sE::REDES_CONTROL_REPETIDORES_DETALLE_ESQUEMAS,
                        "EM" => sE::REDES_CONTROL_METEO_DETALLE_ESQUEMAS,
                        "CC" => sE::USOS_AGUA_CONCESIONES_CONDUCCION_DETALLE_ESQUEMAS,
                        "ACA" => sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_DETALLE_ESQUEMAS,
                    ];

                    $videoPermissions = [
                        "EAA" => sE::REDES_CONTROL_CALIDAD_SAICA_DETALLE_VIDEO,
                        "ECC" => sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_VIDEO,
                        "S" => sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE_VIDEO,
                        "EXC" => sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_DETALLE_VIDEO,
                        "NR" => sE::REDES_CONTROL_NIVELRIO_DETALLE_VIDEO,
                        "PZ" => sE::REDES_CONTROL_PIEZOMETROS_DETALLE_VIDEO,
                        "PZM" => sE::REDES_CONTROL_PIEZOMETROS_MANUALES_DETALLE_VIDEO,
                        "CR" => sE::REDES_CONTROL_AFOROS_DETALLE_VIDEO,
                        "R" => sE::REDES_CONTROL_REPETIDORES_DETALLE_VIDEO,
                        "EM" => sE::REDES_CONTROL_METEO_DETALLE_VIDEO,
                        "CC" => sE::USOS_AGUA_CONCESIONES_CONDUCCION_DETALLE_VIDEO,
                        "ACA" => sE::REDES_CONTROL_AREA_CALIDAD_AMBIENTAL_DETALLE_VIDEO,
                    ];

                    if (in_array($id, ["ECC", "EAA", "EXC", "NR", "PZ", "PZM", "CR","R","EM", "S", "CC","ACA"]) && $idEstacion && in_array($idEstacion, $estaciones_usuario)) {

                        // tiene permiso de sección
                        if (in_array($permList[$id][0], $accesos)) {

                            $DB = ConexionBD::Conexion();

                            $ultimosDatos = $this->visorDao->CargarUltimosDatos($DB);

                            $ult_res_estacion = $this->visorDao->CargarMasterULTRESEstaciones($DB, $idEstacion);
                            if ((isset($permList[$id]) && in_array($permList[$id][2], $accesos))) {
                                $json['ult_res'] = $this->filtrarULT_RES($ult_res_estacion, $estaciones_usuario, $id, $accesos);
                            }
                            
                            if ($id == 'PZ') {
                                $json['ficha'] = in_array(sE::REDES_CONTROL_PIEZOMETROS_DETALLE_FICHA, $accesos) && $this->visorDao->hasEsquemaPiezo($idEstacion);
                            }

                            $json['info'] = (isset($permList[$id][1]) && in_array($permList[$id][1], $accesos)) ? $this->visorDao->GetInfo($DB, $idEstacion) : [];
                            if (in_array($videoPermissions[$id], $accesos)) {
                                $json['video'] = $this->visorDao->CamarasRedes($idEstacion);
                                $json['video'] = $json['video'] ? $this->filtrarVIDEO($json['video'], $video_usuario) : [];
                            }
                            $json['est_res'] = (isset($permList[$id][3]) && in_array($permList[$id][3], $accesos)) ? $this->visorDao->UltimosDatosMedidos("EST/RES", $id, $idEstacion, null, $DB) : [];
                            $json['ult_rtap'] = (isset($permList[$id][4]) && in_array($permList[$id][4], $accesos)) ? $this->visorDao->UltimosDatosMedidos("ULT/RTAP", $id, $idEstacion, null, $DB, $ultimosDatos) : [];

                            if (in_array($esquemaPermissions[$id], $accesos)) {
                                $json['esquemaV2'] = $this->visorDao->getEsquemaV2($DB, $idEstacion);
                            }

                            if ($id == 'CR' && in_array(sE::REDES_CONTROL_AFOROS_DETALLE_COTAS, $accesos)) {
                                $json['niv_detr'] = $this->visorDao->UltimosDatosMedidos("NIV/DETR", null, $idEstacion, null, $DB);
                            }

                            ConexionBD::CerrarConexion($DB);
                            
                            $this->response($json, 200);
                        } else {
                            $this->response(null, 401);
                        }
                    } else {
                        $this->response(null, 401);
                    }
                    break;

                case 'ficha_zonas_aex':
                    $json = array();

                    $permList["AEX"] = [sE::ZONAS_AREA_EXPLOTACION, sE::ZONAS_AREA_EXPLOTACION_DATOS, sE::ZONAS_AREA_EXPLOTACION_COTAS, sE::ZONAS_AREA_EXPLOTACION_PERSONAL, sE::ZONAS_AREA_EXPLOTACION_LLUVIAS, sE::ZONAS_AREA_EXPLOTACION_INFO, sE::ZONAS_AREA_EXPLOTACION_DIRECTORIO];

                    // tiene permiso de sección
                    if (isset($permList['AEX']) && in_array($permList['AEX'][0], $accesos)) {

                        $DB = ConexionBD::Conexion();

                        $datosRTAP = (array) json_decode($this->visorDao->CargarRecursoCache($DB, 'rtap'), true);
                        $json_cache = json_decode($this->visorDao->CargarRecursoCache($DB, "master_ultres_categorias"), true);

                        $json['ult_res'] = (isset($permList['AEX'][1]) && in_array($permList['AEX'][1], $accesos)) ? $json_cache['E'] : [];
                        $json['niv_cfg'] = (isset($permList['AEX'][2]) && in_array($permList['AEX'][2], $accesos)) ? $this->visorDao->UltimosDatosMedidos("NIV/CFG", "AEX", null, $datosRTAP, $DB) : [];
                        $json['dir_res'] = (isset($permList['AEX'][3]) && in_array($permList['AEX'][3], $accesos)) ? $this->visorDao->UltimosDatosMedidos("DIR/RES", "AEX", null, null, $DB) : [];
                        $json['dir_det'] = (isset($permList['AEX'][6]) && in_array($permList['AEX'][6], $accesos)) ? $this->visorDao->UltimosDatosMedidos("DIR/DET", "AEX", null, null, $DB) : [];
                        $json['dir_detV2'] = (isset($permList['AEX'][6]) && in_array($permList['AEX'][6], $accesos)) ? $this->visorDao->ExtDirectorio($DB, "AEX"): [];

                        $json['video'] = $this->visorDao->CamarasAex();
                        $json['video'] = (in_array(sE::ZONAS_AREA_EXPLOTACION_VIDEO, $accesos) && isset($json['video'])) ? $this->filtrarVIDEO($json['video'], $video_usuario) : [];

                        if(in_array($permList['AEX'][4], $accesos)){
                            $json['pluviometria_aemet'] = $this->visorDao->UltimosDatosEmbalsesAemet($estaciones_usuario, $DB);
                            $json['pluviometria_SAIH'] = $this->visorDao->UltimosDatosEmbalsesLluviaSAIH($estaciones_usuario, $DB);
                            $json['pluviometria_SAIH_THIESSEN'] = $this->visorDao->UltimosDatosEmbalsesLluviaPonderadaSAIH($estaciones_usuario, $DB);
                        }

                        $json['info'] = (isset($permList['AEX'][5]) && in_array($permList['AEX'][5], $accesos)) ? $this->visorDao->GetInfo($DB, 'AEX') : [];

                        if (in_array(sE::ZONAS_AREA_EXPLOTACION_ESQUEMAS, $accesos)) {
                            $json['esquemaV2'] = $this->visorDao->getEsquemaV2($DB, 'AEX');
                        }

                        ConexionBD::CerrarConexion($DB);

                        // Cálculos resumidos de embalses para aex
                        if ($json['ult_res'] && $json['ult_res']['valores']) {

                            // volumen y capacidad por zona

                            $zonas = [
                                'Z12' => ['codigo' => 'Z12', 'hist_vol' => [], 'volumen' => 0, 'capacidad' => 0, 'porcentaje' => 0],
                                'Z3' =>  ['codigo' => 'Z3', 'hist_vol' => [], 'volumen' => 0, 'capacidad' => 0, 'porcentaje' => 0],
                                'Z5' =>  ['codigo' => 'Z5', 'hist_vol' => [], 'volumen' => 0, 'capacidad' => 0, 'porcentaje' => 0],
                                'Z67' => ['codigo' => 'Z67', 'hist_vol' => [], 'volumen' => 0, 'capacidad' => 0, 'porcentaje' => 0],
                            ];
                
                            // por cada zona buscar datos de sus embalses y sumarizarlos
                            foreach ($zonas as &$zona) {
                                $hist_vol_temp = []; // array temporal para ir almacenando los datos de embalses diarios
                                foreach ($json['ult_res']['valores'] as $ult_res_embalse) {
                                    if (trim($ult_res_embalse["zona"]) == $zona['codigo']) {
                
                                        // sumarizar los hist_vol de cada embalse dentro de la zona
                                        if (isset($ult_res_embalse["hist_vol"])) {
                                            foreach ($ult_res_embalse["hist_vol"] as $hist_vol_embalse) {
                                                if (!isset($hist_vol_temp[$hist_vol_embalse->day])) {
                                                    // aplicar el valor recibido si no concuerda el día
                                                    $hist_vol_temp[$hist_vol_embalse->day]["day"] = $hist_vol_embalse->day;
                                                    $hist_vol_temp[$hist_vol_embalse->day]["finval"] = $hist_vol_embalse->finval;
                                                } else {
                                                    // sumar si concuerda el día
                                                    $hist_vol_temp[$hist_vol_embalse->day]["finval"] += $hist_vol_embalse->finval;
                                                }
                                            }
                                        }
                
                                        // sumarizar los volumenes, capacidades de cada embalse dentro de la zona
                                        // cálculo de volumen/capacidad/porcentaje según los embalses de la zona
                                        if (isset($ult_res_embalse["VE1"]) && $ult_res_embalse["VE1"] != 0 && $ult_res_embalse["VE1"] != null && isset($ult_res_embalse["PV1"]) && $ult_res_embalse["PV1"] != 0 && $ult_res_embalse["PV1"] != null) {
                                            // calcular los valores de zona a partir de los embalses que incorpora
                                            $embalseVolumen = $ult_res_embalse["VE1"];
                                            $embalsePorcentaje = $ult_res_embalse["PV1"];
                                            $zona['capacidad'] += ($embalseVolumen / $embalsePorcentaje) * 100;
                                            $zona['volumen'] += $embalseVolumen;

                                        }
                                    }
                                }
                                foreach ($hist_vol_temp as $hvt) {
                                    $zona['hist_vol'][] = $hvt;
                                }
                                $zona['porcentaje'] = ($zona['volumen'] / $zona['capacidad']) * 100;
                            }
                            
                            $json['ult_res_aex'] = $zonas;

                            // volumen y capacidad de aex
                            $aexVolumen = 0;
                            $aexCapacidad = 0;
                            $aexPorcentajeVol = 0;
                            $valores = $json['ult_res']['valores'];

                            foreach ($valores as $ult_res_embalse) {
                                if (
                                    isset($ult_res_embalse) 
                                    && isset($ult_res_embalse["VE1"]) 
                                    && $ult_res_embalse["VE1"] != 0 
                                    && $ult_res_embalse["VE1"] != null 
                                    && isset($ult_res_embalse["PV1"]) 
                                    && $ult_res_embalse["PV1"] != 0 
                                    && $ult_res_embalse["PV1"] != null) {

                                    // calcular los valores de aex a partir de los embalses que incorpora
                                    $embalseVolumen = $ult_res_embalse["VE1"];
                                    $embalsePorcentaje = $ult_res_embalse["PV1"];
                                    $aexCapacidad += ($embalseVolumen / $embalsePorcentaje) * 100;
                                    $aexVolumen += $embalseVolumen;
                                    $aexPorcentajeVol = ($aexVolumen / $aexCapacidad) * 100;
                                }
                            }
                            $json['aex_volcap'] = ["volumen" => $aexVolumen, "capacidad" => $aexCapacidad, "porcentajeVol" => $aexPorcentajeVol];
                        }
                
                        // filtrar estaciones
                        if (in_array($permList['AEX'][1], $accesos)) {
                            if (isset($json['ult_res']) && count($json['ult_res'])>0) {
                                $json['ult_res'] = $this->filtrarULT_RES($json['ult_res'], $estaciones_usuario);
                            }
                        }

                        if (in_array($permList['AEX'][2], $accesos)) {
                            if (isset($json['niv_cfg']) && count($json['niv_cfg'])>0) {
                                $json['niv_cfg'] = $this->filtrarNIV_CFG($json['niv_cfg'], $estaciones_usuario);
                            }
                        }

                        if (in_array($permList['AEX'][3], $accesos)) {
                            if (isset($json['dir_res']) && count($json['dir_res'])>0) {
                                $json['dir_res'] = $this->filtrarDIR_RES($json['dir_res'], $estaciones_usuario);
                            }
                        }

                    }
                    $this->response($json, 200);
                    break;

                case 'ficha_zonas_zona':
                    $json = array();

                    $permList["Z12"] = [sE::ZONAS_Z12, sE::ZONAS_Z12_DATOS, sE::ZONAS_Z12_COTAS, sE::ZONAS_Z12_PERSONAL, sE::ZONAS_Z12_VIDEO, sE::ZONAS_Z12_LLUVIAS, sE::ZONAS_Z12_INFO, sE::ZONAS_Z12_DIRECTORIO];
                    $permList["Z3"] =  [sE::ZONAS_Z3, sE::ZONAS_Z3_DATOS, sE::ZONAS_Z3_COTAS, sE::ZONAS_Z3_PERSONAL, sE::ZONAS_Z3_VIDEO, sE::ZONAS_Z3_LLUVIAS, sE::ZONAS_Z3_INFO, sE::ZONAS_Z3_DIRECTORIO];
                    $permList["Z5"] =  [sE::ZONAS_Z5, sE::ZONAS_Z5_DATOS, sE::ZONAS_Z5_COTAS, sE::ZONAS_Z5_PERSONAL, sE::ZONAS_Z5_VIDEO, sE::ZONAS_Z5_LLUVIAS, sE::ZONAS_Z5_INFO, sE::ZONAS_Z5_DIRECTORIO];
                    $permList["Z67"] = [sE::ZONAS_Z67, sE::ZONAS_Z67_DATOS, sE::ZONAS_Z67_COTAS, sE::ZONAS_Z67_PERSONAL, sE::ZONAS_Z67_VIDEO, sE::ZONAS_Z67_LLUVIAS, sE::ZONAS_Z67_INFO, sE::ZONAS_Z67_DIRECTORIO];

                    $esquemaPermission = [ "Z12" => sE::ZONAS_Z12_ESQUEMAS, "Z3" => sE::ZONAS_Z3_ESQUEMAS, "Z5" => sE::ZONAS_Z5_ESQUEMAS, "Z67" => sE::ZONAS_Z67_ESQUEMAS, ];

                    if (isset($permList[$id]) && in_array($permList[$id][0], $accesos)) {
                        
                        $DB = ConexionBD::Conexion();

                        $json_cache = json_decode($this->visorDao->CargarRecursoCache($DB, "master_ultres_categorias"), true);
                        $datosRTAP = (array) json_decode($this->visorDao->CargarRecursoCache($DB, 'rtap'), true);

                        $json['ult_res'] = (isset($permList[$id][1]) && in_array($permList[$id][1], $accesos)) ? $json_cache[$id] : [];
                        $json['niv_cfg'] = (isset($permList[$id][2]) && in_array($permList[$id][2], $accesos)) ? $this->visorDao->UltimosDatosMedidos("NIV/CFG", $id, null, $datosRTAP, $DB) : [];
                        $json['dir_res'] = (isset($permList[$id][3]) && in_array($permList[$id][3], $accesos)) ? $this->visorDao->UltimosDatosMedidos("DIR/RES", $id, null, null, $DB) : [];
                        $json['dir_det'] = (isset($permList[$id][7]) && in_array($permList[$id][7], $accesos)) ? $this->visorDao->UltimosDatosMedidos("DIR/DET", $id, null, null, $DB) : [];
                        $json['dir_detV2'] = (isset($permList[$id][7]) && in_array($permList[$id][7], $accesos)) ? $this->visorDao->ExtDirectorio($DB, $id) : [];
                        $json['pluviometria_aemet'] = (isset($permList[$id][5]) && in_array($permList[$id][5], $accesos)) ? $this->visorDao->UltimosDatosEmbalsesAemet($estaciones_usuario, $DB, $id) : [];
                        $json['pluviometria_SAIH'] = (isset($permList[$id][5]) && in_array($permList[$id][5], $accesos)) ? $this->visorDao->UltimosDatosEmbalsesLluviaSAIH($estaciones_usuario, $DB, $id) : [];
                        $json['pluviometria_SAIH_THIESSEN'] = (isset($permList[$id][5]) && in_array($permList[$id][5], $accesos)) ? $this->visorDao->UltimosDatosEmbalsesLluviaPonderadaSAIH($estaciones_usuario, $DB, $id) : [];
                        $json['info'] = (isset($permList[$id][6]) && in_array($permList[$id][6], $accesos)) ? $this->visorDao->GetInfo($DB, 'AEX') : [];

                        if (in_array($esquemaPermission[$id], $accesos)) {
                            $json['esquemaV2'] = $this->visorDao->getEsquemaV2($DB, $id);
                        }

                        ConexionBD::CerrarConexion($DB);

                        // cálculo de volumen/capacidad/porcentaje según los embalses de la zona
                        if ($json['ult_res'] && $json['ult_res']["valores"]) {
                            $zonaVolumen = 0;
                            $zonaCapacidad = 0;
                            $zonaPorcentajeVol = 0;
                            foreach ($json['ult_res']["valores"] as $ult_res_embalse) {
                                if (isset($ult_res_embalse["VE1"]) && $ult_res_embalse["VE1"] != 0 && $ult_res_embalse["VE1"] != null && isset($ult_res_embalse["PV1"]) && $ult_res_embalse["PV1"] != 0 && $ult_res_embalse["PV1"] != null) {
                                    // calcular los valores de zona a partir de los embalses que incorpora
                                    $embalseVolumen = $ult_res_embalse["VE1"];
                                    $embalsePorcentaje = $ult_res_embalse["PV1"];
                                    $zonaCapacidad += ($embalseVolumen / $embalsePorcentaje) * 100;
                                    $zonaVolumen += $embalseVolumen;
                                    $zonaPorcentajeVol = ($zonaVolumen / $zonaCapacidad) * 100;
                                }
                            }
                            $json['ult_res_zona'] = ["volumen" => $zonaVolumen, "capacidad" => $zonaCapacidad, "porcentajeVol" => $zonaPorcentajeVol];
                        }

                        // datos de cámaras
                        $json['video'] = $this->visorDao->CamarasZona($id);

                        // filtrar                        
                        $json['ult_res'] = (in_array($permList[$id][1], $accesos) && isset($json['ult_res']) && count($json['ult_res'])>0) ? $this->filtrarULT_RES($json['ult_res'], $estaciones_usuario) : [];
                        $json['niv_cfg'] = (in_array($permList[$id][2], $accesos) && isset($json['niv_cfg']) && count($json['niv_cfg'])>0) ? $this->filtrarNIV_CFG($json['niv_cfg'], $estaciones_usuario) : [];
                        $json['dir_res'] = (in_array($permList[$id][3], $accesos) && isset($json['dir_res']) && count($json['dir_res'])>0) ? $this->filtrarDIR_RES($json['dir_res'], $estaciones_usuario) : [];
                        $json['video'] = (in_array($permList[$id][4], $accesos) && isset($json['video'])) ? $this->filtrarVIDEO($json['video'], $video_usuario) : [];
                        // sin filtrar
                        $json['dir_det'] = (isset($json['dir_det'])) ? $json['dir_det'] : [];
                        $json['dir_detV2'] = (isset($json['dir_detV2'])) ? $json['dir_detV2'] : [];
                        $json['ult_res_zona'] = (isset($json['ult_res_zona'])) ? $json['ult_res_zona'] : [];
                    }

                    $this->response($json, 200);
                    break;

                case 'ficha_zonas_embalse':
                    $json = array();

                    // revisar si la estación está disponible
                    if (isset($id) && in_array(trim($id), $estaciones_usuario)) {

                        $json['hist_diario'] = [];

                        // filtrar
                        $permList["Z12"] = [sE::ZONAS_Z12, sE::ZONAS_Z12_EMBALSE_DETALLE_DATOS, sE::ZONAS_Z12_EMBALSE_DETALLE_COTAS, sE::ZONAS_Z12_EMBALSE_DETALLE_VARIABLES, sE::ZONAS_Z12_EMBALSE_DETALLE_VIDEO, sE::ZONAS_Z12_EMBALSE_DETALLE_DIRECTORIO, sE::ZONAS_Z12_EMBALSE_DETALLE_LLUVIAS, sE::ZONAS_Z12_EMBALSE_DETALLE_INFO];
                        $permList["Z3"] =  [sE::ZONAS_Z3, sE::ZONAS_Z3_EMBALSE_DETALLE_DATOS, sE::ZONAS_Z3_EMBALSE_DETALLE_COTAS, sE::ZONAS_Z3_EMBALSE_DETALLE_VARIABLES, sE::ZONAS_Z3_EMBALSE_DETALLE_VIDEO, sE::ZONAS_Z3_EMBALSE_DETALLE_DIRECTORIO, sE::ZONAS_Z3_EMBALSE_DETALLE_LLUVIAS, sE::ZONAS_Z3_EMBALSE_DETALLE_INFO];
                        $permList["Z5"] =  [sE::ZONAS_Z5, sE::ZONAS_Z5_EMBALSE_DETALLE_DATOS, sE::ZONAS_Z5_EMBALSE_DETALLE_COTAS, sE::ZONAS_Z5_EMBALSE_DETALLE_VARIABLES, sE::ZONAS_Z5_EMBALSE_DETALLE_VIDEO, sE::ZONAS_Z5_EMBALSE_DETALLE_DIRECTORIO, sE::ZONAS_Z5_EMBALSE_DETALLE_LLUVIAS, sE::ZONAS_Z5_EMBALSE_DETALLE_INFO];
                        $permList["Z67"] = [sE::ZONAS_Z67, sE::ZONAS_Z67_EMBALSE_DETALLE_DATOS, sE::ZONAS_Z67_EMBALSE_DETALLE_COTAS, sE::ZONAS_Z67_EMBALSE_DETALLE_VARIABLES, sE::ZONAS_Z67_EMBALSE_DETALLE_VIDEO, sE::ZONAS_Z67_EMBALSE_DETALLE_DIRECTORIO, sE::ZONAS_Z67_EMBALSE_DETALLE_LLUVIAS, sE::ZONAS_Z67_EMBALSE_DETALLE_INFO];

                        $esquemaPermission = [ 
                            "Z12" => sE::ZONAS_Z12_EMBALSE_DETALLE_ESQUEMAS,
                            "Z3" => sE::ZONAS_Z3_EMBALSE_DETALLE_ESQUEMAS,
                            "Z5" => sE::ZONAS_Z5_EMBALSE_DETALLE_ESQUEMAS,
                            "Z67" => sE::ZONAS_Z67_EMBALSE_DETALLE_ESQUEMAS, 
                        ];

                        // revisar permisos de acceso almenos a una zona antes de revisar a que zona pertenece la estación
                        $hasZonePerm = false;
                        foreach ($permList as $perm) {
                            if (in_array($perm[0], $accesos)) $hasZonePerm = true;
                        }

                        $idZona = null;
                        if ($hasZonePerm) {
                            $idZona = $this->visorDao->getZonaEstacion($id);
                        }
                        
                        if ($hasZonePerm && $idZona && isset($permList[$idZona]) && in_array($permList[$idZona][0], $accesos) && (in_array($permList[$idZona][1], $accesos) || in_array($permList[$idZona][2], $accesos) || in_array($permList[$idZona][3], $accesos))) {
                            $DB = ConexionBD::Conexion();

                            $datosRTAP = (array) json_decode($this->visorDao->CargarRecursoCache($DB, 'rtap'), true);
                            $json['ult_res'] =      (in_array($permList[$idZona][1], $accesos)) ? $this->visorDao->CargarMasterULTRESEstaciones($DB, trim($id)) : [];
                            $json['niv_det'] =      (in_array($permList[$idZona][2], $accesos)) ? $this->visorDao->UltimosDatosMedidos("NIV/DET", "E", $id, $datosRTAP, $DB) : [];
                            $json['ult_rtap'] =     (in_array($permList[$idZona][3], $accesos)) ? $this->visorDao->UltimosDatosMedidos("ULT/RTAP", "E", $id, $datosRTAP, $DB) : [];
                            $json['video'] =        (in_array($permList[$idZona][4], $accesos)) ? $this->filtrarVIDEO($this->visorDao->CamarasEmbalse($id), $video_usuario) : [];
                            $json['dir_res'] =      (in_array($permList[$idZona][5], $accesos)) ? $this->visorDao->UltimosDatosMedidos("DIR/RES", "E", $id, null, $DB) : [];
                            $json['dir_det'] =      (in_array($permList[$idZona][5], $accesos)) ? $this->visorDao->UltimosDatosMedidos("DIR/DET", "E", $id, null, $DB) : [];
                            $json['lluvia_aemet'] = (in_array($permList[$idZona][6], $accesos)) ? $this->visorDao->UltimosDatosEmbalseAemet($estaciones_usuario, $id, $DB) : [];
                            $json['lluvia_saih'] =  (in_array($permList[$idZona][6], $accesos)) ? $this->visorDao->UltimosDatosEmbalseSAIH($estaciones_usuario, $id, $DB) : [];
                            $json['info'] =         (in_array($permList[$idZona][7], $accesos)) ? $this->visorDao->GetInfo($DB, $id) : [];

                            if (in_array($esquemaPermission[$idZona], $accesos)) {
                                $json['esquemaV2'] = $this->visorDao->getEsquemaV2($DB, $id);
                            }

                            ConexionBD::CerrarConexion($DB);
                        }
                    }

                    $this->response($json, 200);
                    break;

                case 'ficha_zonaregable_general':
                    $json = array();
                    $permList =   [sE::USOS_AGUA_ZONAS_REGABLES, sE::USOS_AGUA_ZONAS_REGABLES_DETALLE_INFO];
                    if (in_array($permList[0], $accesos)) {
                        $DB = ConexionBD::Conexion();
                        $json['info'] = in_array($permList[1], $accesos) ? $this->visorDao->GetInfo($DB, 'zonas_regables') : [];
                        if (in_array(sE::USOS_AGUA_ZONAS_REGABLES_DETALLE_ESQUEMAS, $accesos)) {
                            $json['esquemaV2'] = $this->visorDao->getEsquemaV2($DB, 'ZR');
                        }
                        ConexionBD::CerrarConexion($DB);
                        $this->response($json, 200);
                    } else {
                        $this->response(null, 401);
                    }
                    break;
                case 'ficha_zonaregable':
                    $json = array();
                    $perm = null;
                    $permList =   [sE::USOS_AGUA_ZONAS_REGABLES, sE::USOS_AGUA_ZONAS_REGABLES_DETALLE, sE::USOS_AGUA_ZONAS_REGABLES_DETALLE_DATOS, sE::USOS_AGUA_ZONAS_REGABLES_DETALLE_INFO];
                    if (isset($id)) {
                        if (in_array($permList[0], $accesos) && in_array($permList[1], $accesos)) {
                                $DB = ConexionBD::Conexion();
                                if (in_array($permList[2], $accesos)) {

                                    $ultimosDatos = $this->visorDao->UltimosDatos($DB);

                                    $ultRes_E = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'E', null, null, $DB, $ultimosDatos, $id);
                                    $ultRes_NR = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'NR', null, null, $DB, $ultimosDatos, $id);
                                    $ultRes_CR = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'CR', null, null, $DB, $ultimosDatos, $id);

                                    $json['ult_res']['E'] =(isset($ultRes_E)) ? $this->filtrarULT_RES($ultRes_E, $estaciones_usuario) : null;
                                    $json['ult_res']['NR'] = (isset($ultRes_NR)) ? $this->filtrarULT_RES($ultRes_NR, $estaciones_usuario, "NR", $accesos) : null;
                                    $json['ult_res']['CR'] = (isset($ultRes_CR)) ? $this->filtrarULT_RES($ultRes_CR, $estaciones_usuario) : null;
                                }

                                $json['info'] = in_array($permList[3], $accesos) ? $this->visorDao->GetInfo($DB, $id) : [];                                
                                if (in_array(sE::USOS_AGUA_ZONAS_REGABLES_ESQUEMAS, $accesos)) {
                                    $json['esquemaV2'] = $this->visorDao->getEsquemaV2($DB, $id);
                                }

                                ConexionBD::CerrarConexion($DB);

                                $this->response($json, 200);
                        } else {
                            $this->response(null, 401);
                        }
                    } else {
                        $this->response(null, 400);
                    }
                    break;
                case 'ficha_masb_general':
                    
                    $json = array();

                    $perm = null;
                    $permList =   [sE::MASAS_SUBTERRANEAS, sE::MASAS_SUBTERRANEAS_INFO];

                    // revisar permisos para acceder a la sección
                    if (isset($permList[0]) && in_array($permList[0], $accesos)) {

                        $DB = ConexionBD::Conexion();
                        $json['info'] = (in_array($permList[1], $accesos)) ? $this->visorDao->GetInfo($DB, 'masb') : [];
                        ConexionBD::CerrarConexion($DB);

                        $this->response($json, 200);
                        
                    } else {
                        $this->response(null, 401);
                    }

                    break;
                case 'ficha_masb':
                    $json = array();

                    $perm = null;
                    $permList =   [sE::MASAS_SUBTERRANEAS, sE::MASAS_SUBTERRANEAS_DETALLE, sE::MASAS_SUBTERRANEAS_DETALLE_DATOS, sE::MASAS_SUBTERRANEAS_DETALLE_INFO];

                    // revisar si se entrega una id de masa
                    if (isset($id)) {
                        if (isset($permList[0]) && isset($permList[1])) {

                                $DB = ConexionBD::Conexion();
                            
                                if (in_array($permList[2], $accesos)) {

                                    $ultimosDatos = $this->visorDao->UltimosDatos($DB);

                                    $ultRes_CR = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'CR', null, null, $DB, $ultimosDatos, null, $id);
                                    $ultRes_EM = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'EM', null, null, $DB, $ultimosDatos, null, $id);
                                    $ultRes_PZ = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'PZ', null, null, $DB, $ultimosDatos, null, $id);

                                    $json['ult_res']['CR'] =(isset($ultRes_CR)) ? $this->filtrarULT_RES($ultRes_CR, $estaciones_usuario) : null;
                                    $json['ult_res']['EM'] = (isset($ultRes_EM)) ? $this->filtrarULT_RES($ultRes_EM, $estaciones_usuario) : null;
                                    $json['ult_res']['PZ'] = (isset($ultRes_PZ)) ? $this->filtrarULT_RES($ultRes_PZ, $estaciones_usuario) : null;

                                }

                                if (in_array($permList[3], $accesos)) {
                                    $json['info'] = $this->visorDao->InfoMASb($id, $DB);
                                }

                                ConexionBD::CerrarConexion($DB);


                                $this->response($json, 200);
                            
                        } else {
                            $this->response(null, 401);
                        }
                    } else {
                        $this->response(null, 400);
                    }
                    break;
                case 'historico_precipitacion_estacion':

                    $DB = ConexionBD::Conexion();
                    $json_cache = json_decode($this->visorDao->CargarRecursoCache($DB, "hist_precipitacion_" . $id));
                    ConexionBD::CerrarConexion($DB);

                    //proporcionar datos de volumen de la estación
                    //$fechaFinDate = new DateTime('now');
                    //$fechaFin = $fechaFinDate->format("d/m/Y");
                    $fechaIniDate = new DateTime('now');
                    $fechaIniDate->modify("-7 day");
                    $fechaIni = $fechaIniDate->format("d/m/Y");
                    
                    // encode y guardado de datos historicos de precipitación de cada estación por separado
                    $datos_hist_precipitacion = $this->visorDao->PrecipitacionEstacion($id, $fechaIni);

                    $this->response($datos_hist_precipitacion, 200);
                    break;
                case 'historico_nivel_volumen_estacion':

                    //proporcionar datos de volumen de la estación
                    //$fechaFinDate = new DateTime('now');
                    //$fechaFin = $fechaFinDate->format("d/m/Y");
                    $fechaIniDate = new DateTime('now');
                    $fechaIniDate->modify("-7 day");
                    $fechaIni = $fechaIniDate->format("d/m/Y");
                    
                    // encode y guardado de datos historicos de precipitación de cada estación por separado
                    $datos_nivvol_precipitacion = $this->visorDao->NivelVolumenEstacion($id, $fechaIni);

                    $this->response($datos_nivvol_precipitacion, 200);
                    break;
            
                case 'ficha_meteorologia_observacion':

                        $json = array();

                        $permList = [sE::METEOROLOGIA_OBSERVACION, sE::METEOROLOGIA_OBSERVACION_INFO, sE::METEOROLOGIA_OBSERVACION_AEMET, sE::METEOROLOGIA_OBSERVACION_SAIH];
 
                        if (in_array($permList[0], $accesos)) {
                            $DB = ConexionBD::Conexion();
                            
                            $json['observacion_aemet'] = (isset($permList[2]) && in_array($permList[2], $accesos)) ? $this->visorDao->UltimosDatosAemet($estaciones_usuario, $DB) : [];
                            $json['observacion_saih'] = (isset($permList[3]) && in_array($permList[3], $accesos)) ? $this->visorDao->UltimosDatosLluviaSAIH($estaciones_usuario, $DB) : [];
                            $json['info_saih'] = (isset($permList[1]) && in_array($permList[1], $accesos)) ? $this->visorDao->GetInfo($DB, 'PLUVIOSAIH') : [];
                            $json['info_aemet'] = (isset($permList[1]) && in_array($permList[1], $accesos)) ? $this->visorDao->GetInfo($DB, 'PLUVIOAEMET') : [];
                            
                            ConexionBD::CerrarConexion($DB);
                            $this->response($json, 200);
                        } else {
                            $this->response(null, 401);
                        }

                    break;
                case 'ficha_administrador':
                        $json = array();
                        $permList = [sE::ADMINISTRACION_RESUMEN_ESTACIONES];
                        if (in_array($permList[0], $accesos)) {
                            
                            $DB = ConexionBD::Conexion();
                            // obtener contadores
                            $json["resumen_estaciones"] = (isset($permList[0]) && in_array($permList[0], $accesos)) ? $this->visorDao->getResumenEstaciones($DB) : [];
                            ConexionBD::CerrarConexion($DB);

                            $this->response($json, 200);
                        } else {
                            $this->response(null, 401);
                        }

                    break;
                case 'ficha_administrador_camaras':
                        $json = array();
                        $permList = [sE::ADMINISTRACION_RESUMEN_CAMARAS];
                        if (in_array($permList[0], $accesos)) {
                            $DB = ConexionBD::Conexion();
                            $json["resumen_camaras"] = (isset($permList[0]) && in_array($permList[0], $accesos)) ? $this->visorDao->getResumenCamaras($DB) : [];
                            ConexionBD::CerrarConexion($DB);
                            $this->response($json, 200);
                        } else {
                            $this->response(null, 401);
                        }
                    break;
            }
        } else {
            $this->response(null, 401);
        }
    }

    // SEC
    function filtrarULT_RES($ULT_RES, $estaciones_usuario, $tipo = null, $accesos = null)
    {
        $retorno = array();
        // filtrar encabezado en caso de ser NR
        $retorno["encabezado"] = [];
        if(isset($ULT_RES["encabezado"])){
            $permisoCaudalNR = ($tipo == 'NR' && isset($accesos) && in_array(sE::REDES_CONTROL_NIVELRIO_DATOS_CAUDAL,$accesos)) ? true : false;
            foreach($ULT_RES["encabezado"] as $encabezado){
                if($tipo == "NR" && $encabezado == "QR1"){
                    if ($permisoCaudalNR){
                        $retorno["encabezado"][] = $encabezado;
                    } 

                } else {
                    $retorno["encabezado"][] = $encabezado;
                }
            }
        } 
        
        // filtrar los valores por cod estacion
        $retorno["valores"] = [];
        if(isset($ULT_RES["valores"])){
            foreach ($ULT_RES["valores"] as $estacion) {
                // si el usuario tiene la estación en su lista se filtran los valores ULT_RES
                if (in_array(trim($estacion["cod_estacion"]), $estaciones_usuario)) {

                    // Para NR es necesario un permiso específico para mostrar el caudal
                    if($tipo == 'NR'){
                        if(isset($estacion['QR1']) && !in_array(sE::REDES_CONTROL_NIVELRIO_DATOS_CAUDAL,$accesos)){
                            $estacion['QR1'] = null;
                        }
                    } 
                    
                    $retorno["valores"][] = $estacion;
                    
                }
            }
        }
        return $retorno;
    }

    function filtrarNIV_CFG($NIV_CFG, $estaciones_usuario)
    {
        $retorno = array();
        // no filtrar encabezado
        $retorno["encabezado"] = $NIV_CFG["encabezado"];
        $retorno["valores"] = [];
        // filtrar los valores por cod estacion
        $estaciones = $NIV_CFG["valores"];
        foreach ($estaciones as $estacion) {
            // si el usuario tiene la estación en su lista se filtran los valores NIV_CFG
            if (in_array(trim($estacion["cod_estacion"]), $estaciones_usuario)) {
                $retorno["valores"][] = $estacion;
            }
        }
        return $retorno;
    }

    function filtrarDIR_RES($DIR_RES, $estaciones_usuario)
    {
        $retorno = array();
        // no filtrar encabezado
        $retorno["encabezado"] = $DIR_RES["encabezado"];
        $retorno["valores"] = [];
        // filtrar los valores por cod estacion
        $estaciones = $DIR_RES["valores"];
        foreach ($estaciones as $estacion) {
            // si el usuario tiene la estación en su lista se filtran los valores DIR_RES
            if (in_array(trim($estacion["cod_estacion"]), $estaciones_usuario)) {
                $retorno["valores"][] = $estacion;
            }
        }
        return $retorno;
    }

    function filtrarVIDEO($VIDEO, $video_usuario)
    {
        $retorno = array();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if($tokenValidado && isset($VIDEO) && count($VIDEO)>0){
            $videos = $tokenValidado->payload->videos;
            foreach ($VIDEO as $item_video) {
                if ($this->isVideoUser($video_usuario, $item_video['src'])) {
                    $timelapse_available = $item_video['timelapse'] && in_array($item_video['nombre'], $videos);
                    $item_video['timelapse'] = $timelapse_available;
                    $retorno[] = $item_video;
                }
            }
        }
        return $retorno;
    }

    function isVideoUser($video_usuario, $src) {
        $src_video_usuario = [];
        foreach ($video_usuario as $idVideo) {
            $porciones = explode("-", $idVideo);
            $src_video_usuario[] = $porciones[0] . "-video/" . $idVideo;
        }
        foreach ($src_video_usuario as $src_item_usuario) {
            if (substr($src, 0, strlen($src_item_usuario)) === $src_item_usuario) {
                return true;
            }
        }
        return false;
    }

    function filtrarEST_RES($EST_RES, $estaciones_usuario)
    {
        $retorno = array();
        if (isset($EST_RES["encabezado"])) $retorno["encabezado"] = $EST_RES["encabezado"];
        
        // filtrar valores
        if (isset($EST_RES["valores"])){
            foreach ($EST_RES["valores"] as $estacion) {
                if (isset($estacion["cod_estacion"]) && in_array(trim($estacion["cod_estacion"]), $estaciones_usuario)) {
                    $retorno["valores"][] = $estacion;
                }
            }
        }

        return $retorno;

    }

    // función para obtener la capa de meteoalerta del visor
    function meteoalerta_get()
    {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::METEOALERTA, $accesos) && in_array(sE::METEOALERTA_MAPA, $accesos)) {
                $meteoalerta = $this->visorDao->Meteoalerta();
                $this->response($meteoalerta, 200);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function meteoalerta_multialerta_get()
    {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::METEOALERTA, $accesos) && in_array(sE::METEOALERTA_MAPA, $accesos)) {
                $meteoalerta = $this->visorDao->MeteoMultialerta();
                $this->response($meteoalerta, 200);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function zonas_regables_get()
    {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::USOS_AGUA_ZONAS_REGABLES, $accesos) && in_array(sE::USOS_AGUA_ZONAS_REGABLES_DETALLE_MAPA, $accesos)) {
                $geometries = $this->visorDao->ZonasRegablesGeometry();
                $this->response($geometries, 200);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function lluviaAcumulada_get()
    {
        /*$image = file_get_contents(APPPATH . "/libraries/visor/capa_lluvia/P24h_20220926.tif");
        $b64image = base64_encode($image);
        $this->response(["nombre"=>"donpepito", "capa"=>$b64image], 200);*/

        $listaFicheros = scandir(APPPATH . "/libraries/visor/capa_lluvia");
        $listaCapasLluviaFecha = []; // lista indexada por fechas

        foreach($listaFicheros as $fichero){

            if(substr( $fichero, 0, 1 ) !== "."){

                $nombre_fecha = explode(".", $fichero)[0];

                if(isset($nombre_fecha)){

                    $fecha = explode("_", $nombre_fecha);

                    if(isset($fecha[1])){
                        $fecha = $fecha[1];
                        $listaCapasLluviaFecha[intval($fecha)] = ["fichero" => $fichero, "nombre" => $nombre_fecha] ;
                    }
                }
            }
        }

        if(isset($listaCapasLluviaFecha) && count($listaCapasLluviaFecha)>0){
            ksort($listaCapasLluviaFecha);

            $capaReciente = end($listaCapasLluviaFecha);
            $fichero = $capaReciente["fichero"];
            $nombre = $capaReciente["nombre"];
            $image = file_get_contents(APPPATH . "/libraries/visor/capa_lluvia/". $fichero);
            $b64image = base64_encode($image);
            $this->response(["nombre" => $nombre, "capa" => $b64image], 200);
            
        } else {
            $this->response(null, 200);
        }

       


    }
    
    function masasub_get()
    {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUBTERRANEAS, $accesos) && in_array(sE::MASAS_SUBTERRANEAS_MAPA, $accesos)) {
                $masasub = $this->visorDao->MasasSubterraneas();
                $this->response($masasub, 200);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    /*function cuenca_get()
    {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $masasub = $this->visorDao->CuencaGuadiana();
            $this->response($masasub, 200);
        } else {
            $this->response(null, 401);
        }
    }*/

    // función para enviar las imágenes de las cámaras en base64
    function image_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $src = $parameters['src'];

        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado && $src) {

            $video_usuario = [];

            if (isset($tokenValidado->payload->videos)) {
                $video_usuario = $tokenValidado->payload->videos;
            }
            $exists = $this->isVideoUser($video_usuario, $src);
            if ($exists) {
                $image = file_get_contents(APPPATH . "libraries/visor/video_cache/" . $src);
                $b64image = base64_encode($image);
                $this->response($b64image, 200);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    // función para enviar el timelapse de imágenes en base64
    function timelapse_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $timelapse_directory = $parameters['timelapse_directory'];
        $ar = [];

        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado) {

            if ($timelapse_directory) {

                if (isset($tokenValidado->payload->videos)) {
                    $video_usuario = $tokenValidado->payload->videos;
                }
                $exists = $this->isVideoUser($video_usuario, $timelapse_directory);
                $dir_timelapse = is_dir(APPPATH . "libraries/visor/timelapse_cache/" . $timelapse_directory);
                if ($exists && $dir_timelapse) {

                    chdir(APPPATH . "libraries/visor/timelapse_cache/" . $timelapse_directory);

                    // obtener las imágenes en orden más reciente
                    $files = array_reverse(glob("*.*"));

                    $size = count($files);
                    if ($size > 144) {
                        $size = 144;
                    }

                    // cortar el array
                    $files = array_slice($files, 0, $size);

                    // ordenar de mas antiguo a más reciente
                    $files = array_reverse($files);

                    $ar = [];
                    foreach ($files as $filename) {
                        $image = file_get_contents($filename);
                        $b64image = base64_encode($image);
                        $ar[] = $b64image;
                    }
                    $this->response($ar, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 400);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function informe_embalse_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $id = $parameters['id'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];

        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;

            if (isset($id) && isset($periodo) && isset($fecha_i) && isset($fecha_f)) {

                $permList["Z12"] = [sE::ZONAS_Z12, sE::ZONAS_Z12_EMBALSE_DETALLE_INFORME];
                $permList["Z3"] =  [sE::ZONAS_Z3, sE::ZONAS_Z3_EMBALSE_DETALLE_INFORME];
                $permList["Z5"] =  [sE::ZONAS_Z5, sE::ZONAS_Z5_EMBALSE_DETALLE_INFORME];
                $permList["Z67"] = [sE::ZONAS_Z67, sE::ZONAS_Z67_EMBALSE_DETALLE_INFORME];

                $idZona = null;
                $idZona = $this->visorDao->getZonaEstacion($id);

                if ($idZona && isset($permList[$idZona]) && in_array($permList[$idZona][0], $accesos) && in_array($permList[$idZona][1], $accesos)) {
                    $val = $this->visorDao->InformeEstacion($id, "E", $periodo, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 401);
                }
            } else {
                $this->response(null, 400);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function informe_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        
        $id = $parameters['id'];
        $tipo = $parameters['tipo'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];

        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;

            if (isset($id) && isset($tipo) && in_array($tipo,['E', 'CR', 'PZ', 'EM', 'EAA', 'ECC', 'EXC']) && isset($periodo) && isset($fecha_i) && isset($fecha_f)) {

                $val = $this->visorDao->InformeEstacion($id, $tipo, $periodo, $fecha_i, $fecha_f);
                $this->response($val, 200);
                
            } else {
                $this->response(null, 400);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function testRTAPRAW_get(){
        $datosRTAP = $this->visorDao->ObtenerDatosRTAPRAW();
        $this->response($datosRTAP, 200); 
    }

    function testRTAP_get(){
        $datosRTAP = $this->visorDao->ObtenerDatosRTAP();
        $this->response($datosRTAP, 200); 
    }

    /*function evolucionEMBDetalleInit_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $id = $parameters['id'];
        $zoom = (isset($parameters['zoom'])) ? $parameters['zoom'] : null;
        $fecha_i = (isset($parameters['fecha_i'])) ? $parameters['fecha_i'] : null;
        $fecha_f = (isset($parameters['fecha_f'])) ? $parameters['fecha_f'] : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {

            if (isset($parameters) && isset($parameters['id'])) {

                $val['datos_scroll'] = $this->visorDao->EvolucionEMBDetalle($id, 'd', $fecha_i, $fecha_f);

                $DB = ConexionBD::Conexion();
                $datosRTAP = (array) json_decode($this->visorDao->CargarRecursoCache($DB, 'rtap'), true);
                $ult_res  = $this->visorDao->CargarMasterULTRESEstaciones($DB, trim($id));
                ConexionBD::CerrarConexion($DB);

                // aplicar datos en tiempo real en los datos históricos del embalse si no hay datos del día actual
                if(isset($ult_res) && isset($ult_es['valores'])){

                    $valoresULTRES = $ult_res['valores'];

                    $val['datos_actuales'] = [
                        "NE1" => ($valoresULTRES["NE1"]=='---') ? null : $valoresULTRES["NE1"],
                        "VE1" => ($valoresULTRES["VE1"]=='---') ? null : $valoresULTRES["VE1"],
                        "PRA" => ($valoresULTRES["PRA"]=='---') ? null : $valoresULTRES["PRA"],
                        "AAT" => null,
                        "AWT" => null,
                        "AFT" => null,
                        "EV" => null,
                        "NALIV" => null,
                        "VALIV" => null,
                        "NCOR" => null,
                        "VCOR" => null,
                        "date" => strtotime(explode(" ",$valoresULTRES["timestamp"])[0])*1000
                    ];

                }

                $this->response($val, 200);
            } else {
                $this->response(null, 400);
            }

        }
    }

    function evolucionEMBDetalle_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $id = $parameters['id'];
        $zoom = (isset($parameters['zoom'])) ? $parameters['zoom'] : null;
        $fecha_i = (isset($parameters['fecha_i'])) ? $parameters['fecha_i'] : null;
        $fecha_f = (isset($parameters['fecha_f'])) ? $parameters['fecha_f'] : null;
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {

            if (isset($parameters) && isset($parameters['id'])) {
                $val = $this->visorDao->EvolucionEMBDetalle($id, $zoom, $fecha_i, $fecha_f);
                $this->response($val, 200);
            } else {
                $this->response(null, 400);
            }

        }
    }*/

    function evolucionEMB_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $ids = $parameters['ids'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_EVOLUCION_EMBALSES, $accesos)) {
                if (isset($parameters) && isset($parameters['ids']) && isset($parameters['periodo']) && isset($parameters['fecha_i']) && isset($parameters['fecha_f'])) {
                    $val = $this->visorDao->EvolucionEMB($ids, $periodo, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    /*function evolucionCR_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $ids = $parameters['ids'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_AFOROS, $accesos)) {
                if (isset($parameters) && isset($parameters['ids']) && isset($parameters['periodo']) && isset($parameters['fecha_i']) && isset($parameters['fecha_f'])) {
                    $val = $this->visorDao->EvolucionCR($ids, $periodo, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }*/

    /*function evolucionNR_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $ids = $parameters['ids'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_NIVELRIO, $accesos)) {
                if (isset($parameters) && isset($parameters['ids']) && isset($parameters['periodo']) && isset($parameters['fecha_i']) && isset($parameters['fecha_f'])) {
                    $val = $this->visorDao->EvolucionNR($ids, $periodo, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }*/

    function evolucionEXC_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $ids = $parameters['ids'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_CALIDAD_INDUSTRIAL, $accesos)) {
                if (isset($parameters) && isset($parameters['ids']) && isset($parameters['periodo']) && isset($parameters['fecha_i']) && isset($parameters['fecha_f'])) {
                    $val = $this->visorDao->evolucionEXC($ids, $periodo, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function industriales_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;

            if (in_array(sE::REDES_CONTROL_CALIDAD_INDUSTRIAL, $accesos)) {
                if (isset($parameters['estacion'])) {
                    $val = $this->visorDao->getValoresEXC($parameters['estacion'], $parameters['tiempo'], $parameters['fInicial'], $parameters['fFinal']);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function evolucionSDAMULTI_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $id = $parameters['cod_estacion'];
        $timestamp = $parameters['timestamp'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE, $accesos)) {
                if (isset($parameters) && isset($id) && isset($timestamp)) {
                    $val = $this->visorDao->EvolucionSDAMULTI($id, $timestamp);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    /*function evolucionPZ_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $ids = $parameters['ids'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_PIEZOMETROS, $accesos)) {
                if (isset($parameters) && isset($parameters['ids']) && isset($parameters['fecha_i']) && isset($parameters['fecha_f'])) {
                    $val = $this->visorDao->EvolucionPZ($ids, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }*/

    function evolucionMASASUB_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $id_PZ = $parameters['id_pz'];
        $id_CR = $parameters['id_cr'];
        $id_EM = $parameters['id_em'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUBTERRANEAS_DETALLE, $accesos) && in_array(sE::MASAS_SUBTERRANEAS_DETALLE_GRAFICOS, $accesos)) {
                if (isset($parameters) && isset($id_PZ) && isset($id_CR) && isset($id_EM)) {




                    $val = $this->visorDao->EvolucionMASASUB(trim($id_PZ), trim($id_CR), trim($id_EM), $periodo, $fecha_i, $fecha_f);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    /*function sondas_get()
    {
        $listadoSondas = $this->visorDao->getListadoSondas();
        $this->response($listadoSondas, 200);
    }*/

    function timestampsSondas_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $id = $parameters['cod_estacion'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE, $accesos)) {
                if (isset($parameters) && isset($id)) {
                    $val = $this->visorDao->TimestampsSondas($id);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function profundidadesSonda_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $id = $parameters['cod_estacion'];
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE, $accesos) && in_array(sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE_INFORME, $accesos)) {
                if (isset($parameters) && isset($id)) {
                    $val = $this->visorDao->ProfundidadesSonda($id);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function informeSonda_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        $cod_estacion = $parameters['cod_estacion'];
        $profundidad = $parameters['profundidad'];
        $fecha_inicio = $parameters['fecha_inicio'];
        $fecha_fin = $parameters['fecha_fin'];

        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE, $accesos) && in_array(sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_DETALLE_INFORME, $accesos)) {
                if (isset($parameters) && isset($cod_estacion) && isset($profundidad) && isset($fecha_inicio) && isset($fecha_fin)) {
                    $val = $this->visorDao->InformeSonda($cod_estacion, $profundidad, $fecha_inicio, $fecha_fin);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function calidad_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_CALIDAD_SAICA, $accesos) && in_array(sE::REDES_CONTROL_CALIDAD_SAICA_DETALLE_DATOS, $accesos)) {
                if (isset($parameters['consulta']) && $parameters['consulta'] == 'EAA_DAT') {
                    $val = null;
                    $val = $this->visorDao->getValoresSAICA($parameters['estacion'], $parameters['tiempo'], $parameters['fInicial'], $parameters['fFinal']);
                    $this->response($val, 200);
                } else {
                    $this->response(null, 401);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function reacar_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;

            $video_usuario = [];
            if (isset($tokenValidado->payload->videos)) {
                $video_usuario = $tokenValidado->payload->videos;
            }

            if (in_array(sE::REDES_CONTROL_CALIDAD_REACAR, $accesos) && in_array(sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_DATOS, $accesos)) {
                if (isset($parameters['consulta']) && $parameters['consulta'] == 'ECC_DAT' && isset($parameters['estacion'])) {
                    $val = null;
                    $val = $this->visorDao->getValoresREACAR($parameters['estacion'], $parameters['tiempo'], $parameters['fInicial'], $parameters['fFinal']);
                    // revisar si hay imagenes de la estación en caso de tener los permisos y acceso a esa camara
                    if (in_array(sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_TIMELAPSE, $accesos) && in_array($parameters['estacion'], $video_usuario)) {
                        $val['timestamp'] = $this->visorDao->getPhotosReacar($parameters['nombre_estacion']);
                    }
                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function reacarImagesV2_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $id = $parameters['id'];
        $fichero = $parameters['fichero'];
        $estacion = $parameters['estacion'];
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            $video_usuario = [];
            if (isset($tokenValidado->payload->videos)) {
                $video_usuario = $tokenValidado->payload->videos;
            }
            if (in_array(sE::REDES_CONTROL_CALIDAD_REACAR, $accesos) && in_array(sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_VIDEO, $accesos) && isset($id) && in_array($id, $video_usuario)) {
                if (isset($fichero) && isset($estacion)) {
                    $b64image = $this->visorDao->getPhotoReacar($estacion, $fichero);
                    $this->response($b64image, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function reacarTimelapseV2_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $id = $parameters['id'];
        $estacion = $parameters['estacion'];
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            $video_usuario = [];
            if (isset($tokenValidado->payload->videos)) {
                $video_usuario = $tokenValidado->payload->videos;
            }
            if (in_array(sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE, $accesos) && in_array(sE::REDES_CONTROL_CALIDAD_REACAR_DETALLE_TIMELAPSE, $accesos) && isset($id) && in_array($id, $video_usuario)) {
                if (isset($estacion)) {
                    $response = $this->visorDao->getTimelapsePhotos($estacion);
                    $this->response($response, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function version_get()
    {
        $version = $this->visorDao->getVersion();
        $this->response($version, 200);
    }

    function esquema_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            // TODO add permission
            $accesos = $tokenValidado->payload->elementos;
            $DB = ConexionBD::Conexion();
            $getParameters = $this->input->get();
            $id = $getParameters['id'];

            if(isset($getParameters['fecha'])){
                $data = $this->visorDao->getEsquemaFecha($DB, $id, $getParameters['fecha']);
            } else {
                $data = $this->visorDao->getEsquemaV2($DB, $id);
            }

            $this->response($data, 200);
        }
    }

    function esquemaHidrologico_get()
    {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_ESQUEMAS, $accesos)) {
                $DB = ConexionBD::Conexion();
                $this->response($this->visorDao->getEsquema($DB, 'esquema_hidrologico'), 200);
                ConexionBD::CerrarConexion($DB);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function esquemaHidrologicoV2_get()
    {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_ESQUEMAS, $accesos)) {
                $DB = ConexionBD::Conexion();
                $this->response($this->visorDao->getEsquemaV2($DB, 'esquema_hidrologico'), 200);
                ConexionBD::CerrarConexion($DB);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function caudales_get()
    {
        $DB = ConexionBD::Conexion();
        $this->visorDao->caudales($DB);
        ConexionBD::CerrarConexion($DB);
    }

    function permisosElementos_get(){
        $DB = ConexionBD::Conexion();
        $this->visorDao->getPermisosElementos($DB);
        ConexionBD::CerrarConexion($DB);
    }

    function piezos_get() {
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            if (
                (in_array(sE::REDES_CONTROL_PIEZOMETROS, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_FORMULARIO, $accesos))
                || (in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES_FORMULARIO, $accesos))
            ) {
                $piezos = $this->visorDao->getPiezosListForm();
                $this->response($piezos, 200);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function datoPiezoManualV2_add_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (
                (in_array(sE::REDES_CONTROL_PIEZOMETROS, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_FORMULARIO, $accesos))
                || (in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES_FORMULARIO, $accesos))
            ) {
                try {
                    $datos = $parameters['datos_piezo'];
                    $response = $this->visorDao->saveDatoPiezoManualV2($datos);
                    $this->response($response, 200);
                } catch (Exception $e) {
                    $this->response($e->getMessage(), 200);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function datoPiezoManualV2_delete_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (
                (in_array(sE::REDES_CONTROL_PIEZOMETROS, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_FORMULARIO, $accesos))
                || (in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES_FORMULARIO, $accesos))
            ) {
                try {
                    $datos = $parameters['datos_piezo'];
                    $response = $this->visorDao->deleteDatoPiezoManualV2($datos);
                    $this->response($response, 200);
                } catch (Exception $e) {
                    $this->response($e->getMessage(), 200);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function datoPiezoManual_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $datos = $parameters['datos_piezo'];

        if(isset($datos) && count($datos)>0){
            foreach($datos as &$dato){
                if(isset($dato["fecha"])){
                    $dato["timestamp"] = $dato["fecha"];
                    unset($dato["fecha"]);
                }
                if(isset($dato["nivel"])){
                    $dato["valor"] = $dato["nivel"];
                    unset($dato["nivel"]);
                }
                if(isset($dato["cod_piezo"])){
                    $dato["cod_variable"] = $dato["cod_piezo"]."/CP";
                    unset($dato["cod_piezo"]);
                }
            }
        }

        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (
                (in_array(sE::REDES_CONTROL_PIEZOMETROS, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_FORMULARIO, $accesos))
                || (in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES_FORMULARIO, $accesos))
            ) {
                try {
                    $response = $this->visorDao->saveDatoPiezoManual($datos);
                    $this->response($response, 200);
                } catch (Exception $e) {
                    $this->response($e->getMessage(), 200);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function piezosManualesHist_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $estacion = $parameters['estacion'];
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            if (
                (in_array(sE::REDES_CONTROL_PIEZOMETROS, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_FORMULARIO, $accesos))
                || (in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES, $accesos) && in_array(sE::REDES_CONTROL_PIEZOMETROS_MANUALES_FORMULARIO, $accesos))
            ) {
                $datos = $this->visorDao->getPiezosManualesHist($estacion);
                $this->response($datos, 200);
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function ficha_piezometro_post() 
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        $parametros = $this->input->post();
        if($parametros && $tokenValidado){
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::REDES_CONTROL_PIEZOMETROS_DETALLE_FICHA, $accesos)) {
                $datos = $this->visorDao->getEsquemaPiezo($parametros['id']);
                $this->load->helper('download');
                force_download('esquema_piezo.pdf', $datos); 
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function explotacion_sistema_delete() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            $id = intval($this->uri->segments[3]);

            require_once APPPATH . '/libraries/visor/class_explotacionV3.php';
            $isGlobal = $id == null ? $global : ExplotacionV3::isSystemGlobal($id);
            $permission = $isGlobal ? sE::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION_GLOBAL : se::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION_LOCAL;
            if (!in_array($permission, $accesos)) {
                $this->response(null , 401);
            }
            ExplotacionV3::deleteSystem($id);
        } else {
            $this->response(null, 401);
        }
    }

    function explotacion_sistema_put() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;
            $parameters = $this->input->post();

            $id = $parameters['id_sistema'] ?? null;
            $nombre = $parameters['nombre'] ?? null;
            $global = $parameters['global'] ?? null;
            $variables = $parameters['variables'] ?? null;

            if (!$nombre) {
                $this->response(null , 400);
            }
            require_once APPPATH . '/libraries/visor/class_explotacionV3.php';
            $isGlobal = $id == null ? $global : ExplotacionV3::isSystemGlobal($id);
            $permission = $isGlobal ? sE::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION_GLOBAL : se::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION_LOCAL;
            if (!in_array($permission, $accesos)) {
                $this->response(null , 401);
            }

            if ($variables !== null && !ExplotacionV3::variableExists($variables)) {
                $this->response(null, 400);
            }

            if ($id) {
                if ($nombre) {
                    ExplotacionV3::updateSystemName($nombre, $id);
                }
                if ($global !== null) {
                    ExplotacionV3::updateSystemVisibility($global, $tokenValidado->payload->sub, $id);
                }
                if ($variables !== null) {
                    ExplotacionV3::updateVariables($variables, $id);
                }
            } else {
                $id = ExplotacionV3::addSystem($nombre, $tokenValidado->payload->sub, $global, $variables);
            }
            $this->response($id, 200);
        } else {
            $this->response(null, 401);
        }
    }

    function explotacion_initV3_get() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $retorno = array();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            // se filtra la lista de estaciones recibida con la base de datos para recibir datos extra de las estaciones
            $estacionesPorTipo = (array) $tokenValidado->payload->estaciones;
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION, $accesos)) {

                $idEstaciones = [];
                foreach ($estacionesPorTipo as $tipo) {
                    foreach ($tipo as $idEstacion) {
                        $idEstaciones[] = trim($idEstacion);
                    }
                }
                require_once APPPATH . '/libraries/visor/class_explotacionV3.php';
                $retorno["variables"] = ExplotacionV3::obtenerVariables($idEstaciones);
                $retorno["sistemas"] = ExplotacionV3::obtenerSistemas($tokenValidado->payload->sub);
                $this->response($retorno, 200);

            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function explotacion_init_get() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $retorno = array();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            // se filtra la lista de estaciones recibida con la base de datos para recibir datos extra de las estaciones
            $estacionesPorTipo = (array) $tokenValidado->payload->estaciones;
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION, $accesos)) {

                $idEstaciones = [];
                foreach ($estacionesPorTipo as $tipo) {
                    foreach ($tipo as $idEstacion) {
                        $idEstaciones[] = trim($idEstacion);
                    }
                }
                require_once APPPATH . '/libraries/visor/class_explotacionV2.php';
                $retorno["variables"] = ExplotacionV2::obtenerVariables($idEstaciones);
                $retorno["sistemas"] = ExplotacionV2::obtenerSistemas();

                // activar personalizado
                $retorno["sistemas"][] = ["nombre"=>'Personalizado',"variables"=>[]];

                $this->response($retorno, 200);

            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function explotacion_datos_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();

        $variables = $parameters['variables'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];

        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION, $accesos)) {
                if (isset($parameters) && isset($variables) && isset($periodo) && isset($fecha_i) && isset($fecha_f)) {
                    require_once APPPATH . '/libraries/visor/class_explotacion.php';

                    $cabeceras = Explotacion::crearEncabezado($variables);
                    
                    $val["encabezado"] = $cabeceras[2];
                    $val["encabezadoAdicional"] = [$cabeceras[0],$cabeceras[1]];
                    $val["valores"] = Explotacion::getExplotacion($variables, $periodo, $fecha_i, $fecha_f);

                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function explotacion_test_get() {
        require_once APPPATH . '/libraries/visor/class_evolucion_estacion.php';
        $variables = ["CC2-31/AT1"];
        $zoom = 'd';
        $fecha_i = 1339113628;
        $fecha_f = 1654526488.765;
        $DB = ConexionBD::Conexion();
        $res = EvolucionEstacion::obtener($DB, $zoom, $variables, $fecha_i, $fecha_f, false);
        ConexionBD::CerrarConexion($DB);
    }

    function explotacion_datosV3_post() {
        $this->explotacion_datosV2_post();
    }

    function explotacion_datosV2_post() {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();

        $variables = $parameters['variables'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];

        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            if (in_array(sE::MASAS_SUPERFICIALES_SISTEMAS_EXPLOTACION, $accesos)) {
                if (isset($parameters) && isset($variables) && isset($periodo) && isset($fecha_i) && isset($fecha_f)) {
                    require_once APPPATH . '/libraries/visor/class_explotacionV2.php';

                    $cabeceras = ExplotacionV2::crearEncabezado($variables);
                    
                    $val["encabezado"] = $cabeceras[2];
                    $val["encabezadoAdicional"] = [$cabeceras[0],$cabeceras[1]];
                    $val["valores"] = ExplotacionV2::getExplotacion($variables, $periodo, $fecha_i, $fecha_f);

                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }

    function evolucionVariables_post()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $parameters = $this->input->post();
        
        $variables = $parameters['variables'];
        $periodo = $parameters['periodo'];
        $fecha_i = $parameters['fecha_i'];
        $fecha_f = $parameters['fecha_f'];

        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado && $tokenValidado->payload && $tokenValidado->payload->elementos) {
            $accesos = $tokenValidado->payload->elementos;
            //if (in_array(sE::REDES_CONTROL_AFOROS, $accesos) && in_array(sE::REDES_CONTROL_AFOROS_GRAFICOS, $accesos)) {
                if (isset($parameters) && isset($variables) && isset($periodo) && isset($fecha_i) && isset($fecha_f)) {
                    
                    require_once APPPATH . '/libraries/visor/class_explotacion.php';
                    $cabeceras = Explotacion::crearEncabezado($variables);
                    $val["encabezado"] = $cabeceras[2];
                    $val["encabezadoAdicional"] = [$cabeceras[0],$cabeceras[1]];
                    $val["valores"] = Explotacion::getExplotacion($variables, $periodo, $fecha_i, $fecha_f);

                    $this->response($val, 200);
                } else {
                    $this->response(null, 400);
                }
            /*} else {
                $this->response(null, 401);
            }*/
        } else {
            $this->response(null, 401);
        }
    }

    function evolucionEstacion_post(){

        $_POST = json_decode(file_get_contents('php://input'), true);
        $parameters = $this->input->post();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        
        $variables = $parameters['variables'];
        $zoom = (isset($parameters['zoom'])) ? $parameters['zoom'] : null;
        $fecha_i = (isset($parameters['fecha_i'])) ? $parameters['fecha_i'] : null;
        $fecha_f = (isset($parameters['fecha_f'])) ? $parameters['fecha_f'] : null;
        $encabezado = (isset($parameters['encabezado'])) ? $parameters['encabezado'] : false;

        require_once APPPATH . '/libraries/visor/class_evolucion_estacion.php';

        $DB = ConexionBD::Conexion();
        $val = EvolucionEstacion::obtener($DB, $zoom, $variables, $fecha_i, $fecha_f, $encabezado);

        ConexionBD::CerrarConexion($DB);
        $this->response($val, 200);

    }

    function evolucionEstacionTest_get(){

        $variables = ["E1-05/VE1","E1-05/NE1","E1-05/COR","E1-05/ALIV","E1-05/RE","E1-05/PRA","E1-05/EV","E1-05/VRED","E1-05/AFT","E1-05/AAT","E1-05/AWT"];
        $zoom = 'd';
        $fecha_i = 1637326140;
        $fecha_f = 1650365280;
        $encabezado = true;

        require_once APPPATH . '/libraries/visor/class_evolucion_estacion.php';

        $DB = ConexionBD::Conexion();
        $val = EvolucionEstacion::obtener($DB, $zoom, $variables, $fecha_i, $fecha_f, $encabezado);
        ConexionBD::CerrarConexion($DB);
        $this->response($val, 200);

    }

    function informeSemaforico_get(){
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
        if ($tokenValidado) {
            $accesos = $tokenValidado->payload->elementos;

            $params = $this->input->get();
            $stationType = $params['tipo'] ?? null;
            $fecha = $params['fecha'] ?? null;

            $permission = [
                'EAA' => sE::REDES_CONTROL_CALIDAD_SAICA_INFORMESEMAFORICO,
                'ECC' => sE::REDES_CONTROL_CALIDAD_REACAR_INFORMESEMAFORICO,
                'EXC' => sE::REDES_CONTROL_CALIDAD_INDUSTRIAL_INFORMESEMAFORICO,
                'S' => sE::REDES_CONTROL_CALIDAD_SMULTIPARAM_INFORMESEMAFORICO,
            ];
            if (array_key_exists($stationType, $permission) && in_array($permission[$stationType], $accesos)) {
                if ($stationType === 'EAA') {
                    require_once APPPATH . '/libraries/visor/class_informe_semaforico.php';
                    $this->response(InformeSemaforico::getInfomeEAA($fecha), 200);
                } else if ($stationType === 'ECC') {
                    require_once APPPATH . '/libraries/visor/class_informe_semaforico.php';
                    $this->response(InformeSemaforico::getInfomeECC($fecha), 200);
                } else if ($stationType === 'EXC') {
                    require_once APPPATH . '/libraries/visor/class_informe_semaforico.php';
                    $this->response(InformeSemaforico::getInfomeEXC($fecha), 200);
                } else if ($stationType === 'S') {
                    require_once APPPATH . '/libraries/visor/class_informe_semaforico.php';
                    $this->response(InformeSemaforico::getInfomeS($fecha), 200);
                } else {
                    $this->response(null, 400);
                }
            } else {
                $this->response(null, 401);
            }
        } else {
            $this->response(null, 401);
        }
    }


    function informeVariableInit_get(){

        /*$token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado) {*/

            require_once APPPATH . '/libraries/visor/class_informe_variable.php';

            $conf = InformeVariable::init();

            $this->response($conf, 200);

        /*} else {
            $this->response(null, 401);
        }*/
    }


    function informeVariable_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);
        $params = $this->input->post();
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);

        if ($tokenValidado) {

            $fecha_i = (isset($params['fecha_i'])) ? $params['fecha_i'] : null;
            $fecha_f = (isset($params['fecha_f'])) ? $params['fecha_f'] : null;
            $zona = (isset($params['zona'])) ? $params['zona'] : null;

            if(!isset($params['tipo']) && !in_array($params['tipo'],['evolucion','variacion'])) return;
            
            if(!isset($params['variables'])) return;

            if($params['tipo'] == 'variacion' && ($fecha_i == null || $fecha_f == null)) return;

            $DB = ConexionBD::Conexion();

            require_once APPPATH . '/libraries/visor/class_informe_variable.php';

            if($params['tipo']=='evolucion'){
                // datos actuales de embalses
                $ult_res_embalses = $this->visorDao->CargarMasterULTRESCategorias($DB, 'E');
                $datosInforme = InformeVariable::getDataEvolucion($DB, $ult_res_embalses, $params['variables'], $zona);
            }

            // si la fecha final es la actual se usan datos de tiempo real
            else if($params['tipo']=='variacion' && date('Y-m-d') == $fecha_f){
                // datos actuales de embalses
                $ult_res_embalses = $this->visorDao->CargarMasterULTRESCategorias($DB, 'E');
                $datosInforme = InformeVariable::getDataDiferenciaULTRES($DB, $ult_res_embalses, $params['variables'], $zona, $fecha_i);
            }

            // si la fecha final es distinta a la actual se usan solo datos históricos
            else if($params['tipo']=='variacion'){
                $datosInforme = InformeVariable::getDataDiferencia($DB, $params['variables'], $zona, $fecha_i, $fecha_f);
            }

            ConexionBD::CerrarConexion($DB);

            $this->response($datosInforme, 200);

        } else {
            $this->response(null, 401);
        }
    }

}
