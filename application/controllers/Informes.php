<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
require_once APPPATH . '/libraries/visor/class_jasper_reports.php';
class Informes extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, AUTHJWT, Authorization, Access-Control-Request-Method, Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('auth2_helper');
        $this->load->library('session');        
        $this->load->helper('jwt_helper');        
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->library('common/user');           
        $this->load->model('BasicCrudDao');
        $this->load->model('SecurityDao');
        $this->load->library('email');
        
    } 
    
    function getMisInformes_get(){
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $tokenValidado = AUTHORIZATION::validateToken($token);
		$idUsuario = $tokenValidado->payload->sub;
        
        
        $permisosInformes = $this->SecurityDao->getInformesUsuario($idUsuario);
        $idInformes         =  $this->SecurityDao->getVisibleElements($permisosInformes, 'cod_informe');
        if (sizeof($idInformes)>0){
            $filters  = array();
            $filter = new Filter();
            $filter->column = "cod_informe";
            $filter->values = $idInformes;
            $filter->type = Constants::TYPE_FILTER_ARRAY_INTEGER;
            array_push($filters,$filter);  
            $this->response($this->BasicCrudDao->getData("INFORMES", $filters, null), 200);
        }
        else{
            $this->response([], 200);
        }   
   }

   function jasper_informe_post(){
        $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
        $_POST = json_decode(file_get_contents('php://input'), true);
        $parametros = $this->input->post();
        if($parametros){
            if ($this->hayPermisoInforme($parametros['id'],$token)){
                $datos = JasperReports::get_informe($parametros['uri'], $parametros['parametros'], Constants::USER_JASPER, Constants::PWD_JASPER);
                $this->load->helper('download');
                force_download('test.pdf', $datos); 
            }
            else{
                $this->response(null, 401);    
            }
        } else {
            $this->response(null, 401);
        }
    }   

   function jasper_informe_xls_post(){
    $token = (!is_null($this->input->get_request_header('AUTHJWT'))) ? $this->input->get_request_header('AUTHJWT') : $this->input->get_request_header('Authorization');
    $_POST = json_decode(file_get_contents('php://input'), true);
    $parametros = $this->input->post();
    if($parametros){
        if ($this->hayPermisoInforme($parametros['id'],$token)){
            $datos = JasperReports::get_informe($parametros['uri'], $parametros['parametros'], Constants::USER_JASPER,  Constants::PWD_JASPER,'xls');
            $this->load->helper('download');
            force_download('test.xls', $datos); 
        }
        else{
            $this->response(null, 401);    
        }
    } else {
        $this->response(null, 401);
    }
}

function jasper_informe_inputcontrols_get(){
    $parameters = $this->input->get();
        if($parameters['uri']){
            
            $inputs = JasperReports::get_inputControls_infome($parameters['uri'], Constants::USER_JASPER,  Constants::PWD_JASPER);
            $this->response($inputs, 200);
        } else {
            $this->response($inputs, 400);
        }
}

function hayPermisoInforme($id_informe, $token){
    $tokenValidado = AUTHORIZATION::validateToken($token);
    $idUsuario = $tokenValidado->payload->sub;
    //echo $idUsuario;
    //$idUsuario=10;
    $permisosInformes = $this->SecurityDao->getInformesUsuario($idUsuario);
    $idInformes         =  $this->SecurityDao->getVisibleElements($permisosInformes, 'cod_informe');
    return in_array($id_informe, $idInformes);
}


}
