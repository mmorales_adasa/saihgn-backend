<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");
class DatoDiario extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method,Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('http_helper');        
        $this->load->library('session');
        $this->load->library('common/user');      
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');
        $this->load->model('datosDiariosDao');
        $this->load->library('sgi/VariableModel');
        $this->load->library('sgi/EstacionModel');
        $this->load->library('sgi/DatoDiarioModel'); 
    }    
    function findVariables_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $this->response( $this->datosDiariosDao->findVariables($criteria), 200);    
        
    }
    function findData_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filters     = isset($parameters['data']['filters']) ? $parameters['data']['filters'] : [];        
        $criteria = $sql= $this->filter->readCriteria($filters);        
        $this->response( $this->datosDiariosDao->findData($criteria), 200);    
    }   
    function saveData_post() {                   
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $data     = isset($parameters['data']) ? $parameters['data'] : [];        
        $this->response( $this->datosDiariosDao->saveData($data), 200);    
    }         
}
