<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . "/libraries/REST_Controller.php");
require(APPPATH . "/models/queries.php");

class Esquema extends REST_Controller  {
	function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Request-Method,Access-Control-Allow-Origin");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {            
            die();
        }
        parent::__construct();
        $this->load->database();          
        $this->load->helper('url'); 
        $this->load->helper('file');
        $this->load->helper('auth_helper');
        $this->load->helper('http_helper');        
        $this->load->library('session');
        $this->load->library('common/user');      
        $this->load->library('common/termFormula');  
        $this->load->library('common/node');      
        $this->load->library('common/filter');
        $this->load->library('common/utilDao');
        $this->load->library('common/constants');        
        $this->load->model('esquemaDao');
        $this->load->library('sgi/VariableModel');
        $this->load->library('sgi/EstacionModel');

        $this->load->library('common/EsquemaFile');  
        
    }    
    function getData_post(){
        $files = scandir(APPPATH . DIRECTORY_SEPARATOR . Constants::PATH_ESQUEMAS);
        $filer = '/(.html|.htm)/';
        $esquemas  = array();
        foreach($files as $file){
          if(preg_match($filer, strtolower($file))){
            $esquema = new EsquemaFile();
            $esquema->setFileName($file);
            array_push($esquemas,$esquema); 
            
          }
        }
        $this->response( $esquemas, 200);            
    }  
    function remove_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filename      = $parameters['data']['filename'];          
        $path_parts = pathinfo($filename);
        $nameNoExt = $path_parts['filename'];
        $this->esquemaDao->remove($nameNoExt);
        unlink(APPPATH . DIRECTORY_SEPARATOR . Constants::PATH_ESQUEMAS . DIRECTORY_SEPARATOR  .  $filename);
        $this->response(1, 200);            
    }  
    function download_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filename      = $parameters['data']['filename'];          
        $content= file_get_contents(APPPATH . DIRECTORY_SEPARATOR . Constants::PATH_ESQUEMAS . DIRECTORY_SEPARATOR  .  $filename);
        $this->response($content, 200);            
    }  

    function upload_post(){
        $_POST = json_decode(file_get_contents('php://input'), true);  
        $parameters  = $this->input->post();
        $filename      = $parameters['data']['filename'];          
        $content       = $parameters['data']['content'];          
        $nameNoExt     = explode(".", $filename)[0];
        $this->esquemaDao->inserOrUpdate($nameNoExt,$content);
        $content = file_put_contents(APPPATH . DIRECTORY_SEPARATOR . Constants::PATH_ESQUEMAS . DIRECTORY_SEPARATOR  .  $filename, $content);

        $this->response($content, 200);            
    }  
}
