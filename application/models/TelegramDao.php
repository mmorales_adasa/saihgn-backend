<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
require_once APPPATH . '/libraries/visor/class_cache.php';
require_once APPPATH . '/libraries/visor/class_sondas.php';
require_once APPPATH . '/libraries/visor/class_evolucion_estacion.php';
require(APPPATH . "/libraries/visor/security_elements.php");

class TelegramDao extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function getCodEstaciones($cod_estacion)
	{
		$resultado = array();
		$query = "SELECT cod_variable FROM tbl_variable WHERE cod_variable LIKE '$cod_estacion/%';";
		$data = ConexionBD::EjecutarConsultaDev($query);

		if (count($data) > 0) {
			foreach($data as $valor) {
				array_push($resultado, $valor["cod_variable"]);
			}
		}
		return $resultado;
	}

	function getEstaciones($nombreEstacion, $codEstacion, $tipoEstacion, $zona, $grantedStations = null)
	{
        $resultado = array();
        $ultres = $this->CargarMasterULTRESEstacionesTelegram();
		$query = "SELECT distinct (te.cod_estacion), te.nombre, te.tipo, te.latitud, te.longitud, te.disponible, te.zona, te.orden, te.profundidad, te.cota_boca_sondeo, te.acronimo 
                  FROM tbl_estacion te
                  INNER JOIN tbl_color_alarmas_tr tcat ON te.tipo = tcat.tipo";
                  if (isset($grantedStations)) {
                    $query .= " INNER JOIN tbl_sec_rel_nivel_acceso_datos tad ON tad.cod_item = te.cod_estacion AND (tad.cod_nivel_acceso = 0 OR te.cod_estacion IN ($grantedStations))";
                  } else {
                    $query .= " INNER JOIN tbl_sec_rel_nivel_acceso_datos tad ON tad.cod_item = te.cod_estacion AND tad.cod_nivel_acceso = 0";
                  }
        $query .= " WHERE te.disponible = true";      

        if (isset($nombreEstacion)) {
			$query .= " AND te.nombre = '$nombreEstacion'";
		} 
		if (isset($codEstacion)) {
			$query .= " AND te.cod_estacion = '$codEstacion'";
		}
		if (isset($tipoEstacion)) {
			$query .= " AND te.tipo = '$tipoEstacion'";
		}
		if (isset($zona)) {
			$query .= " AND te.zona = '$zona'";
		}

		$data = ConexionBD::EjecutarConsultaDev($query);
        
        if (count($data) > 0) {
            foreach($data as $dato) {
                $estacion = new EstacionTelegram();

                $estacion->setCodEstacion($dato['cod_estacion']);
                $estacion->setNombre($dato['nombre']);
                $estacion->setTipo($dato['tipo']);
                $estacion->setLatitud($dato['latitud']);
                $estacion->setLongitud($dato['longitud']);
                $estacion->setDisponible($dato['disponible']);
                $estacion->setZona($dato['zona']);
                $estacion->setOrden($dato['orden']);
                $estacion->setProfundidad($dato['profundidad']);
                $estacion->setCotaBocaSondeo($dato['cota_boca_sondeo']);
                $estacion->setAcronimo($dato['acronimo']);
                $ultres = $this->eliminarInfoDuplicada($ultres, $dato['cod_estacion']);
                $ultres[$dato['cod_estacion']]['valores'][0]['estado_comunicacion'] = $this->getEstadoComunicacion($tipoEstacion, $ultres[$dato['cod_estacion']]['valores'][0]['timestamp'], $dato);
                $estacion->setUltres($ultres[$dato['cod_estacion']]);

                array_push($resultado, $estacion);
            }
        }

		return $resultado;
	}

    function getZonas() {
        $retorno = array();
        $estacionesDisponibles = $this->VisorDao->Estaciones(null);

        foreach ($estacionesDisponibles as $estacion) {
            if (isset($ultres[$estacion['cod_estacion']])) $estacion['ultres'] = $ultres[$estacion['cod_estacion']];
            
            // por cada estación disponible se añadie en el apartado de zonas si tiene el permiso para esa zona y es de tipo Embalse
            if (trim($estacion["zona"]) == "Z3" && $estacion["tipo"] == "E") $retorno["zonas"]["Z3"][] = $estacion;
            if (trim($estacion["zona"]) == "Z5" && $estacion["tipo"] == "E") $retorno["zonas"]["Z5"][] = $estacion;
            if (trim($estacion["zona"]) == "Z12" && $estacion["tipo"] == "E") $retorno["zonas"]["Z12"][] = $estacion;
            if (trim($estacion["zona"]) == "Z67" && $estacion["tipo"] == "E") $retorno["zonas"]["Z67"][] = $estacion;
        }
        return $retorno;
    }

    function CargarMasterULTRESEstacionesTelegram() {
        $DB = ConexionBD::Conexion();
		return Cache::CargarMasterULTRESEstacionesTelegram($DB);
	}

    function getValoresEstacionCache($codEstacion, $grantedStations = null) {
        $ultres = $this->CargarMasterULTRESEstacionesTelegram();
        $ultres = $this->generarEstadoComunicacion($ultres);

        $query = "SELECT * FROM tbl_estacion te";
        if (isset($grantedStations)) {
            $query .= " INNER JOIN tbl_sec_rel_nivel_acceso_datos tad ON tad.cod_item = te.cod_estacion AND (tad.cod_nivel_acceso = 0 OR te.cod_estacion IN ($grantedStations))";
        } else {
            $query .= " INNER JOIN tbl_sec_rel_nivel_acceso_datos tad ON tad.cod_item = te.cod_estacion AND tad.cod_nivel_acceso = 0";
        }
        $query .= " WHERE te.disponible = true";
        
        if (isset($codEstacion)) {
			$query .= " AND te.cod_estacion = '$codEstacion'";
		}
        $data = ConexionBD::EjecutarConsultaDev($query);

        if (isset($codEstacion) && count($data) > 0 && isset($ultres[$codEstacion])) {
            unset($ultres[$codEstacion]['encabezado']);
            return $ultres[$codEstacion];
        } else {
            return array();
        }

    }

    function getEvolucionEstacion($zoom, $variables, $fecha_i, $fecha_f, $encabezado) {
        $DB = ConexionBD::Conexion();
        $val = EvolucionEstacion::obtener($DB, $zoom, $variables, $fecha_i, $fecha_f, $encabezado);

        ConexionBD::CerrarConexion($DB);
        return $val;
    }

    function getColorAlarmas() {
        $resultado = array();
        $query = "SELECT * FROM tbl_color_alarmas_tr";
        $data = ConexionBD::EjecutarConsultaDev($query);

        if (count($data) > 0) {
            foreach($data as $dato) {
                array_push($resultado, $dato);
            }
        }
        return $resultado;
    }

    function getTipoEstacion($cod_estacion) {
        $query = "SELECT tipo FROM tbl_estacion WHERE cod_estacion = '$cod_estacion'";
        $data = ConexionBD::EjecutarConsultaDev($query);

        if (count($data) > 0) {
            return $data[0]["tipo"];
        }
        return null;
    }

	function getImagen($cod_estacion, $videos = null, $grantedStations = null) {
        $cod_camaras = array();
        $resultado = new ImagenesBase64();
        $videosPublicos = array();
        $videosPrivados = array();
		$estacion = $this->getEstaciones(null, $cod_estacion, null, null, $grantedStations);
        if (count($estacion) > 0) {
            $acronimo = $estacion[0]->acronimo;
        } else {
            return [];
        }

        if (isset($videos) && count($videos) > 0) {
            foreach ($videos as $video) {
                if (stripos($video, $acronimo) !== false) {
                    array_push($cod_camaras, $video);
                }
            }
        }

        $queryPublica = "SELECT distinct (v.cod_video), v.tipo, v.descripcion, v.modelo, v.ip, v.mac, v.timelapse, v.destacado
                         FROM tbl_video v
                         LEFT JOIN tbl_sec_rel_nivel_acceso_video tav on tav.cod_video = v.cod_video
                         WHERE v.cod_video LIKE '$acronimo-%' AND tav.visible = 1 AND tav.cod_nivel_acceso = 0
                         ORDER BY v.cod_video ASC";

        $camarasPublicas = ConexionBD::EjecutarConsultaDev($queryPublica);
        if (count($camarasPublicas) > 0) {
            $videosPublicos = $this->getCamarasB64($camarasPublicas, $acronimo);
            $resultado->setPublicas($videosPublicos);
        }

        if(count($cod_camaras) > 0) {
            $cod_videos = str_replace(' ', '',implode('|', $cod_camaras));
            $queryPrivada = "SELECT distinct (v.cod_video), v.tipo, v.descripcion, v.modelo, v.ip, v.mac, v.timelapse, v.destacado
            FROM tbl_video v
            LEFT JOIN tbl_sec_rel_nivel_acceso_video tav on tav.cod_video = v.cod_video WHERE v.cod_video similar to '%($cod_videos)%'
            ORDER BY v.cod_video ASC";
            $camarasPrivadas = ConexionBD::EjecutarConsultaDev($queryPrivada);
            if (count($camarasPrivadas) > 0) {
                $videosPrivados = $this->getCamarasB64($camarasPrivadas, $acronimo);
                $resultado->setPrivadas($videosPrivados);
            }
        }

        return $resultado;
	}

	static function Camara($camara, $tipo, $acronimo) {
		require APPPATH.'/libraries/visor/config.php';
        
        $array = Array();
        $array['nombre'] = null;
        $array['descripcion'] = null;
        $array['src'] = null;
        $array['url'] = null;
        $array['urlInterna'] = null;
        $array["timelapse"] = false;
        $array['timelapse_directory'] = false;
        $array['tipo'] = $tipo;

        // si no existe la camara en db no buscar el fichero
        if($tipo != "no_disponible") {
            $file = APPPATH  ."libraries/visor/video_cache/" . $acronimo . "-video/" . $camara['cod_video'] . ".jpg";
            if (file_exists($file)){
                $array['src'] = $acronimo . "-video/" . $camara['cod_video'] . ".jpg";
            }else{
                $array['src'] = "";
            }
        }

        if($camara['timelapse']){
            $dir_timelapse = APPPATH  ."libraries/visor/timelapse_cache/" . $acronimo . "-video/" . $camara['cod_video'] . "_timelapse/";
            
            if(is_dir($dir_timelapse)){
                $array["timelapse"] = true;
                $array['timelapse_directory'] = $acronimo . "-video/" . $camara['cod_video'] . "_timelapse";
            }
        }

        switch ($tipo){
            case 'sistema':
                $array['nombre'] = $acronimo . '-video';
                $array['descripcion'] = 'Presa de ' . $camara['nombre_estacion'];
                
                $aux = explode('.',trim($camara['ip']));
                $puerto_externo = '5'. $aux[2].'0';
                $ip_interna = $aux[0] .'.'. $aux[1] .'.'. $aux[2] .'.200';
                $array["url"] = 'http://www.saihguadiana.com:' . $puerto_externo;
                $array["urlInterna"] = 'http://'.$ip_interna.':80';

                break;
            case 'presa':
                $array['nombre'] = $camara['cod_video'];
                $array['descripcion'] = $camara['descripcion'];
                $array["url"] = 'http://'.$camara['ip'];
                break;
            
            case 'saica':
                $array['nombre'] = $estacion['nombre'];
                $array['descripcion'] = $camara['cod_video'] . ': ' . $camara['descripcion'];
                $array["url"] = 'http://'.$camara['ip'];
                break;

            case 'no_disponible':
                $array['nombre'] = $acronimo . '-video';
                $array['descripcion'] = 'Presa de ' . $camara['nombre_estacion'];
                $array['status'] = "servidor no disponible";
                break;
        }

        return $array;
    }

    function ConfAlarmas_tr($tipo)
	{
		$query = "SELECT * FROM tbl_color_alarmas_tr WHERE tipo = '$tipo'";
		$data = ConexionBD::EjecutarConsultaDev($query);
        
		return count($data) > 0 ? $data[0] : null;
	}

    function getEstadoComunicacion($tipo, $timestamp, $dato)
    {
        if ($timestamp == null || $timestamp == "") {
            return null;
        }

        if (!isset($dato['segundos_1'])) {
            $dato = $this->ConfAlarmas_tr('E');
        }

        $segundos = strtotime(date(Constants::DATEFORMAT,time())) - strtotime($timestamp);
        $segundos = abs($segundos);

        if ($dato['segundos_4'] <= $segundos) {
            return $dato['color_contador_4'];
        } else if ($dato['segundos_3'] <= $segundos) {
            return $dato['color_contador_3'];
        } else if ($dato['segundos_2'] <= $segundos) {
            return $dato['color_contador_2'];
        } else {
            return $dato['color_contador_1'];
        }
    }

    function getBuscadorEstaciones($filtro, $grantedStations = null) {
        $query = "SELECT distinct (cod_estacion), nombre, acronimo, tipo
                  FROM tbl_estacion te";
        if (isset($grantedStations)) {
            $query .= " INNER JOIN tbl_sec_rel_nivel_acceso_datos tad ON tad.cod_item = te.cod_estacion AND (tad.cod_nivel_acceso = 0 OR te.cod_estacion IN ($grantedStations))";
        } else {
            $query .= " INNER JOIN tbl_sec_rel_nivel_acceso_datos tad ON tad.cod_item = te.cod_estacion AND tad.cod_nivel_acceso = 0";
        }
        $query .= " WHERE te.disponible = true";
        
        if (isset($filtro)) {
			$query .= " AND (UPPER(te.cod_estacion) LIKE UPPER('%$filtro%') OR UPPER(te.nombre) LIKE UPPER('%$filtro%') OR UPPER(te.acronimo) LIKE UPPER('%$filtro%'))";
		}

		$data = ConexionBD::EjecutarConsultaDev($query);
        return $data;
    }

    function generarEstadoComunicacion($ultres) {
        $alarmas = $this->getColorAlarmas();
        foreach($ultres as $item) {
            $ultres[$item['valores'][0]['cod_estacion']]['valores'][0]['estado_comunicacion'] = null;
        }
    
        foreach($alarmas as $alarma) {
            foreach($ultres as $item) {
                if ($alarma['tipo'] == $item['valores'][0]['tipo']) {
                    $ultres[$item['valores'][0]['cod_estacion']]['valores'][0]['estado_comunicacion'] = $this->getEstadoComunicacion($alarma['tipo'], $item['valores'][0]['timestamp'], $alarma);
                }
            }
        }
        return $ultres;
    }

    function eliminarInfoDuplicada($ultres, $codEstacion) {
        unset($ultres[$codEstacion]['encabezado']);
        unset($ultres[$codEstacion]['valores'][0]['tipo']);
        unset($ultres[$codEstacion]['valores'][0]['orden']);
        unset($ultres[$codEstacion]['valores'][0]['nombre']);
        unset($ultres[$codEstacion]['valores'][0]['zona']);
        unset($ultres[$codEstacion]['valores'][0]['cod_estacion']);

        return $ultres;
    }

    function getHistoricoSondasMulti($id, $antiguedad) {
        $timestamp = array();
        if ($antiguedad) {
            $timestamp = Sondas::getTimestampsSondasBOT($id, $antiguedad);
        } else {
            $timestamp = Sondas::getTimestampsSondasBOT($id);
        }
        if (count($timestamp) > 0 && isset($timestamp[0]["timestamp_sondeo"])) {
            return Sondas::getEvolucionSondas($id, $timestamp[0]["timestamp_sondeo"]);
        }
    }

    function getUserByTelegram($cod_telegram) {
        $query = "SELECT * FROM tbl_sec_usuarios tsu WHERE tsu.usuario_telegram = '$cod_telegram'";

        $data = ConexionBD::EjecutarConsultaDev($query);
        if (count($data) > 0) {
            return $data[0];
        }
        return null;
    }

    function getCamarasB64($camaras, $acronimo) {
        $videos = Array();
        $videosB64 = array();
        foreach($camaras as $camara) {
            if($camara['cod_video']){
                $videos[] = $this->Camara($camara, 'presa', $acronimo);
            }
            else {
                $videos[] = $this->Camara($camara, 'no_disponible', $acronimo);
            }
        }
        foreach($videos as $video) {
            $imagenFinal = new ImagenBase64();
            if (is_file(APPPATH . "libraries/visor/video_cache/" . $video['src'])) {
                $image = file_get_contents(APPPATH . "libraries/visor/video_cache/" . $video['src']);
                $b64image = base64_encode($image);
                $imagenFinal->setNombre($video['nombre']);
                $imagenFinal->setDescripcion($video['descripcion']);
                $imagenFinal->setBase64($b64image);
                array_push($videosB64, $imagenFinal);
            }
        }
        return $videosB64; 
    }

    function getValoresEmbalsesZonaAndDate($idZona, $fecha, $tipo) {
        $resultado = array();
        
        if ($tipo == 'CR') {
            $query = "SELECT cod_estacion, fecha, acronimo, nombre, orden, zona, coalesce(SUM(valor_media_NR1), null) AS valor_media_NR1, coalesce(SUM(valor_media_QR1),null) AS valor_media_QR1
                  FROM ( SELECT d.cod_estacion, d.cod_variable, e.acronimo, e.nombre, e.orden, e.zona, to_date(concat(LPAD(d.anyo::text, 4, '0'), LPAD(d.mes::text,2, '0'), LPAD(d.dia::text, 2, '0')), 'YYYYMMDD') fecha,
                         CASE WHEN d.cod_variable like '%/NR1' THEN ROUND(d.valor_media::DECIMAL, 6) END AS valor_media_NR1,
                         CASE WHEN d.cod_variable like '%/QR1' THEN ROUND(d.valor_media::DECIMAL, 6) END AS valor_media_QR1
                         FROM tbl_olap_medidas_dias d 
                         LEFT JOIN tbl_estacion e ON d.cod_estacion = e.cod_estacion
                         WHERE (cod_variable like '%/NR1' or cod_variable like '%/QR1') and d.data_medicion = '$fecha' and d.cod_tipo_estacion = '$tipo' and e.disponible = true) datos
                  GROUP BY cod_estacion, fecha, acronimo, nombre, orden, zona
                  ORDER BY zona ASC, orden ASC, cod_estacion ASC";
            $data = ConexionBD::EjecutarConsultaDev($query);
        }
        if ($tipo == 'E') {
            $query = "SELECT cod_estacion, fecha, acronimo, nombre, orden, zona, coalesce(SUM(valor_media_VE1), null) AS valor_media_VE1, coalesce(SUM(valor_media_NE1),null) AS valor_media_NE1
                  FROM ( SELECT d.cod_estacion, d.cod_variable, e.acronimo, e.nombre, e.orden, e.zona, to_date(concat(LPAD(d.anyo::text, 4, '0'), LPAD(d.mes::text,2, '0'), LPAD(d.dia::text, 2, '0')), 'YYYYMMDD') fecha,
                         CASE WHEN d.cod_variable like '%/VE1' THEN ROUND(d.valor_media::DECIMAL, 6) END AS valor_media_VE1,
                         CASE WHEN d.cod_variable like '%/NE1' THEN ROUND(d.valor_media::DECIMAL, 6) END AS valor_media_NE1
                         FROM tbl_olap_medidas_dias d
                         LEFT JOIN tbl_estacion e ON d.cod_estacion = e.cod_estacion
                         WHERE (cod_variable like '%/VE1' or cod_variable like '%/NE1') 
                         AND d.data_medicion = '$fecha'
                         AND d.cod_tipo_estacion = '$tipo'
                         AND e.disponible = true";
                         isset($idZona) ?  $query .= " AND e.zona = '$idZona'" : $query .= "";
                $query .= " ) datos GROUP BY cod_estacion, fecha, acronimo, nombre, orden, zona
                         ORDER BY zona ASC, orden ASC, cod_estacion ASC";
                $data = ConexionBD::EjecutarConsultaDev($query);
        }
        if ($tipo == 'NR') {
            $query = "SELECT cod_estacion, fecha, acronimo, nombre, orden, zona, coalesce(SUM(valor_media_NR1), null) AS valor_media_NR1, coalesce(SUM(valor_media_QR1),null) AS valor_media_QR1
                  FROM ( SELECT d.cod_estacion, d.cod_variable, e.acronimo, e.nombre, e.orden, e.zona, to_date(concat(LPAD(d.anyo::text, 4, '0'), LPAD(d.mes::text,2, '0'), LPAD(d.dia::text, 2, '0')), 'YYYYMMDD') fecha,
                         CASE WHEN d.cod_variable like '%/NR1' THEN ROUND(d.valor_media::DECIMAL, 6) END AS valor_media_NR1,
                         CASE WHEN d.cod_variable like '%/QR1' THEN ROUND(d.valor_media::DECIMAL, 6) END AS valor_media_QR1
                         FROM tbl_olap_medidas_dias d
                         LEFT JOIN tbl_estacion e ON d.cod_estacion = e.cod_estacion
                         WHERE (cod_variable like '%/NR1' or cod_variable like '%/QR1') and d.data_medicion = '$fecha' and d.cod_tipo_estacion = '$tipo' and e.disponible = true) datos
                  GROUP BY cod_estacion, fecha, acronimo, nombre, orden, zona
                  ORDER BY zona ASC, orden ASC, cod_estacion ASC";
            $data = ConexionBD::EjecutarConsultaDev($query);
        }

        if (isset($data) && count($data) > 0) {
            foreach($data as $dato) {
                $valores = new ValoresEmbalse();
                $valores->setCodEstacion(isset($dato['cod_estacion']) ? $dato['cod_estacion'] : null);
                $valores->setNombre(isset($dato['nombre']) ? $dato['nombre'] : null);
                $valores->setAcronimo(isset($dato['acronimo']) ? $dato['acronimo'] : null);
                $valores->setFecha(isset($dato['fecha']) ? $dato['fecha'] : null);
                if ($tipo == 'CR') {
                    $valores->setNR1(isset($dato['valor_media_nr1']) ? (float) $dato['valor_media_nr1'] : null);
                    $valores->setQR1(isset($dato['valor_media_qr1']) ? (float) $dato['valor_media_qr1'] : null);
                    unset($valores->VE1);
                    unset($valores->NE1);
                }
                if ($tipo == 'E') {
                    $valores->setVE1(isset($dato['valor_media_ve1']) ? (float) $dato['valor_media_ve1'] : null);
                    $valores->setNE1(isset($dato['valor_media_ne1']) ? (float) $dato['valor_media_ne1'] : null);
                    unset($valores->NR1);
                    unset($valores->QR1);
                }
                if ($tipo == 'NR') {
                    $valores->setNR1(isset($dato['valor_media_nr1']) ? (float) $dato['valor_media_nr1'] : null);
                    $valores->setQR1(isset($dato['valor_media_qr1']) ? (float) $dato['valor_media_qr1'] : null);
                    unset($valores->VE1);
                    unset($valores->NE1);
                }
                array_push($resultado, $valores);
            }
        }
        

        return $resultado;
    }
}