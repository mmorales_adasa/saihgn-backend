<?php
class CurvaGastoDao extends CI_Model 
{
	function getInterpolacion($cod_curva){				
        $params = array();
        $params['cod_curva']['value'] = $cod_curva;
        $params['cod_curva']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CURVAS_GASTO"]["getInterpolacion"], $params);                
        $query = $this->db->query($sql);
        return $query->result();	     
    }      
	function calcula($cod_curva,$cod_estacion,  $cod_variable_caudal,$cod_variable_nivel,$fecha_ini,$fecha_fin){				
        $params = array();
        $params['cod_curva']['value'] = $cod_curva;
        $params['cod_curva']['type']  = Constants::TYPE_INTEGER;
        $params['cod_estacion']['value'] = $cod_estacion;
        $params['cod_estacion']['type']  = Constants::TYPE_TEXT;
        $params['cod_variable_caudal']['value'] = $cod_variable_caudal;
        $params['cod_variable_caudal']['type']  = Constants::TYPE_TEXT;
        $params['cod_variable_nivel']['value'] = $cod_variable_nivel;
        $params['cod_variable_nivel']['type']  = Constants::TYPE_TEXT;
        $params['data_inicial']['value'] = $fecha_ini;
        $params['data_inicial']['type']  = Constants::TYPE_TEXT;
        $params['data_final']['value'] = $fecha_fin;
        $params['data_final']['type']  = Constants::TYPE_TEXT;

        
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CURVAS_GASTO"]["calcula"], $params);                
        $query = $this->db->query($sql);
        //echo $query->result()[0]->result;        
        //return $query->result();	
        return true;    
    }      
	function importXMLData($data){				
        // Reviso si existe la estacion en bb.dd
        $params  = array();
        $errores = array();
        $params['cod_estacion']['value'] = $data['codEstacion'];
        $params['cod_estacion']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["ESTACIONES"]["detail"], $params);                
        $query = $this->db->query($sql);
        if (count($query->result()) == 0){
            array_push($errores, 'La estación con código ' . $data['codEstacion'] . ' no existe en base de datos'); 
        }
        // Reviso si existe la variable de caudal
        $params  = array();
        $params['cod_variable']['value'] = $data['codVariableCaudal'];
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["VARIABLES"]["detail"], $params);     
        $query = $this->db->query($sql);
        if (count($query->result()) == 0){
            array_push($errores, 'La variable de caudal con código ' . $data['codVariableCaudal'] . ' no existe en base de datos'); 
        }
        // Reviso si existe la variable de caudal
        $params  = array();
        $params['cod_variable']['value'] = $data['codVariableNivel'];
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["VARIABLES"]["detail"], $params);     
        $query = $this->db->query($sql);
        if (count($query->result()) == 0){
            array_push($errores, 'La variable de nivel con código ' . $data['codVariableNivel'] . ' no existe en base de datos'); 
        }

        // Reviso si existe alguna curva con ese nombre
        $params  = array();
        $params['desc_curva']['value'] = $data['nameCurva'];
        $params['desc_curva']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CURVAS_GASTO"]["detailByName"], $params);     
        $query = $this->db->query($sql);
        if (count($query->result()) > 0){
            array_push($errores, 'Ya existe una curva con ese nombre ' . $data['nameCurva']); 
        }
        if (count($errores)==0){
            $values  = $data['values'];
            foreach ($values as $item) { 
                //echo $item['nivel'];
            }

            $params = array();
            $params['cod_estacion']['value'] = $data['codEstacion'];
            $params['cod_estacion']['type']  = Constants::TYPE_TEXT;

            $params['desc_curva']['value'] = $data['nameCurva'];
            $params['desc_curva']['type']  = Constants::TYPE_TEXT;
            
            $params['cod_variable_nivel']['value'] = $data['codVariableNivel'];
            $params['cod_variable_nivel']['type']  = Constants::TYPE_TEXT;
            
            $params['cod_variable_caudal']['value'] = $data['codVariableCaudal'];
            $params['cod_variable_caudal']['type']  = Constants::TYPE_TEXT;
            
            $params['data_inicial']['value'] = $data['data_inicial'];
            $params['data_inicial']['type']  = Constants::TYPE_TEXT;        

            $params['data_final']['value'] = $data['data_final'];
            $params['data_final']['type']  = Constants::TYPE_TEXT;

            $query= $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CURVAS_GASTO"]["insert"], $params);
            $this->db->query($query);	

			$sql = Queries::BASIC_ENTITY['CURVAS_GASTO']["lastId"];
			$query = $this->db->query($sql);
			$cod_curva =  $query->result()[0]->id;            

            foreach ($values as $item) { 
                $params = array();

                $params['cod_curva']['value'] = $cod_curva;
                $params['cod_curva']['type']  = Constants::TYPE_TEXT;       

                $params['nivel']['value'] = $item['nivel'];
                $params['nivel']['type']  = Constants::TYPE_INTEGER;                  

                $params['caudal']['value'] = $item['caudal'];
                $params['caudal']['type']  = Constants::TYPE_INTEGER;      
                
                $query= $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CURVAS_GASTO"]["insertInterpolacion"], $params);
                $this->db->query($query);	                

            }

            return true;	    
        }
        else{
            return $errores;
        }


         
    }          
}
?>