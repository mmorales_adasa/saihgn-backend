<?php
class DatosBrutosDao extends CI_Model 
{
    function applyFiltersFindDataDatosBrutos($sql, $ts_col,  $filters){
        $filters2  = array();		
        $date_from;
        $date_to;
        $and = true;
        foreach ($filters as $filter) {    
            //if ($filter->getColumn() != 'cod_variable'){ 
            if ($filter->getColumn() != 'date_from' && $filter->getColumn() != 'date_to'){            
                $f = $filter->clone();
                array_push($filters2,$f);                               
            }
            if ($filter->getColumn() == 'date_from'){
                $date_from = $filter;
            }      
            if ($filter->getColumn() == 'date_to'){
                $date_to = $filter;
            }   
        }   
        $sql.= $this->utildao->applyFilters($filters2);

        if (isset($date_from) && isset($date_from->getValues()[0])){
            $date_from->setColumn($ts_col);
            $sql.= $this->utildao->applyFiltersDate($date_from, $and,'>=');
        }
        if (isset($date_to) && isset($date_to->getValues()[0])){
            $date_to->setColumn($ts_col);
            $sql.= $this->utildao->applyFiltersDate($date_to, $and, '<');
        }           

        return $sql;
    }
    function applyFiltersFindVariablesDatosBrutos($sql, $ts_col, $filters, $tipoInvalidez){
        $filters2  = array();		
        $and = true;
        $invalidos  = true;
        $date_from;
        $date_to;
        foreach ($filters as $filter) {    
            if ($filter->getColumn() != 'invalidas' && $filter->getColumn() != 'conditions'&& $filter->getColumn() != 'date_from' && $filter->getColumn() != 'date_to'){            
                $f = $filter->clone();
                array_push($filters2,$f);   
            }
            if ($filter->getColumn() == 'conditions'){
                if ($filter->getValues()[0]==2){                    
                    $and=false;
                }
            }
            if ($filter->getColumn() == 'invalidas'){
                $invalidas = $filter->getValues()[0];
            }
            if ($filter->getColumn() == 'date_from'){
                $date_from = $filter;
            }      
            if ($filter->getColumn() == 'date_to'){
                $date_to = $filter;
            }                    
        }       
        
        $sql.= $this->utildao->applyFilters($filters2);
        if (isset($date_from)){
            $date_from->setColumn($ts_col);
            $sql.= $this->utildao->applyFiltersDate($date_from, $and,'>=');
        }
        if (isset($date_to)){
            $date_to->setColumn($ts_col);
            $sql.= $this->utildao->applyFiltersDate($date_to, $and, '<');
        }                
        if (isset($invalidas) && $invalidas==1){
            if ($tipoInvalidez==1){
                //$sql .= $this->utildao->getLogica($and) . ' cod_motivo_invalidacion is not null';
                $sql .= $this->utildao->getLogica($and) . " ind_validacion='S'";
                
            }
            else{
                $sql .= $this->utildao->getLogica($and) . ' validez>1 and validez is not null  ';
            }
        }       

        return $sql; 

    }
    function findVariablesPorRed($filters, $red){
        if ($red=='PZ')
        {
            $sql = Queries::BASIC_ENTITY["DATOS_BRUTOS_27"]["findVariablesRedPiezo"];
            $sql = $this->applyFiltersFindVariablesDatosBrutos($sql, 'timestamp', $filters, 2);
            //$sql .= " AND tipo='" . $red . "'";
            
            
            $sql .= " group by T1.cod_variable, T1.descripcion, T2.cod_estacion,  T2.municipio";
            $sql.= $this->utildao->applySort("cod_estacion asc");            
        }
        else {
            $sql = Queries::BASIC_ENTITY["DATOS_BRUTOS_27"]["findVariablesRed"];
            $sql = $this->applyFiltersFindVariablesDatosBrutos($sql, 'timestamp', $filters, 2);
            $sql .= " AND tipo='" . $red . "'";
            $sql .= " group by T1.cod_variable, T1.descripcion, T2.cod_estacion,  T2.nombre";
            $sql.= $this->utildao->applySort("cod_estacion asc");            
        }
		$sql.= $this->utildao->applyLimit(1000, 0);	        
        $sql = str_replace('<<table>>', $this->getTableDatosRed($red), $sql);
        $variables  = array();
        //$db = $this->load->database('visor', TRUE);
        //$query = $db->query($sql);        
        $query = $this->db->query($sql);
        foreach ($query->result() as $row){
            
            $variable = new VariableModel();
            $variable->setCod($row->cod_variable);
            $variable->setDesc($row->descripcion);
            $variable->setDataInvalids($row->has_invalids> 0 ? true : false);
            $estacion = new EstacionModel();
            $estacion->setCod($row->cod_estacion);
            $estacion->setDesc($row->nombre);            
            $variable->setEstacion($estacion);
            array_push($variables,$variable);    
        }        
        return $variables;	        
    }
	function findVariables($filters ){			        
        $sql = Queries::BASIC_ENTITY["DATOS_BRUTOS"]["findVariables"];
        $sql = $this->applyFiltersFindVariablesDatosBrutos($sql, 'timestamp_medicion', $filters, 1);
        $sql .= " group by T1.cod_variable, T1.desc_variable, T1.cod_estacion,  T1.cod_variable, T1.desc_variable, T1.cod_estacion,  T2.desc_estacion";
		$sql.= $this->utildao->applySort("cod_estacion asc");
		$sql.= $this->utildao->applyLimit(1000, 0);	        
        $variables  = array();
        $query = $this->db->query($sql);      
          
        foreach ($query->result() as $row){
            
            $variable = new VariableModel();
            $variable->setCod($row->cod_variable);
            $variable->setDesc($row->desc_variable);
            $variable->setDataInvalids($row->has_invalids>0 ? true : false);
            $estacion = new EstacionModel();
            $estacion->setCod($row->cod_estacion);
            $estacion->setDesc($row->desc_estacion);            
            $variable->setEstacion($estacion);
            array_push($variables,$variable);    
        }        
        return $variables;	
    }     
          
    function createData($data){
        
        $params = array();
        
        
        $params['cod_estacion']['value'] = $data['cod_variable'];
        $params['cod_estacion']['type']  = Constants::TYPE_TEXT;
        
        $params['cod_variable']['value'] = $data['cod_variable'];
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        
        $params['timestamp_medicion']['value'] = $data['fullDate'];
        $params['timestamp_medicion']['type']  = Constants::TYPE_TEXT;
        
        $params['anyo']['value'] = $data['anyo'];
        $params['anyo']['type']  = Constants::TYPE_INTEGER;
        
        $params['mes']['value'] = $data['mes'];
        $params['mes']['type']  = Constants::TYPE_INTEGER;
        
        $params['dia']['value'] = $data['dia'];
        $params['dia']['type']  = Constants::TYPE_INTEGER;

        $params['ind_validacion']['value'] = $data['ind_validacion'];
        $params['ind_validacion']['type']  = Constants::TYPE_TEXT;        

        $params['cod_motivo_invalidacion']['value'] = $data['cod_motivo_invalidacion'];
        $params['cod_motivo_invalidacion']['type']  = Constants::TYPE_TEXT;

        $params['valor']['value'] = $data['value'];
        $params['valor']['type']  = Constants::TYPE_INTEGER;


        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["insert"], $params);                
        $query = $this->db->query($sql);

        return true;
    }
    function saveData($data){
        

         
        $i=0;
        $vars_derivadas;
        foreach ($data as $item) {         
            $params = array();
            $params['cod_variable']['value'] = $item['cod_variable'];
            $params['cod_variable']['type']  = Constants::TYPE_TEXT;

            $params['ind_validacion']['value'] = $item['valido'];
            $params['ind_validacion']['type']  = Constants::TYPE_TEXT;

            $params['cod_motivo_invalidacion']['value'] = $item['motivoInvalidacion']['cod'];
            $params['cod_motivo_invalidacion']['type']  = Constants::TYPE_TEXT;

            $params['timestamp_medicion']['value'] = $item['date'];
            $params['timestamp_medicion']['type']  = Constants::TYPE_TEXT;

            // Modificación de datos
            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["update"], $params);                
            $query = $this->db->query($sql);            
           // Marcado de sumarización
            $params['data_sumarizar']['value'] = date("Y-m-d", strtotime($item['date'])) . " 00:00:00" ;
            $params['data_sumarizar']['type']  = Constants::TYPE_TEXT;
            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["markSumarizacion"], $params);                
            $query = $this->db->query($sql);            
            // Obtengo las derivadas solo la primera vez
            if ($i==0){
                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["VAR_DERIVADAS_Y_DEPENDIENTES"]["get"], $params); 
                $query = $this->db->query($sql);   
                $vars_derivadas =  $query->result();    
            }
            // Marcamos las derivadas
            foreach ($vars_derivadas as $row){
                $params['cod_variable']['value'] = $row->cod_variable_destino;                
                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["markSumarizacion"], $params);                
                $query = $this->db->query($sql);
            }
            $i++;

        }
        return true;
    }
    function saveDataPorRed($data, $red){
        
        foreach ($data as $item) {             
            $params = array();
            $params['cod_variable']['value'] = $item['cod_variable'];
            $params['cod_variable']['type']  = Constants::TYPE_TEXT;

            //$params['validez']['value'] = $item['valido'];
            //$params['validez']['type']  = Constants::TYPE_INTEGER;

            $params['cod_motivo_invalidacion']['value'] = $item['motivoInvalidacion']['cod'];
            $params['cod_motivo_invalidacion']['type']  = Constants::TYPE_TEXT;

            $params['timestamp']['value'] = $item['date'];
            $params['timestamp']['type']  = Constants::TYPE_TEXT;


            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS_27"]["update"], $params);             
            $sql = str_replace('<<table>>', $this->getTableDatosRed($red), $sql);  
            //$db = $this->load->database('visor', TRUE);
            //$query = $db->query($sql);
            $query = $this->db->query($sql);   
                 
        }
        return true;
    }

	function findData($filters)
	{	
        $sql = Queries::BASIC_ENTITY["DATOS_BRUTOS"]["findData"];
        $sql = $this->applyFiltersFindDataDatosBrutos($sql, 'timestamp_medicion', $filters);
        $sql.= $this->utildao->applySort("timestamp_medicion asc");
        $query = $this->db->query($sql);        
        $datos  = array();			        
        foreach ($query->result() as $row){
            $dato = new DatoBrutoModel();
            $dato->setCodVariable($row->cod_variable);
            $dato->setValue($row->valor);
            $motivo = new MotivoInvalidacionModel();
            $motivo->setCod($row->cod_motivo_invalidacion); 
            $motivo->setDesc($row->desc_motivo_invalidacion);            
            $dato->setValido($row->ind_validacion);
            $dato->setMotivoInvalidacion($motivo);
            $date = $row->timestamp_medicion;//;date("Y-m-d H:i:s", strtotime('+'. $k.'hours'));
            $dato->setDate($date);
            array_push($datos,$dato);    
        }               
        return $datos;	
    }  
	function findDataPorRed($filters, $red)
	{	
        $sql = Queries::BASIC_ENTITY["DATOS_BRUTOS_27"]["findDataRed"];
        $sql   = $this->applyFiltersFindDataDatosBrutos($sql, 'timestamp', $filters);        
        $sql.= $this->utildao->applySort("timestamp asc");

        
        $sql = str_replace('<<table>>', $this->getTableDatosRed($red), $sql);
        //$db = $this->load->database('visor', TRUE);
        //$query = $db->query($sql);        
        $query = $this->db->query($sql);
        $datos  = array();			        
        foreach ($query->result() as $row){
            $dato = new DatoBrutoModel();
            $dato->setCodVariable($row->cod_variable);
            $dato->setValue($row->valor);
            $motivo = new MotivoInvalidacionModel();
            $motivo->setCod($row->cod_motivo_invalidacion); 
            $motivo->setDesc($row->desc_motivo_invalidacion);            
            $dato->setValido($row->ind_validacion);
            $dato->setMotivoInvalidacion($motivo);
            $date = $row->timestamp;
            $dato->setDate($date);
            array_push($datos,$dato);    
        }               
        return $datos;	
    }      
    /********************************************* */
    /****************AQUADAM********************** */
    /********************************************* */      
    function saveDataAquadamMasivo($criteria, $data){        
        $params = array();
        
        $params['val_temperatura']['value'] = $data['temperatura' ];
        $params['val_temperatura']['type']  = Constants::TYPE_INTEGER;
        $params['val_ph']['value'] = $data['ph' ];
        $params['val_ph']['type']  = Constants::TYPE_INTEGER;
        $params['val_oxigeno']['value'] = $data['oxigeno' ];
        $params['val_oxigeno']['type']  = Constants::TYPE_INTEGER;
        $params['val_redox']['value'] = $data['redox' ];
        $params['val_redox']['type']  = Constants::TYPE_INTEGER;
        $params['val_turbidez']['value'] = $data['turbidez' ];
        $params['val_turbidez']['type']  = Constants::TYPE_INTEGER;
        $params['val_secchi']['value'] = $data['secchi' ];
        $params['val_secchi']['type']  = Constants::TYPE_INTEGER;
        $params['val_clorofila1']['value'] = $data['clorofila' ];
        $params['val_clorofila1']['type']  = Constants::TYPE_INTEGER;
        $params['val_ficocianina']['value'] = $data['ficocianina' ];
        $params['val_ficocianina']['type']  = Constants::TYPE_INTEGER;        
        $params['val_conductividad']['value'] = $data['conductividad' ];
        $params['val_conductividad']['type']  = Constants::TYPE_INTEGER;   
        

        $where = " WHERE 1=1 " ;
        $where = $this->utildao->applyFilters($criteria);        
        $sql = Queries::BASIC_ENTITY["DATOS_SONDAS_AQUADAM_27"]["updateMasivoPartial"] . $where;        
        $sql = Queries::BASIC_ENTITY["DATOS_SONDAS_AQUADAM_27"]["updateMasivoPartial2"] . " WHERE 1=1 and (COD_SONDA, TIMESTAMP_SONDEO, NUM_LECTURA) IN (" . $sql . ")";
        $sql = $this->utildao->bindingParameters($sql, $params);     
        
        log_message('info', $sql);  
        
        $query = $this->db->query($sql);
        return true;             
        
    }

    function saveDataAquadam($data){        
        //$db = $this->load->database('visor', TRUE);
        $params = array();
            $params['cod_sonda']['value'] = $data['cod_sonda'];
            $params['cod_sonda']['type']  = Constants::TYPE_TEXT;
            
            $params['timestamp_sondeo']['value'] = $data['timestamp_sondeo'];
            $params['timestamp_sondeo']['type']  = Constants::TYPE_TEXT;

            $params['val_temperatura']['value'] = $data['temperatura' ];
            $params['val_temperatura']['type']  = Constants::TYPE_INTEGER;
            $params['val_ph']['value'] = $data['ph' ];
            $params['val_ph']['type']  = Constants::TYPE_INTEGER;
            $params['val_oxigeno']['value'] = $data['oxigeno' ];
            $params['val_oxigeno']['type']  = Constants::TYPE_INTEGER;
            $params['val_redox']['value'] = $data['redox' ];
            $params['val_redox']['type']  = Constants::TYPE_INTEGER;
            $params['val_turbidez']['value'] = $data['turbidez' ];
            $params['val_turbidez']['type']  = Constants::TYPE_INTEGER;
            $params['val_secchi']['value'] = $data['secchi' ];
            $params['val_secchi']['type']  = Constants::TYPE_INTEGER;
            $params['val_clorofila1']['value'] = $data['clorofila' ];
            $params['val_clorofila1']['type']  = Constants::TYPE_INTEGER;
            $params['val_ficocianina']['value'] = $data['ficocianina' ];
            $params['val_ficocianina']['type']  = Constants::TYPE_INTEGER;  
            $params['val_conductividad']['value'] = $data['conductividad' ];
            $params['val_conductividad']['type']  = Constants::TYPE_INTEGER;                         

            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_SONDAS_AQUADAM_27"]["update"], $params);                
            //$db = $this->load->database('visor', TRUE);
            //$query = $db->query($sql);   
            log_message('info', $sql); 
                
            $query = $this->db->query($sql);            
            return true ;
        
    }

    function getReferenceDays($data){                    
            $params = array();
            $params['cod_sonda']['value'] = $data['cod_sonda'];
            $params['cod_sonda']['type']  = Constants::TYPE_TEXT;            
            $params['timestamp_sondeo']['value'] = $data['timestamp_sondeo'];
            $params['timestamp_sondeo']['type']  = Constants::TYPE_TEXT;
            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_SONDAS_AQUADAM_27"]["getReferenceDays"], $params);                
            //$db = $this->load->database('visor', TRUE);
            //$query = $db->query($sql);               
            $query = $this->db->query($sql);            
            return $query->result()[0];
    }

    function findDataAquadam($filters)
    {	
        $sql = Queries::BASIC_ENTITY["DATOS_SONDAS_AQUADAM_27"]["detailMuestra"];
        $sql.= $this->utildao->applyFilters($filters);
        $sql.= $this->utildao->applySort("timestamp_sondeo asc");		        
        //$db = $this->load->database('visor', TRUE);
        //$query = $db->query($sql);      
        $query = $this->db->query($sql);            
		return $query->result();	        
    }

    function getDatosNivel($data){        
        
        $params = array();
        $cod_embalse = trim(str_replace('S','E',$data['cod_sonda']));
        $params['cod_nivel']['value'] = $cod_embalse . '/NMN';
        $params['cod_nivel']['type']  = Constants::TYPE_TEXT;            
        $params['cod_variable']['value'] = $cod_embalse . '/VE1';
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;            
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_SONDAS_AQUADAM_27"]["getDatosNivel"], $params);                
        //$db = $this->load->database('visor', TRUE);
        //$query = $db->query($sql);                    
        $query = $this->db->query($sql);
        return $query->result()[0];
}    
function getDatosCotas($data){        
        
    $params = array();
    $cod_embalse = trim(str_replace('S','E',$data['cod_sonda']));
    $params['cod_nivel']['value'] = '%'. $cod_embalse . '%';
    $params['cod_nivel']['type']  = Constants::TYPE_TEXT;            


    $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_SONDAS_AQUADAM_27"]["getDatosCotas"], $params);                
    //$db = $this->load->database('visor', TRUE);
    //$query = $db->query($sql);                    
    $query = $this->db->query($sql);
    return $query->result();
}    



function getTableDatosRed($red){        
    if ($red=='ECC'){
        return ' tbl_dato_reacar ';
    }
    else if ($red=='E'){
        return ' tbl_dato_auscultacion ';
    }
    else if ($red=='CR'){
        return ' tbl_dato_medido ';
    }
    else if ($red=='EAA'){
        return ' tbl_dato_saica ';
    }
    else if ($red=='PZ'){
        return ' tbl_dato_piezo ';
    }
    }
    
    function fillHoles($frecuencia, $fechaIni, $fechaFin, $codVariable){ 
        log_message('debug', 'frecuencia '. $frecuencia);
        log_message('debug', 'fechaIni '. $fechaIni);
        log_message('debug', 'fechaFin '. $fechaFin);

        // Miro si tiene derivadas
        $params = array();    
        $params['cod_variable']['value'] = trim($codVariable);
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;                        
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["VAR_DERIVADAS_Y_DEPENDIENTES"]["get"], $params); 
        $query = $this->db->query($sql);   
        $vars_derivadas_calculadas =  $query->result();        
        
	
        $interval = $frecuencia*60; // Interval in seconds
        $time_first     = strtotime($fechaIni);
        $time_second    = strtotime($fechaFin);
        $holes = array();
        $startHole = null;
        $filled = 0;
        for ($i = $time_first; $i < $time_second; $i += $interval){
            $fecha  = date('Y-m-d H:i', $i);
            $fechaSinHora  = date('Y-m-d', $i);
            if (!$this->existDatoBruto($fecha,$codVariable)){
                // Completamos dato
                $params['cod_variable']['value'] = trim($codVariable);
                $params['cod_variable']['type']  = Constants::TYPE_TEXT;                                
                $params['timestamp_medicion']['value'] =  $fecha;
                $params['timestamp_medicion']['type']  = Constants::TYPE_TEXT;    
                $params['valor']['value'] = -9999;
                $params['valor']['type']  = Constants::TYPE_INTEGER;                  
                $params['ind_validacion']['value'] =  'N';
                $params['ind_validacion']['type']  = Constants::TYPE_TEXT;                
                $params['cod_mod_fuente_origen']['value'] =  'M1';
                $params['cod_mod_fuente_origen']['type']  = Constants::TYPE_TEXT;                                 

                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["insert"], $params);
                $query = $this->db->query($sql);
                
                $filled++;
                // Marcamos para sumarizar
                $params['data_sumarizar']['value'] = $fechaSinHora . " 00:00:00" ;
                $params['data_sumarizar']['type']  = Constants::TYPE_TEXT;
                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["markSumarizacion"], $params);    
                $query = $this->db->query($sql);  
                
                
                foreach ($vars_derivadas_calculadas as $row){
                    if ($row->tipo==1){
                        // DERIVADAS
                        $params['cod_variable']['value'] = $row->cod_variable_destino;                
                        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["markSumarizacion"], $params);                
                        $query = $this->db->query($sql);
                    }
                }                
            }  
        }

        foreach ($vars_derivadas_calculadas as $row){
            if ($row->tipo==2){
                log_message('debug','RECALCULO CURVAS DE GASTO ' . $row->cod_variable_origen . ' -- ' . $row->cod_variable_destino . '  ' .  $row->cod_estacion);
                $params = array();
                $params['cod_curva']['value'] = $row->cod_curva;
                $params['cod_curva']['type']  = Constants::TYPE_INTEGER;
                $params['cod_estacion']['value'] = $row->cod_estacion;
                $params['cod_estacion']['type']  = Constants::TYPE_TEXT;
                $params['cod_variable_caudal']['value'] = $row->cod_variable_destino;
                $params['cod_variable_caudal']['type']  = Constants::TYPE_TEXT;
                $params['cod_variable_nivel']['value'] = $row->cod_variable_origen;
                $params['cod_variable_nivel']['type']  = Constants::TYPE_TEXT;
                $params['data_inicial']['value'] = $fechaIni;
                $params['data_inicial']['type']  = Constants::TYPE_TEXT;
                $params['data_final']['value'] =  $fechaFin;
                $params['data_final']['type']  = Constants::TYPE_TEXT;                        
                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CURVAS_GASTO"]["calcula"], $params);                                
                $query = $this->db->query($sql);              
               
            }
        }

        $output['filled'] = $filled;        
        return $output;
    }

    function importData($conf, $vars, $datos){       
        log_message('debug', 'Estrategia '. $conf['estrategia']);

        $output =  [];
        $output['errors'] = [];
        $errors  = array();	
        // Validación de las variables
        foreach ($vars as $var) { 
            log_message('debug', "Itero " . $var);
            $params = array();         
            $params['cod_variable']['value'] = trim($var);
            $params['cod_variable']['type']  = Constants::TYPE_TEXT;            
            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["validateVariable"], $params);                               
            $query = $this->db->query($sql);
            if (count($query->result())==0){
                $msg= "Variable " . $var . " no permitida su modificación ";
                array_push($errors,$msg);    
            }



        }
        if (count($errors)>0){
            $output['errors'] = $errors;           
        }
        else{            
            if ($conf['includeValidacion']==1){
                $i = 1;
                $inserts = 0;
                $updates = 0;
                foreach ($vars as $var) {
                    foreach ($datos as $dato) { 
                        if (trim($dato[$i])!= ""){ // Excluimos si no han introducido dato
                            if (!$this->existDatoBruto($dato[0],$var)){
                                $inserts++;
                            }
                            else{
                                // Inserts && Updates
                                if ($conf['estrategia']=="SC"){
                                    $updates++;
                                }
                            }    
                        }          
                    }
                    $i++;
                }
                $output['inserts'] = $inserts;
                if ($conf['estrategia']=="SC"){     
                    $output['updates'] = $updates;
                }
                else{
                    $output['updates'] = 0;
                }
            }
            // Calculamos intervalo de fecha del fichero para luego lanzar el recálculo de curvas si procede
            $fechaMinRecalculo;
            $fechaMaxRecalculo;
            foreach ($datos as $dato) { 
                $fecha = trim($dato[0]);
                $fecha = strtotime(str_replace('/', '-', $fecha));
                if (!isset($fechaMinRecalculo)){
                    $fechaMinRecalculo = $fecha;
                }                
                if (!isset($fechaMaxRecalculo)){
                    $fechaMaxRecalculo = $fecha;
                }  
                if ($fecha<$fechaMinRecalculo){
                    $fechaMinRecalculo=$fecha;
                }
                if ($fecha>$fechaMaxRecalculo){
                    $fechaMaxRecalculo=$fecha;
                }
            }
            
            if ($conf['includeExecution']==1){

                $i = 1;
                foreach ($vars as $var) {
                    $params = array();    
                    $params['cod_variable']['value'] = trim($var);
                    $params['cod_variable']['type']  = Constants::TYPE_TEXT;                        
                    $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["VAR_DERIVADAS_Y_DEPENDIENTES"]["get"], $params); 
                    $query = $this->db->query($sql);   
                    $vars_derivadas_calculadas =  $query->result(); 
                    foreach ($datos as $dato) { 
                        if (trim($dato[$i])!= ""){
                            //Modificación de datos
                            $params['cod_variable']['value'] = trim($var);
                            $params['cod_variable']['type']  = Constants::TYPE_TEXT;                                
                            $params['timestamp_medicion']['value'] =  trim($dato[0]);
                            $params['timestamp_medicion']['type']  = Constants::TYPE_TEXT;    
                            $params['valor']['value'] = trim($dato[$i]);
                            $params['valor']['type']  = Constants::TYPE_INTEGER; 

                            $params['ind_validacion']['value'] =  'S';
                            $params['ind_validacion']['type']  = Constants::TYPE_TEXT;                
                            $params['cod_mod_fuente_origen']['value'] =  'M1';
                            $params['cod_mod_fuente_origen']['type']  = Constants::TYPE_TEXT;                              
                            

                            // ESTRATEGIA: Inserts && Updates
                            $affected = false;
                            if ($conf['estrategia']=="SC"){                            
                                $affected = true;
                                // Insert en el log (antes para tener el valor anterior)
                                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["insertLog"], $params);
                                $query = $this->db->query($sql);                                  
                                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["inserOrUpdate"], $params);
                                $query = $this->db->query($sql);  

                            }else{
                                // Si no existe lo creo, si existe no hago nada
                                if (!$this->existDatoBruto($dato[0],$var)){
                                    log_message('debug', 'No existe, inserto para ' .  $dato[0] . ' y variable '. $params['cod_variable']['value']);
                                    // Insert en el log
                                    $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["insertLog"], $params);
                                    $query = $this->db->query($sql);  
                                    //log_message('debug', $sql); 

                                    $affected = true;
                                    $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["insert"], $params);
                                   $query = $this->db->query($sql);  
                                }
                            }
                            if ($affected){
                                // Marcado de sumarización
                                $partesFecha = explode(" ", trim($dato[0]));
                                $params['data_sumarizar']['value'] = trim($partesFecha[0]). " 00:00:00" ;
                                $params['data_sumarizar']['type']  = Constants::TYPE_TEXT;
                                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["markSumarizacion"], $params);    
                                $query = $this->db->query($sql);       
                                     
                                // Marcamos las derivadas
                                foreach ($vars_derivadas_calculadas as $row){
                                    if ($row->tipo==1){
                                        // DERIVADAS
                                        $params['cod_variable']['value'] = $row->cod_variable_destino;                
                                        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["markSumarizacion"], $params);                
                                        $query = $this->db->query($sql);
                                    }
                                }
                            }
                        }
                    }
                 // Recálculo de curvas de gasto par atodo el intervalo
                    foreach ($vars_derivadas_calculadas as $row){
                        if ($row->tipo==2){
                            // DERIVADAS
                            log_message('debug','RECALCULO CURVAS DE GASTO ' . $row->cod_variable_origen . ' -- ' . $row->cod_variable_destino . '  ' .  $row->cod_estacion);
                            $params = array();
                            $params['cod_curva']['value'] = $row->cod_curva;
                            $params['cod_curva']['type']  = Constants::TYPE_INTEGER;
                            $params['cod_estacion']['value'] = $row->cod_estacion;
                            $params['cod_estacion']['type']  = Constants::TYPE_TEXT;
                            $params['cod_variable_caudal']['value'] = $row->cod_variable_destino;
                            $params['cod_variable_caudal']['type']  = Constants::TYPE_TEXT;
                            $params['cod_variable_nivel']['value'] = $row->cod_variable_origen;
                            $params['cod_variable_nivel']['type']  = Constants::TYPE_TEXT;
                            $params['data_inicial']['value'] = date("d/m/Y H:i:s", $fechaMinRecalculo);
                            $params['data_inicial']['type']  = Constants::TYPE_TEXT;
                            $params['data_final']['value'] = date("d/m/Y H:i:s", $fechaMaxRecalculo);
                            $params['data_final']['type']  = Constants::TYPE_TEXT;                        
                            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CURVAS_GASTO"]["calcula"], $params);                
                            $query = $this->db->query($sql);                        
                        }
                    }
                    $i++;        
                }
            }
        }
        return $output;
     }
     function existDatoBruto($timestamp_medicion, $cod_variable){       
        $params = array();         
        $params['cod_variable']['value'] = trim($cod_variable);
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;   

        $params['timestamp_medicion']['value'] =  trim($timestamp_medicion);
        $params['timestamp_medicion']['type']  = Constants::TYPE_TEXT;                

        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["checkExistenceDato"], $params);                               
        $query = $this->db->query($sql); 
        return count($query->result())>0;

     }
     function showHistory($cod_variable, $timestamp_medicion){   
        $params = array();
        
        $params['cod_variable']['value'] = $cod_variable;
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $params['timestamp_medicion']['value'] =  $timestamp_medicion;
        $params['timestamp_medicion']['type']  = Constants::TYPE_TEXT;                          
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["IMPORT_MINUTAL"]["showHistory"], $params);                
        $query = $this->db->query($sql);    
        return $query->result();                   
    }

    function forceSumarizacion( $fechaIni, $fechaFin, $cod_variable){ 
        log_message('debug', 'codVariable '. $cod_variable);
        log_message('debug', 'fechaIni '. $fechaIni);
        log_message('debug', 'fechaFin '. $fechaFin);

        $interval = 86400; // Interval in seconds
        
        $time_first     = strtotime($fechaIni);
        $time_second    = strtotime($fechaFin);
        $filled = 0;
        for ($i = $time_first; $i <= $time_second; $i += $interval){
            $fecha  = date('Y-m-d H:i', $i);
            $fechaSinHora  = date('Y-m-d', $i);
            $params = array();
            $params['cod_variable']['value'] = $cod_variable;
            $params['cod_variable']['type']  = Constants::TYPE_TEXT;
            $params['data_sumarizar']['value'] =  $fechaSinHora;
            $params['data_sumarizar']['type']  = Constants::TYPE_TEXT; 
            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_BRUTOS"]["markSumarizacion"], $params);                
            //log_message('debug', $sql);
            $query = $this->db->query($sql);                            
        }

        return true;
    }
}
?>