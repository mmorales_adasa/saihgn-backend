<?php
class FormulaDao extends CI_Model 
{
    public  $nodos = array();

	function getNextValueSequence()
	{				
		$sql = Queries::BASIC_ENTITY["VARIABLES"]["nexId"];
		$query = $this->db->query($sql);
        $id='';
        foreach ($query->result() as $row){                        
            $id=$row->id;
        }        
        return $id;	
    }           
	function getEstacionesVariablesYConstantes($text)
	{				
		$sql = Queries::BASIC_ENTITY["FORMULAS"]["getEstacionesVariablesConstantes"];
        if (isset($text) && $text!= ""){
            //$sql .= " WHERE UPPER(code)      like '%"  . trim(strtoupper($text)) . "%'";
            //$sql .= " OR UPPER(description)  like '%"  . trim(strtoupper($text)) . "%'";
            $sql .= " WHERE UPPER(cod_estacion) like '%"  . trim(strtoupper($text)) . "%'";
            $sql .= " OR UPPER(desc_estacion) like '%" . trim(strtoupper($text)) . "%'";
        }
        $sql .= 'ORDER BY cod_estacion LIMIT 1000 OFFSET 0';
        //echo $sql;
        $query = $this->db->query($sql);
        $variables  = array();
        foreach ($query->result() as $row){
            
            $variable = new VariableModel();
            $variable->setCod($row->code);
            $variable->setDesc($row->description);
            $variable->setType($row->type);
            $estacion = new EstacionModel();
            $estacion->setCod($row->cod_estacion);
            $estacion->setDesc($row->desc_estacion);            
            $variable->setEstacion($estacion);
            array_push($variables,$variable);    
        }        
        return $variables;	
    }    
    function setRecalculo($cod_variable, $fecha_ini, $fecha_fin){
        $data = array(
                'cod_variable'   => $cod_variable,
                'data_ini_calc'  => $fecha_ini,
                'data_fin_calc' => $fecha_fin,
                'ind_calc' => 'S');       
        return $this->db->insert('tbl_gest_formulas_recalc', $data);
    }   
    function checkTokensFormula($operadors){
        $terms  = array();
        foreach ($operadors as $op) {
            $term = new TermFormula();
            $term->setCode($op);
            if ((strpos($op, 'V_') === 0) || (strpos($op, 'F_') === 0)) {
                $cod_variable = str_replace('V_','',$op); 
                $cod_variable = str_replace('F_','',$cod_variable); 
                $params = array();
                $params['cod_variable']['value'] = trim($cod_variable);
                $params['cod_variable']['type'] = Constants::TYPE_TEXT;
                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["VARIABLES"]["detail"], $params);		
                
                $query = $this->db->query($sql);
                if ($query->num_rows() > 0)
                {
                    $desc_variable = $query->result()[0]->desc_variable;
                    $term->setTranslation($desc_variable);
                }
                else{
                    $term->setTranslation(null);
                }
            }
            if ((strpos($op, 'C_') === 0)) {
                $cod_constante = str_replace('C_','',$op); 
                $params = array();
                $params['cod_constante']['value'] = $cod_constante;
                $params['cod_constante']['type'] = Constants::TYPE_TEXT;
                $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CONSTANTES"]["detail"], $params);		
                $query = $this->db->query($sql);
                $desc_constante = $query->result()[0]->desc_constante;
                $val_constante = $query->result()[0]->valor;
                $term->setTranslation($desc_constante);            
                $term->setValue($val_constante);            
            }
            array_push($terms,$term);    

        }     
        return $terms;        
    }
	function getTokensFormula($cod_variable){				
        $params = array();
        $params['cod_variable']['value'] = $cod_variable;
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["FORMULAS"]["getTokensFormula"], $params);        
        
        $query = $this->db->query($sql);
        return $query->result();	     
    }   
	function obtenerNodoPorCodVariable($cod_variable){				
        log_message('debug', "***** JMR obtenerNodoPorCodVariable cod_variable: " . $cod_variable);
        $params = array();
        $params['cod_variable']['value'] = $cod_variable;
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["FORMULAS"]["obtenerNodoPorCodVariable"], $params);                
        $query = $this->db->query($sql);
        if (count($query->result())==1){
            log_message('debug', "***** JMR obtenerNodoPorCodVariable OK!");
            $node = $query->result()[0];
            $node->visitat = false; 
            return $node;
        }else{
            return null;
        }
    } 
	function obtenerHijosPorNodo($node){				
        $params = array();
        $params['cod_variable']['value'] = $node->cod_variable;
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["FORMULAS"]["obtenerHijosPorNodo"], $params);                
        $query = $this->db->query($sql);
        return $query->result();	     
    }  
	function obtenerPadresPorNodo($node){				
        $params = array();
        $params['cod_variable']['value'] = $node->cod_variable;
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["FORMULAS"]["obtenerPadresPorNodo"], $params);                
        $query = $this->db->query($sql);
        return $query->result();	     
    }        
	function updateNivellCalculNode($node){				
        $params = array();
        $nivel_calculo = $node->nivel_calculo;
        if ($node->nivel_calculo == 0){
            $nivel_calculo=null;
        }                
        $params['cod_variable']['value']    = $node->cod_variable;
        $params['cod_variable']['type']     = Constants::TYPE_TEXT;
        $params['nivel_calculo']['type']  = Constants::TYPE_INTEGER;    
        $params['nivel_calculo']['value'] = $nivel_calculo;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["FORMULAS"]["updateNivellCalculNode"], $params);                
        $query = $this->db->query($sql);
        log_message('debug', "***** JMR updateNivellCalculNode :" . $node->cod_variable . " nivel " .  $nivel_calculo);
        return true;	     
    }      
    function afegir_a_cache_nodos($node){
        log_message('debug', "*****kk: " . $node->cod_variable);

        if (isset($node)){
            log_message('debug', "***** JMR afegir_a_cache_nodos: " . $node->cod_variable);
            $this->nodos[$node->cod_variable] = $node;        
        }
    }    
	function upgradeTokensFormula($cod_variable, $tokens){				


        $cap = new Node();
        $cap->cod_variable = $cod_variable;
        $fills_anteriors = $this->getFills($cap);
        log_message('debug', "***** JMR fills_anteriors: " . count($fills_anteriors));

        $params = array();
        $params['cod_variable']['value'] = $cod_variable;
        $params['cod_variable']['type']  = Constants::TYPE_TEXT;
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["FORMULAS"]["deleteTokensFormula"], $params);        
        $query = $this->db->query($sql);

        $this->tractarVarsAillades ($fills_anteriors);


        foreach ($tokens as $token) {            
            if ($token->cod_tipo_termino=='VAR'){                
                $node = $this->obtenerNodoPorCodVariable($token->variable);                
                log_message('debug', "***** JMR Nodo obtenido: " . $node->cod_variable);
                if ($node->nivel_calculo == 0){                
                    $node->nivel_calculo = 1;
                    $this->updateNivellCalculNode($node);
                    $this->afegir_a_cache_nodos($node);
                }                                           
            }
            if ($token->cod_tipo_termino=='VARC'){ 
                $node = $this->obtenerNodoPorCodVariable($token->calculada);
                log_message('debug', "***** JMR Nodo obtenido: " . $token->calculada);

            }
            $data = array(
                'cod_variable'      => $cod_variable,
                'num_termino'       => $token->num_termino,
                'cod_tipo_termino'  => $token->cod_tipo_termino,
                'tipo_valor'        => $token->tipo_valor,
                'constante'         => $token->constante,
                'constante_num'     => $token->constante_num,
                'variable'          => $token->variable,
                'calculada'         => $token->calculada,
                'operador'          => $token->operador
                );       
            $this->db->insert('tbl_gest_formulas', $data);  
             
        }
         $llista_visitats = array();
         $cicle = $this->canviarNivellsBottomUp($cod_variable, $llista_visitats);
         if (count($cicle) > 0){
            echo "HAY CICLOS!!";
         }        
        return true;
        
    }   
    
    function canviarNivellsBottomUp($cod_node, $llista_visitats){	
        

        $node = new Node();
        
        log_message('debug', "***** JMR canviarNivellsBottomUp Nodo: " . $cod_node);
        log_message('debug', "***** JMR canviarNivellsBottomUp :" . count($llista_visitats) . " visitados");
        $cicle    = array();
        $ll_pares = array();
        $node = $this->obtenir_de_cache_nodos($cod_node);
        if  ($node->visitat)
        {
            log_message('debug', "canviarNivellsBottomUp: " . $cod_node . " ja ha estat visitat");      
            $node->visitat = true;
            $cicle = $this->extreuCicle($node, $llista_visitats);
            return $cicle;
        } else {
            log_message('debug', "canviarNivellsBottomUp: " . $cod_node . " NO ja ha estat visitat");      
        }                
        $nivell_node_actual = $this->maxNivellFillsNode ($node) + 1;
        // En este caso se retorna lista vacia, ya que se supone que no hay cambios de nivel de calculo a relizar
        if  ($node->nivel_calculo == $nivell_node_actual){
            log_message('debug', "no hay cambios de nivel de calculo a relizar. Nivel actual : " . $nivell_node_actual);      
            return $cicle;
        }        
        $node->nivel_calculo = $nivell_node_actual;
        $this->updateNivellCalculNode ($node);
        $node->visitat = true;
        $this->afegir_a_cache_nodos($node);
        array_push($llista_visitats,$node);

        $ll_pares = $this->getPares($node);
        for ($i = 0; $i < count($ll_pares); $i++){
            $pare;
            $pare = $ll_pares[$i];
            $cicle = $this->canviarNivellsBottomUp ($pare->cod_variable, $llista_visitats);
            if (count($cicle) > 0) 
                return $cicle;
        }
        $node->visitat=false;
        $this->afegir_a_cache_nodos($node);

        // treure ultim de llista_visitats        
        array_pop($llista_visitats);
        return $cicle;        

    }
    function obtenir_de_cache_nodos ($cod_node)	{
    $node;
    if (isset($this->nodos[$cod_node])){
        log_message('debug', "obtenir_de_cache_nodos: " . $cod_node . " apareix a hashmap nodos ");      
        $node = $this->nodos[$cod_node];
    }
    else
    {
        log_message('debug', "obtenir_de_cache_nodos: " . $cod_node . " NO apareix a hashmap nodos ");                              
        $node = $this->obtenerNodoPorCodVariable($cod_node);
    }        
    return $node;
    }    
    // ------------------------------------------------------------------
    //  Funcion que extrae el ciclo de los nodos visitados
    //  para retornarlos
    // ------------------------------------------------------------------    
    function extreuCicle($node, $llista_visitats) {        
        log_message('debug', "***** JMR extreuCicle ");                              
        $cicle = array();        
        array_push($cicle,$node);
        $n = count($llista_visitats) - 1;

        for ($i = n; $i >= 0 ; $i--){
            $elem = $llista_visitats[i];
            array_push($cicle,$elem);
            if ($elem->cod_variable == $node->cod_variable){            // OJO ESTO 
                return $cicle;
            }
        }        
        return $cicle;
    }    
// ------------------------------------------------------------------
//  Llama a getFills para obtener el Maximo Nivel de Calculo de la lista
//  Se proporciona un Nodo
// ------------------------------------------------------------------

function maxNivellFillsNode($n)  {
    
    $max_nivel = 0;
    $nivel = 0;
    $fills = $this->getFills($n);
    for ($i = 0; $i < count($fills); $i++){        
        $nm = $fills[$i];            
        $nivel = $nm->nivel_calculo;
        if ($max_nivel < $nivel){
            $max_nivel = $nivel;
        }    
    }
    log_message('debug', "***** JMR maxNivellFills (NODO) " . $n->cod_variable . " es " . $max_nivel);                              
    return $max_nivel;
}
// ------------------------------------------------------------------
//  Llama a FormulaDAO para obtener una lista de Nodos Hijos
// ------------------------------------------------------------------
function getFills($n) {
    log_message('debug', "***** JMR getFills (NODO) " . $n->cod_variable);                              
    $l = $this->obtenerHijosPorNodo($n);    
    // ponemos en hashmap si no está         
    for ($i = 0; $i < count($l); $i++){
      $nm = $l[$i];            
      log_message('debug', " GetFills: " . $i . " es " . $nm->cod_variable);
      if (!isset($this->nodos[$nm->cod_variable])){                  
        $nm->visitat = false;
        $this->afegir_a_cache_nodos($nm);
      }
    }    
    return $l;
}

function getPares ($n) {    
    $l = $this->obtenerPadresPorNodo($n);
    log_message('debug', "***** JMR getPares " . $n->cod_variable . " -> " . count($l));
    // ponemos en hashmap si no está         
    for ($i = 0; $i < count($l); $i++){
        $nm = $l[$i];
        if (!isset($this->nodos[$nm->cod_variable])){        
            $nm->visitat = false;
            $this->afegir_a_cache_nodos($nm);
          }
    }
    log_message('debug', " GetPares: " . count($l) . " hijos");
    return $l;
}
function  tractarVarsAillades ($ll_fills){
    log_message('debug', "**** JMR tractarVarsAillades " . count($ll_fills));
    for ($i = 0; $i < count($ll_fills); $i++){
        log_message('debug', " llego");

    $nodo = $ll_fills[$i];
    if (isset($nodo)){
    if ($this->getNumPares($nodo) == 0 && $nodo->nivel_calculo == 1){
        $nodo->nivel_calculo = 0;
        $this->updateNivellCalculNode ($nodo);
        $this->afegir_a_cache_nodos (nodo);
    }
}
} 
}
function getNumPares($n) {
    log_message('debug', "**** JMR getNumPares (nodo");
    return count($this->getPares($n));
}

}
?>