<?php
require_once APPPATH . '/libraries/visor/class_data.php';
require_once APPPATH . '/libraries/visor/class_graficas.php';
require_once APPPATH . '/libraries/visor/class_graficas_saica.php';
require_once APPPATH . '/libraries/visor/class_graficas_reacar.php';
require_once APPPATH . '/libraries/visor/class_operaciones.php';
require_once APPPATH . '/libraries/visor/class_esquema_explotacion.php';
require_once APPPATH . '/libraries/visor/class_esquemas.php';
require_once APPPATH . '/libraries/visor/class_conexion.php';
require_once APPPATH . '/libraries/visor/class_video.php';
require_once APPPATH . '/libraries/visor/class_video_reacar.php';
require_once APPPATH . '/libraries/visor/class_cache.php';

class VisorDao extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Insertar en tbl_dato_tr, por ahora limitado a variables PI
	 */
	function setCincoMinutalFromRTAP($DB, $datos) {

		// tiempo sin zona
		date_default_timezone_set("UTC");

		$insert_query = 'INSERT INTO tbl_dato_tr (cod_variable, timestamp, valor) VALUES ';
		$tiempo_actual = strtotime("now");

		foreach($datos as $tipoEstacion => $datosTipoEstacion){
			if(in_array($tipoEstacion,["E","CR","CP","EM","NR"])){
				$query_parts = array();
				$query = "";
				foreach ($datosTipoEstacion as $clave => $valor) {
					if(isset($valor['timestamp']) && isset($valor['valor'])){

						// solo revisar variables PI
						$val = explode('/', $valor['cod_variable']);
						if(count($val) == 2){
							$variable = $val[1];
							if($variable=="PI"){

								$tiempo_dato = strtotime($valor['timestamp']);
								$intervalo  = abs($tiempo_dato - $tiempo_actual);
								$minutos = round($intervalo / 60);

								if (!is_null($valor['timestamp']) && (bool) preg_match('/[A-Za-z]+/', $valor['valor']) == false && is_numeric($valor['valor'])) { // revisar validez del timestamp, detectar errores de dato
									
									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 5||':00')::timestamp(0) ) , '" . round((float)$valor['valor'], 4) . "')";

								}
							}
						}
					}
				}

				if(count($query_parts)>0){
					$query = $insert_query . implode(',', $query_parts) . " ON CONFLICT (cod_variable, timestamp) DO UPDATE SET valor = EXCLUDED.valor";
					ConexionBD::EjecutarConsultaConDB($DB, $query, false);
				}
 
			}
		}

	}

	function setCincoMinutalFromRTAPTest($DB, $datos) {

		// tiempo sin zona
		date_default_timezone_set("UTC");

		$insert_query = 'INSERT INTO prueba_tbl_dato_tr (cod_variable, timestamp, valor, timestamp_original, timestamp_ejecucion, error_no_insercion, motivo_no_insercion) VALUES ';
		$tiempo_actual = strtotime("now");

		foreach($datos as $tipoEstacion => $datosTipoEstacion){
			if(in_array($tipoEstacion,["E","CR","CP","EM","NR"])){
				$query_parts = array();
				$query = "";
				foreach ($datosTipoEstacion as $clave => $valor) {
					if(isset($valor['timestamp']) && isset($valor['valor'])){

						// solo revisar variables PI
						$val = explode('/', $valor['cod_variable']);
						if(count($val) == 2){
							$variable = $val[1];
							if($variable=="PI"){

								$fecha = new DateTime();
								$fecha->setTimestamp($tiempo_actual);
								$tiempo_ejecucion = $fecha->format('Y-m-d H:i:s');

								if (!is_null($valor['timestamp']) && (bool) preg_match('/[A-Za-z]+/', $valor['valor']) == false && is_numeric($valor['valor'])) { // revisar validez del timestamp, detectar errores de dato
									
									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 5||':00')::timestamp(0) ) , '" . round((float)$valor['valor'], 4) . "','".$valor['timestamp']."','".$tiempo_ejecucion."', false, null)";

								} else if(is_null($valor['timestamp'])){

									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 5||':00')::timestamp(0) ) , '" . round((float)$valor['valor'], 4) . "','".$valor['timestamp']."','".$tiempo_ejecucion."', true, 'timestamp nulo')";

								} else if((bool) preg_match('/[A-Za-z]+/', $valor['valor']) == false){

									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 5||':00')::timestamp(0) ) , '" . round((float)$valor['valor'], 4) . "','".$valor['timestamp']."','".$tiempo_ejecucion."', true, 'carácteres inválidos encontrados')";

								} else {

									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 5||':00')::timestamp(0) ) , '" . round((float)$valor['valor'], 4) . "','".$valor['timestamp']."','".$tiempo_ejecucion."', true, 'error desconocido')";

								}
							}
						}
					}
				}

				if(count($query_parts)>0){
					$query = $insert_query . implode(',', $query_parts);
					
					ConexionBD::EjecutarConsultaConDB($DB, $query, false);
					
				}
			}
		}
	}

	function setDiezMinutalFromRTAP($DB, $datos) {

		// tiempo sin zona
		date_default_timezone_set("UTC");

		$insert_query = 'INSERT INTO tbl_dato_tr (cod_variable, timestamp, valor) VALUES ';
		$tiempo_actual = strtotime("now");

		foreach($datos as $tipoEstacion => $datosTipoEstacion){
			if(in_array($tipoEstacion,["E","CR","CP","EM","NR"])){
				$query_parts = array();
				$query = "";
				foreach ($datosTipoEstacion as $clave => $valor) {
					if(isset($valor['timestamp']) && isset($valor['valor'])){

						// solo revisar variables que no sean PI
						$val = explode('/', $valor['cod_variable']);
						if(count($val) == 2){
							$variable = $val[1];
							if($variable!="PI"){

								$tiempo_dato = strtotime($valor['timestamp']);
								$intervalo  = abs($tiempo_dato - $tiempo_actual);
								$minutos = round($intervalo / 60);

								if (!is_null($valor['timestamp']) && (bool) preg_match('/[A-Za-z]+/', $valor['valor']) == false && is_numeric($valor['valor'])) { // revisar validez del timestamp, detectar errores de dato
									
									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 10||':00')::timestamp(0) ) , '" . round((float)$valor['valor'], 4) . "')";

								}
							}
						}
					}
				}

				if(count($query_parts)>0){
					$query = $insert_query . implode(',', $query_parts) . " ON CONFLICT (cod_variable, timestamp) DO UPDATE SET valor = EXCLUDED.valor";
					ConexionBD::EjecutarConsultaConDB($DB, $query, false);
				}
 
			}
		}
	}

	function setDiezMinutalFromRTAPTest($DB, $datos) {

		// tiempo sin zona
		date_default_timezone_set("UTC");

		$insert_query = 'INSERT INTO prueba_tbl_dato_tr (cod_variable, timestamp, valor, timestamp_original, timestamp_ejecucion, error_no_insercion, motivo_no_insercion) VALUES ';
		$tiempo_actual = strtotime("now");

		foreach($datos as $tipoEstacion => $datosTipoEstacion){
			if(in_array($tipoEstacion,["E","CR","CP","EM","NR"])){
				$query_parts = array();
				$query = "";
				foreach ($datosTipoEstacion as $clave => $valor) {
					if(isset($valor['timestamp']) && isset($valor['valor'])){

						// solo revisar variables TA
						$val = explode('/', $valor['cod_variable']);
						if(count($val) == 2){
							
							$variable = $val[1];

							if($variable=="TA" || $variable=="NR1"){

								$fecha = new DateTime();
								$fecha->setTimestamp($tiempo_actual);
								$tiempo_ejecucion = $fecha->format('Y-m-d H:i:s');

								if (!is_null($valor['timestamp']) && (bool) preg_match('/[A-Za-z]+/', $valor['valor']) == false && is_numeric($valor['valor'])) { // revisar validez del timestamp, detectar errores de dato
									
									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 10||':00')::timestamp(0) ) , '" . round((float)$valor['valor'], 4) . "','".$valor['timestamp']."','".$tiempo_ejecucion."', false, null)";

								} else if(is_null($valor['timestamp'])){

									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 10||':00')::timestamp(0) ) , null,'".$valor['timestamp']."','".$tiempo_ejecucion."', true, 'timestamp nulo, val:".$valor['valor']."')";

								} else if((bool) preg_match('/[A-Za-z]+/', $valor['valor']) == false){

									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 10||':00')::timestamp(0) ) , null,'".$valor['timestamp']."','".$tiempo_ejecucion."', true, 'carácteres inválidos encontrados, val:".$valor['valor']."')";

								} else {

									$query_parts[] = "('" . $valor['cod_variable'] . "', ( select (TO_CHAR(TIMESTAMP '".$valor['timestamp']."', 'YYYY-MM-DD HH24')||':'||EXTRACT(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer - extract(MINUTE from TIMESTAMP '".$valor['timestamp']."'::timestamp(0))::integer % 10||':00')::timestamp(0) ) , null,'".$valor['timestamp']."','".$tiempo_ejecucion."', true, 'error desconocido, val:".$valor['valor']."')";

								}
							}
						}
					}
				}

				if(count($query_parts)>0){
					$query = $insert_query . implode(',', $query_parts);
					
					ConexionBD::EjecutarConsultaConDB($DB, $query, false);
					
				}
			}
		}
	}

	//Obtención de datos de RTAP
	function ObtenerDatosRTAP()
	{
		$datosRTAP = new ObtenerDatos();
		$datos = $datosRTAP->DatosRTAP();
		return $datos;
	}

	//Obtención de datos de RTAP sin post procesado
	function ObtenerDatosRTAPRAW()
	{
		$datosRTAP = new ObtenerDatos();
		$datos = $datosRTAP->DatosRTAPRAW();
		return $datos;
	}

	/** 
     * Obtener todos los últimos datos de todas las estaciones 
     **/
    function generar_master_ultres($DB, $ultimosDatos) {
		
		$categorias = array("Z12", "Z3", "Z5", "Z67", "E", "CR", "CAL", "EAA", "ECC", "EXC", "PZ", "PZM", "R", "EM", "NR", "D", "S", "CC", "EMAEMET", "ACA");

        $datos_estaciones_categorias = array();
        $datos_estaciones_codestacion = array();

        // generar datos por cada categoria de estacion
        foreach ($categorias as $categoria) {

            $datos_estaciones = array();

            if ($categoria == "CAL") {

                $datos_estaciones['EAA'] = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'EAA', null, null, $DB, $ultimosDatos);
                $datos_estaciones['ECC'] = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'ECC', null, null, $DB, $ultimosDatos);
                $datos_estaciones['EXC'] = $this->visorDao->UltimosDatosMedidos("ULT/RES", 'EXC', null, null, $DB, $ultimosDatos);

            } else if($categoria == "D"){

                $datos_estaciones = $this->visorDao->EstacionesEnProyecto($DB);

            } else if($categoria == "S"){

                $datos_estaciones = $this->visorDao->getListadoSondas($DB);
			
			} else if($categoria == "ACA"){

                $datos_estaciones = $this->visorDao->getListadoACA($DB);

            } else {

                $datos_estaciones = $this->visorDao->UltimosDatosMedidos("ULT/RES", $categoria, null, null, $DB, $ultimosDatos);

            }

			// rellenar ultimos datos agrupados por categoria y estaciones
            $datos_estaciones_categorias[$categoria] = $datos_estaciones;

			// rellenar ultimos datos agrupados solo por estacion
			if(isset($datos_estaciones["valores"]) && isset($datos_estaciones["encabezado"])){
            	foreach($datos_estaciones["valores"] as $ultresestacionvalores){
					if(isset($ultresestacionvalores["cod_estacion"]) && !isset($datos_estaciones_codestacion[$ultresestacionvalores["cod_estacion"]])){
						$cod_estacion = $ultresestacionvalores["cod_estacion"];
						$datos_estaciones_codestacion[$cod_estacion]["valores"][] = $ultresestacionvalores;
						$datos_estaciones_codestacion[$cod_estacion]["encabezado"] = $datos_estaciones["encabezado"];
					}
				}
            }
        }

		$this->visorDao->GuardarMasterULTRESCategorias($DB, $datos_estaciones_categorias);
		$this->visorDao->GuardarMasterULTRESEstaciones($DB, $datos_estaciones_codestacion);

    }

	function generar_datos_pluviometria($DB){
		require_once APPPATH . '/libraries/visor/class_aemet.php';
		require_once APPPATH . '/libraries/visor/class_lluviasaih.php';

		$datosPuntualesAEMET = AEMET::UltimosDatosAemet($DB);
		$datosThiessenEmbalsesAEMET = AEMET::UltimosDatosEmbalsesAemet($DB);
		$datosPuntualesSAIH = LLUVIASAIH::UltimosDatosLluviaSAIH($DB);
		$datosPuntualesEmbalsesSAIH = LLUVIASAIH::UltimosDatosEmbalsesLluviaSAIH($DB);
		$datosThiessenEmbalsesSAIH = LLUVIASAIH::UltimosDatosEmbalsesLluviaPonderadaSAIH($DB);

		$json_datosPuntualesAEMET = json_encode($datosPuntualesAEMET, JSON_HEX_APOS);
		$json_datosThiessenEmbalsesAEMET = json_encode($datosThiessenEmbalsesAEMET, JSON_HEX_APOS);
		$json_datosPuntualesSAIH = json_encode($datosPuntualesSAIH, JSON_HEX_APOS);
		$json_datosPuntualesEmbalsesSAIH = json_encode($datosPuntualesEmbalsesSAIH, JSON_HEX_APOS);
		$json_datosThiessenEmbalsesSAIH = json_encode($datosThiessenEmbalsesSAIH, JSON_HEX_APOS);

		Cache::GuardarRecursoCache($DB, "datosPuntualesAEMET", $json_datosPuntualesAEMET);
		Cache::GuardarRecursoCache($DB, "datosThiessenEmbalsesAEMET", $json_datosThiessenEmbalsesAEMET);
		Cache::GuardarRecursoCache($DB, "datosPuntualesSAIH", $json_datosPuntualesSAIH);
		Cache::GuardarRecursoCache($DB, "datosPuntualesEmbalsesSAIH", $json_datosPuntualesEmbalsesSAIH);
		Cache::GuardarRecursoCache($DB, "datosThiessenEmbalsesSAIH", $json_datosThiessenEmbalsesSAIH);

	}

	function generar_esquemas($DB, $ultimosDatos){

		$query = "select id_esquema from tbl_esquemas where activo is true";
		$id_esquemas = ConexionBD::EjecutarConsultaConDB($DB, $query);

		if(count($id_esquemas)>0){
			$datosAnterioresJson = $this->visorDao->CargarRecursoCache($DB, 'esquemas_datos_actuales');
			$datosAnteriores = null;
			if (isset($datosAnterioresJson)) {
				$this->visorDao->GuardarRecursoCache($DB, 'esquemas_datos_anteriores', $datosAnterioresJson);
				$datosAnteriores = json_decode($datosAnterioresJson, true);
			}
			$datosActuales = Esquemas::getDatosExplotacion($DB, $ultimosDatos);
			$json = json_encode($datosActuales, JSON_HEX_APOS); // escapar apostrofes
			$this->visorDao->GuardarRecursoCache($DB, 'esquemas_datos_actuales', $json);

			$cabeceras = $this->visorDao->CabecerasV2();
			foreach($id_esquemas as $id_esquema){
				$id = $id_esquema["id_esquema"];
				$json_esquema = Esquemas::generateEsquema($id, $datosActuales, $datosAnteriores, $cabeceras["cabeceras"]);
				if ($json_esquema != null) {
					$this->updateEsquema($DB, $id, $json_esquema);
				}
			}
		}
	}

	function updateEsquema($DB, $idEsquema, $jsonEsquema) {
		$queryUpdate = "UPDATE tbl_esquemas SET esquema_procesado = '" . $jsonEsquema . "' WHERE id_esquema = '". $idEsquema. "';";
		ConexionBD::EjecutarConsultaConDB($DB, $queryUpdate, false);
	}

	function test($datosRTAP, $DB){
		return ObtenerDatos::UltimosDatos($DB);
	}

	/**
	 * @nombre UltimosDatosMedidos
	 * @descripcion retorna la información de tbl_ultimo_dato_medido
	 */
	function UltimosDatosMedidos($tipo, $categoria, $estacion = null, $datosRTAP = null, $DB = null, $ultimosDatos = null, $cod_zona_regable = null, $cod_masa = null)
	{
		require APPPATH . '/libraries/visor/config.php';
		$categoria = trim($categoria);
		$estacion = trim($estacion);

		switch ($tipo) {

			//OBTENCIÓN DE TODAS LAS VARIABLES DE TIEMPO REAL RTAP
			case ($tipo == "ULT/RTAP"):

				$tabla_datos = "tbl_variable v, tbl_tipo_variable t";
				$camposF = array("v.cod_variable", "v.descripcion", "v.cod_tipo_variable", "t.unidad");
				$seleccion	= Operaciones::campos2seleccion($camposF);
				$query = "SELECT " . $seleccion . " FROM " . $tabla_datos . " WHERE disponible = 1 AND v.cod_tipo_variable = t.cod_tipo_variable AND split_part(v.cod_variable,'/',1) = '" . $estacion . "' ORDER by v.cod_variable asc";

				$camposV = array("valor", "timestamp");

				$campos = ["cod_variable", "descripcion", "cod_tipo_variable", "unidad", "valor", "timestamp"];

				$datosF = ConexionBD::EjecutarConsultaConDB($DB, $query);
				$datosV = ($ultimosDatos!=null) ? $ultimosDatos : ObtenerDatos::UltimosDatos($DB);

				//Para particularizar la función
				$codigoF = 'cod_variable';
				$codigoV = 'cod_variable';
				$valor = 'valor';

				$datos = ObtenerDatos::UnirDatosFijosVariablesConTimestamp2($codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV);

				// parche para E2-19, editar las variables QC1 y QC2 de RTAP por las últimas almacenadas por bdd
				/*if ($estacion == "E2-19") {
					foreach ($datos as &$dato) {
						if ($dato["cod_variable"] == "E2-19/QC1") {
							$queryQC1 = "select timestamp_medicion as timestamp, valor from tbl_hist_medidas where cod_variable like 'E2-19/QC1' AND timestamp_medicion = (select MAX(timestamp_medicion) from tbl_hist_medidas where cod_variable like 'E2-19/QC1')";
							$valoresQC1 = ConexionBD::EjecutarConsulta($queryQC1);
							if (isset($valoresQC1[0])) {
								$dato["valor"] = $valoresQC1[0]["valor"];
								$dato["timestamp"] = $valoresQC1[0]["timestamp"];
							}
						} else if ($dato["cod_variable"] == "E2-19/QC2") {
							$queryQC2 = "select timestamp_medicion as timestamp, valor from tbl_hist_medidas where cod_variable like 'E2-19/QC2' AND timestamp_medicion = (select MAX(timestamp_medicion) from tbl_hist_medidas where cod_variable like 'E2-19/QC2')";
							$valoresQC2 = ConexionBD::EjecutarConsulta($queryQC2);
							if (isset($valoresQC2[0])) {
								$dato["valor"] = $valoresQC2[0]["valor"];
								$dato["timestamp"] = $valoresQC2[0]["timestamp"];
							}
						}
					}
				}*/

				// parche para E1-04, mostrar la unidad de NE1 como 'm' en vez de m.s.n.m
				if ($estacion == "E1-04") {
					foreach ($datos as &$dato) {
						if ($dato["cod_variable"] == "E1-04/NE1") {

							$dato["unidad"] = "m";
						}
					}
				}

				break;

				//DATOS EN TIEMPO REAL
			case ($tipo == "ULT/RES"):

				switch ($categoria) { //Elección del rango de estaciones según la elección

					case ($categoria == "E" or $categoria == "AEX" or $categoria == "Z12" or $categoria == "Z3" or $categoria == "Z5" or $categoria == "Z67"):

						if ($estacion) {
							$tabla_datosF = null;
							$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "p.acronimo", "e.nombre", "e.orden");
							$camposV = array("NE1", "VE1", "PV1", "PRA", "PI", "timestamp"); 
							$campos = ["tipo", "zona", "acronimo", "cod_estacion", "nombre", "NE1", "VE1", "PV1", "PRA", "PI", "timestamp"];
						} else {
							$tabla_datosF = null;
							$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "p.acronimo", "e.nombre", "e.orden");
							$camposV = array("NE1", "VE1", "PV1", "SE1", "PRA", "PI", "timestamp"); 
							$campos = ["tipo", "zona", "acronimo", "cod_estacion", "nombre", "NE1", "VE1", "PV1", "SE1", "PRA", "PI", "timestamp"];
						}

						break;

					case ($categoria == "S"):
						break;

					case ($categoria == "CR"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("QR1", "NR1", "PRA", "PI", "timestamp");
						$campos = ["tipo", "zona", "cod_estacion", "nombre", "QR1", "NR1", "PRA", "PI", "timestamp"];
						break;

					case ($categoria == "NR"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("NR1", "QR1", "timestamp");
						$campos = ["tipo", "zona", "cod_estacion", "nombre","NR1", "QR1", "timestamp"];
						break;

					case ($categoria == "PZ"):
						$tabla_datosF = 'tbl_estacion e';
						$camposV = array("CP", "timestamp");
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.cota_boca_sondeo", "m.nombre_masasub", "e.orden");
						$campos = ["tipo", "zona", "cod_estacion", "nombre", "nombre_masasub", "CP", "cota_boca_sondeo", "timestamp"];
						break;

					case ($categoria == "PZM"):
						$tabla_datosF = 'tbl_estacion e';
						$camposV = array("CP", "timestamp");
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.cota_boca_sondeo", "m.nombre_masasub", "e.orden");
						$campos = ["tipo", "zona", "cod_estacion", "nombre", "nombre_masasub", "CP", "cota_boca_sondeo", "timestamp"];
						break;

					case ($categoria == "EM"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("PRA", "PI", "TA", "VV", "DV", "HR", "PA", "RT", "timestamp");
						$campos = ["tipo", "cod_estacion", "nombre","PRA", "PI", "TA", "VV", "DV", "HR", "PA", "RT", "timestamp"];
						break;

					case ($categoria == "EMAEMET"):
						$tabla_datosF = '';
						$camposF = array("tipo", "cod_estacion", "nombre");
						$camposV = array("TA", "PR", "timestamp");
						$campos = ["tipo", "cod_estacion", "nombre", "TA", "PR", "timestamp"];
						break;

					case ($categoria == "EAA"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("TA", "TB", "PH", "OX", "CD", "NH", "NO", "PO", "SAC", "timestamp");
						$campos = ["tipo","zona","cod_estacion","nombre","TA", "TB", "PH", "OX", "CD", "NH", "NO", "PO", "SAC", "timestamp"];
						break;

					case ($categoria == "ECC"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("TA", "TB", "PH", "OX", "CD", "RD", "SAC", "QV", "timestamp");
						$campos = ["tipo","zona","cod_estacion","nombre","TA","TB","PH","OX","CD","RD","SAC","QV","timestamp"];
						break;

					case ($categoria == "EXC"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("QV", "TA", "PH", "CD", "NF", "COT", "SAC", "timestamp");
						$campos = ["tipo","cod_estacion","nombre","QV","TA","PH","CD","NF","COT", "SAC", "timestamp"];
						break;

					case ($categoria == "R"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("timestamp");
						$campos = ["tipo","cod_estacion","nombre","timestamp"];
						break;

					case ($categoria == "CC"):
						$tabla_datosF = 'tbl_estacion e';
						$camposF = array("distinct e.cod_estacion", "e.tipo", "e.zona", "e.nombre", "e.orden");
						$camposV = array("AD1", "AD2", "AD3", "AD4", "AD5", "timestamp");
						$campos = ["tipo","cod_estacion","nombre", "AD1", "AD2", "AD3", "AD4", "AD5", "timestamp"];
						break;
				}

				$datosF = ObtenerDatos::DatosFijos($camposF, $tabla_datosF, $categoria, $estacion, $cod_masa, $cod_zona_regable, $DB);
				$datosV = ($ultimosDatos!=null) ? $ultimosDatos : ObtenerDatos::UltimosDatos($DB);

				//Para particularizar la funci�n
				$codigoF = 'cod_estacion';
				$codigoV = 'cod_variable';
				$valor = 'valor';

				$datos = ObtenerDatos::UnirDatosFijosVariablesConTimestamp($codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV);
				break;

				//OBTENCI�N DE LOS NIVELES DE UNA ZONA
				//VERIFICADA 11.05.2020
			case ($tipo == "NIV/RES" or $tipo == "NIV/CFG"):
				
				$tabla_datosF = null;
				$tabla_datos = "tbl_nivel n";

				$camposF = array("distinct e.cod_estacion", "e.zona", "p.acronimo", "e.nombre", "e.orden" );

				//Elecci�n del tipo de tabla de niveles
				switch ($tipo) {
					case (explode('/', $tipo)[1] == 'RES'): //resumen de niveles
						$camposV1 = array("NE1");
						$camposV2 = array("ALIV", "NMN", "NAP", "NAE", "MAX", "COR", "RE1", "RE2"); //,"COR","DF","ABA1","ABA2","MIN","USR1","USR2","USR3","USR4"
						$campos = ["zona", "acronimo", "cod_estacion", "nombre","NE1","ALIV", "NMN", "NAP", "NAE", "MAX", "COR", "RE1", "RE2"];	//mezcla los campos del encabezado
						break;

					case (explode('/', $tipo)[1] == 'CFG'): //tabla completa de niveles
						$camposV1 = array("NE1");
						$camposV2 = array("CAU", "MIN", "MIE", "DF", "ABA1", "ABA2", "RE1", "RE2", "ALIV", "NMN", "NAP", "NAE", "MAX", "COR", "EXP");
						$campos = ["zona", "acronimo", "cod_estacion", "nombre","NE1","CAU", "MIN", "MIE", "DF", "ABA1", "ABA2", "RE1", "RE2", "ALIV", "NMN", "NAP", "NAE", "MAX", "COR", "EXP"];	//mezcla los campos del encabezado
						break;
				}

				//$query1 = "SELECT ".$seleccion." FROM ".$tabla_estacion." WHERE e.cod_estacion = p.cod_estacion AND e.tipo='E' AND e.cod_estacion <> 'E1-04' AND e.zona <> 'Z8' AND e.zona is not null ORDER by e.orden asc";
				$query2 = "SELECT * FROM " . $tabla_datos;

				$datosF = ObtenerDatos::DatosFijos($camposF, $tabla_datosF, $categoria, null, null, null, $DB);
				$datosV1 = ObtenerDatos::CotaActualEmbalse($datosRTAP);
				$datosV2 = ConexionBD::EjecutarConsultaConDB($DB, $query2);

				//Para particularizar la función
				$codigoF = 'cod_estacion';
				$codigoV = 'cod_nivel';
				$valor = 'nivel';
				$datos = $datosF;

				$datos = ObtenerDatos::UnirDatosFijosVariablesSinTimestamp($codigoF, $codigoV, $valor, $camposF, $camposV1, $datos, $datosV1);
				$datos = ObtenerDatos::UnirDatosFijosVariablesSinTimestamp($codigoF, $codigoV, $valor, $camposF, $camposV2, $datos, $datosV2);
				//var_dump($datos);

				break;

				//OBTENCI�N DE LAS ESTACIONES NO OPERATIVAS
				//ARREGLAR: 
			case ($tipo == "EST/RES"):
				
				$tabla_datos = "tbl_estacion e, tbl_nota n";
				$campos = array("zona", "cod_estacion", "disponible", "nombre", "tipo", "timestamp_nota", "timestamp_ultvalor", "descripcion");

				if($estacion){
					
					$query = "SELECT e.zona, n.cod_estacion, e.disponible, e.nombre, e.tipo, n.timestamp as timestamp_nota, udmt.timestamp as timestamp_ultvalor, n.descripcion
								FROM 
								tbl_estacion e 
								JOIN tbl_nota n ON e.cod_estacion = n.cod_estacion
								LEFT JOIN 
								(
									SELECT split_part(udm.cod_variable,'/',1) as cod_estacion, max(udm.timestamp) as timestamp 
									FROM tbl_ultimo_dato_medido udm
									WHERE split_part(udm.cod_variable,'/',1) = '" .$estacion. "'
									AND udm.validez = 1
									GROUP BY cod_estacion
								) udmt ON udmt.cod_estacion = e.cod_estacion
								WHERE 
								e.cod_estacion='" .$estacion. "'
								AND e.tipo='" . $categoria . "'
								AND n.categoria='Estado' 
								AND ( e.disponible='false' OR e.estado_adquisicion=2 OR e.estado_adquisicion=3 )
								ORDER by e.zona asc, n.cod_estacion asc";

				} else {
					$query = "SELECT e.zona, n.cod_estacion, e.disponible, e.nombre, e.tipo, n.timestamp as timestamp_nota, udmt.timestamp as timestamp_ultvalor, n.descripcion
								FROM 
								tbl_estacion e 
								JOIN tbl_nota n ON e.cod_estacion = n.cod_estacion
								LEFT JOIN 
								(
									SELECT split_part(udm.cod_variable,'/',1) as cod_estacion, max(udm.timestamp) as timestamp 
									FROM tbl_ultimo_dato_medido udm
									WHERE udm.validez = 1
									GROUP BY cod_estacion
								) udmt ON udmt.cod_estacion = e.cod_estacion
								WHERE e.tipo='" . $categoria . "'
								AND n.categoria='Estado' 
								AND ( e.disponible='false' OR e.estado_adquisicion=2 OR e.estado_adquisicion=3 )
								ORDER by e.zona asc, n.cod_estacion asc";
				}
				
				$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);
				break;

			case ($tipo == "NIV/DET"):

				$campos = array("cod_nivel", "descripcion", "nivel", "volumen", "superficie");
				$query = "SELECT n.cod_nivel, n.descripcion, n.nivel, n.volumen, n.superficie FROM tbl_nivel n WHERE split_part(n.cod_nivel,'/',1)= '" . $estacion . "' ORDER by n.nivel desc";

				$aux = array();
				$datos = array();
				$aux = ConexionBD::EjecutarConsultaConDB($DB, $query);
				foreach ($aux as $clave => $linea) {
					$datos[$clave]['cod_nivel'] = $linea['cod_nivel'];
					$datos[$clave]['descripcion'] = $linea['descripcion'];
					$datos[$clave]['nivel'] = $linea['nivel'];
					$datos[$clave]['volumen'] = $linea['volumen'];
					$datos[$clave]['superficie'] = $linea['superficie'];
				}

				// Añadir valiable NE1 en el nivel correcto
				if (isset($datosRTAP['E']) && isset($datosRTAP['E'][$estacion . "/NE1"]) && isset($datosRTAP['E'][$estacion . "/NE1"]['valor'])) {

					$i = 0;
					$nivelRTAP = floatval($datosRTAP['E'][$estacion . "/NE1"]['valor']);

					foreach ($datos as $nivel) {
						$nivel = floatval($nivel['nivel']);
						if ($nivel != null && ($nivel <= $nivelRTAP)) {
							break;
						}
						$i++;
					}

					$cotaNivel = array();
					$cotaNivel['cod_nivel'] = $estacion . "/NE1";
					$cotaNivel['descripcion'] = "Cota de embalse actual";
					$cotaNivel['nivel'] = strval($datosRTAP['E'][$estacion . "/NE1"]['valor']);
					$cotaNivel['volumen'] = ($datosRTAP['E'][$estacion . "/VE1"] && $datosRTAP['E'][$estacion . "/VE1"]['valor']) ? strval($datosRTAP['E'][$estacion . "/VE1"]['valor']) : null;
					$cotaNivel['superficie'] = (isset($datosRTAP['E'][$estacion . "/SE1"]) && isset($datosRTAP['E'][$estacion . "/SE1"]['valor'])) ? strval($datosRTAP['E'][$estacion . "/SE1"]['valor']) : null;

					$datosCotaNivel = array();
					$datosCotaNivel = array_merge(array_slice($datos, 0, $i), array($cotaNivel));
					$datosCotaNivel = array_merge($datosCotaNivel, array_slice($datos, $i, count($datos) - $i));
					$datos = $datosCotaNivel;
				}

				unset($aux);
				break;

			case ($tipo == "NIV/DETR"):

				$campos = array("cod_nivel", "nivel", "caudal", "descripcion");
				$query = "SELECT cod_nivel, nivel, caudal, descripcion FROM tbl_nivel_referencia_relativa n WHERE split_part(n.cod_nivel,'/',1)= '" . $estacion . "' ORDER by n.nivel desc";

				$aux = array();
				$datos = array();
				$aux = ConexionBD::EjecutarConsultaConDB($DB, $query);
				foreach ($aux as $clave => $linea) {
					$datos[$clave]['cod_nivel'] = $linea['cod_nivel'];
					$datos[$clave]['nivel'] = $linea['nivel'];
					$datos[$clave]['caudal'] = $linea['caudal'];
					$datos[$clave]['descripcion'] = $linea['descripcion'];	
				}

				$ultDatos = ($ultimosDatos!=null) ? $ultimosDatos : ObtenerDatos::UltimosDatos($DB);

				$valorNivel = null;

				foreach($ultDatos as $dato){
					if($dato["cod_variable"] == $estacion.'/NR1'){
						$valorNivel = $dato["valor"];
					}
				}

				// Añadir valiable NE1 en el nivel correcto
				if ($valorNivel!=null) {

					$i = 0;
					$nivelRio = floatval($valorNivel);

					foreach ($datos as $nivel) {
						$nivel = floatval($nivel['nivel']);
						if ($nivel != null && ($nivel <= $nivelRio)) {
							break;
						}
						$i++;
					}

					$cotaNivel = array();
					$cotaNivel['cod_nivel'] = $estacion . "/NR1";
					$cotaNivel['nivel'] = $valorNivel;
					$cotaNivel['caudal'] = null;
					$cotaNivel['descripcion'] = "Nivel de río actual";

					$datosCotaNivel = array();
					$datosCotaNivel = array_merge(array_slice($datos, 0, $i), array($cotaNivel));
					$datosCotaNivel = array_merge($datosCotaNivel, array_slice($datos, $i, count($datos) - $i));
					$datos = $datosCotaNivel;
				}

				unset($aux);
				break;

			case ($tipo == "DIR/RES"):

				// DIRECTORIO TELEFÓNICO
				$campos = array("zona", "acronimo", "cod_estacion", "nombre", "director", "director_adjunto", "encargado", "jefe_equipo", "electricista", "mecanico", "operario");

				$where = "";
				switch ($categoria) {
					case ($categoria == "AEX"):
						//$query = "SELECT ".$seleccion." FROM ".$tabla_datos." WHERE e.cod_estacion = p.cod_estacion AND e.zona is not null ORDER by e.zona, p.cod_estacion asc";
						$where = "AND e.zona is not null";
						break;

					case ($categoria == "Z12" or $categoria == "Z3" or $categoria == "Z5" or $categoria == "Z67"):
						//$query = "SELECT ".$seleccion." FROM ".$tabla_datos." WHERE e.cod_estacion = p.cod_estacion AND e.zona = '".$categoria."' ORDER by e.zona, p.cod_estacion asc";
						$where = "AND e.zona = '" . $categoria . "'";
						break;

					default:
						//$query = "SELECT ".$seleccion." FROM ".$tabla_datos." WHERE e.cod_estacion = p.cod_estacion AND e.cod_estacion = '".$categoria."' ORDER by e.zona, p.cod_estacion asc";
						$where = "AND e.cod_estacion = '" . $estacion . "'";
				}

				$query = "SELECT e.zona,p.acronimo,e.cod_estacion,e.nombre,
						p.director, CONCAT(cd.nombre,'|',cd.apellidos,'|',cd.email,'|',cd.fijo,'|',cd.movil,'|',cd.ext_despacho_1,'|',cd.ext_chg,'|',cd.movil_particular,'|',cd.fijo_particular) datos_director,
						p.director_adjunto, CONCAT(cda.nombre,'|',cda.apellidos,'|',cda.email,'|',cda.fijo,'|',cda.movil,'|',cda.ext_despacho_1,'|',cda.ext_chg,'|',cda.movil_particular,'|',cda.fijo_particular) datos_dadjunto,
						p.encargado, CONCAT(ce.nombre,'|',ce.apellidos,'|',ce.email,'|',ce.fijo,'|',ce.movil,'|',ce.ext_despacho_1,'|',ce.ext_chg,'|',ce.movil_particular,'|',ce.fijo_particular) datos_encargado,
						p.jefe_equipo, CONCAT(cje.nombre,'|',cje.apellidos,'|',cje.email,'|',cje.fijo,'|',cje.movil,'|',cje.ext_despacho_1,'|',cje.ext_chg,'|',cje.movil_particular,'|',cje.fijo_particular) datos_jequipo,
						p.electricista, CONCAT(cel.nombre,'|',cel.apellidos,'|',cel.email,'|',cel.fijo,'|',cel.movil,'|',cel.ext_despacho_1,'|',cel.ext_chg,'|',cel.movil_particular,'|',cel.fijo_particular) datos_electricista,
						p.mecanico, CONCAT(cm.nombre,'|',cm.apellidos,'|',cm.email,'|',cm.fijo,'|',cm.movil,'|',cm.ext_despacho_1,'|',cm.ext_chg,'|',cm.movil_particular,'|',cm.fijo_particular) datos_mecanico,
						p.operario, CONCAT(co.nombre,'|',co.apellidos,'|',co.email,'|',co.fijo,'|',co.movil,'|',co.ext_despacho_1,'|',co.ext_chg,'|',co.movil_particular,'|',co.fijo_particular) datos_operario
						FROM tbl_estacion e, tbl_presa p
						full join tbl_contacto cd ON p.director = cd.cod_contacto
						full join tbl_contacto cda ON p.director_adjunto = cda.cod_contacto
						full join tbl_contacto ce ON p.encargado = ce.cod_contacto
						full join tbl_contacto cje ON p.jefe_equipo = cje.cod_contacto
						full join tbl_contacto cel ON p.electricista = cel.cod_contacto
						full join tbl_contacto cm ON p.mecanico = cm.cod_contacto
						full join tbl_contacto co ON p.operario = co.cod_contacto
						WHERE e.cod_estacion = p.cod_estacion " . $where . " ORDER by e.zona, p.cod_estacion asc";

				$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);
				//$datos = $query;
				break;


				//OBTENCI�N DE LA TABLA RESUMEN DE EXTENSIONES
				//ARREGLAR: Arreglar los formatos con respecto al encabezado
			case ($tipo == "DIR/CFG"):

				$tabla_datosF = null;
				$camposF = array("distinct e.cod_estacion", "e.zona", "p.acronimo", "e.nombre");

				$tabla_datosV = "tbl_extension";
				$camposV = array("ext00", "ext01", "ext02", "ext03", "ext04", "ext05", "ext06", "ext07", "ext08", "ext09", "ext10", "ext11", "ext12", "ope00");

				$datosF = ObtenerDatos::DatosFijos($camposF, $tabla_datosF, $categoria, null, null, null, $DB);
				$datosV = ObtenerDatos::DatosVariables($camposV, $tabla_datosV, 'NIV', null, null, $DB);

				$campos = ["zona", "acronimo", "cod_estacion", "nombre","ext00", "ext01", "ext02", "ext03", "ext04", "ext05", "ext06", "ext07", "ext08", "ext09", "ext10", "ext11", "ext12", "ope00"];	//mezcla los campos del encabezado

				//Para particularizar la funci�n
				$codigoF = 'acronimo';
				$codigoV = 'cod_extension';
				$valor = 'extension';

				$datos = ObtenerDatos::UnirDatosFijosVariablesSinTimestamp($codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV);
				break;

				//DIRECTORIO TELEF�NICO COMPLETO DE UNA ENTIDAD
				//VERIFICADA 25.11.2020
			case ($tipo == "DIR/DET"):

				$campos = array("cod_extension", "extension", "descripcion");

				switch ($categoria) {
					case ($categoria == "RCI" or $categoria == "S"):
						$query = "SELECT cod_extension, extension, descripcion FROM tbl_extension WHERE cod_extension LIKE 'CC%' ORDER by cod_extension asc";
						break;
					case ($categoria == "AEX"):
						$query = "SELECT cod_extension, extension, descripcion FROM tbl_estacion JOIN tbl_presa ON trim(tbl_estacion.cod_estacion) = trim(tbl_presa.cod_estacion) JOIN tbl_extension ON tbl_presa.acronimo = split_part(tbl_extension.cod_extension,'/',1) ORDER by cod_extension asc";
						break;
					case ($categoria == "Z12" or $categoria == "Z3" or $categoria == "Z5" or $categoria == "Z67"):
						$query = "SELECT cod_extension, extension, descripcion FROM tbl_estacion JOIN tbl_presa ON trim(tbl_estacion.cod_estacion) = trim(tbl_presa.cod_estacion) JOIN tbl_extension ON tbl_presa.acronimo = split_part(tbl_extension.cod_extension,'/',1) WHERE TRIM(tbl_estacion.zona) like '" . $categoria . "' ORDER by cod_extension asc";
						break;
					default:
						$query = "SELECT cod_extension, extension, descripcion FROM tbl_estacion LEFT JOIN tbl_presa ON trim(tbl_estacion.cod_estacion) = trim(tbl_presa.cod_estacion) LEFT JOIN tbl_extension ON tbl_presa.acronimo = split_part(tbl_extension.cod_extension,'/',1) WHERE TRIM(tbl_estacion.cod_estacion) like '" . $estacion . "' ORDER by cod_extension asc";
						break;
				}

				$aux = array();
				$datos = array();
				$aux = ConexionBD::EjecutarConsultaConDB($DB, $query);
				foreach ($aux as $clave => $linea) {
					$datos[$clave]['cod_extension'] = $linea['cod_extension'];
					$datos[$clave]['extension'] = $linea['extension'];
					$datos[$clave]['descripcion'] = $linea['descripcion'];
				}
				//var_dump($datos);
				unset($aux);
				break;


				//OBTENCI�N DE DATOS PROCEDENTES DE tbl_ultimo_dato_medido DETALLADO PARA CADA ESTACI�N (bien para EAA, ECC pero faltan datos del resto) No se emplea apenas ya. S�lo en presas para comprobaciones.
				//VERIFICADA 11.05.2020
			case ($tipo == "ULT/DET"):

				$campos = array("cod_variable", "descripcion", "cod_tipo_variable", "valor", "unidad", "timestamp");
				$seleccion	= Operaciones::campos2seleccion($campos);
				$query = "SELECT v.cod_variable, v.descripcion, v.cod_tipo_variable, d.valor, t.unidad, d.timestamp FROM tbl_variable v, tbl_ultimo_dato_medido d, tbl_tipo_variable t WHERE v.cod_variable = d.cod_variable AND v.cod_tipo_variable = t.cod_tipo_variable AND split_part(v.cod_variable,'/',1) = '" . $estacion . "' ORDER by v.cod_variable asc";
				$datos = ConexionBD::EjecutarConsultaConDB($DB, $query);
				break;

		}

		$datosRetorno = array("encabezado" => $campos, "valores" => $datos);

		return $datosRetorno;
	}

	/**
	 * @nombre UltimosDatosMedidosMASb
	 * @descripcion retorna la información de cada masa
	 */
	/*function UltimosDatosMedidosMASb($cod_masasub, $datosRTAP = null, $DB = null)
	{
		//require APPPATH . '/libraries/visor/config.php';
		$datos = [];
		$categorias = ["CR", "PZ", "EM"];
		$datosRetorno = array();

		foreach ($categorias as $categoria) {
			switch ($categoria) {

				case "CR":
					$tabla_datosF = 'tbl_estacion e';
					$tabla_datosV = '';
					$camposF = array("e.tipo", "e.zona", "e.cod_estacion", "e.nombre");
					$camposV = array("QR1", "NR1", "PRA", "PI", "timestamp");
					$campos = array("tipo","zona","cod_estacion","nombre","QR1","NR1","PRA","PI","timestamp");
					break;

				case "PZ":
					$tabla_datosF = 'tbl_estacion e';
					$tabla_datosV = 'tbl_ultimo_dato_medido d';
					$camposV = array("CP", "timestamp");
					$camposF = array("e.tipo", "e.cod_estacion", "e.nombre", "m.nombre_masasub");
					$campos = array("tipo", "cod_estacion", "nombre", "nombre_masasub", "CP", "timestamp");
					break;

				case "EM":
					$tabla_datosF = 'tbl_estacion e';
					$tabla_datosV = '';
					$camposF = array("e.tipo", "e.cod_estacion", "e.nombre");
					$camposV = array("PRA", "PI", "TA", "VV", "DV", "HR", "PA", "RT", "timestamp");
					$campos = array("tipo", "cod_estacion", "nombre","PRA", "PI", "TA", "VV", "DV", "HR", "PA", "RT", "timestamp");
					break;
			}
			$datosF = ObtenerDatos::DatosFijos($camposF, $tabla_datosF, $categoria, null, $cod_masasub, null, $DB);
			$datosV = ObtenerDatos::UltimosDatos($DB);

			//Para particularizar la funci�n
			$codigoF = 'cod_estacion';
			$codigoV = 'cod_variable';
			$valor = 'valor';

			$datos = ObtenerDatos::UnirDatosFijosVariablesConTimestamp($codigoF, $codigoV, $valor, $camposF, $camposV, $datosF, $datosV);
			$datosRetorno[$categoria] = array("encabezado" => $campos, "valores" => $datos);
		}

		return $datosRetorno;
	}*/

	function UltimosDatos($DB){
		return ObtenerDatos::UltimosDatos($DB);
	}
	function CabecerasV2()
	{

		return [ 
			"cabeceras" => $this->Cabeceras(),
			"grupos" => $this->Grupos(),
		];
	}

	function Grupos() {
		$query = "SELECT id, campo, descripcion, unidad, formato, color, escala_min, escala_max, umbral_min_alerta, umbral_max_alerta, umbral_min_alarma, umbral_max_alarma, formato_amcharts, formato_angular, alineacion_texto, tipo_formato from tbl_campo_encabezado_grp";
		$grupos = ConexionBD::EjecutarConsulta($query);
		$this->fillFormat($grupos);
		return $grupos;
	}

	function Cabeceras()
	{
		$query = "select cod_campo,campo,descripcion,unidad,formato,color,umbral_min_alerta,umbral_max_alerta,umbral_min_alarma,umbral_max_alarma,escala_min,escala_max,id_grupo from tbl_campo_encabezado";
		$cabeceras = ConexionBD::EjecutarConsulta($query);
		$this->fillFormat($cabeceras);
		return $cabeceras;
	}


	function fillFormat(&$entries) {
		// Añadir decimales y alineacion manualmente (se podría añadir una columna en db para esto pero al ser del cliente no se puede)
		foreach ($entries as $clave => &$cabecera) {

			$cabecera["decimales"] = NULL;
			$cabecera["alineacion"] = NULL;

			switch ($cabecera["formato"]) {
				case 'prec1d':
					$cabecera["decimalesAngular"] = '1.1-1';
					$cabecera["decimalesAmcharts"] = '#,###.0';
					$cabecera["decimales"] = 1;
					$cabecera["alineacion"] = "d";
					break;
				case 'dec0d_1k6':
					$cabecera["decimalesAngular"] = '1.0-0';
					$cabecera["decimalesAmcharts"] = '#,###';
					$cabecera["decimales"] = 0;
					$cabecera["alineacion"] = "d";
					break;
				case 'dec4d':
					$cabecera["decimalesAngular"] = '1.4-4';
					$cabecera["decimalesAmcharts"] = '#,###.0000';
					$cabecera["decimales"] = 4;
					$cabecera["alineacion"] = "d";
					break;
				case 'dec3d':
					$cabecera["decimalesAngular"] = '1.3-3';
					$cabecera["decimalesAmcharts"] = '#,###.000';
					$cabecera["decimales"] = 3;
					$cabecera["alineacion"] = "d";
					break;
				case 'dec2d':
					$cabecera["decimalesAngular"] = '1.2-2';
					$cabecera["decimalesAmcharts"] = '#,###.00';
					$cabecera["decimales"] = 2;
					$cabecera["alineacion"] = "d";
					break;
				case 'dec1d':
					$cabecera["decimalesAngular"] = '1.1-1';
					$cabecera["decimalesAmcharts"] = '#,###.0';
					$cabecera["decimales"] = 1;
					$cabecera["alineacion"] = "d";
					break;
				case 'dec0d':
					$cabecera["decimalesAngular"] = '1.0-0';
					$cabecera["decimalesAmcharts"] = '#,###.';
					$cabecera["decimales"] = 0;
					$cabecera["alineacion"] = "d";
					break;
				case 'txti':
					$cabecera["alineacion"] = "i";
					break;
				case 'txtc':
					$cabecera["alineacion"] = "c";
					break;
				case 'phonec':
					$cabecera["alineacion"] = "c";
					break;
			}
		}
	}


	function Meteoalerta() {
		$query = "SELECT row_to_json(fc)
		FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As data
		FROM (SELECT 'Feature' As type, 
		   ST_AsGeoJSON(lg.geom)::json As geometry, 
		   (
		   select row_to_json(t) from (select nom_z , nom_ccaa , nom_prov, cod_ccaa, estado, descripcion, fecha_expiracion as fecha_no_efectivo) t
		   )
		   As properties
		  FROM tbl_meteoalerta As lg   ) As f )  As fc;";
		$cabeceras = ConexionBD::EjecutarConsulta($query);
		return $cabeceras[0]["row_to_json"];
	}

	function MeteoMultialerta() {
		require_once APPPATH . '/libraries/visor/class_meteoalerta.php';
		return Meteoalerta::getMeteoAlertas();
	}

	/**
	 * Listado de estaciones, si se proporciona un array de ids de estación realiza un filtro con estas
	 */
	function Estaciones($idEstaciones = null)
	{

		$estacionesIn = "";
		if ($idEstaciones != null) $estacionesIn = " AND cod_estacion IN ('" . implode("','", $idEstaciones) . "')";

		$query = "select * from (
			select TRIM(cod_estacion) cod_estacion,nombre,tipo,destacado,rio,municipio,TRIM(zona) zona,subcuenca,provincia,comunidad,orden from tbl_estacion where disponible=true " . $estacionesIn . "
		) estaciones ORDER BY tipo,orden,zona,cod_estacion asc";

		$data = ConexionBD::EjecutarConsulta($query);
		return $data;
	}

	/**
	 * Listado de estaciones, si se proporciona un array de ids de estación realiza un filtro con estas
	 */
	function EstacionesV2($DB, $idEstaciones = null)
	{
		$estacionesIn = "";
		if ($idEstaciones != null) $estacionesIn = " AND e.cod_estacion IN ('" . implode("','", $idEstaciones) . "')";

		$query = "select * from (
			select TRIM(e.cod_estacion) cod_estacion, ST_AsGeoJSON(ST_Transform(geom,4326))::jsonb geom, nombre, tipo, destacado, rio, municipio, TRIM(zona) zona, cod_masasub, tezr.cod_zona_regable, subcuenca, provincia, comunidad, pluviometro, estacion_meteo, orden from tbl_estacion e left join tbl_estacion_zona_regable tezr on tezr.cod_estacion  = e.cod_estacion where disponible = true " . $estacionesIn . "
			UNION
			select TRIM(e.cod_estacion) cod_estacion, ST_AsGeoJSON(ST_Transform(geom,4326))::jsonb geom,nombre geom,'EMAEMET' as tipo,null as destacado,null as rio,municipio, null as zona, null as cod_masasub, null as cod_zona_regable, null as subcuenca,provincia,comunidad,null as pluviometro, null as estacion_meteo, null as orden from tbl_estacion_aemet e where disponible=true " . $estacionesIn . "
			UNION
			SELECT TRIM(cod_estacion) cod_estacion, ST_AsGeoJSON(ST_Transform(geom,4326))::jsonb geom, nombre, 'D' as tipo, destacado,rio,municipio,TRIM(zona) zona,cod_masasub AS null, null as cod_zona_regable, subcuenca,provincia,comunidad,null as pluviometro, null as estacion_meteo, orden from tbl_estacion e
			left join tbl_tipo_estacion tte on tte.cod_tipo_estacion = e.tipo 
			where tipo like 'D%' and geom IS NOT NULL 
					
		) estaciones ORDER BY tipo,orden,zona,cod_estacion asc";

		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);

		foreach($data as &$estacion) {
			if (isset($estacion['geom']) && $estacion['geom'] != null) {
				$geometry = json_decode($estacion['geom']);
				$coordinates = $geometry->coordinates;
				$estacion['longitud'] = $coordinates[0];
				$estacion['latitud'] = $coordinates[1];
			}
			unset($estacion['geom']);
		}
		return $data;
	}

	function Masas($DB)
	{
		$query = "select code as cod_masa, nombre_masasub as nombre from tbl_masasub order by cod_masa asc";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);
		return $data;
	}

	function ZonasRegables($DB)
	{
		$query = "select cod_zona_regable, descripcion as nombre from tbl_zona_regable order by cod_zona_regable asc";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);
		return $data;
	}

	function ConfAlarmas_tr($DB)
	{
		$query = "select * from tbl_color_alarmas_tr";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);
		return $data;
	}

	function TiposEstacion($DB){
		$query = "select cod_tipo_estacion, descripcion from tbl_tipo_estacion order by cod_tipo_estacion asc";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);
		return $data;
	}

	function EstacionesEnProyecto($DB)
	{
		$query = "SELECT TRIM(cod_estacion) cod_estacion, nombre, tipo, destacado,rio,municipio,TRIM(zona) zona,subcuenca,provincia,comunidad,orden,latitud,longitud, tte.descripcion as estado 
		from tbl_estacion e
		left join tbl_tipo_estacion tte on tte.cod_tipo_estacion = e.tipo 
		where tipo like 'D%' and geom IS NOT NULL 
		ORDER BY orden asc, cod_estacion asc";

		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);

		$datosRetorno = array("encabezado" => ["cod_estacion","nombre","estado"], "valores" => $data);

		return $datosRetorno;
	}

	function ZonasRegablesGeometry()
	{
		$query = "SELECT jsonb_build_object(
			'type',     'FeatureCollection',
			'features', jsonb_agg(feature)
		)
		FROM (
		  SELECT jsonb_build_object(
			'type',       'Feature',
			'id',         cod_zona_regable,
			'geometry',   ST_AsGeoJSON(ST_Transform(geom,4326))::jsonb,
			'properties', to_jsonb(row) - 'cod_zona_regable' - 'geom'
		    ) AS feature
		    FROM (SELECT * FROM tbl_zona_regable) row) features;";
		$cabeceras = ConexionBD::EjecutarConsulta($query);
		return $cabeceras[0]["jsonb_build_object"];
	}

	function MasasSubterraneas()
	{
		$query = "SELECT jsonb_build_object(
			'type',     'FeatureCollection',
			'features', jsonb_agg(feature)
		)
		FROM (
		  SELECT jsonb_build_object(
			'type',       'Feature',
			'id',         id,
			'geometry',   ST_AsGeoJSON(ST_Transform(geom,4326))::jsonb,
			'properties', to_jsonb(row) - 'id' - 'geom'
		  ) AS feature
		  FROM (SELECT * FROM tbl_masasub) row) features;";
		$cabeceras = ConexionBD::EjecutarConsulta($query);
		return $cabeceras[0]["jsonb_build_object"];
	}

	/*function CuencaGuadiana()
	{
		$query = "SELECT jsonb_build_object(
			'type',     'FeatureCollection',
			'features', jsonb_agg(feature)
		)
		FROM (
		  SELECT jsonb_build_object(
			'type',       'Feature',
			'id',         id,
			'geometry',   ST_AsGeoJSON(ST_Transform(geom,4326))::jsonb,
			'properties', to_jsonb(row) - 'id' - 'geom'
		  ) AS feature
		  FROM (SELECT * FROM tbl_demar_hidro) row) features;";
		$cabeceras = ConexionBD::EjecutarConsulta($query);
		return $cabeceras[0]["jsonb_build_object"];
	}*/

	function codigoEstacionesDisponibles($tipo){
		$return = array();
		$query = "select cod_estacion from tbl_estacion where disponible=true and tipo='".$tipo."'";
		$estaciones = ConexionBD::EjecutarConsulta($query);
		foreach($estaciones as $estacion){
			$return[] = trim($estacion['cod_estacion']);
		}
		return $return;
	}

	function codigoMasasDisponibles(){
		$return = array();
		$query = "select distinct cod_masasub from tbl_estacion where disponible=true and cod_masasub notnull";
		$masasubs = ConexionBD::EjecutarConsulta($query);
		foreach($masasubs as $masasub){
			$return[] = $masasub['cod_masasub'];
		}
		return $return;
	}

	function getZonaEstacion($cod_estacion)
	{ // obtener la zona de la estacion
		$query = "select TRIM(zona) zona from tbl_estacion where cod_estacion = '" . $cod_estacion . "' and disponible=true";
		$data = ConexionBD::EjecutarConsulta($query);
		if (isset($data) && isset($data[0]) && isset($data[0]["zona"])) return $data[0]["zona"];
		return false;
	}

	/**
	 * Retorna los valoes iniciales,finales, máximos y mínimos diarios del volumen de un embalse en el rango de fechas proporcionado
	 */
	function VolumenMaxMinDiarioEmbalse($idEmbalse, $fechaMin, $fechaFin)
	{
		return Graficas::getVolumenMaxMinDiarioEmbalse($idEmbalse, $fechaMin, $fechaFin);
	}

	function VolumenMaxMinSemanalEmbalse($idEmbalse, $fechaMin, $fechaFin)
	{
		return Graficas::getVolumenMaxMinSemanalEmbalse($idEmbalse, $fechaMin, $fechaFin);
	}

	function VolumenDiarioEmbalse($idEmbalse, $fechaMin, $fechaFin)
	{
		return Graficas::getVolumenDiarioEmbalse($idEmbalse, $fechaMin, $fechaFin);
	}

	function PrecipitacionEstacion($idEstacion, $fechaMin)
	{
		return Graficas::getPrecipitacionEstacion($idEstacion, $fechaMin);
	}

	function NivelVolumenEstacion($idEstacion, $fechaMin)
	{
		return Graficas::getNivelVolumenEstacion($idEstacion, $fechaMin);
	}

	function CamarasEmbalse($idEmbalse)
	{
		return Video::CamarasEmbalse($idEmbalse);
	}

	function CamarasRedes($idEstacion)
	{
		return Video::CamarasRedes($idEstacion);
	}

	function CamarasRedesGeneral($category)
	{
		return Video::CamarasRedesGeneral($category);
	}

	function CamarasZona($idZona)
	{
		return Video::CamarasZona($idZona);
	}

	function CamarasEmbalses()
	{
		return Video::CamarasAEX();
	}

	function CamarasAex()
	{
		return Video::CamarasAEX();
	}

	function InformeEstacion($codEstacion, $tipo, $periodo, $fecha_i, $fecha_f)
	{
		require_once APPPATH . '/libraries/visor/class_informe.php';
		return Informe::InformeEstacion($codEstacion, $tipo, $periodo, $fecha_i, $fecha_f);
	}


	function ExtDirectorio($DB, $category) {

		switch ($category) {
			case ($category == "AEX"):
				$query = "SELECT e.cod_estacion, nombre, zona, cod_extension, extension, descripcion FROM tbl_estacion e JOIN tbl_presa p ON trim(e.cod_estacion) = trim(p.cod_estacion) JOIN tbl_extension ex ON p.acronimo = split_part(ex.cod_extension,'/',1) ORDER by cod_extension asc";
				$extQuery = "SELECT distinct split_part(cod_extension,'/',2) cod_campo FROM tbl_estacion e JOIN tbl_presa p ON trim(e.cod_estacion) = trim(p.cod_estacion) JOIN tbl_extension ex ON p.acronimo = split_part(ex.cod_extension,'/',1) ORDER by cod_campo asc";
				break;
			case ($category == "Z12" or $category == "Z3" or $category == "Z5" or $category == "Z67"):
				$query = "SELECT e.cod_estacion, nombre, zona, cod_extension, extension, descripcion FROM tbl_estacion e JOIN tbl_presa p ON trim(e.cod_estacion) = trim(p.cod_estacion) JOIN tbl_extension ex ON p.acronimo = split_part(ex.cod_extension,'/',1) WHERE TRIM(e.zona) like '" . $category . "' ORDER by cod_extension asc";
				$extQuery = "SELECT distinct split_part(cod_extension,'/',2) cod_campo FROM tbl_estacion e JOIN tbl_presa p ON trim(e.cod_estacion) = trim(p.cod_estacion) JOIN tbl_extension ex ON p.acronimo = split_part(ex.cod_extension,'/',1) WHERE TRIM(e.zona) = '" . $category . "' ORDER by cod_campo asc";
				break;
		}

		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);

		// cumplimentar encabezado
		$campos_extension = array_map(fn($f) => $f['cod_campo'], ConexionBD::EjecutarConsultaConDB($DB, $extQuery));
		//$encabezado = array_merge( ["zona", "cod_estacion", "nombre"], $encabezados_extension);

		$encabezadoV2 = [
			["id" => "zona", "cod_campo" => "zona"],
			["id" => "cod_estacion", "cod_campo" => "cod_estacion", "sticky" => true],
			["id" => "nombre", "cod_campo" => "nombre"]
		];

		foreach($campos_extension as $campoExt){
			$encabezadoV2[] = ["id" => $campoExt, "cod_campo" => $campoExt, "label" => $campoExt];
		}

		$map = [];
		foreach($data as $row) {
			$est= $row['cod_estacion'];
			if (!isset($map[$est])) {
				$map[$est] = [
					'cod_estacion' => $est,
					'zona' => $row['zona'],
					'nombre' => $row['nombre'],
				];
			}
			$extension = explode("/", $row['cod_extension'])[1];
			$map[$est][$extension] = $row['extension'];
		}

		$datos = array_values($map);
		usort($datos, fn($d1, $d2) => $d1['zona'] === $d2['zona'] ? strcmp($d1['cod_estacion'], $d2['cod_estacion']) : strcmp($d1['zona'], $d2['zona']));
		$result = [ "encabezado" => $encabezadoV2, "valores" => $datos ];
		return $result;
	}

	function EvolucionEMB($ids, $periodo, $fecha_i, $fecha_f)
	{
		require_once APPPATH . '/libraries/visor/class_evolucion.php';
		return Evolucion::getEvolucionEmbalse($ids, $periodo, $fecha_i, $fecha_f);
	}

	function EvolucionCR($ids, $periodo, $fecha_i, $fecha_f)
	{
		require_once APPPATH . '/libraries/visor/class_aforos.php';
		return Aforos::getEvolucionAforos($ids, $periodo, $fecha_i, $fecha_f);
	}

	function EvolucionNR($ids, $periodo, $fecha_i, $fecha_f)
	{
		require_once APPPATH . '/libraries/visor/class_nivel_rio.php';
		return NivelRio::getEvolucionNivelRio($ids, $periodo, $fecha_i, $fecha_f);
	}

	function EvolucionEXC($ids, $periodo, $fecha_i, $fecha_f)
	{
		require_once APPPATH . '/libraries/visor/class_externas.php';
		return IndustrialExternos::getIndustrialExternos($ids, $periodo, $fecha_i, $fecha_f);
	}

	function EvolucionSDAMULTI($id, $timestamp)
	{
		require_once APPPATH . '/libraries/visor/class_sondas.php';
		return Sondas::getEvolucionSondas($id, $timestamp);
	}

	function EvolucionPZ($ids, $fecha_i, $fecha_f)
	{
		require_once APPPATH . '/libraries/visor/class_piezometros.php';
		return Piezometros::getEvolucionPiezometro($ids, $fecha_i, $fecha_f);
	}

	function EvolucionMASASUB($id_PZ, $id_CR, $id_EM, $periodo, $fecha_i, $fecha_f)
	{
		require_once APPPATH . '/libraries/visor/class_masasub.php';
		return MasasSubterraneas::getEvolucion($id_PZ, $id_CR, $id_EM, $periodo, $fecha_i, $fecha_f);
	}

	function TimestampsSondas($id)
	{
		require_once APPPATH . '/libraries/visor/class_sondas.php';
		return Sondas::getTimestampsSondas($id);
	}

	function ProfundidadesSonda($id)
	{
		require_once APPPATH . '/libraries/visor/class_sondas.php';
		return Sondas::getProfundidadesSonda($id);
	}

	function InformeSonda($cod_estacion, $profundidad, $fecha_inicio, $fecha_fin)
	{
		require_once APPPATH . '/libraries/visor/class_sondas.php';
		return Sondas::getInformeSonda($cod_estacion, $profundidad, $fecha_inicio, $fecha_fin);
	}

	function getListadoSondas($DB)
	{
		require_once APPPATH . '/libraries/visor/class_sondas.php';
		return Sondas::getSondas($DB);
	}

	function getListadoACA($DB)
	{
        $query = "SELECT cod_estacion, nombre, latitud,longitud, tipo, '' as timestamp, disponible FROM tbl_estacion where tipo = 'ACA' and disponible = true order by orden";
        $data = ConexionBD::EjecutarConsultaConDB($DB, $query);
		$datosRetorno = array("encabezado" => ["cod_estacion","nombre"], "valores" => $data);
        return $datosRetorno;
	}


	function ValorDiarioEmbalse($idEmbalse, $fecha_i, $fecha_f)
	{
		return Graficas::getValoresDiariosEMB($idEmbalse, $fecha_i, $fecha_f);
	}

	function getValoresSAICA($codigo, $tiempo, $fInicial, $fFinal)
	{
		return GraficasSaica::getValoresSAICA($codigo, $tiempo, $fInicial, $fFinal);
	}

	function getValoresREACAR($codigo, $tiempo, $fInicial, $fFinal)
	{
		return GraficasReacar::getValoresREACAR($codigo, $tiempo, $fInicial, $fFinal);
	}

	function getPhotosReacar($estacion)
	{
		return VideoReacar::getTimelapse($estacion);
	}

	function getPhotoReacar($estacion, $fichero)
	{
		return VideoReacar::getPhoto($estacion, $fichero);
	}

	function getValoresEXC($codigo, $tiempo, $fInicial, $fFinal)
	{
		require_once APPPATH . '/libraries/visor/class_externas.php';
		return IndustrialExternos::getValoresEXC($codigo, $tiempo, $fInicial, $fFinal);
	}

	function getTimelapsePhotos($estacion){
		return VideoReacar::getTimelapsePhotos($estacion);
	}

	function GuardarRecursoCache($DB, $idRecurso, $json){
		return Cache::GuardarRecursoCache($DB, $idRecurso, $json);
	}

	function CargarRecursoCache($DB, $idRecurso){
		return Cache::CargarRecursoCache($DB, $idRecurso);
	}

	function GuardarUltimosDatos($DB, $datos){
		return Cache::GuardarUltimosDatos($DB, $datos);
	}

	function CargarUltimosDatos($DB){
		return Cache::CargarUltimosDatos($DB);
	}

	function GuardarMasterULTRESCategorias($DB, $datos){
		return Cache::GuardarMasterULTRESCategorias($DB, $datos);
	}

	function CargarMasterULTRESCategorias($DB, $idCategoria){
		return Cache::CargarMasterULTRESCategorias($DB, $idCategoria);
	}

	function GuardarMasterULTRESEstaciones($DB, $datos){
		return Cache::GuardarMasterULTRESEstaciones($DB, $datos);
	}

	function CargarMasterULTRESEstaciones($DB ,$idEstacion){
		return Cache::CargarMasterULTRESEstaciones($DB ,$idEstacion);
	}

	function GetInfo($DB, $codInfo){
		$query = "select titulo, texto, texto_corto as descripcion from tbl_info where cod_info like '".$codInfo."'";
		$data = ConexionBD::EjecutarConsultaConDB($DB, $query);

		if(isset($data) && count($data)>0){
			return $data[0];
		}

		return ["descripcion"=> null, "texto" => "<p>Información pendiente de elaborar.</p>", "titulo" => null];

	}

	function getVersion()
	{
		$query = "select * from tbl_version_visor order by timestamp desc";
		$data = ConexionBD::EjecutarConsulta($query);
		return $data;
	}


	function getEsquemaV2($DB, $id){
		return Esquemas::getEsquemaV2($DB, $id);
	}

	function getEsquema($DB, $id){
		return Esquemas::getEsquema($DB, $id);
	}

	function getEsquemaFecha($DB, $id, $fecha){
		$cabeceras = $this->visorDao->CabecerasV2();
		return Esquemas::getEsquemaFecha($DB, $id, $fecha, $cabeceras);
	}

	function caudales($DB)
	{
		$datos_recientes = json_decode(Cache::CargarRecursoCache($DB, "rtap_reciente_esquema_aex"), true);
		$datos_anteriores = json_decode(Cache::CargarRecursoCache($DB, "rtap_anterior_esquema_aex"), true);
		$esquemaHTML = EsquemaExplotacion::getCalculosQSalida($datos_anteriores, $datos_recientes);
		return $esquemaHTML;
	}

	function getEsquemaPiezo($id) {
		require_once APPPATH . '/libraries/visor/class_ficha_piezometro.php';
		return FichaPiezometro::getFicha($id);
	}

	function hasEsquemaPiezo($id) {
		require_once APPPATH . '/libraries/visor/class_ficha_piezometro.php';
		return FichaPiezometro::fichaExists($id);
	}

	function getListadoPiezoManual($DB){
		require_once APPPATH . '/libraries/visor/class_piezometros_manuales.php';
		return PiezometrosManuales::getPiezosManuales($DB);
	}

	function getPiezosListForm() {
		require_once APPPATH . '/libraries/visor/class_piezometros_manuales.php';
		return PiezometrosManuales::getPiezosListForm();
	}

	function getPiezosManualesHist($idPiezoManual) {
		require_once APPPATH . '/libraries/visor/class_piezometros_manuales.php';
		return PiezometrosManuales::getPiezosManualesHist($idPiezoManual);
	}

	function saveDatoPiezoManual($datos)
	{
		$data = ConexionBD::Insert('tbl_dato_piezo_manual', $datos);
		return $data;
	}

	function saveDatoPiezoManualV2($datos)
	{
		require_once APPPATH . '/libraries/visor/class_piezometros_manuales.php';
		return PiezometrosManuales::savePiezosManualesV2($datos);
		return $datos;
	}

	function deleteDatoPiezoManualV2($datos)
	{
		require_once APPPATH . '/libraries/visor/class_piezometros_manuales.php';
		return PiezometrosManuales::deletePiezosManualesV2($datos);
		return $datos;
	}

	function UltimosDatosEmbalseAemet($idEstaciones, $idEmbalse, $DB){
		require_once APPPATH . '/libraries/visor/class_aemet.php';
		$data = AEMET::UltimosDatosEmbalseAemet($idEstaciones, $idEmbalse, $DB);
		return $data;
	}

	function UltimosDatosEmbalseSAIH($idEstaciones, $idEmbalse, $DB){
		require_once APPPATH . '/libraries/visor/class_lluviasaih.php';
		$data = LLUVIASAIH::UltimosDatosEmbalseSAIH($idEstaciones, $idEmbalse, $DB);
		return $data;
	}

	// datos pluiométricos cargados en caché

	function UltimosDatosAemet($idEstaciones, $DB){
		$datosPuntualesAEMET = json_decode($this->visorDao->CargarRecursoCache($DB, "datosPuntualesAEMET"), true);
		return $this->visorDao->filtrarValoresEstacionesPluviometria($idEstaciones, $datosPuntualesAEMET);
	}

	function UltimosDatosLluviaSAIH($idEstaciones, $DB){
		$datosPuntualesSAIH = json_decode($this->visorDao->CargarRecursoCache($DB, "datosPuntualesSAIH"), true);
		return $this->visorDao->filtrarValoresEstacionesPluviometria($idEstaciones, $datosPuntualesSAIH);
	}

	function UltimosDatosEmbalsesAemet($idEstaciones, $DB, $zona = null){
		$datosThiessenEmbalsesAEMET = json_decode($this->visorDao->CargarRecursoCache($DB, "datosThiessenEmbalsesAEMET"), true);
		return $this->visorDao->filtrarValoresEstacionesPluviometria($idEstaciones, $datosThiessenEmbalsesAEMET, $zona);
	}

	function UltimosDatosEmbalsesLluviaSAIH($idEstaciones, $DB, $zona = null){
		$datosPuntualesEmbalsesSAIH = json_decode($this->visorDao->CargarRecursoCache($DB, "datosPuntualesEmbalsesSAIH"), true);
		return $this->visorDao->filtrarValoresEstacionesPluviometria($idEstaciones, $datosPuntualesEmbalsesSAIH, $zona);
	}

	function UltimosDatosEmbalsesLluviaPonderadaSAIH($idEstaciones, $DB, $zona = null){
		$datosThiessenEmbalsesSAIH = json_decode($this->visorDao->CargarRecursoCache($DB, "datosThiessenEmbalsesSAIH"), true);
		return $this->visorDao->filtrarValoresEstacionesPluviometria($idEstaciones, $datosThiessenEmbalsesSAIH, $zona);
	}

	function filtrarValoresEstacionesPluviometria($idEstaciones, $datos, $idZona = null){
		$retorno_datos = [];

		if(isset($datos['encabezado'])){
			$retorno_datos['encabezado'] = $datos['encabezado'];
		
		}

		if(isset($datos['valores'])){
			$retorno_datos['valores'] = [];
			foreach($datos['valores'] as $valor){
				// si hay zona debe filtrarse por esta
				if(($idZona!=null && isset($valor['zona']) && $idZona == trim($valor['zona'])) || $idZona == null){
					// si hay codigo de estación debe filtrarse por la lista de estaciones recibida / del usuario
					if((isset($valor['cod_estacion']) && in_array($valor["cod_estacion"], $idEstaciones)) || !isset($valor['cod_estacion'])){
						$retorno_datos['valores'][] = $valor;
					}
				}
			}
		}

		return $retorno_datos;
	}

	// fin datos pluviométricos cargados desde caché

	function InfoMASb($id, $DB){
		require_once APPPATH . '/libraries/visor/class_masasub.php';
		return MasasSubterraneas::InfoMASb($id, $DB);
	}

	function getResumenEstaciones($DB){
		require_once APPPATH . '/libraries/visor/class_administrador.php';

		// obtener configuración de alarmas de timestamp de dato
		$confAlarmas = $this->visorDao->ConfAlarmas_tr($DB);    
		// obtener configuración listado de tipos de estación
		$tiposEstacion = $this->visorDao->TiposEstacion($DB);  
		// obtener los últimos datos tr de todas las estaciones
		$masterUltRes = json_decode($this->visorDao->CargarRecursoCache($DB, "master_ultres_categorias"), true);

		$data = Administrador::getResumenEstaciones($confAlarmas, $tiposEstacion, $masterUltRes);

		return $data;
	}

	function getResumenCamaras($DB){
		require_once APPPATH . '/libraries/visor/class_administrador_camaras.php';
		$data = AdministradorCamaras::getResumenCamaras();
		return $data;
	}

	function getPermisosElementos($DB){
		require_once APPPATH . '/libraries/visor/class_administrador.php';
		$data = Administrador::getPermisosElementos($DB);
		return $data;
	}
}
