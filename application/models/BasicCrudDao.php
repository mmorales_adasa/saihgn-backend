<?php
class BasicCrudDao extends CI_Model 
{
	function getData($entity, $filters, $sort)
	{				
		/*if (in_array($entity,Constants::ENTITIES_SERVER_27)){
			$db = $this->load->database('visor', TRUE);
		}
		else{
			$db  = $this->db;
		}*/
		$db  = $this->db;
		$sql = Queries::BASIC_ENTITY[$entity]["get"];
		$sql.= $this->utildao->applyFilters($filters);		
		
		if (isset($sort)){			
			$sql.= $this->utildao->applySort($sort);
		}
	
		$query = $db->query($sql);	

		return $query->result();			
	}
	function save($entity,$mode,$model)
	{	
							
		$sql = Queries::BASIC_ENTITY[$entity]["update"];
		if ($mode==Constants::DATABASE_OPERATION_INSERT){
			$sql = Queries::BASIC_ENTITY[$entity]["insert"];
		}
		else if ($mode==Constants::DATABASE_OPERATION_DELETE){
			$sql = Queries::BASIC_ENTITY[$entity]["delete"];
		}		
		$parts = explode(";", $sql);
		foreach ($parts as $sql1){
			if (trim($sql1) != ""){
				$query= $this->utildao->bindingParameters($sql1, $model);
				//echo $query;
				log_message('debug', $query);
				$this->db->query($query);	
			}
		}
		return true;		
	} 	
	function getLastId($entity){
		if (isset(Queries::BASIC_ENTITY[$entity]["lastId"])){
			$sql = Queries::BASIC_ENTITY[$entity]["lastId"];
			$query = $this->db->query($sql);
			return $query->result()[0]->id;
		}
		return null;
	}
	
	function getDataPagination($entity, $limit, $offset, $filters, $sort)
	{		
		if (in_array($entity,Constants::ENTITIES_SERVER_27)){
			$db = $this->load->database('visor', TRUE);
		}
		else{
			$db  = $this->db;
		}	

		$sql  = Queries::BASIC_ENTITY[$entity]["get"];
        $sql.= $this->utildao->applyFilters($filters);
		$sql.= $this->utildao->applySort($sort);		
		$sql.= $this->utildao->applyLimit($limit, $offset);				
		//echo $sql;
		$query = $db->query($sql);        
		log_message('info', $sql);

		return $query->result();		
    }    
	function getDataPaginationCount($entity, $filters)
	{		
		if (in_array($entity,Constants::ENTITIES_SERVER_27)){		
			$db = $this->load->database('visor', TRUE);
		}
		else{
			$db  = $this->db;
		}	
		if (isset(Queries::BASIC_ENTITY[$entity]["count"])){
			$sql  = Queries::BASIC_ENTITY[$entity]["count"];
			$sql.= $this->utildao->applyFilters($filters);		
			$query = $db->query($sql);
			//log_message('info', $sql);
			return $query->result()[0]->total;	
		}
		else{
			$sql  = Queries::BASIC_ENTITY[$entity]["get"];
			$sql.= $this->utildao->applyFilters($filters);		
			$query = $db->query($sql);
			return sizeOf($query->result());		
		}
	}        
}
?>