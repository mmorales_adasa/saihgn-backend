<?php
class SecurityDao extends CI_Model 
{
    public function __construct(){
        parent::__construct();        
	}
	
	function getPermisosNivel($cod_nivel_acceso){	
		$params = array();
		$params['cod_nivel_acceso']['value'] = trim($cod_nivel_acceso);
		$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["getPermisos"], $params);	
		$sql.= $this->utildao->applySort(" cod_elemento asc");
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	function getInformesNivel($cod_nivel_acceso){	
		$params = array();
		$params['cod_nivel_acceso']['value'] = trim($cod_nivel_acceso);
		$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["getInformes"], $params);	
		$sql.= $this->utildao->applySort(" cod_informe asc");
		$query = $this->db->query($sql);		
		return $query->result();	
	}	
	function getVideosNivel($cod_nivel_acceso){	
		$params = array();
		$params['cod_nivel_acceso']['value'] = trim($cod_nivel_acceso);
		$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["getVideos"], $params);	
		$sql.= $this->utildao->applySort(" cod_video asc");
		$query = $this->db->query($sql);		
		return $query->result();	
	}		
	function getPermisosUsuario($cod_usuario){	
		$params = array();
		$params['cod_usuario']['value'] = trim($cod_usuario);
		$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["getPermisos"], $params);	
		$sql.= $this->utildao->applySort(" cod_elemento asc");
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	function getInformesUsuario($cod_usuario){	
		$params = array();
		$params['cod_usuario']['value'] = trim($cod_usuario);
		$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["getInformes"], $params);	
		$sql.= $this->utildao->applySort(" cod_informe asc");
		$query = $this->db->query($sql);		
		return $query->result();	
	}	
	function getVideosUsuario($cod_usuario){	
		$params = array();
		$params['cod_usuario']['value'] = trim($cod_usuario);
		$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["getVideos"], $params);	
		$sql.= $this->utildao->applySort(" cod_video asc");
		$query = $this->db->query($sql);		
		return $query->result();	
	}		
	function getUbicacionesUsuario($cod_usuario){	
		$params = array();
		$params['cod_usuario']['value'] = trim($cod_usuario);
		$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["getUbicaciones"], $params);	
		$sql.= $this->utildao->applySort(" nombre_ubicacion asc");
		$query = $this->db->query($sql);		
		return $query->result();	
	}		
	function getPermisosDataUsuario2($cod_usuario){	
		$params = array();
		$params['cod_usuario']['value'] = trim($cod_usuario);
		$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["getPermisosAll"], $params);	
		$query = $this->db->query($sql);		
		return $query->result();	
	}		
	function getPermisosDataNivel($cod_nivel_acceso){	
		$params = array();
		$params['cod_nivel_acceso']['value'] = trim($cod_nivel_acceso);
		$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["getPermisosData"], $params);	
		$sql.= $this->utildao->applySort(" cod_tipo_item asc, cod_item asc");
		$query = $this->db->query($sql);		
		return $query->result();	 
	}	
	function getPermisosDataUsuario($cod_usuario){	
		$params = array();
		$params['cod_usuario']['value'] = trim($cod_usuario);
		$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["getPermisosData"], $params);	
		$sql.= $this->utildao->applySort(" cod_tipo_item asc, cod_item asc");
		$query = $this->db->query($sql);		
		return $query->result();	 
	}	

	function getPermisosElemento($cod_elemento){	
		$params = array();
		$params['cod_elemento']['value'] = trim($cod_elemento); 
		$params['cod_elemento']['type'] = Constants::TYPE_TEXT;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["ELEMENTOS_VISOR"]["getPermisosNivel"], $params);	
		$query = $this->db->query($sql);		
		return $query->result();	
	}
	function getPermisosVideo($cod_video){	
		$params = array();
		$params['cod_video']['value'] = trim($cod_video); 
		$params['cod_video']['type'] = Constants::TYPE_TEXT;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CAMARAS"]["getPermisosNiveles"], $params);	
		$query = $this->db->query($sql);		
		return $query->result();	
	}	
	function savePermisosUsuario($permisos,  $estaciones, $videos, $informes,  $cod_usuario){      
		$params['cod_usuario']['value'] =$cod_usuario; 
		$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["deletePermisosElementos"], $params);	
		$query = $this->db->query($sql);				
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["deletePermisosVideos"], $params);	
		$query = $this->db->query($sql);				
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["deletePermisosInformes"], $params);	
		$query = $this->db->query($sql);						
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["deletePermisosDatos"], $params);	
		$query = $this->db->query($sql);			

		foreach ($permisos as $item) { 
				$params['cod_elemento']['value'] =$item['cod']; 
				$params['cod_elemento']['type'] = Constants::TYPE_TEXT;
				$params['cod_usuario']['value'] =$cod_usuario; 
				$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
				$params['visible']['value'] = $item['visible']; 
				$params['visible']['type'] = Constants::TYPE_INTEGER;						
				$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["insertSecUsuarioElemento"], $params);	
				$query = $this->db->query($sql);	
			}						
			foreach ($estaciones as $item) { 
				$params['cod_tipo_item']['value'] =$item['tipo']; 
				$params['cod_tipo_item']['type'] = Constants::TYPE_TEXT;
				$params['cod_item']['value'] =$item['cod']; 
				$params['cod_item']['type'] = Constants::TYPE_TEXT;				
				$params['cod_usuario']['value'] =$cod_usuario; 
				$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
				$params['visible']['value'] =$item['visible']; 
				$params['visible']['type'] = Constants::TYPE_INTEGER;

				$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["insertSecNivelDato"], $params);	
				$query = $this->db->query($sql);	
		}	
		foreach ($videos as $item) { 
			$params['cod_video']['value'] =$item['cod']; 
			$params['cod_video']['type'] = Constants::TYPE_TEXT;
			$params['cod_usuario']['value'] =$cod_usuario; 
			$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
			$params['visible']['value'] = $item['visible']; 
			$params['visible']['type'] = Constants::TYPE_INTEGER;						
			$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["insertSecUsuarioVideo"], $params);	
			$query = $this->db->query($sql);	
		}			
		foreach ($informes as $item) { 
			$params['cod_informe']['value'] =$item['cod']; 
			$params['cod_informe']['type'] = Constants::TYPE_INTEGER;
			$params['cod_usuario']['value'] =$cod_usuario; 
			$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;
			$params['visible']['value'] = $item['visible']; 
			$params['visible']['type'] = Constants::TYPE_INTEGER;						
			$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["insertSecUsuarioInforme"], $params);	
			$query = $this->db->query($sql);	
		}

			return true;					
	}
	function savePermisosVideo($niveles, $cod_video){  
		
		$params['cod_video']['value'] = $cod_video; 
		$params['cod_video']['type']  =  Constants::TYPE_TEXT;
		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CAMARAS"]["deletePermisosNivel"], $params);	
		$query = $this->db->query($sql);	

		foreach ($niveles as $item) { 
			$params['cod_video']['value'] =$cod_video; 
			$params['cod_video']['type'] = Constants::TYPE_TEXT;			
		
			$params['cod_nivel_acceso']['value'] =$item['COD']; 
			$params['cod_nivel_acceso']['type']  = Constants::TYPE_INTEGER;			

			$params['visible']['value'] = 1; 
			$params['visible']['type'] = Constants::TYPE_INTEGER;			

			$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["CAMARAS"]["insertPermisosNivel"], $params);	
			$query = $this->db->query($sql);
			
			//echo "savePermisosVideo <". $cod_video .  ',' . $item['COD'] . '>';
		}
		return true;
	}
	function savePermisosUbicaciones($ubicaciones, $cod_usuario){  
		
		$params['cod_usuario']['value'] = $cod_usuario; 
		$params['cod_usuario']['type']  =  Constants::TYPE_INTEGER;
		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["deleteUbicaciones"], $params);	
		$query = $this->db->query($sql);	

		foreach ($ubicaciones as $item) { 
			$params['cod_usuario']['value'] =$cod_usuario; 
			$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;			
		
			$params['id_ubicacion']['value'] =$item['COD']; 
			$params['id_ubicacion']['type']  = Constants::TYPE_INTEGER;			

			$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["insertSecUbicacion"], $params);	
			$query = $this->db->query($sql);
			
			//echo "savePermisosVideo <". $cod_video .  ',' . $item['COD'] . '>';
		}
		return true;
	}	
	function savePermisosNivel($permisos, $estaciones,$videos, $informes, $cod_nivel_acceso){      
		$params['cod_nivel_acceso']['value'] =$cod_nivel_acceso; 
		$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["deletePermisosElementos"], $params);	
		$query = $this->db->query($sql);						
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["deletePermisosVideos"], $params);	
		$query = $this->db->query($sql);			
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["deletePermisosInformes"], $params);	
		$query = $this->db->query($sql);			
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["deletePermisosDatos"], $params);	
		$query = $this->db->query($sql);				
		
		foreach ($estaciones as $item) { 
				$params['cod_tipo_item']['value'] =$item['tipo']; 
				$params['cod_tipo_item']['type'] = Constants::TYPE_TEXT;
				$params['cod_item']['value'] =$item['cod']; 
				$params['cod_item']['type'] = Constants::TYPE_TEXT;				
				$params['cod_nivel_acceso']['value'] =$cod_nivel_acceso; 
				$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
				$params['visible']['value'] =$item['visible']; 
				$params['visible']['type'] = Constants::TYPE_INTEGER;

				$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["insertSecNivelDato"], $params);	
				$query = $this->db->query($sql);	
		}	
		
		foreach ($permisos as $item) { 
			
				$params['cod_elemento']['value'] =$item['cod']; 
				$params['cod_elemento']['type'] = Constants::TYPE_TEXT;
				$params['cod_nivel_acceso']['value'] =$cod_nivel_acceso; 
				$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
				$params['visible']['value'] =$item['visible']; 
				$params['visible']['type'] = Constants::TYPE_INTEGER;
				$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["insertSecNivelElemento"], $params);	
				$query = $this->db->query($sql);					
		}		

		foreach ($videos as $item) { 
			
			$params['cod_video']['value'] =$item['cod']; 
			$params['cod_video']['type'] = Constants::TYPE_TEXT;
			$params['cod_nivel_acceso']['value'] =$cod_nivel_acceso; 
			$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
			$params['visible']['value'] =$item['visible']; 
			$params['visible']['type'] = Constants::TYPE_INTEGER;
			$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["insertSecNivelVideo"], $params);	
			$query = $this->db->query($sql);					
		}		
		foreach ($informes as $item) { 
			
			$params['cod_informe']['value'] =$item['cod']; 
			$params['cod_informe']['type'] = Constants::TYPE_INTEGER;
			$params['cod_nivel_acceso']['value'] =$cod_nivel_acceso; 
			$params['cod_nivel_acceso']['type'] = Constants::TYPE_INTEGER;
			$params['visible']['value'] =$item['visible']; 
			$params['visible']['type'] = Constants::TYPE_INTEGER;
			$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["insertSecNivelInforme"], $params);	
			$query = $this->db->query($sql);					
		}			

		return true;					
	}	
    function savePermisosElementoNivel($data){      
		
		if (sizeof($data)>0){
			
			$params['cod_elemento']['value'] =$data[0]['cod_elemento']; 
			$params['cod_elemento']['type'] = Constants::TYPE_TEXT;
			$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["ELEMENTOS_VISOR"]["deleteSecNivel"], $params);	
			$query = $this->db->query($sql);							
		}		
		foreach ($data as $item) {  
			if ($item['visible'] !=-1){
				$params['cod_elemento']['value'] =$item['cod_elemento']; 
				$params['cod_elemento']['type'] = Constants::TYPE_TEXT;
				$params['cod_nivel_acceso']['value'] =$item['cod_nivel_acceso']; 
				$params['cod_nivel_acceso']['type'] = Constants::TYPE_TEXT;
				$params['visible']['value'] =isset($item['visible']) ? $item['visible'] : 0; 
				$params['visible']['type'] = Constants::TYPE_INTEGER;						
				$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["ELEMENTOS_VISOR"]["insertSecNivel"], $params);	
				$query = $this->db->query($sql);	
			}			
		}
		return true;
	}	
	function getUsers()
	{
       // $this->db = $this->load->database('dev_test', TRUE);

		$sql = "SELECT * FROM test ";
		$query = $this->db->query($sql);
		$origenes  = array();
		foreach ($query->result() as $row)foreach ($query->result() as $row)
		{
            array_push($origenes, $row->id);
            
		}
		return $origenes;	
	}
	function getSessionInfo($sessionId,&$profile){
		$decodedToken = AUTHORIZATION::validateToken($sessionId);
		$cod_usuario = $decodedToken->payload->sub;
		if ($this->getUserInfo($cod_usuario, $profile)){
			return true;
		}
		return false;
	}
	function getUserInfo($cod_usuario, &$user)
	{	
		$sql = "SELECT * FROM tbl_sec_usuarios T1 WHERE cod_usuario=? ";
		$results  = $this->db->query($sql, array($cod_usuario));
		$rows  = $results->result() ;
		$total=count($rows);
		if ($total>0){			
			$user = new User();
			$user->setCodUsuario($rows[0]->cod_usuario);
			$user->setUsername($rows[0]->username);
			$user->setEmail($rows[0]->email);
			$user->setNombre($rows[0]->nombre_completo);			
			$user->setPermisos(array());			
		}		
		return ($total>0);
	}

	function getPasswordByUsername($username){
		$sql = "SELECT password FROM tbl_sec_usuarios T1 WHERE username=? ";
		$results = $this->db->query($sql, array($username ));
		$rows = $results->result();
		if (count($rows)==1){
			return $rows[0]->password;
		} 
		return false;
	}

	function changePassword($username, $password){
		$sql = "UPDATE tbl_sec_usuarios set password=? WHERE email='pfranco@adasasistemas.com' and  username=?";
		$this->db->query($sql, array($password, $username ));
		return 1;
	}

	function changePasswordVisor($username, $password){
		$sql = "UPDATE tbl_sec_usuarios set password=? WHERE username=?";
		return $this->db->query($sql, array($password, $username ));
	}
	
	function recover($username)
	{	
		$sql = "SELECT * FROM tbl_sec_usuarios T1 WHERE origen='db' and username=? AND enabled=1 AND username  is not null";
		$results  = $this->db->query($sql, array($username));
		$rows  = $results->result() ;
		$total=count($rows);
		if ($total>0){		
			$pwd =  $this->randomPassword();	
			$config = array();
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = Constants::EMAIL_SMTP_HOST;
			$config['smtp_user'] = Constants::EMAIL_SMTP_USER;
			$config['smtp_pass'] = Constants::EMAIL_SMTP_PWD;
			$config['smtp_port'] = Constants::EMAIL_SMTP_PORT;
			$this->email->initialize($config);			
			$this->email->from(Constants::EMAIL_SMTP_FROM_EMAIL, Constants::EMAIL_SMTP_FROM_NAME);
			$this->email->to($rows[0]->email);
			$this->email->subject('Recuperación de constraseña');
			$this->email->message('Su nueva contraseña es ' . $pwd);
			if($this->email->send()){
					$params['password']['value'] =$pwd; 
					$params['password']['type'] = Constants::TYPE_TEXT;
					$params['cod_usuario']['value'] = $rows[0]->cod_usuario; 
					$params['cod_usuario']['type'] = Constants::TYPE_INTEGER;								
					$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["updatePassword"], $params);	
					$query = $this->db->query($sql);	
					return true;
			}else{
				return false;
			}
		}
		return false;
	}

	function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	function login($username, $password)
	{	
		$sql = "SELECT * FROM tbl_sec_usuarios T1 WHERE username=? AND password= ? AND enabled=1";
		$results  = $this->db->query($sql, array($username, $password));
		$rows  = $results->result() ;
		$total=count($rows);
		return ($total>0);
	}

	function getVisibleElements($permisos, $fieldKey ){
		$items  = array();
		foreach ($permisos as $row){
			//echo $row->cod_elemento . $row->visible;
			//echo "\n";
			if (is_null($row->visible)){
				if ($row->visible_nivel==1){
					array_push($items, $row->{$fieldKey}); 
				}
				else if (is_null($row->visible_nivel)){
					if ($row->visible_public==1){
						array_push($items, $row->{$fieldKey}); 
					}						
				}	
			}
			else if ($row->visible==1){					
				array_push($items, $row->{$fieldKey}); 
			}				
		}
		return $items;
	}
	function getVisibleEstaciones($permisos){
		$estaciones  = array();
		foreach ($permisos as $row){		
			
			//echo "\n";
			if (is_null($row->visible)){
				if ($row->visible_nivel==1){
					if (!array_key_exists($row->cod_tipo_item,$estaciones)){
						$estaciones[$row->cod_tipo_item] = [];
					}					
					array_push($estaciones[$row->cod_tipo_item], $row->cod_item); 					
				}
				else if (is_null($row->visible_nivel)){
					if ($row->visible_public==1){
						if (!array_key_exists($row->cod_tipo_item,$estaciones)){
							$estaciones[$row->cod_tipo_item] = [];
						}						
						array_push($estaciones[$row->cod_tipo_item], $row->cod_item); 						
					}						
				}	
			}
			else if ($row->visible==1){					
				if (!array_key_exists($row->cod_tipo_item,$estaciones)){
					$estaciones[$row->cod_tipo_item] = [];
				}				
				array_push($estaciones[$row->cod_tipo_item], $row->cod_item); 
			}				
		}
		return $estaciones;
	}	
	function createToken($cod_usuario, $elementos, $estaciones, $videos, $allow_sgi){
		if(!isset($estaciones)) $estaciones = [];
		if(!isset($videos)) $videos = [];
		$tokenData = array();
		$tokenData['header'] = array("typ" => "JWT", "alg" => "HS256");
		$tokenData['payload'] = array(
			'elementos' => $elementos,
			'estaciones' => $estaciones,
			'videos' => $videos,
			'sgi' => $allow_sgi,
			'sub' => $cod_usuario,
			'exp' => time() + (Constants::DAYS_TOKEN * 24 * 60 * 60),
			'iat' => time(),
			'jti' => 1
		); 
		$token = AUTHORIZATION::generateToken($tokenData);
		return $token;

	}
	function getProfile($username, $origin, $app,  &$profile)
	{	
		// Public profile
		if ($app == Constants::APP_VISOR && $username=='public'){
			$profile = new User();
			$profile->setCodUsuario($username);				
			$profile->setOrigen('');
			$profile->setUsername($username);
			$profile->setEmail("");				
			$profile->setNombre($username);
			$elementos  = array();
			$videos  = array();
			$estaciones = [];
			$permisos = $this->getPermisosNivel(Constants::NIVEL_ACCESO_PUBLIC);
			$permisosVideo = $this->getVideosNivel(Constants::NIVEL_ACCESO_PUBLIC);
			$permisosData = $this->getPermisosDataNivel(Constants::NIVEL_ACCESO_PUBLIC);
			foreach ($permisos as $row){
				if ($row->visible==1){
					array_push($elementos, $row->token_code); 
				}
			}	
			foreach ($permisosVideo as $row){
				if ($row->visible==1){
					array_push($videos, $row->cod_video); 
				}
			}						
			foreach ($permisosData as $row){
				
				if ($row->visible==1){
					if (!array_key_exists($row->cod_tipo_item,$estaciones)){
						$estaciones[$row->cod_tipo_item] = [];
					}
					array_push($estaciones[$row->cod_tipo_item], $row->cod_item); 
				}
			}						
			$token = $this->createToken($username,$elementos, $estaciones, $videos,  0);
			$profile->setSesionId($token);				
			return true;
		}

		$sql = "SELECT * FROM tbl_sec_usuarios T1 WHERE enabled=1 and username=? ";
		if ($app == Constants::APP_SGI)  
		{ 
			$sql.= " AND enabled_sgi=1 "; 
		}
		$results  = $this->db->query($sql, array($username));
		$rows  = $results->result() ;
		$total=count($rows);
		if ($total>0){			
			$profile = new User();
			$profile->setCodUsuario($rows[0]->cod_usuario);
			$profile->setOrigen($rows[0]->origen);
			$profile->setUsername($rows[0]->username);
			$profile->setEmail($rows[0]->email);
			$profile->setNombre($rows[0]->nombre_completo);		
			$profile->setEnabledSGI($rows[0]->enabled_sgi);	
			// Permisos a nivel de elemento
			$permisos           = $this->getPermisosUsuario($rows[0]->cod_usuario);
			$elementos          = $this->getVisibleElements($permisos, 'token_code');
			// Permisos a nivel de videos
			$permisosVideos     = $this->getVideosUsuario($rows[0]->cod_usuario);
			$videos             = $this->getVisibleElements($permisosVideos, 'cod_video');
			// Permisos a nivel de estacion
			$permisosData   = $this->getPermisosDataUsuario2($rows[0]->cod_usuario);
			$estaciones = $this->getVisibleEstaciones($permisosData);						
		
			$token = $this->createToken($rows[0]->cod_usuario,$elementos, $estaciones, $videos, $rows[0]->enabled_sgi);
			$profile->setSesionId($token);
			return true;		
		}
		else{
			if ($origin=="ldap"){	// Al SGI solo permito usarios internos
				if ($app == Constants::APP_SGI){
					return false;
				}
				else{		
					$params['username']['value'] = $username; 
					$params['username']['type']  = Constants::TYPE_TEXT;
					$params['password']['value'] = null; 
					$params['password']['type']  = Constants::TYPE_TEXT;
					$params['email']['value'] = $username . '@chguadiana.es'; 
					$params['email']['type']  = Constants::TYPE_TEXT;
					$params['nombre_completo']['value'] = $username; 
					$params['nombre_completo']['type']  = Constants::TYPE_TEXT;				
					$params['enabled']['value']  = 1; 
					$params['enabled']['type']   = Constants::TYPE_INTEGER;
					$params['enabled_sgi']['value']  = 0; 
					$params['enabled_sgi']['type']   = Constants::TYPE_INTEGER;
					$params['cod_nivel_acceso']['value']  = Constants::NIVEL_ACCESO_INVITADO; 
					$params['cod_nivel_acceso']['type']   = Constants::TYPE_INTEGER;
					$params['origen']['value']   = strtolower($origin);
					$params['origen']['type']    = Constants::TYPE_TEXT;						
					$params['enabled_app_aus']['value']  = 1; 
					$params['enabled_app_aus']['type']   = Constants::TYPE_INTEGER;					
					$params['enabled_mailing']['value']  = 0; 
					$params['enabled_mailing']['type']   = Constants::TYPE_INTEGER;				
					$params['usuario_telegram']['value'] = ''; 
					$params['usuario_telegram']['type']  = Constants::TYPE_TEXT;										
					$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["insert"], $params);	
					$query = $this->db->query($sql);	
					$profile = new User();				
					$profile->setCodUsuario($this->db->insert_id());				
					$profile->setOrigen($origin);
					$profile->setUsername($username);
					$profile->setEmail("");				
					$profile->setNombre($username);						
					$profile->setEnabledSGI(0);
					// Permisos a nivel de elemento
					$permisos   = $this->getPermisosUsuario($profile->getCodUsuario());
					$elementos  = $this->getVisibleElements($permisos,'token_code');
					// Permisos a nivel de estacion
					$permisosData   = $this->getPermisosDataUsuario2($profile->getCodUsuario());
					$estaciones = $this->getVisibleEstaciones($permisosData);					
					// Permisos a nivel de videos
					$permisosVideos     = $this->getVideosUsuario($profile->getCodUsuario());
					$videos             = $this->getVisibleElements($permisosVideos, 'cod_video');
					$token = $this->createToken($profile->getCodUsuario(), $elementos, $videos, $estaciones, 0);
					$profile->setSesionId($token);				
					return true;
				}		
			}

		}
		return false;
	}
	function duplicateUser($cod_usuario ){		
		$params['cod_usuario']['value']  = $cod_usuario; 
		$params['cod_usuario']['type']   = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["duplicate"], $params);	
		$query = $this->db->query($sql);		
		$id = $this->db->insert_id();

		$params['cod_usuario_new']['value']  = $id;  
		$params['cod_usuario_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["duplicateSecUsuarioElemento"], $params);	

		$params['cod_usuario_new']['value']  = $id;  
		$params['cod_usuario_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["duplicateSecUsuarioVideo"], $params);	
		
		$params['cod_usuario_new']['value']  = $id;  
		$params['cod_usuario_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["duplicateSecUsuarioInforme"], $params);	

		$params['cod_usuario_new']['value']  = $id;
		$params['cod_usuario_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["USUARIOS"]["duplicateSecUsuarioDatos"], $params);	


		$query = $this->db->query($sql);
		return true;
	}

	function duplicateNivelAcceso($cod_nivel_acceso ){		
		$params['cod_nivel_acceso']['value']  = $cod_nivel_acceso; 
		$params['cod_nivel_acceso']['type']   = Constants::TYPE_INTEGER;
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["duplicate"], $params);	
		$query = $this->db->query($sql);		
		$id = $this->db->insert_id();
		$params['cod_nivel_acceso_new']['value']  = $id; 
		$params['cod_nivel_acceso_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["duplicateSecNivelAcceso"], $params);	
		$query = $this->db->query($sql);

		$params['cod_nivel_acceso_new']['value']  = $id; 
		$params['cod_nivel_acceso_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["duplicateSecNivelAccesoVideos"], $params);	
		$query = $this->db->query($sql);		

		$params['cod_nivel_acceso_new']['value']  = $id; 
		$params['cod_nivel_acceso_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["duplicateSecNivelAccesoInformes"], $params);	
		$query = $this->db->query($sql);		


		$params['cod_nivel_acceso_new']['value']  = $id; 
		$params['cod_nivel_acceso_new']['type']   = Constants::TYPE_INTEGER;		
		$sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["NIVELES_ACCESO"]["duplicateSecNivelAccesoDatos"], $params);	
		$query = $this->db->query($sql);

		return true;
	}	

	

}
?>