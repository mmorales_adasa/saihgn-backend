<?php
class DatosDiariosDao extends CI_Model 
{

	function findVariables($filters){			        
        $sql = Queries::BASIC_ENTITY["DATOS_DIARIOS"]["findVariables"];
        $filters2  = array();		
        $and = true;
        $invalidos  = true;
        $date_from;
        $date_to;
        foreach ($filters as $filter) {    
            if ($filter->getColumn() != 'no_confirmadas' && $filter->getColumn() != 'conditions'&& $filter->getColumn() != 'date_from' && $filter->getColumn() != 'date_to'){            
                $f = $filter->clone();
                array_push($filters2,$f);   
            }
            if ($filter->getColumn() == 'conditions'){
                if ($filter->getValues()[0]==2){                    
                    $and=false;
                }
            }
            if ($filter->getColumn() == 'no_confirmadas'){
                $no_confirmadas = $filter->getValues()[0];
            }
            if ($filter->getColumn() == 'date_from'){
                $date_from = $filter;
            }      
            if ($filter->getColumn() == 'date_to'){
                $date_to = $filter;
            }                    
        }       
        
        $sql.= $this->utildao->applyFilters($filters2);
        if (isset($date_from)){
            $date_from->setColumn('data_medicion');
            $sql.= $this->utildao->applyFiltersDate($date_from, $and,'>=');
        }
        if (isset($date_to)){
            $date_to->setColumn('data_medicion');
            $sql.= $this->utildao->applyFiltersDate($date_to, $and, '<');
        }        
        
        if (isset($no_confirmadas) && $no_confirmadas==1){
            $sql .= $this->utildao->getLogica($and) . " ind_confirmacion <> 'S' ";
        }



        $sql .= " group by T1.cod_variable, T1.desc_variable, T1.cod_estacion,  T1.cod_variable, T1.desc_variable, T1.cod_estacion,  T2.desc_estacion";
		$sql.= $this->utildao->applySort("cod_estacion asc");
		$sql.= $this->utildao->applyLimit(1000, 0);	        
        
        $variables  = array();
        
        $query = $this->db->query($sql);        
        foreach ($query->result() as $row){
            
            $variable = new VariableModel();
            $variable->setCod($row->cod_variable);
            $variable->setDesc($row->desc_variable);
            $variable->setIsAcum($row->is_acum);            
            $variable->setDataInvalids($row->has_invalids>0 ? true : false);
            $estacion = new EstacionModel();
            $estacion->setCod($row->cod_estacion);
            $estacion->setDesc($row->desc_estacion);            
            $variable->setEstacion($estacion);
            array_push($variables,$variable);    
        }        
        return $variables;	
    }           
    function saveData($data){
        
        foreach ($data as $item) {             
            $params = array();
            $params['cod_variable']['value'] = $item['cod_variable'];
            $params['cod_variable']['type']  = Constants::TYPE_TEXT;

            $params['data_medicion']['value'] = $item['date'];
            $params['data_medicion']['type']  = Constants::TYPE_TEXT;

            $params['value']['value'] = $item['value'];
            $params['value']['type']  = Constants::TYPE_INTEGER;            

            if ($item['is_acum']){
             $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_DIARIOS"]["update_acum"], $params);                
            }
            else{
            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_DIARIOS"]["update_media"], $params);                
            }

            $query = $this->db->query($sql);
            $params['data_sumarizar']['value'] = date("Y-m-d", strtotime($item['date'])) . " 00:00:00" ;
            $params['data_sumarizar']['type']  = Constants::TYPE_TEXT;
            $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["DATOS_DIARIOS"]["markSumarizacion"], $params);         
            $query = $this->db->query($sql);
            
            
            //return $query->result();	     
        }
        return true;
    }
	function findData($filters)
	{	
        $sql = Queries::BASIC_ENTITY["DATOS_DIARIOS"]["findData"];
        $filters2  = array();		
        $date_from;
        $date_to;
        $is_acum;
        $and = true;
        foreach ($filters as $filter) {    
            //if ($filter->getColumn() != 'cod_variable'){ 
            if ($filter->getColumn() != 'date_from' && $filter->getColumn() != 'date_to' && $filter->getColumn() != 'is_acum'){            
                $f = $filter->clone();
                array_push($filters2,$f);                               
            }
            if ($filter->getColumn() == 'date_from'){
                $date_from = $filter;
            }      
            if ($filter->getColumn() == 'date_to'){
                $date_to = $filter;
            }   
            if ($filter->getColumn() == 'is_acum'){
                $is_acum = $filter;
            }               
        }    
 
        

        $sql.= $this->utildao->applyFilters($filters2);
        
        if (isset($date_from) && isset($date_from->getValues()[0])){
            $date_from->setColumn('data_medicion');
            $sql.= $this->utildao->applyFiltersDate($date_from, $and,'>=');
        }
        if (isset($date_to) && isset($date_to->getValues()[0])){
            $date_to->setColumn('data_medicion');
            $sql.= $this->utildao->applyFiltersDate($date_to, $and, '<');
        }           
        $sql.= $this->utildao->applySort("data_medicion asc");
       // echo $sql;
        $query = $this->db->query($sql);        
        $datos  = array();			        
        foreach ($query->result() as $row){
            $dato = new DatoDiarioModel();
            $dato->setCodVariable($row->cod_variable);
            $dato->setMedia($row->valor_media);
            $dato->setAcum($row->valor_acum);
            $dato->setMin($row->valor_min);
            $dato->setMax($row->valor_max);
            $dato->setFiabilidad($row->valor_fiabilidad);
            $dato->setIndConfirmacion($row->ind_confirmacion);
            
            if ($is_acum->getValues()[0] == 1){
                $dato->setValue($dato->getAcum());
                $dato->setIsAcum(true);
            }
            else{
                    $dato->setValue($dato->getMedia());
                $dato->setIsAcum(false);
            }
            $date = $row->data_medicion;//;date("Y-m-d H:i:s", strtotime('+'. $k.'hours'));
            $dato->setDate($date);
            $dato->setTimestampMin($row->timestamp_min);
            $dato->setTimestampMax($row->timestamp_max);
            array_push($datos,$dato);    
        }               
        return $datos;	
	}    
}
?>