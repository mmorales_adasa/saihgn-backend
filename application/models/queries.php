<?php
class Queries
{
	const BASIC_ENTITY =  array(
		'USUARIOS'=>  array('get' 	 => "select T1.cod_usuario, T1.username, T1.password,T1.enabled_app_aus, T1.enabled_mailing, T1.email, T1.enabled, T1.enabled_sgi, T1.nombre_completo, T1.usuario_telegram,
										lower(T1.origen) as origen,
										CASE WHEN T1.enabled=1  THEN 'Sí' WHEN T1.enabled=0 THEN 'No' ELSE  null  END as desc_enabled,
										CASE WHEN T1.enabled_sgi=1  THEN 'Sí' WHEN T1.enabled_sgi=0 THEN 'No' ELSE  null  END as desc_enabled_sgi,
										CASE WHEN T1.enabled_app_aus=1  THEN 'Sí' WHEN T1.enabled_app_aus=0 THEN 'No' ELSE  null  END as desc_enabled_app_aus,																				
										CASE WHEN T1.enabled_mailing=1  THEN 'Sí' WHEN T1.enabled_mailing=0 THEN 'No' ELSE  null  END as desc_enabled_mailing,																				
										T1.cod_nivel_acceso, T2.nombre as desc_nivel_acceso 
										from tbl_sec_usuarios  T1
										left join tbl_sec_nivel_acceso T2 on T2.cod_nivel_acceso=T1.cod_nivel_acceso",
							'update' => 'update tbl_sec_usuarios set username=:username:,password=:password:,email=:email:,enabled=:enabled:,
										usuario_telegram=:usuario_telegram:,
										enabled_sgi=:enabled_sgi:,nombre_completo=:nombre_completo:,cod_nivel_acceso=:cod_nivel_acceso:,enabled_app_aus=:enabled_app_aus:,   
										enabled_mailing=:enabled_mailing:
										where cod_usuario=:cod_usuario: ',
							'updatePassword' => 'update tbl_sec_usuarios set password=:password:  where cod_usuario=:cod_usuario: ',
							'insert' => "insert into tbl_sec_usuarios (username, password, email, enabled, nombre_completo, origen, cod_nivel_acceso,enabled_sgi,enabled_app_aus,enabled_mailing,usuario_telegram) VALUES (
											:username:, :password:, :email:, :enabled:, :nombre_completo:,:origen:,:cod_nivel_acceso:,:enabled_sgi:,:enabled_app_aus:,:enabled_mailing:,:usuario_telegram:)",	
							'delete' => 'delete from tbl_sec_usuarios where cod_usuario=:cod_usuario:; 
										 delete from tbl_sec_rel_usuario_elemento where cod_usuario=:cod_usuario:;
										 delete from tbl_sec_rel_usuario_datos    where cod_usuario=:cod_usuario:;
										 delete from tbl_sec_rel_usuario_video    where cod_usuario=:cod_usuario:;
										 delete from tbl_sec_rel_usuario_informe    where cod_usuario=:cod_usuario:;
										 delete from tbl_sec_rel_usuario_biblioteca_ubicacion    where cod_usuario=:cod_usuario:;
										  ',	
							'insertSecUsuarioElemento' => "insert into tbl_sec_rel_usuario_elemento (cod_elemento, cod_usuario, visible) VALUES (:cod_elemento:, :cod_usuario:, :visible:)",	  
							'insertSecUsuarioVideo' => "insert into tbl_sec_rel_usuario_video (cod_video, cod_usuario, visible) VALUES (:cod_video:, :cod_usuario:, :visible:)",	  
							'insertSecUsuarioInforme' => "insert into tbl_sec_rel_usuario_informe (cod_informe, cod_usuario, visible) VALUES (:cod_informe:, :cod_usuario:, :visible:)",	  
							'deletePermisosElementos'  => 'delete from tbl_sec_rel_usuario_elemento where cod_usuario=:cod_usuario:',
							'deletePermisosVideos'  => 'delete from tbl_sec_rel_usuario_video where cod_usuario=:cod_usuario:',
							'deletePermisosInformes'  => 'delete from tbl_sec_rel_usuario_informe where cod_usuario=:cod_usuario:',
							'deletePermisosDatos'      => 'delete from tbl_sec_rel_usuario_datos where cod_usuario=:cod_usuario:',
							'deleteUbicaciones'      => 'delete from tbl_sec_rel_usuario_biblioteca_ubicacion where cod_usuario=:cod_usuario:',
							'insertSecUbicacion'  => 'insert into tbl_sec_rel_usuario_biblioteca_ubicacion (cod_usuario, id_ubicacion) VALUES (:cod_usuario:, :id_ubicacion:)',
							'insertSecNivelElemento'  => 'insert into tbl_sec_rel_usuario_datos (cod_usuario, cod_elemento, visible) VALUES (:cod_usuario:, :cod_elemento:,:visible:)',
							'insertSecNivelDato'  => 'insert into tbl_sec_rel_usuario_datos (cod_tipo_item, cod_usuario, cod_item, visible) VALUES (:cod_tipo_item:, :cod_usuario:, :cod_item:,:visible:)',

							'duplicate' => "insert into tbl_sec_usuarios (username, password,email,enabled, nombre_completo, origen, cod_nivel_acceso, enabled_sgi,enabled_app_aus,enabled_mailing,usuario_telegram)
											select username || '_copia',password,email,enabled, nombre_completo, origen, cod_nivel_acceso, enabled_sgi ,enabled_app_aus,enabled_mailing,usuario_telegram
											FROM tbl_sec_usuarios 
										    where cod_usuario=:cod_usuario:",
					    	'duplicateSecUsuarioElemento' => "insert into tbl_sec_rel_usuario_elemento (cod_usuario,cod_elemento, visible)
															  select :cod_usuario_new:,cod_elemento,visible from tbl_sec_rel_usuario_elemento where cod_usuario=:cod_usuario:",										 
							'duplicateSecUsuarioVideo' => "insert into tbl_sec_rel_usuario_video (cod_usuario,cod_video, visible)
															  select :cod_usuario_new:,cod_video,visible from tbl_sec_rel_usuario_video where cod_usuario=:cod_usuario:",																  
							'duplicateSecUsuarioInforme' => "insert into tbl_sec_rel_usuario_informe (cod_usuario,cod_informe, visible)
															  select :cod_usuario_new:,cod_informe,visible from tbl_sec_rel_usuario_informe where cod_usuario=:cod_usuario:",																  															  
							'duplicateSecUsuarioDatos' => "insert into tbl_sec_rel_usuario_datos (cod_usuario,cod_tipo_item, cod_item, visible)
														   select :cod_usuario_new:,cod_tipo_item, cod_item, visible from tbl_sec_rel_usuario_datos where cod_usuario=:cod_usuario:",																  
							'getPermisos' => ' 
								select distinct T1.cod_elemento, T1.notes, T2.visible as visible, T3.visible as visible_public, T4.visible as visible_nivel, T1.token_code 
								from tbl_sec_elementos T1
								LEFT JOIN tbl_sec_rel_usuario_elemento T2 ON (T1.cod_elemento=T2.cod_elemento and T2.cod_usuario=:cod_usuario:)
								LEFT JOIN tbl_sec_rel_nivel_acceso_elemento T3 ON (T1.cod_elemento=T3.cod_elemento and T3.cod_nivel_acceso=0)
								LEFT JOIN tbl_sec_rel_nivel_acceso_elemento T4 ON (T1.cod_elemento=T4.cod_elemento and T4.cod_nivel_acceso=(select cod_nivel_acceso from tbl_sec_usuarios where cod_usuario=:cod_usuario:))														
							',
							'getVideos' => ' 
								select distinct T1.cod_video, T1.descripcion,  T2.visible as visible, T3.visible as visible_public, T4.visible as visible_nivel
								from tbl_video T1
								LEFT JOIN tbl_sec_rel_usuario_video T2 ON (T1.cod_video=T2.cod_video and T2.cod_usuario=:cod_usuario:)
								LEFT JOIN tbl_sec_rel_nivel_acceso_video T3 ON (T1.cod_video=T3.cod_video and T3.cod_nivel_acceso=0)
								LEFT JOIN tbl_sec_rel_nivel_acceso_video T4 ON (T1.cod_video=T4.cod_video and T4.cod_nivel_acceso=(select cod_nivel_acceso from tbl_sec_usuarios where cod_usuario=:cod_usuario:))	
							',
							'getInformes' => ' 
								select distinct T1.cod_informe, T1.nombre as descripcion,  T2.visible as visible, T3.visible as visible_public, T4.visible as visible_nivel
								from tbl_mast_informes T1
								LEFT JOIN tbl_sec_rel_usuario_informe T2 ON (T1.cod_informe=T2.cod_informe and T2.cod_usuario=:cod_usuario:)
								LEFT JOIN tbl_sec_rel_nivel_acceso_informe T3 ON (T1.cod_informe=T3.cod_informe and T3.cod_nivel_acceso=0)
								LEFT JOIN tbl_sec_rel_nivel_acceso_informe T4 ON (T1.cod_informe=T4.cod_informe and T4.cod_nivel_acceso=(select cod_nivel_acceso from tbl_sec_usuarios where cod_usuario=:cod_usuario:))	
							',										
							'getUbicaciones' => ' 
								select t1.*,(case when t2.cod_usuario is null then 0 else 1 end) as visible from tbl_biblioteca_ubicaciones t1
								left join tbl_sec_rel_usuario_biblioteca_ubicacion t2 on (t1.id_ubicacion=t2.id_ubicacion and t2.cod_usuario=:cod_usuario:)								
							',													
							'getPermisosAll' => "
										select distinct T1.cod_estacion as cod_item, T1.tipo as cod_tipo_item, T2.visible as visible, T3.visible as visible_public, T4.visible as visible_nivel
										from 
											(
												select cod_estacion, tipo from tbl_estacion 
												where tipo IN ('ECC','CR','S','R','E','EXC','EAA','EM','NR','D','PZ','PZM','CC','CCT','ACA')
												UNION 
												select cod_sonda, 'SDAMULTI' from tbl_sonda_mult , tbl_estacion where replace(cod_sonda,'S','E') = tbl_estacion.cod_estacion
												UNION 
												select cod_estacion, 'EMAEMET' from tbl_estacion_aemet
												UNION
												select cod_estacion, 'AUS' as tipo  from tbl_estacion 
												where tipo IN ('E')

												
											) T1
										LEFT JOIN tbl_sec_rel_usuario_datos T2      ON (T1.tipo=T2.cod_tipo_item and t1.cod_estacion=t2.cod_item and T2.cod_usuario=:cod_usuario:)
										LEFT JOIN tbl_sec_rel_nivel_acceso_datos T3 ON (T1.tipo=T3.cod_tipo_item and t1.cod_estacion=t3.cod_item and T3.cod_nivel_acceso=0)
										LEFT JOIN tbl_sec_rel_nivel_acceso_datos T4 ON (T1.tipo=T4.cod_tipo_item and T1.cod_estacion=t4.cod_item and T4.cod_nivel_acceso=(select cod_nivel_acceso from tbl_sec_usuarios where cod_usuario=:cod_usuario:))				
										order by tipo, cod_estacion									
							",
							 /*'getPermisos' => '
								select 
								T1.app, T1.token_code, T1.cod_elemento, T1.notes, T1.visible as visible_elemento,
								COALESCE(T2.visible, -1)  visible_usuario,
								T3.visible  visible_nivel
								from tbl_sec_elementos T1
								LEFT JOIN tbl_sec_rel_usuario_elemento T2      ON (T1.cod_elemento=T2.cod_elemento and T2.cod_usuario=:cod_usuario:)
								LEFT JOIN (
								SELECT visible, cod_elemento FROM tbl_sec_rel_nivel_acceso_elemento  T4
								INNER JOIN tbl_sec_usuarios T5 ON T4.cod_nivel_acceso=T5.cod_nivel_acceso
								where cod_usuario=:cod_usuario:
								) T3 ON (T1.cod_elemento=T3.cod_elemento) 
								ORDER BY T1.orden							
							',*/
							'getPermisosData' => '
								select t1.cod_usuario, trim(cod_item) as cod_item, cod_tipo_item, COALESCE(visible, -1) as visible, (   
									select visible
									from tbl_sec_rel_nivel_acceso_datos t2 
									where t1.cod_tipo_item=t2.cod_tipo_item and t1.cod_item=t2.cod_item and t2.cod_nivel_acceso=t1.cod_usuario
								) as visible_nivel,	
								(   
									select visible
									from tbl_sec_rel_nivel_acceso_datos t2 
									where t1.cod_tipo_item=t2.cod_tipo_item and t1.cod_item=t2.cod_item and t2.cod_nivel_acceso=0
								) as visible_public								
								from tbl_sec_rel_usuario_datos t1 
								where t1.cod_usuario=:cod_usuario:
								',							
				), 	
		'ELEMENTOS_VISOR'=>  array('get' 	 => "select *,
												CASE
												WHEN visible=1  THEN 'Sí'
												ELSE  'No'   		END 
												as desc_visible			
												from tbl_sec_elementos ",
								   'update' => 'update tbl_sec_elementos set app=:app:, cod_elemento=:cod_elemento:, notes=:notes: where cod_elemento=:cod_elemento: ',
								   'insert' => "insert into tbl_sec_elementos (app, cod_elemento, notes) VALUES (:app:, :cod_elemento:, :notes:)",	
								   'delete' => 'delete from tbl_sec_elementos where cod_elemento=:cod_elemento:;
								   				delete from tbl_sec_rel_nivel_acceso_elemento where cod_elemento=:cod_elemento:;
												delete from tbl_sec_rel_usuario_elemento      where cod_elemento=:cod_elemento:;
								   ',  
								   'deleteSecNivel' => 'delete from tbl_sec_rel_nivel_acceso_elemento where cod_elemento=:cod_elemento:',
								   'insertSecNivel' => "insert into tbl_sec_rel_nivel_acceso_elemento (cod_elemento, cod_nivel_acceso, visible) VALUES (:cod_elemento:, :cod_nivel_acceso:, :visible:)",	  
								   'getPermisosNivel' => 'select t1.*,:cod_elemento: as cod_elemento,
								   COALESCE((select visible
									from tbl_sec_rel_nivel_acceso_elemento t2
									where cod_nivel_acceso=t1.cod_nivel_acceso and t2.cod_elemento=:cod_elemento:),-1) as visible
								    from tbl_sec_nivel_acceso t1
								    order by t1.cod_nivel_acceso'
		),
		'NIVELES_ACCESO'=>  array('get' 	 => "select cod_nivel_acceso,nombre from tbl_sec_nivel_acceso ",
							'update' => 'update tbl_sec_nivel_acceso set nombre=:nombre: where cod_nivel_acceso=:cod_nivel_acceso: ',
							'insert' => "insert into tbl_sec_nivel_acceso (nombre) VALUES (:nombre:)",	
							'delete' => 'delete from tbl_sec_nivel_acceso where cod_nivel_acceso=:cod_nivel_acceso:; 
										 delete from tbl_sec_rel_nivel_acceso_elemento where cod_nivel_acceso=:cod_nivel_acceso:;
										 delete from tbl_sec_rel_nivel_acceso_datos where cod_nivel_acceso=:cod_nivel_acceso:;
										 delete from tbl_sec_rel_nivel_acceso_informe where cod_nivel_acceso=:cod_nivel_acceso:;
										 delete from tbl_sec_rel_nivel_acceso_video where cod_nivel_acceso=:cod_nivel_acceso:;
										 '										 ,
							'duplicate' => "insert into tbl_sec_nivel_acceso (nombre)
							               select nombre || '_copia' FROM tbl_sec_nivel_acceso where cod_nivel_acceso=:cod_nivel_acceso:",
					    	'duplicateSecNivelAcceso' => "insert into tbl_sec_rel_nivel_acceso_elemento (cod_nivel_acceso,cod_elemento, visible)
														   select :cod_nivel_acceso_new:,cod_elemento,visible from tbl_sec_rel_nivel_acceso_elemento where cod_nivel_acceso=:cod_nivel_acceso:",										 							
							'duplicateSecNivelAccesoVideos' => "insert into tbl_sec_rel_nivel_acceso_video (cod_nivel_acceso,cod_video, visible)
								 						  select :cod_nivel_acceso_new:,cod_video,visible from tbl_sec_rel_nivel_acceso_video where cod_nivel_acceso=:cod_nivel_acceso:",										 																					   

							'duplicateSecNivelAccesoInformes' => "insert into tbl_sec_rel_nivel_acceso_informe (cod_nivel_acceso,cod_informe, visible)
								 						  select :cod_nivel_acceso_new:,cod_informe,visible from tbl_sec_rel_nivel_acceso_informe where cod_nivel_acceso=:cod_nivel_acceso:",										 																					   

							'duplicateSecNivelAccesoDatos' => "insert into tbl_sec_rel_nivel_acceso_datos (cod_nivel_acceso,cod_tipo_item, cod_item, visible)
														  select :cod_nivel_acceso_new:,cod_tipo_item, cod_item, visible from tbl_sec_rel_nivel_acceso_datos where cod_nivel_acceso=:cod_nivel_acceso:",										 																						  
							'deletePermisosElementos' => 'delete from tbl_sec_rel_nivel_acceso_elemento where cod_nivel_acceso=:cod_nivel_acceso:',
							'deletePermisosVideos'    => 'delete from tbl_sec_rel_nivel_acceso_video where cod_nivel_acceso=:cod_nivel_acceso:',
							'deletePermisosInformes'    => 'delete from tbl_sec_rel_nivel_acceso_informe where cod_nivel_acceso=:cod_nivel_acceso:',
							'deletePermisosDatos'     => 'delete from tbl_sec_rel_nivel_acceso_datos where cod_nivel_acceso=:cod_nivel_acceso:',
							'insertSecNivelDato'  => 'insert into tbl_sec_rel_nivel_acceso_datos (cod_tipo_item, cod_nivel_acceso, cod_item, visible) VALUES (:cod_tipo_item:, :cod_nivel_acceso:, :cod_item:,:visible:)',
							'insertSecNivelElemento'  => 'insert into tbl_sec_rel_nivel_acceso_elemento (cod_nivel_acceso, cod_elemento, visible) VALUES (:cod_nivel_acceso:, :cod_elemento:,:visible:)',
							'insertSecNivelVideo'  => 'insert into tbl_sec_rel_nivel_acceso_video (cod_nivel_acceso, cod_video, visible) VALUES (:cod_nivel_acceso:, :cod_video:,:visible:)',
							'insertSecNivelInforme'  => 'insert into tbl_sec_rel_nivel_acceso_informe (cod_nivel_acceso, cod_informe, visible) VALUES (:cod_nivel_acceso:, :cod_informe:,:visible:)',
							'getPermisosData' => 'select cod_nivel_acceso, trim(cod_item) as cod_item, cod_tipo_item, COALESCE(visible, -1) as visible 
							from tbl_sec_rel_nivel_acceso_datos where cod_nivel_acceso=:cod_nivel_acceso:',							
							'getPermisos'     => 'select distinct T1.cod_elemento, T1.notes, T2.visible as visible, T3.visible as visible_public, T1.token_code 
							from tbl_sec_elementos T1
							LEFT JOIN tbl_sec_rel_nivel_acceso_elemento T2 ON (T1.cod_elemento=T2.cod_elemento and T2.cod_nivel_acceso=:cod_nivel_acceso:)
							LEFT JOIN tbl_sec_rel_nivel_acceso_elemento T3 ON (T1.cod_elemento=T3.cod_elemento and T3.cod_nivel_acceso=0)',
							'getVideos' => 'select distinct T1.cod_video, T1.descripcion,   T2.visible as visible, T3.visible as visible_public
							from tbl_video T1
							LEFT JOIN tbl_sec_rel_nivel_acceso_video T2 ON (T1.cod_video=T2.cod_video and T2.cod_nivel_acceso=:cod_nivel_acceso:)
							LEFT JOIN tbl_sec_rel_nivel_acceso_video T3 ON (T1.cod_video=T3.cod_video and T3.cod_nivel_acceso=0)',

							'getInformes' => 'select distinct T1.cod_informe, T1.nombre as descripcion,   T2.visible as visible, T3.visible as visible_public
							from tbl_mast_informes T1
							LEFT JOIN tbl_sec_rel_nivel_acceso_informe T2 ON (T1.cod_informe=T2.cod_informe and T2.cod_nivel_acceso=:cod_nivel_acceso:)
							LEFT JOIN tbl_sec_rel_nivel_acceso_informe T3 ON (T1.cod_informe=T3.cod_informe and T3.cod_nivel_acceso=0)',							
							
								
				), 					
		'MOTIVOS_INVALIDACION'=>  array('get' 	 => 'select * from tbl_mast_motivos_invalidacion', 		
		'insert' => 'emd',
		'update' => 'emd',	
		'delete' => 'pdo'),
		'PROCESOS'=>  array('get' 	 => "select cod_proceso, to_char(timestamp_ini, 'DD/MM/YYYY HH24:MI:SS') as timestamp_ini, to_char(timestamp_fin, 'DD/MM/YYYY HH24:MI:SS') as timestamp_fin, resultado, num_proceso, num_regs_error from tbl_log_procesos  "),
		'OPERACIONES'=>  array('get' 	 => "select cod_operacion, to_char(timestamp_operacion, 'DD/MM/YYYY HH24:MI:SS') as timestamp_operacion, cod_usuario, tipo_operacion, desc_observaciones from tbl_log_operaciones  "),
		'CURVAS_GASTO'  =>  array(
								'detailByName' => 'select * from tbl_gest_curvas_gasto where desc_curva=:desc_curva:',
								'delete' => 'delete from tbl_gest_curvas_gasto where cod_curva=:cod_curva:;
											 delete from tbl_gest_tablas_interpolacion where cod_curva=:cod_curva:',
								'get'   => "select cod_curva, desc_curva,
											desc_estacion, t1.cod_estacion, ind_activa,t1.formula, 
											t1.data_inicial, t1.data_final,t1.data_alta,
											to_char(t1.data_inicial, 'DD/MM/YYYY') as data_inicial_format,
											to_char(t1.data_final, 'DD/MM/YYYY') as data_final_format,
											to_char(t1.data_alta, 'DD/MM/YYYY') as data_alta_format,
											CASE
											WHEN ind_activa='S'  THEN 'Sí'
											WHEN ind_activa='N'  THEN 'No'
											ELSE  null   		END as desc_activa,
											t1.cod_variable_nivel,  t3.desc_variable as desc_variable_nivel,
											t1.cod_variable_caudal, t4.desc_variable as desc_variable_caudal,
											desc_curva,
											t6.desc_unidad_medida as desc_unidad_medida_nivel,
											t8.desc_unidad_medida as desc_unidad_medida_caudal											
											from tbl_gest_curvas_gasto t1
											inner join tbl_mast_estaciones t2 on t1.cod_estacion=t2.cod_estacion
											inner join tbl_mast_variables  t3 on t1.cod_variable_nivel=t3.cod_variable
											inner join tbl_mast_variables  t4 on t1.cod_variable_caudal=t4.cod_variable
											inner join tbl_mast_tipos_variables  t5 on t3.cod_tipo_variable=t5.cod_tipo_variable
											inner join tbl_mast_unidades_medida  t6 on t5.cod_unidad_medida=t6.cod_unidad_medida
											inner join tbl_mast_tipos_variables  t7 on t4.cod_tipo_variable=t7.cod_tipo_variable
											inner join tbl_mast_unidades_medida  t8 on t7.cod_unidad_medida=t8.cod_unidad_medida											
											",
								'update' => 'update tbl_gest_curvas_gasto set ind_activa=:ind_activa: where cod_curva=:cod_curva: ',
								'insert' => "insert into tbl_gest_curvas_gasto (cod_curva, desc_curva, cod_estacion, cod_variable_nivel, cod_variable_caudal, data_inicial, data_final, data_alta, ind_activa, formula) VALUES (
											(select max(cod_curva)+1 from tbl_gest_curvas_gasto),
											:desc_curva:, :cod_estacion:, :cod_variable_nivel:, :cod_variable_caudal:, :data_inicial:, :data_final:, now(), 'N', null)",
								'insertInterpolacion' => "insert into tbl_gest_tablas_interpolacion (cod_curva,nivel, caudal) VALUES (:cod_curva:, :nivel:, :caudal:)",											
								//'calcula' => 'update tbl_gest_curvas_gasto set data_inicial=:data_inicial:, data_final=:data_final: where cod_curva=:cod_curva: ',
								'calcula' => "select si_interpol_si_interpol(:cod_curva:,:cod_estacion:,:cod_variable_nivel:,:cod_variable_caudal:,:data_inicial:,:data_final:) as result ",
								'lastId' => 'select max(cod_curva) as id from tbl_gest_curvas_gasto',	
								'getInterpolacion' => "select * from tbl_gest_tablas_interpolacion where cod_curva=:cod_curva: order by nivel"),
		'FORMULAS'  =>  array('getEstacionesVariablesConstantes'   => "	
								select type, code, description, cod_estacion, desc_estacion 
								FROM (
								select 'VARIABLE' as type, T1.cod_variable as code, T1.desc_variable as description, T1.cod_estacion, T2.desc_estacion
								from tbl_mast_variables T1 
								INNER JOIN tbl_mast_estaciones T2 ON T1.cod_estacion=t2.cod_estacion
								LEFT JOIN tbl_mast_tipos_variables T3 ON T1.cod_tipo_variable=T3.cod_tipo_variable
								WHERE COD_ORIGEN is NULL
								UNION
								select 'CALCULADA' as type, T1.cod_variable as code, T1.desc_variable as description, T1.cod_estacion, T2.desc_estacion
								from tbl_mast_variables T1 
								INNER JOIN tbl_mast_estaciones T2 ON T1.cod_estacion=t2.cod_estacion
								LEFT JOIN tbl_mast_tipos_variables T3 ON T1.cod_tipo_variable=T3.cod_tipo_variable
								WHERE COD_ORIGEN='CALCULADA' 
								UNION
								SELECT 'CONSTANTE' as type, CAST(T1.cod_constante AS varchar) as code , T1.desc_constante as description, T1.cod_estacion, T2.desc_estacion FROM tbl_mast_constantes T1
								INNER JOIN tbl_mast_estaciones T2 ON T1.cod_estacion=t2.cod_estacion
								) AUX		
								",
								'deleteTokensFormula' => "delete from tbl_gest_formulas where cod_variable= :cod_variable:",
								'getTokensFormula'    => "select * from tbl_gest_formulas where cod_variable= :cod_variable: order by num_termino",
								'isInUseFormula'      => "select count(*) as total from tbl_gest_formulas where variable= :cod_variable: and cod_variable <> :cod_variable_parent: ",
								'updateNivellCalculNode'      => "update tbl_mast_variables set nivel_calculo=:nivel_calculo: where cod_variable= :cod_variable:",
								'obtenerNodoPorCodVariable' => "select *  from tbl_mast_variables where cod_variable= :cod_variable:",
								'obtenerHijosPorNodo' => " select * from tbl_mast_variables where cod_variable in (select concat(variable,calculada) from tbl_gest_formulas where cod_variable= :cod_variable:)",
								'obtenerPadresPorNodo' => "select * from  tbl_mast_variables natural join  tbl_gest_formulas WHERE concat(variable,calculada) = :cod_variable: ",
								

								),
		'VARIABLES' =>  array('get'   => 'select t1.*, t2.desc_estacion, t3.desc_tipo_variable from tbl_mast_variables t1 inner join tbl_mast_estaciones t2 on t1.cod_estacion=t2.cod_estacion inner join tbl_mast_tipos_variables t3 on t1.cod_tipo_variable=t3.cod_tipo_variable ',
							  'update' => "insert into tbl_mast_variables (cod_variable, desc_variable, cod_tipo_variable, cod_estacion, cod_punto_medida, cod_fuente_origen, data_calc_agujero, data_calc_ceros_bdx, cod_origen, formula, nivel_calculo) VALUES (
										  :cod_variable:,:desc_variable:,:cod_tipo_variable:,:cod_estacion:,:cod_punto_medida:,:cod_fuente_origen:,null,null,:cod_origen:,:formula:,0)
										  on conflict (cod_variable) do
										  update set desc_variable=:desc_variable:, cod_fuente_origen=:cod_fuente_origen:, formula=:formula:,cod_tipo_variable=:cod_tipo_variable:,cod_estacion=:cod_estacion:,cod_punto_medida=:cod_punto_medida:
							  ",	
							  'insert' => "insert into tbl_mast_variables (cod_variable, desc_variable, cod_tipo_variable, cod_estacion, cod_punto_medida, cod_fuente_origen, data_calc_agujero, data_calc_ceros_bdx, cod_origen, formula, nivel_calculo) VALUES (
										  :cod_variable:,:desc_variable:,:cod_tipo_variable:,:cod_estacion:,:cod_punto_medida:,:cod_fuente_origen:,null,null,:cod_origen:,:formula:,0)
										  on conflict (cod_variable) do
										  update set desc_variable=:desc_variable:, cod_fuente_origen=:cod_fuente_origen:, cod_origen=:cod_origen:, formula=:formula:,cod_tipo_variable=:cod_tipo_variable:,cod_estacion=:cod_estacion:,cod_punto_medida=:cod_punto_medida:
							  ",								  
							  'delete' => 'delete from tbl_mast_variables where cod_variable=:cod_variable:; delete from tbl_gest_formulas where cod_variable= :cod_variable:',
							  'detail' => 'select * from tbl_mast_variables where cod_variable=:cod_variable:',
							  'nexId' => "select lpad(cast(cast(max(cod_variable) as integer)+1 as text),6,'0') as id from tbl_mast_variables where cod_origen='CALCULADA'"
							), 		
							
		'CONSTANTES'=>  array('get' 	 => 'select t1.*,t2.desc_tipo_constante, T3.desc_estacion from tbl_mast_constantes t1 LEFT JOIN  tbl_mast_tipos_constantes t2  on t1.cod_tipo_constante=t2.cod_tipo_constante  LEFT JOIN tbl_mast_estaciones t3 on t1.cod_estacion=T3.cod_estacion ', 		
		'insert' => 'insert into tbl_mast_constantes (desc_constante, cod_tipo_constante, valor, cod_estacion) values ( :desc_constante:, :cod_tipo_constante:, :valor:, :cod_estacion: )',
		'update' => 'update tbl_mast_constantes set desc_constante=:desc_constante:, cod_tipo_constante=:cod_tipo_constante:, valor=:valor:, cod_estacion=:cod_estacion: where cod_constante=:cod_constante: ',	
		'delete' => 'delete from tbl_mast_constantes where cod_constante=:cod_constante:',
		'detail' => 'select * from tbl_mast_constantes where cod_constante=:cod_constante:',	
		),
		'ESQUEMAS'=>array('get'    => 'select * from tbl_esquemas where UPPER(id_esquema)=:id_esquema:',
						  'delete' => 'delete   from tbl_esquemas where UPPER(id_esquema)=:id_esquema:',
						  'insert' => 'insert into tbl_esquemas(id_esquema, activo) values (:id_esquema:,true)'
		),

		'CAMARAS'=>  array('get' 	 => 'select distinct t1.*, CAST(timelapse as integer),CAST(destacado as integer) from tbl_video t1 
										 left join (
											 select cast(cod_nivel_acceso as varchar), cod_video, visible 
											 from tbl_sec_rel_nivel_acceso_video 
											 where visible=1
										 ) t2 on t1.cod_video=t2.cod_video
		', 		
			'insert' => "insert into tbl_video (cod_video, tipo, descripcion, modelo, ip,mac,timelapse, destacado) values ( :cod_video:,'camara',:descripcion:,:modelo:,:ip:,:mac:,CAST(:timelapse: as boolean),CAST(:destacado: as boolean))",
			'update' => 'update tbl_video set  descripcion=:descripcion:, modelo=:modelo:, ip=:ip:, mac=:mac:, timelapse=CAST(:timelapse: as boolean), destacado=timelapse=CAST(:destacado: as boolean)  where cod_video=:cod_video: ',	
			'delete' => 'delete from tbl_video where cod_video=:cod_video:; delete from tbl_sec_rel_nivel_acceso_video where cod_video=:cod_video:;delete from tbl_sec_rel_usuario_video where cod_video=:cod_video:;',
			'detail' => 'select * from tbl_video where cod_video=:cod_video:',	
			'deletePermisosNivel' => 'delete from tbl_sec_rel_nivel_acceso_video where cod_video=:cod_video:',	
			'insertPermisosNivel'  => 'insert into tbl_sec_rel_nivel_acceso_video (cod_nivel_acceso, cod_video, visible) VALUES (:cod_nivel_acceso:, :cod_video:,:visible:)',
			'getPermisosNiveles' => 'select t1.cod_nivel_acceso, t2.visible from tbl_sec_nivel_acceso T1
									 left join  tbl_sec_rel_nivel_acceso_video t2 on (t1.cod_nivel_acceso=T2.cod_nivel_acceso and t2.cod_video=:cod_video:)'			
		),		
		'TIPOS_CONSTANTES'=>  array('get' 	 => 'select t1.*,t2.desc_unidad_medida from tbl_mast_tipos_constantes t1 LEFT JOIN tbl_mast_unidades_medida t2 on t1.cod_unidad_medida=t2.cod_unidad_medida ', 		
		'update' => 'update tbl_mast_tipos_constantes set desc_tipo_constante=:desc_tipo_constante:, cod_unidad_medida=:cod_unidad_medida: where cod_tipo_constante=:cod_tipo_constante: ',	
		'insert' => 'insert into tbl_mast_tipos_constantes (desc_tipo_constante, cod_unidad_medida) values (:desc_tipo_constante:, :cod_unidad_medida:)',
		'delete' => 'delete from tbl_mast_tipos_constantes where cod_tipo_constante=:cod_tipo_constante:'),
		'UNIDADES'=>  array('get' 	 => 'select * from tbl_mast_unidades_medida ', 		
		'insert' => 'emd',
		'update' => 'emd',	
		'delete' => 'pdo'),	
		'INFORMES'=>  array('get' 	 => 'select * from tbl_mast_informes  ', 		
		'update' => 'update tbl_mast_informes set nombre=:nombre:,url=:url:  where cod_informe=:cod_informe: ',	
		'insert' => 'insert into tbl_mast_informes (nombre,url) values (:nombre:,:url:)',
		'delete' => 'delete from tbl_mast_informes where cod_informe=:cod_informe:'),			
		'ESCENARIOS'=>  array('get' 	 => 'select * from tbl_dcr_escenarios  '),				
		'MODELOS'=>  array('get' 	 => 'select * from tbl_dcr_decretos  ', 		
		'update' => 'update tbl_dcr_decretos set desc_decreto=:desc_decreto:  where cod_decreto=:cod_decreto: ',	
		'insert' => 'insert into tbl_dcr_decretos (desc_decreto) values (:desc_decreto:)',
		'delete' => 'delete from tbl_dcr_decretos where cod_decreto=:cod_decreto:'),			
		'ESCENARIOS'=>  array('get' 	 => 'select * from tbl_dcr_escenarios  '),			
		'BIBLIOTECA_UBICACIONES'=>  array('get' 	 => 'select * from tbl_biblioteca_ubicaciones  '),			
		'SISTEMAS'=>  array('get' 	 => 'select * from tbL_dcr_sistemas  ',	
		'update' => 'update tbL_dcr_sistemas set desc_sistema=:desc_sistema:  where cod_sistema=:cod_sistema: ',	
		'insert' => 'insert into tbL_dcr_sistemas (desc_sistema) values (:desc_sistema:)',
		'delete' => 'delete from tbL_dcr_sistemas where cod_sistema=:cod_sistema:'),			
		'MODEL_UMBRAL_COMPUESTO'=>  array('get' 	 => 'select * from tbl_dcr_umbrales_compuestos ',
		'insert' 	 => 'insert into tbl_dcr_umbrales_compuestos (cod_valor_ref,anyo,anyoh,trimestre_h,valor_var1_min,valor_var1_max,valor_var2_min, valor_var2_max, valor, observaciones ) values (:cod_valor_ref:,:anyo:,:anyoh:,:trimestre_h:,:valor_var1_min:,:valor_var1_max:,:valor_var2_min:, :valor_var2_max:, :valor:, :observaciones: )',
		'update' 	 => '',
		'delete' 	 => 'delete from tbl_dcr_umbrales_compuestos where cod_valor_ref = :cod_valor_ref: ',
		),		
		'MODEL_VALORES_UMBRAL'=>  array('get' 	 => 'select * from tbl_dcr_valores_umbral ',
		'insert' 	 => 'insert into tbl_dcr_valores_umbral (cod_valor_ref, anyo, anyoh, trimestre_h, mes, semana, dia, valor, observaciones ) values (:cod_valor_ref:, :anyo:, :anyoh:, :trimestre_h:, :mes: , :semana:, :dia:, :valor:, :observaciones:)',
		'update' 	 => '',
		'delete' 	 => 'delete from tbl_dcr_valores_umbral where cod_valor_ref = :cod_valor_ref: ',
		),	
		
		'MODEL_NIVEL_TEMPORAL'=>  array('get' 	 => 'select * from tbl_dcr_nivel_temp  '),			
		'MODEL_INDICADOR_VARIABLE'=>  array('get' 	 => 'select * from tbl_dcr_indicadores_variable  '),			
		'MODEL_INDICADOR_UMBRAL'=>  array('get' 	 => 'select * from tbl_dcr_indicadores_umbral  '),	
		'MODEL_VARIABLE_REF' =>  array('insert' => 'insert into tbl_dcr_variables_ref  (cod_valor_ref,cod_nivel_temp1,cod_nivel_temp2,cod_var_ref1,cod_var_ref2) values (:cod_valor_ref:,:cod_nivel_temp1:,:cod_nivel_temp2:,:cod_var_ref1:,:cod_var_ref2:)',
									   'update' => 'update tbl_dcr_variables_ref set cod_nivel_temp1=:cod_nivel_temp1:, cod_nivel_temp2=:cod_nivel_temp2:, cod_var_ref1=:cod_var_ref1:,cod_var_ref2=:cod_var_ref2: where cod_valor_ref=:cod_valor_ref:',
								 	   'delete' => 'delete from tbl_dcr_variables_ref where cod_valor_ref=:cod_valor_ref:; '	
	    ),	
		'SISTEMAS_ESCENARIOS'=>  array('get' 	 => "select T1.*, desc_decreto, desc_sistema, desc_escenario, t5.desc_variable, desc_indicador_variable,desc_nivel_temp,
											desc_decreto || ' / '  || desc_sistema || ' / '  || desc_escenario as  desc_escenario_decreto
											from tbL_dcr_escenarios_decreto t1
											inner join tbl_dcr_decretos t2 on t1.cod_decreto=t2.cod_decreto
											inner join tbl_dcr_escenarios t3 on t1.cod_escenario=t3.cod_escenario
											inner join tbL_dcr_sistemas t4 on t1.cod_sistema=t4.cod_sistema
											inner join tbl_mast_variables t5 on t1.cod_variable=t5.cod_variable  
											inner join tbl_dcr_nivel_temp t6 on t1.cod_nivel_temp=t6.cod_nivel_temp
											inner join tbl_dcr_indicadores_variable t7 on t1.cod_indicador_variable=t7.cod_indicador_variable											
											", 		
		'update' => 'update tbL_dcr_escenarios_decreto set cod_decreto=:cod_decreto:, cod_sistema=:cod_sistema:, cod_escenario=:cod_escenario:, cod_variable=:cod_variable:, cod_nivel_temp=:cod_nivel_temp:, cod_indicador_variable=:cod_indicador_variable: where cod_escenario_decreto=:cod_escenario_decreto:',	
		'insert' => 'insert into tbL_dcr_escenarios_decreto (cod_decreto,cod_sistema,cod_escenario,cod_variable,cod_nivel_temp,cod_indicador_variable) values (:cod_decreto:,:cod_sistema:,:cod_escenario:,:cod_variable:,:cod_nivel_temp:,:cod_indicador_variable:)',
		'delete' => 'delete from tbL_dcr_escenarios_decreto where cod_escenario_decreto=:cod_escenario_decreto:'),					
		'VALOR_REFERENCIA'=>  array('get' 	 => 'select t1.*,t5.desc_decreto, desc_sistema, desc_escenario, t3.desc_nivel_temp, t4.desc_indicador_umbral,
												t8.cod_var_ref1,t8.cod_var_ref2,t8.cod_nivel_temp1,t8.cod_nivel_temp2, t2.cod_variable, tmv.desc_variable,
												tiv.desc_indicador_variable, 
												tnt1.desc_nivel_temp as desc_nivel_temp1,
												tnt2.desc_nivel_temp as desc_nivel_temp2
												from tbl_dcr_valores_referencia t1												
												inner join tbL_dcr_escenarios_decreto t2 on t1.cod_escenario_decreto=t2.cod_escenario_decreto
												left  join tbl_dcr_indicadores_variable tiv on tiv.cod_indicador_variable=t2.cod_indicador_variable
												inner join tbl_dcr_decretos t5 on t2.cod_decreto=t5.cod_decreto
												inner join tbl_dcr_sistemas t6 on t2.cod_sistema=t6.cod_sistema
												inner join tbl_dcr_escenarios t7 on t2.cod_escenario=t7.cod_escenario
												inner join tbl_dcr_nivel_temp t3 on t1.cod_nivel_temp=t3.cod_nivel_temp
												inner join tbl_dcr_indicadores_umbral t4 on t1.cod_indicador_umbral=t4.cod_indicador_umbral
												left join tbl_dcr_variables_ref t8 on t8.cod_valor_ref=t1.cod_valor_ref
												left join tbl_mast_variables tmv on tmv.cod_variable=t2.cod_variable
												left join tbl_dcr_nivel_temp tnt1 on t8.cod_nivel_temp1=tnt1.cod_nivel_temp
												left join tbl_dcr_nivel_temp tnt2 on t8.cod_nivel_temp2=tnt2.cod_nivel_temp
												',
												'update' => 'update tbl_dcr_valores_referencia set cod_escenario_decreto=:cod_escenario_decreto:,cod_nivel_temp=:cod_nivel_temp:, cod_indicador_umbral=:cod_indicador_umbral:,ind_valor_oficial=:ind_valor_oficial:  where cod_valor_ref=:cod_valor_ref:',
												'insert' => 'insert into tbl_dcr_valores_referencia (cod_nivel_temp, cod_indicador_umbral, cod_escenario_decreto, ind_valor_oficial) values (:cod_nivel_temp:,:cod_indicador_umbral:,:cod_escenario_decreto:,:ind_valor_oficial:) ',
												'delete' => 'delete from tbl_dcr_valores_referencia where cod_valor_ref=:cod_valor_ref:; 
															 delete from tbl_dcr_variables_ref     where cod_valor_ref=:cod_valor_ref:; 
															 delete from tbl_dcr_umbrales_compuestos where cod_valor_ref=:cod_valor_ref:;
															 delete from tbl_dcr_valores_umbral where cod_valor_ref=:cod_valor_ref:;
															 ',
												'lastId' => 'select max(cod_valor_ref) as id from tbl_dcr_valores_referencia'												
											),
		'TIPO_ESTACIONES'=>  array('get' 	 => 'select * from tbl_mast_tipos_estaciones '),			
		'SUBCUENCAS'=>  array('get' 	 => 'select * from tbl_mast_subcuencas '),
		'ESTADO_ESTACIONES'=>  array('get' 	 => 'select * from tbl_mast_estados_estacion '),
		'COMUNIDAD'=>  array('get' 	 => 'select * from tbl_mast_comunidades '),		
		'PROVINCIA'=>  array('get' 	 => 'select * from tbl_mast_provincias '),		
		'COMARCA'=>  array('get' 	 => 'select * from tbl_mast_comarcas '),		
		'MUNICIPIO'=>  array('get' 	 => 'select * from tbl_mast_terminos_municipales '),		
		'ESTACIONES'=>  array('get' 	 => 'select t1.*, t2.desc_tipo_estacion, t3.desc_estado_estacion from tbl_mast_estaciones t1 left join tbl_mast_tipos_estaciones t2 on t1.cod_tipo_estacion=t2.cod_tipo_estacion left join tbl_mast_estados_estacion t3 on t1.cod_estado_estacion=t3.cod_estado_estacion ', 		
		'detail' => 'select * from tbl_mast_estaciones where cod_estacion=:cod_estacion:',
		'update' => 'update tbl_mast_estaciones set desc_estacion=:desc_estacion:, desc_estacion_ext=:desc_estacion_ext: where cod_estacion=:cod_estacion: ',	
		'insert' => 'insert into tbl_mast_estaciones (cod_estacion, cod_tipo_estacion, desc_estacion, desc_estacion_ext, cod_comarca, cod_provincia, cod_comunidad, cod_termino_municipal) values (:cod_estacion: , :cod_tipo_estacion:,  :desc_estacion:, :desc_estacion_ext:, :cod_comarca:, :cod_provincia:, :cod_comunidad:, :cod_termino_municipal:)',
		'delete' => 'delete from tbl_mast_estaciones where cod_estacion=:cod_estacion:'	),	
//		'VARIABLES'=>  array('get' 	 => 'select t1.*, desc_estacion from tbl_mast_variables t1 left join tbl_mast_estaciones t2 on t1.cod_estacion=t2.cod_estacion  ', 		
//		'insert' => 'emd',
//		'update' => 'emd',	
//		'delete' => 'pdo'),	
		'TIPOS_VARIABLE'=>  array('get' 	 => 'select * from tbl_mast_tipos_variables  ', 		
		'insert' => 'emd',
		'update' => 'emd',	
		'delete' => 'pdo'),									
		'ZONAS'=>  array('get' 	 => 'select * from tbl_mast_zonas  ', 		
		'insert' => 'emd',
		'update' => 'emd',	
		'delete' => 'pdo'),
		'AREAS'=>  array('get' 	 => 'select * from tbl_mast_areas  ', 		
		'insert' => 'emd',
		'update' => 'emd',	
		'delete' => 'pdo'),
		'ORIGENES'=>  array('get' 	 => 'select * from tbl_mast_fuentes_origen  ', 		
		'insert' => 'emd',
		'update' => 'emd',	
		'delete' => 'pdo'),		
		'VAR_DERIVADAS_Y_DEPENDIENTES'=>  array('get' 	 => "select t1.cod_variable_destino, t1.tipo, t2.cod_estacion, t1.cod_curva, :cod_variable: as cod_variable_origen
															  from (SELECT cod_variable_destino as cod_variable_destino, 1 as tipo, 0 as cod_curva 
																	  from tbl_gest_sum_vars_derivadas 
																	  where cod_variable_origen=:cod_variable:		
															  UNION
																	  SELECT cod_variable_caudal  as cod_variable_destino, 2 as tipo, cod_curva  
																	  FROM tbl_gest_curvas_gasto
																	  WHERE cod_variable_nivel = :cod_variable: and ind_activa = 'S' 
															 ) t1 INNER JOIN  tbl_mast_variables t2 on t1.cod_variable_destino=t2.cod_variable
															  "),
		'DATOS_BRUTOS'=>  array('findVariables' 	 => 'select distinct T1.cod_variable, T1.desc_variable, T1.cod_estacion, 
														 T2.desc_estacion, 
														 COUNT(cod_motivo_invalidacion) as has_invalids 
														 from tbl_mast_variables T1														
														INNER JOIN tbl_mast_estaciones T2 ON T1.cod_estacion=t2.cod_estacion
														INNER JOIN tbl_mast_tipos_variables T3 ON T1.cod_tipo_variable=T3.cod_tipo_variable
														INNER JOIN tbl_hist_medidas T4 ON T4.cod_variable=T1.cod_variable
														', 		
						 	   'findData' => 		   'select t1.*, t2.desc_motivo_invalidacion from tbl_hist_medidas t1
														left join tbl_mast_motivos_invalidacion t2 on t1.cod_motivo_invalidacion=t2.cod_motivo_invalidacion',
				'insert' => 'insert into tbl_hist_medidas(cod_estacion,cod_variable,timestamp_medicion,anyo,mes,dia,timestamp_insercion,ind_validacion,valor, cod_motivo_invalidacion) values 
							(:cod_estacion:,:cod_variable:,:timestamp_medicion:,:anyo:,:mes:,:dia:,CURRENT_TIMESTAMP,:ind_validacion:,:valor:,:cod_motivo_invalidacion:)
							',
				'update' => "update tbl_hist_medidas set ind_validacion=:ind_validacion:,cod_motivo_invalidacion=:cod_motivo_invalidacion: 
							 where 
							 cod_variable IN (SELECT :cod_variable: 
											  UNION 
											  SELECT cod_variable_caudal FROM public.tbl_gest_curvas_gasto WHERE cod_variable_nivel = :cod_variable: and ind_activa = 'S')
							 and timestamp_medicion=:timestamp_medicion:",
				'markSumarizacion' => "
							insert into tbl_ctl_sumarizaciones (cod_estacion,cod_variable,data_sumarizar, ind_sum_dia, ind_sum_semana, ind_sum_mes, ind_sum_anyo, ind_calc_formulas, ind_sum_hora) VALUES (
						   (SELECT COD_ESTACION FROM tbl_mast_variables where cod_variable=:cod_variable:),:cod_variable:,:data_sumarizar:, 'S','S','S','S','S','N')
						   on conflict (cod_variable, data_sumarizar) do
						   update set ind_sum_dia='S', ind_sum_semana='S', ind_sum_mes='S',ind_sum_anyo='S',ind_calc_formulas='S',ind_sum_hora='N';"							
							 ,	
				'delete' => 'pdo'),													
		'DATOS_DIARIOS'=>  array('findVariables' 	 => 'select distinct T1.cod_variable, T1.desc_variable, T1.cod_estacion, 
														T2.desc_estacion, 
														false as has_invalids,
														fn_sgi_is_acum(T1.cod_variable) as is_acum
														from tbl_mast_variables T1														
														INNER JOIN tbl_mast_estaciones T2 ON T1.cod_estacion=t2.cod_estacion
														INNER JOIN tbl_mast_tipos_variables T3 ON T1.cod_tipo_variable=T3.cod_tipo_variable
														INNER JOIN tbl_olap_medidas_dias T4 ON T4.cod_variable=T1.cod_variable
														', 		
							'findData' => 				'select t1.*  from tbl_olap_medidas_dias t1														 
							',
							'markSumarizacion' => "
							insert into tbl_ctl_sumarizaciones (cod_estacion,cod_variable,data_sumarizar, ind_sum_dia, ind_sum_semana, ind_sum_mes, ind_sum_anyo, ind_calc_formulas, ind_sum_hora) VALUES (
						   (SELECT COD_ESTACION FROM tbl_mast_variables where cod_variable=:cod_variable:),:cod_variable:,:data_sumarizar:, 'N','S','S','S','S','N')
						   on conflict (cod_variable, data_sumarizar) do
						   update set ind_sum_dia='N', ind_sum_semana='S', ind_sum_mes='S',ind_sum_anyo='S',ind_calc_formulas='S',ind_sum_hora='N';",							
		'insert' => 'emd',
		'update_acum'  => 'update tbl_olap_medidas_dias set valor_acum=:value: where cod_variable=:cod_variable: and data_medicion=:data_medicion:',	
		'update_media' => 'update tbl_olap_medidas_dias set valor_media=:value: where cod_variable=:cod_variable: and data_medicion=:data_medicion:',	
		'delete' => 'pdo'),			
		// Tablas copia de la bb.dd de manuel	
		'TIPO_ESTACIONES_GENERAL'=>  array( 'get' => "select * from 
											(select * from tbl_tipo_estacion where cod_tipo_estacion IN ('PZ','PZM','ECC','CR','S','R','E','EXC','EAA','EM','NR','D','CC','CCT','ACA','AUS') 
											UNION select 'EMAEMET', 'AEMET'
											) as aux
											"),		
		/*'TIPO_ESTACIONES_GENERAL'=>  array( 'get' 	 => "select * from tbl_tipo_estacion 
											where cod_tipo_estacion IN ('ECC','CR','CP','S','R','E','EXC','EAA','EM','NR','D')
											/*UNION select 'SDAMULTI', 'Sonda Multiparamétrica' 
											UNION select 'PIEZO_AUTO', 'Piezo Automático' 
											"),*/
		'GEST_VALIDACION'=>  array( 'get' 	 => "SELECT *, ind_aplicacion_validacion_aut,
													       CASE WHEN ind_aplicacion_validacion_aut='S' THEN 'Sí' ELSE  'No'  END AS desc_aplicacion_validacion_aut		
		    FROM (
			select  T1.cod_regla_val, T1.cod_tipo_variable, T2.desc_tipo_variable,  T1.cod_variable, T3.cod_estacion, T4.desc_estacion,T1.cod_tipo_validacion_aut, T5.desc_tipo_validacion_aut,T1.ind_aplicacion_validacion_aut,
			CASE WHEN T1.cod_variable='****' THEN 0 ELSE  1  END AS order_variable, valor_min_validacion_aut, valor_max_validacion_aut,valor_pend_validacion_aut
			 from tbl_gest_val_variables T1
			LEFT JOIN tbl_mast_tipos_variables T2 on T1.cod_tipo_variable=T2.cod_tipo_variable
			LEFT JOIN tbl_mast_variables T3 on T1.cod_variable=T3.cod_variable
			LEFT JOIN tbl_mast_estaciones T4 on T3.cod_estacion=T4.cod_estacion
			LEFT JOIN tbl_mast_tipos_validaciones_au T5 on T1.cod_tipo_validacion_aut=T5.cod_tipo_validacion_aut
			) AUX",
			'update' =>  "update tbl_gest_val_variables set ind_aplicacion_validacion_aut=:ind_aplicacion_validacion_aut:,
						  cod_variable=:cod_variable:, cod_tipo_variable=:cod_tipo_variable:,
						  cod_tipo_validacion_aut=:cod_tipo_validacion_aut:,
						  valor_max_validacion_aut=:valor_max_validacion_aut:,valor_min_validacion_aut=:valor_min_validacion_aut:,
						  valor_pend_validacion_aut=:valor_pend_validacion_aut:
						  where cod_regla_val = :cod_regla_val: ",
			'delete' => " delete from tbl_gest_val_variables where cod_regla_val=:cod_regla_val:",
			'insert' => " insert into tbl_gest_val_variables (cod_regla_val, cod_variable, cod_tipo_variable,ind_aplicacion_validacion_aut,cod_tipo_validacion_aut,valor_min_validacion_aut,
			valor_max_validacion_aut,valor_pend_validacion_aut			
			) values ((select max(cod_regla_val)+1 from tbl_gest_val_variables), :cod_variable:,:cod_tipo_variable:,
			:ind_aplicacion_validacion_aut:,:cod_tipo_validacion_aut:,:valor_min_validacion_aut:,
			:valor_max_validacion_aut:,:valor_pend_validacion_aut:
			
			)",
		),
		'GEST_SUMARIZACION'=>  array( 'get' 	 => "SELECT *	
		    FROM (
			select  T1.cod_regla_sum, T1.cod_tipo_variable, T2.desc_tipo_variable,  T1.cod_variable, T3.cod_estacion, T4.desc_estacion,
			ind_sum,ind_sum_dia,ind_sum_semana,ind_sum_mes,ind_sum_anyo,
			CASE WHEN T1.cod_variable='****' THEN 0 ELSE  1  END AS order_variable 
			 from tbl_gest_sum_variables T1
			LEFT JOIN tbl_mast_tipos_variables T2 on T1.cod_tipo_variable=T2.cod_tipo_variable
			LEFT JOIN tbl_mast_variables T3 on T1.cod_variable=T3.cod_variable
			LEFT JOIN tbl_mast_estaciones T4 on T3.cod_estacion=T4.cod_estacion			
			) AUX",
			'update' =>  "update tbl_gest_sum_variables set ind_sum=:ind_sum:,ind_sum_dia=:ind_sum_dia:,ind_sum_semana=:ind_sum_semana:,ind_sum_mes=:ind_sum_mes:,ind_sum_anyo=:ind_sum_anyo:
						  where cod_regla_sum = :cod_regla_sum: ",
			'delete' => " delete from tbl_gest_sum_variables where cod_regla_sum=:cod_regla_sum:",
			'insert' => " insert into tbl_gest_sum_variables (cod_regla_sum, cod_variable, cod_tipo_variable,ind_sum,ind_sum_dia,ind_sum_semana,ind_sum_mes,ind_sum_anyo) 
						 values 
						 ((select max(cod_regla_sum)+1 from tbl_gest_sum_variables), :cod_variable:,:cod_tipo_variable:,:ind_sum:,:ind_sum_dia:,:ind_sum_semana:,:ind_sum_mes:,:ind_sum_anyo:)",
		),		
        'ESTACIONES_GENERAL'=>  array( 'get' 	 => "
		select * from (
			select trim(cod_estacion) as cod_estacion, nombre, tipo,
			(select visible from tbl_sec_rel_nivel_acceso_datos T2 
			where cod_nivel_acceso=0 
			and t2.cod_tipo_item=t1.tipo 
			and t2.cod_item=t1.cod_estacion) as visible_public
			from tbl_estacion t1
			UNION
			select trim(cod_estacion) as cod_estacion, nombre, 'AUS' as tipo,
			(select visible from tbl_sec_rel_nivel_acceso_datos T2 
			where cod_nivel_acceso=0 
			and t2.cod_tipo_item='AUS'
			and t2.cod_item=t1.cod_estacion) as visible_public
			from tbl_estacion t1 where t1.tipo='E'			
			UNION
			select cod_estacion as cod_estacion, nombre as nombre, 'EMAEMET' as tipo,										
			(select visible from tbl_sec_rel_nivel_acceso_datos T2 
			where cod_nivel_acceso=0 
			and t2.cod_tipo_item='EMAEMET'
			and t2.cod_item=t1.cod_estacion) as visible_public													   
			from tbl_estacion_aemet t1			
		) as aux
			"),
/*																						
		'ESTACIONES_GENERAL'=>  array( 'get' 	 => "
										select * from (select trim(cod_estacion) as cod_estacion, nombre, tipo,
										(select visible from tbl_sec_rel_nivel_acceso_datos T2 
										where cod_nivel_acceso=0 
										and t2.cod_tipo_item=t1.tipo 
										and t2.cod_item=t1.cod_estacion) as visible_public
										from tbl_estacion t1
										UNION
										select cod_piezo as cod_estacion, municipio as nombre, 'PIEZO_AUTO' as tipo,										
										(select visible from tbl_sec_rel_nivel_acceso_datos T2 
										where cod_nivel_acceso=0 
										and t2.cod_tipo_item='PIEZO_AUTO'
										and t2.cod_item=t1.cod_piezo) as visible_public													   
										from tbl_estacion_piezo t1										
										UNION
										select cod_sonda as cod_estacion, nombre, 'SDAMULTI' as tipo,
										(select visible from tbl_sec_rel_nivel_acceso_datos T2 
										where cod_nivel_acceso=0 
										and t2.cod_tipo_item='SDAMULTI'
										and t2.cod_item=t1.cod_sonda) as visible_public
										from tbl_sonda_mult  t1, tbl_estacion 
										where replace(cod_sonda,'S','E') = tbl_estacion.cod_estacion) aux
										"),
*/										
		// Acceso a base de datos 27			
		'MOTIVOS_INVALIDACION_27'=>  array('get' 	 => 'select * from tbl_mast_motivos_invalidacion'),  		
		'DATOS_BRUTOS_27'=>  array(	
			'findVariablesRedPiezo' 	 => "select t1.cod_variable, t1.descripcion, t2.cod_estacion  as cod_estacion, t2.municipio as nombre,
																sum(CASE WHEN validez=1 or validez is null or validez=0  THEN 0 ELSE  1  END ) as has_invalids 
																from tbl_variable T1
																inner join tbl_estacion T2 on  T2.tipo IN ('PZ', 'PZM')and split_part(T1.cod_variable,'/',1)=t2.cod_estacion 
																inner join tbl_dato_piezo T3 ON T3.cod_variable=T1.cod_variable
																",	
			'findVariablesRed' 	 => "select t1.cod_variable, t1.descripcion,t2.cod_estacion, t2.nombre,
																sum(CASE WHEN validez=1 or validez is null or validez=0  THEN 0 ELSE  1  END ) as has_invalids 
																from tbl_variable T1
																inner join tbl_estacion T2 on  split_part(T1.cod_variable,'/',1)=t2.cod_estacion 
																inner join <<table>> T3 ON T3.cod_variable=T1.cod_variable
																",																	

									'findDataRed' =>        "select t1.*, 
																 CASE WHEN T1.validez=1 THEN 'S' WHEN T1.validez=0 OR T1.validez is null then null  ELSE  'N'  END as ind_validacion,
																 t2.cod_motivo_invalidacion as cod_motivo_invalidacion, 
																 t2.desc_motivo_invalidacion 
																 from <<table>> t1
																 left join tbl_mast_motivos_invalidacion t2 on t1.validez =t2.cod_validez
																 ",	
									'update' => 'update <<table>> set validez=(select cod_validez from tbl_mast_motivos_invalidacion where cod_motivo_invalidacion=:cod_motivo_invalidacion:)									
												 where cod_variable=:cod_variable: and timestamp=:timestamp:',	
									),																								
		'ESTACIONES_27'=>  array('get' 	 => " select cod_estacion, cod_estacion || ' - ' || nombre as nombre from tbl_estacion "),
		'ESTACIONES_PIEZO_27'=>  array('get' 	 => " select * from (select cod_estacion as cod_estacion, cod_estacion || ' - ' || municipio as nombre from tbl_estacion where tipo IN ('PZM','PZ')) aux "),
		'VARIABLES_27'=>  array('get' 	 => " select * from tbl_variable T1 
											  inner join tbl_estacion T2 on  split_part(T1.cod_variable,'/',1)=t2.cod_estacion "),
		'VARIABLES_PIEZO_27'=>  array('get' 	 => " select * from tbl_variable T1 
											  		 inner join tbl_estacion T2 on  T2.tipo IN ('PZ','PZM') AND split_part(T1.cod_variable,'/',1)=t2.cod_estacion"),											  
		'SONDAS_AQUADAM_27'=>  array('get' 	 => "select trim(cod_sonda) as cod_sonda,  trim(cod_sonda) ||  ' - ' || nombre as nombre,temperatura, ph, oxigeno, conductividad, redox, turbidez, secchi, clorofila1, ficocianina  
												 from (select cod_sonda, nombre, temperatura, ph, oxigeno, conductividad, redox, turbidez, secchi, clorofila1, ficocianina 
												 from tbl_sonda_mult, tbl_estacion t1 where replace(cod_sonda,'S','E') = t1.cod_estacion ) AUX ", 		
										"get2" 	 => "select cod_estacion ||  ' - ' || nombre as nombre, cod_estacion as cod_sonda from tbl_estacion  ", 		
										'insert' => 'emd',
										'update' => 'emd',	
										'delete' => 'pdo'),											
		'DATOS_SONDAS_AQUADAM_27'=>  array(
								  'count' 	 => 
												  " select  COUNT(distinct cod_sonda ||  timestamp_sondeo) as total FROM (
														  select t1.*,t1.timestamp_sondeo as date_from, t1.timestamp_sondeo as date_to,
														  CASE WHEN TO_CHAR(T1.timestamp_sondeo,'HH24') IN ('00','06','12','18') THEN '1' ELSE '0' end as  solo_programados,
														  CASE WHEN 
														  (COALESCE(val_temperatura,1)+COALESCE(val_ph,1)+COALESCE(val_oxigeno,1)
														  +COALESCE(val_conductividad,1)+COALESCE(val_redox,1)+COALESCE(val_turbidez,1)+
														  +COALESCE(val_secchi,1)+COALESCE(val_redox,1)+COALESCE(val_ficocianina,1)
														  +COALESCE(val_clorofila1,1))>10	then '1'
														  else '0'
														  end														  
														  as hay_invalidados														  
														from tbl_dato_sonda_mult t1  
													) aux", 
								  'get' 	 => "select *, 
								  						CASE WHEN AUX2.hay_invalidados='0' THEN 'No' ELSE  'Sí' END as hay_invalidados_desc								  
														  from (select distinct 
														  CASE WHEN 
															(COALESCE(val_temperatura,1)+COALESCE(val_ph,1)+COALESCE(val_oxigeno,1)
															+COALESCE(val_conductividad,1)+COALESCE(val_redox,1)+COALESCE(val_turbidez,1)+
															+COALESCE(val_secchi,1)+COALESCE(val_redox,1)+COALESCE(val_clorofila1,1)
															+COALESCE(val_ficocianina,1))>10	then '1'
															else '0'
															end															
															as hay_invalidados,		   
															CASE WHEN TO_CHAR(T1.timestamp_sondeo,'HH24') IN ('00','06','12','18') THEN '1' ELSE '0' end as  solo_programados,
								  								  trim(T1.cod_sonda) as cod_sonda, T1.timestamp_sondeo, 
																  T1.timestamp_sondeo as date_from, 
																  T1.timestamp_sondeo as date_to,
																  AUX.nombre																  																  
								  								  from tbl_dato_sonda_mult T1
								  								  INNER JOIN (select replace(cod_estacion,'E','S') as cod, nombre from tbl_estacion where tipo='E') AUX
																	ON AUX.cod=t1.cod_sonda
																)  AUX2  ", 		
								  'detailMuestra' => 'select * from tbl_dato_sonda_mult',
								  'getReferenceDays' => 'select a.timestamp_sondeo,
														(select max(timestamp_sondeo) from tbl_dato_sonda_mult where cod_sonda=a.cod_sonda) as last_date,
														(select min(timestamp_sondeo) from tbl_dato_sonda_mult where cod_sonda=a.cod_sonda and timestamp_sondeo>a.timestamp_sondeo) as next_date,
														(select max(timestamp_sondeo) from tbl_dato_sonda_mult where cod_sonda=a.cod_sonda and timestamp_sondeo<a.timestamp_sondeo) as prev_date								  
														from tbl_dato_sonda_mult a											   
														where a.cod_sonda=:cod_sonda: and a.timestamp_sondeo=:timestamp_sondeo:
								  ',
								  'getDatosNivel' => "select volumen, nivel as nmn, valor as nivel, Round(valor*100/volumen::numeric, 2) as porcentaje  
								  				   	  FROM tbl_nivel, tbl_ultimo_dato_medido  
													  where  trim(cod_nivel) = :cod_nivel: and  trim(cod_variable) = :cod_variable:",
								  'getDatosCotas' => "select * FROM tbl_nivel   
													  where  trim(cod_nivel) like :cod_nivel: ",														
								  'update' => 'update tbl_dato_sonda_mult 
												 set val_temperatura=:val_temperatura:,
												 val_ph=:val_ph:,
												 val_conductividad=:val_conductividad:,
												 val_oxigeno=:val_oxigeno:,
												 val_redox=:val_redox:,
												 val_turbidez=:val_turbidez:,
												 val_secchi=:val_secchi:,
												 val_clorofila1=:val_clorofila1:,
												 val_ficocianina=:val_ficocianina:
												 where cod_sonda=:cod_sonda: and timestamp_sondeo=:timestamp_sondeo: ',
									'updateMasivoPartial2' 	 => "update tbl_dato_sonda_mult 
																set val_temperatura=:val_temperatura:,
																val_ph=:val_ph:,
																val_conductividad=:val_conductividad:,
																val_oxigeno=:val_oxigeno:,
																val_redox=:val_redox:,
																val_turbidez=:val_turbidez:,
																val_secchi=:val_secchi:,
																val_clorofila1=:val_clorofila1:,
																val_ficocianina=:val_ficocianina:
																
																",
									'updateMasivoPartial' 	 => 
												 " select  COD_SONDA, TIMESTAMP_SONDEO, NUM_LECTURA FROM (
														 select t1.*,t1.timestamp_sondeo as date_from, t1.timestamp_sondeo as date_to,
														 CASE WHEN 
														 (COALESCE(val_temperatura,1)+COALESCE(val_ph,1)+COALESCE(val_oxigeno,1)
														 +COALESCE(val_conductividad,1)+COALESCE(val_redox,1)+COALESCE(val_turbidez,1)+
														 +COALESCE(val_ficocianina,1)+COALESCE(val_secchi,1)+COALESCE(val_redox,1)
														 +COALESCE(val_clorofila1,1))>10	then 1
														 else 0
														 end
														 
														 as hay_invalidados														  

													   from tbl_dato_sonda_mult t1  
												   ) aux",												 
								  'delete' => 'pdo'),
	'IMPORT_MINUTAL'=>  array(
			// Excluimos calculadas y derivadas
			'validateVariable' 	 => "select * from tbl_mast_variables where cod_variable=:cod_variable: 
									and cod_origen IS null and cod_variable NOT IN 
									(select cod_variable_destino from tbl_gest_sum_vars_derivadas)
									and cod_variable not in (
										select cod_variable_caudal from tbl_gest_curvas_gasto where ind_activa='S' 
									)
									",
			'checkExistenceDato' 	 => 'select * from tbl_hist_medidas where cod_variable=:cod_variable: and 
										 timestamp_medicion=:timestamp_medicion:',
			'inserOrUpdate' 	 => "
					insert into tbl_hist_medidas (cod_estacion,cod_variable,timestamp_medicion, anyo, mes, dia, timestamp_insercion, ind_validacion, valor, cod_mod_fuente_origen) 
					SELECT cod_estacion, :cod_variable:, :timestamp_medicion:,
					EXTRACT(year from timestamp :timestamp_medicion:) ,
					EXTRACT(month from timestamp :timestamp_medicion:),
					EXTRACT(day from timestamp :timestamp_medicion:),
					CURRENT_TIMESTAMP,:ind_validacion:,:valor:,:cod_mod_fuente_origen:
					FROM tbl_mast_variables where cod_variable=:cod_variable:				
					on conflict (cod_variable, timestamp_medicion) do
					update set valor=:valor:, cod_motivo_invalidacion=null, timestamp_insercion=CURRENT_TIMESTAMP,ind_validacion=:ind_validacion:,cod_mod_fuente_origen=:cod_mod_fuente_origen:
				",
			'insert' 	 => "
				insert into tbl_hist_medidas (cod_estacion,cod_variable,timestamp_medicion, anyo, mes, dia, timestamp_insercion, ind_validacion, valor, cod_mod_fuente_origen) 
				SELECT cod_estacion, :cod_variable:, :timestamp_medicion:,
				EXTRACT(year from timestamp :timestamp_medicion:) ,
				EXTRACT(month from timestamp :timestamp_medicion:),
				EXTRACT(day from timestamp :timestamp_medicion:),
				CURRENT_TIMESTAMP,:ind_validacion:,:valor:,:cod_mod_fuente_origen:
				FROM tbl_mast_variables where cod_variable=:cod_variable:
			",				
			'showHistory' 	 => "select * from tbl_log_hist_medidas where cod_variable=:cod_variable: and timestamp_medicion = :timestamp_medicion: order by timestamp_insercion desc",
			'insertLog' 	 => "
				insert into tbl_log_hist_medidas (cod_variable,timestamp_medicion, timestamp_insercion, ind_validacion, valor, valor_ant,timestamp_insercion_ant) 
				SELECT :cod_variable:, :timestamp_medicion:,
				CURRENT_TIMESTAMP,:ind_validacion:,:valor:, 
				(SELECT valor FROM tbl_hist_medidas where cod_variable=:cod_variable: and timestamp_medicion=:timestamp_medicion:),
				(SELECT timestamp_insercion FROM tbl_hist_medidas where cod_variable=:cod_variable: and timestamp_medicion=:timestamp_medicion:)
				FROM tbl_mast_variables where cod_variable=:cod_variable:				
				on conflict (cod_variable, timestamp_medicion,timestamp_insercion) do
				update set valor=:valor:, ind_validacion=:ind_validacion:
			",
			),									  		
	);
}