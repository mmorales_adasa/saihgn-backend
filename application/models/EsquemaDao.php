<?php
class EsquemaDao extends CI_Model 
{
	function remove($name){
		$params = array();
		$params['id_esquema']['value'] = strtoupper($name);
		$params['id_esquema']['type'] = Constants::TYPE_TEXT;        
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["ESQUEMAS"]["delete"], $params);                
        $query = $this->db->query($sql);           
        return true;
    }
	function inserOrUpdate($name, $content){
        $this->remove($name);
		$params = array();
		$params['id_esquema']['value'] = strtoupper($name);
        $params['id_esquema']['type'] = Constants::TYPE_TEXT;        
		/*$params['esquema_procesado']['value'] = json_encode($content, JSON_HEX_APOS);
        $params['esquema_procesado']['type'] = Constants::TYPE_TEXT;*/    
        
        $sql = $this->utildao->bindingParameters(Queries::BASIC_ENTITY["ESQUEMAS"]["insert"], $params);                
        $query = $this->db->query($sql);        
        return true;
    }    
}
?>