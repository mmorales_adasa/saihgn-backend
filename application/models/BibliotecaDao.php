<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';

class BibliotecaDao extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function getUltimoRefNumero()
	{
		$query = "SELECT max(ref_numero) as ref_numero FROM tbl_biblioteca_proyectos;";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data[0]['ref_numero'];
	}

	function listaCategorias()
	{
		$query = "SELECT * FROM tbl_biblioteca_categoria";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaUbicaciones()
	{
		$query = "SELECT * FROM tbl_biblioteca_ubicaciones";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaMunicipios()
	{
		$query = "SELECT * FROM tbl_biblioteca_municipio";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaProvincias()
	{
		$query = "SELECT * FROM tbl_biblioteca_provincia";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaZonas()
	{
		$query = "SELECT * FROM tbl_biblioteca_zona";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaClases()
	{
		$query = "SELECT * FROM tbl_biblioteca_clase";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaTipos()
	{
		$query = "SELECT * FROM tbl_biblioteca_tipo";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaToponimos()
	{
		$query = "select distinct toponimo from tbl_biblioteca_proyectos where toponimo is not null and toponimo != '' order by toponimo asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaNotas()
	{
		$query = "select distinct notas from tbl_biblioteca_proyectos where notas is not null and notas != '' order by notas asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaContratistas()
	{
		$query = "select distinct contratista from tbl_biblioteca_proyectos where contratista is not null and contratista != '' order by contratista asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaAutores()
	{
		$query = "select distinct autor from tbl_biblioteca_proyectos where autor is not null and autor != '' order by autor asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaRefletras()
	{
		$query = "select distinct ref_letra from tbl_biblioteca_proyectos where ref_letra is not null and ref_letra != '' order by ref_letra asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaArchivos()
	{
		$query = "select id_ubicacion, mueble, json_agg(

			balda order by case
			   when substring(balda from '^\d+$') is null then 9999
			   else cast(balda as integer)
			 end
		   
		   ) as baldas from tbl_biblioteca_archivos group by id_ubicacion, mueble order by case
			   when substring(mueble from '^\d+$') is null then 9999
			   else cast(mueble as integer)
			 end";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaProyectos($filters)
	{
		$retorno = [];

		$query = "SELECT 
					p.id_proyecto, 
					ref_numero, 
					fecha, 
					tip.nombre_tipo, 
					stip.nombre_subtipo, 
					clave,
					p.titulo,
					u.ubicaciones as ubicacion,
					case WHEN (pres.fecha_prestamo is not null AND pres.fecha_devolucion is null) THEN 'Sí' ELSE 'No' end as prestado
				FROM tbl_biblioteca_proyectos p 
				left join tbl_biblioteca_prestados pres on pres.id_proyecto = p.id_proyecto and pres.fecha_devolucion is null
				left join tbl_biblioteca_tipo tip on tip.id_tipo = p.id_tipo and pres.fecha_devolucion is null
				left join tbl_biblioteca_subtipo stip on stip.id_subtipo = p.id_subtipo and pres.fecha_devolucion is null
				left join (
					select id_proyecto, 
					string_agg(ubicaciones,', ') ubicaciones
					from(
						select 
							id_proyecto,
							case WHEN (nombre_ubicacion is not null) 
								THEN CONCAT(nombre_ubicacion,': ', string_agg(mueble_balda,', '),'') 
								ELSE null end as ubicaciones
						from(
							SELECT 
								p.id_proyecto, 
								tbu.nombre_ubicacion,
								CONCAT('Mueble ', tba.mueble, ' Balda ', string_agg(tba.balda,', '),'') mueble_balda
							FROM tbl_biblioteca_proyectos p 
							left join tbl_biblioteca_proyectos_archivos tbpa on tbpa.id_proyecto = p.id_proyecto 
							left join tbl_biblioteca_archivos tba on tba.id_archivo = tbpa.id_archivo
							left join tbl_biblioteca_ubicaciones tbu on tbu.id_ubicacion = tba.id_ubicacion
							group by p.id_proyecto, tbu.nombre_ubicacion, tba.mueble
						) as umb
						group by id_proyecto, nombre_ubicacion
					)as umbg
					group by id_proyecto
				) as u on u.id_proyecto = p.id_proyecto";

		$provincia = $filters['provincia'] ?? null;
		$zona = $filters['zona'] ?? null;
		$municipio = $filters['municipio'] ?? null;
		$tipo = $filters['tipo'] ?? null;
		$subtipo = $filters['subtipo'] ?? null;
		$categoria = $filters['categoria'] ?? null;
		$subcategoria = $filters['subcategoria'] ?? null;
		$clase = $filters['clase'] ?? null;
		$balda = $filters['balda'] ?? null;
		$numero = $filters['numero'] ?? null;
		$clave = $filters['clave'] ?? null;
		$toponimo = $filters['toponimo'] ?? null;
		$notas = $filters['notas'] ?? null;
		$contratista = $filters['contratista'] ?? null;
		$autor = $filters['autor'] ?? null;
		$fecha = $filters['anyo'] ?? null;
		$ubicacion = $filters['ubicacion'] ?? null;
		$balda = $filters['balda'] ?? null;
		$mueble = $filters['mueble'] ?? null;

		$filterQueries = [];
		if ($fecha != null) {
			array_push($filterQueries, "fecha = $fecha");
		}
		if ($provincia != null) {
			array_push($filterQueries, "id_provincia = $provincia");
		}
		if ($zona != null) {
			array_push($filterQueries, "id_zona=$zona");
		}
		if ($municipio != null) {
			array_push($filterQueries, "id_municipio=$municipio");
		}
		if ($tipo != null) {
			array_push($filterQueries, "tip.id_tipo=$tipo");
		}
		if ($subtipo != null) {
			array_push($filterQueries, "stip.id_subtipo=$subtipo");
		}
		if ($categoria != null) {
			array_push($filterQueries, "id_categoria=$categoria");
		}
		if ($subcategoria != null) {
			array_push($filterQueries, "id_subcategoria=$subcategoria");
		}
		if ($clase != null) {
			array_push($filterQueries, "id_clase=$clase");
		}
		if ($numero != null) {
			array_push($filterQueries, "ref_numero=$numero");
		}
		if ($clave != null) {
			array_push($filterQueries, "clave='$clave'");
		}
		if ($toponimo != null) {
			array_push($filterQueries, "toponimo='$toponimo'");
		}
		if ($notas != null) {
			array_push($filterQueries, "notas='$notas'");
		}
		if ($contratista != null) {
			array_push($filterQueries, "contratista='$contratista'");
		}
		if ($autor != null) {
			array_push($filterQueries, "autor='$autor'");
		}

		/*if ($ubicacion != null || $balda != null || $mueble != null) {
			array_push($filterQueriesArchivos, "tba.id_ubicacion=$ubicacion");
		}
		if ($balda != null) {
			array_push($filterQueriesArchivos, "tba.balda='$balda'");
		}
		if ($mueble != null) {
			array_push($filterQueriesArchivos, "tba.mueble='$mueble'");
		}*/

		if (count($filterQueries) > 0) {
			$where = implode(" and ", $filterQueries);
			$query .= " where $where";
		}

		$data = ConexionBD::EjecutarConsultaDev($query);

		// filtrado de proyectos por ubicaciones, se realiza por php para no complicar más la consulta general

		if ($ubicacion != null || $mueble != null || $balda != null) {
			
			$filterQueriesArchivos = [];

			if($ubicacion != null) array_push($filterQueriesArchivos, "tba.id_ubicacion=$ubicacion");
			if($mueble != null) array_push($filterQueriesArchivos, "tba.mueble like '$mueble'");
			if($balda != null) array_push($filterQueriesArchivos, "tba.balda like '$balda'");

			$where2 = implode(" AND ", $filterQueriesArchivos);

			$query2 = "SELECT DISTINCT id_proyecto 
						FROM tbl_biblioteca_archivos tba
						INNER JOIN tbl_biblioteca_proyectos_archivos tbpa ON tba.id_archivo = tbpa.id_archivo
						WHERE $where2";

			$data2 = ConexionBD::EjecutarConsultaDev($query2);

			$idProyectosFiltrados = [];

			foreach($data2 as $proyectosArchivos){
				$idProyectosFiltrados[] = $proyectosArchivos["id_proyecto"];
			}

			foreach($data as $proyecto){
				if(in_array($proyecto["id_proyecto"], $idProyectosFiltrados)) $retorno[] = $proyecto;
			}

		} else {
			$retorno = $data;
		}

		return $retorno;
	}

	function listaPrestados($filters)
	{

		$query = "SELECT 
			p.id_proyecto, 
			ref_numero, 
			fecha, 
			nombre_tipo, 
			nombre_subtipo, 
			clave,
			p.titulo,
			pres.fecha_prestamo,
			pres.username,
			u.ubicaciones as ubicacion
		FROM tbl_biblioteca_prestados pres
		inner join tbl_biblioteca_proyectos p on pres.id_proyecto = p.id_proyecto  and pres.fecha_devolucion is null
		inner join tbl_biblioteca_proyectos_archivos tbpa on tbpa.id_proyecto = p.id_proyecto 
		inner join tbl_biblioteca_archivos tba on tba.id_archivo = tbpa.id_archivo 
		inner join tbl_sec_rel_usuario_biblioteca_ubicacion bu on bu.id_ubicacion = tba.id_ubicacion AND bu.cod_usuario = " . $filters['cod_user'] ."
		left join tbl_biblioteca_tipo tip on tip.id_tipo = p.id_tipo
		left join tbl_biblioteca_subtipo stip on stip.id_subtipo = p.id_subtipo 
		left join (
			select id_proyecto, 
			string_agg(ubicaciones,', ') ubicaciones
			from(
				select 
					id_proyecto,
					case WHEN (nombre_ubicacion is not null) 
						THEN CONCAT(nombre_ubicacion,': ', string_agg(mueble_balda,', '),'') 
						ELSE null end as ubicaciones
				from(
					SELECT 
						p.id_proyecto, 
						tbu.nombre_ubicacion,
						CONCAT('Mueble ', tba.mueble, ' Balda ', string_agg(tba.balda,', '),'') mueble_balda
					FROM tbl_biblioteca_proyectos p 
					left join tbl_biblioteca_proyectos_archivos tbpa on tbpa.id_proyecto = p.id_proyecto 
					left join tbl_biblioteca_archivos tba on tba.id_archivo = tbpa.id_archivo
					left join tbl_biblioteca_ubicaciones tbu on tbu.id_ubicacion = tba.id_ubicacion
					group by p.id_proyecto, tbu.nombre_ubicacion, tba.mueble
				) as umb
				group by id_proyecto, nombre_ubicacion
			)as umbg
			group by id_proyecto
		) as u on u.id_proyecto = p.id_proyecto";

		$provincia = $filters['provincia'] ?? null;
		$zona = $filters['zona'] ?? null;
		$municipio = $filters['municipio'] ?? null;
		$tipo = $filters['tipo'] ?? null;
		$subtipo = $filters['subtipo'] ?? null;
		$categoria = $filters['categoria'] ?? null;
		$subcategoria = $filters['subcategoria'] ?? null;
		$clase = $filters['clase'] ?? null;

		$filterQueries = [];
		if ($provincia != null) {
			array_push($filterQueries, "id_provincia = $provincia");
		}
		if ($zona != null) {
			array_push($filterQueries, "id_zona=$zona");
		}
		if ($municipio != null) {
			array_push($filterQueries, "id_municipio=$municipio");
		}
		if ($tipo != null) {
			array_push($filterQueries, "id_tipo=$tipo");
		}
		if ($subtipo != null) {
			array_push($filterQueries, "id_subtipo=$subtipo");
		}
		if ($categoria != null) {
			array_push($filterQueries, "id_categoria=$categoria");
		}
		if ($subcategoria != null) {
			array_push($filterQueries, "id_subcategoria=$subcategoria");
		}
		if ($clase != null) {
			array_push($filterQueries, "id_clase=$clase");
		}
		if (count($filterQueries) > 0) {
			$query .= implode(" and ", $filterQueries);
		}
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}


	function listaSubtipos()
	{
		$query = "SELECT * FROM tbl_biblioteca_subtipo";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaTipoSubtipos($tipo) {
		$resultado = array();
		$query = "SELECT id_subtipo FROM tbl_biblioteca_tipo_subtipo WHERE id_tipo = $tipo";
		$data = ConexionBD::EjecutarConsultaDev($query);
		if (count($data) > 0) {
			foreach($data as $dato) {
				array_push($resultado, $dato["id_subtipo"]);
			}
		}
		return $resultado;
	}

	function listaCategoriaSubcategoria($categoria) {
		$resultado = array();
		$query = "SELECT * FROM tbl_biblioteca_categoria_subcategoria WHERE id_categoria = $categoria";
		$data = ConexionBD::EjecutarConsultaDev($query);
		if (count($data) > 0) {
			foreach($data as $dato) {
				array_push($resultado, $dato["id_subcategoria"]);
			}
		}
		return $resultado;
	}

	function listaCategoriaClase($categoria) {
		$resultado = array();
		$query = "SELECT id_clase FROM tbl_biblioteca_categoria_clase WHERE id_categoria = $categoria";
		$data = ConexionBD::EjecutarConsultaDev($query);
		if (count($data) > 0) {
			foreach($data as $dato) {
				array_push($resultado, $dato["id_clase"]);
			}
		}
		return $resultado;
	}

	function listaSubcategoria()
	{
		$query = "SELECT * FROM tbl_biblioteca_subcategoria";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaUsuarios()
	{
		$query = "SELECT * FROM tbl_sec_usuarios";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaBaldas()
	{
		$query = "select distinct balda from tbl_biblioteca_proyectos where balda is not null order by balda asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaNumeros()
	{
		$query = "select distinct ref_numero as numero from tbl_biblioteca_proyectos where ref_numero is not null order by ref_numero asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaClaves()
	{
		$query = "select distinct clave from tbl_biblioteca_proyectos where clave is not null and clave != '' order by clave asc";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function altaDocumento($propiedades) {
		foreach($propiedades as &$propiedad){
			if($propiedad == '') $propiedad = null;
		}

		$propiedades['zona'] = (!is_null($propiedades['zona'])) ?  					$propiedades['zona'] : 'NULL';
		$propiedades['provincia'] = (!is_null($propiedades['provincia'])) ? 		$propiedades['provincia'] : 'NULL';
		$propiedades['categoria'] = (!is_null($propiedades['categoria'])) ? 		$propiedades['categoria'] : 'NULL';
		$propiedades['tipo'] = (!is_null($propiedades['tipo'])) ? 					$propiedades['tipo'] : 'NULL';
		$propiedades['clase'] = (!is_null($propiedades['clase'])) ? 				$propiedades['clase'] : 'NULL';
		$propiedades['municipio'] = (!is_null($propiedades['municipio'])) ? 		$propiedades['municipio'] : 'NULL';
		$propiedades['subcategoria'] = (!is_null($propiedades['subcategoria'])) ? 	$propiedades['subcategoria'] : 'NULL';
		$propiedades['subtipo'] = (!is_null($propiedades['subtipo'])) ? 			$propiedades['subtipo'] : 'NULL';
		$propiedades['numero'] = (!is_null($propiedades['numero'])) ? 				$propiedades['numero'] : 'NULL';
		$propiedades['titulo'] = (!is_null($propiedades['titulo'])) ? "'". 			$propiedades['titulo'] ."'" : 'NULL';
		$propiedades['autor'] = (!is_null($propiedades['autor'])) ? "'". 			$propiedades['autor'] ."'" : 'NULL';
		$propiedades['fecha'] = (!is_null($propiedades['fecha'])) ? 				$propiedades['fecha'] : 'NULL';
		$propiedades['clave'] = (!is_null($propiedades['clave'])) ? "'". 			$propiedades['clave'] ."'" : 'NULL';
		$propiedades['toponimo'] = (!is_null($propiedades['toponimo'])) ? "'". 		$propiedades['toponimo'] ."'" : 'NULL';
		$propiedades['notas'] = (!is_null($propiedades['notas'])) ? "'". 			$propiedades['notas'] ."'" : 'NULL';
		$propiedades['contratista'] = (!is_null($propiedades['contratista'])) ? "'".$propiedades['contratista'] ."'" : 'NULL';
		$propiedades['ref_letra'] = (!is_null($propiedades['ref_letra'])) ? "'". 	$propiedades['ref_letra'] ."'" : 'NULL';
		$propiedades['ubicacion'] = (!is_null($propiedades['ubicacion'])) ? 		$propiedades['ubicacion'] : 'NULL';
		$propiedades['mueble'] = (!is_null($propiedades['mueble'])) ? "'". 			$propiedades['mueble'] ."'" : 'NULL';
		
		$query = "INSERT INTO tbl_biblioteca_proyectos (
			id_zona,
			id_provincia,
            id_categoria,
            id_tipo,
            id_clase,
            id_municipio,
            id_subcategoria,
            id_subtipo,
            ref_numero,
			referencia,
            titulo,
            autor,
            fecha,
            clave,
            toponimo,
            notas,
            contratista,
            ref_letra
		) VALUES ("
			. $propiedades['zona'] . ","
			. $propiedades['provincia'] . "," 
			. $propiedades['categoria'] . "," 
			. $propiedades['tipo'] . "," 
			. $propiedades['clase'] . "," 
			. $propiedades['municipio'] . "," 
			. $propiedades['subcategoria'] . "," 
			. $propiedades['subtipo'] . "," 
			. $propiedades['numero'] . "," 
			. $propiedades['numero'] . "," 
			. $propiedades['titulo'].  ","
			. $propiedades['autor'].  ","
			. $propiedades['fecha'].  ","
			. $propiedades['clave'].  ","
			. $propiedades['toponimo'].  ","
			. $propiedades['notas'].  ","
			. $propiedades['contratista'].  ","
			. $propiedades['ref_letra']
		.") returning id_proyecto";

		$proyecto = ConexionBD::EjecutarConsultaDev($query, true);

		$this->insertarProyectoUbicacion($propiedades, $proyecto[0]['id_proyecto']);
	}

	function insertarProyectoUbicacion($propiedades, $id_proyecto) {
		$archivos = array();
		if (isset($propiedades['balda'])) {
			foreach($propiedades['balda'] as $balda) {
				$query2 = "SELECT id_archivo FROM tbl_biblioteca_archivos WHERE balda = '$balda' AND mueble =" . $propiedades['mueble'] . " AND id_ubicacion = " . $propiedades['ubicacion'] . ";";
				$data2 = ConexionBD::EjecutarConsultaDev($query2, true);
				array_push($archivos, $data2);
			}
		}
		if (count($archivos) > 0) {
			foreach($archivos as $archivo) {
				$query3 = "INSERT INTO tbl_biblioteca_proyectos_archivos (id_proyecto, id_archivo) VALUES (". $id_proyecto . "," . $archivo[0]['id_archivo'] .");";
				ConexionBD::EjecutarConsultaDev($query3, false);
			}
		}
	}

	function updateDocumento($propiedades)
	{

		foreach($propiedades as &$propiedad){
			if($propiedad == '') $propiedad = null;
		}

		$propiedades['zona'] = (!is_null($propiedades['zona'])) ?  					$propiedades['zona'] : 'NULL';
		$propiedades['provincia'] = (!is_null($propiedades['provincia'])) ? 		$propiedades['provincia'] : 'NULL';
		$propiedades['categoria'] = (!is_null($propiedades['categoria'])) ? 		$propiedades['categoria'] : 'NULL';
		$propiedades['tipo'] = (!is_null($propiedades['tipo'])) ? 					$propiedades['tipo'] : 'NULL';
		$propiedades['clase'] = (!is_null($propiedades['clase'])) ? 				$propiedades['clase'] : 'NULL';
		$propiedades['municipio'] = (!is_null($propiedades['municipio'])) ? 		$propiedades['municipio'] : 'NULL';
		$propiedades['subcategoria'] = (!is_null($propiedades['subcategoria'])) ? 	$propiedades['subcategoria'] : 'NULL';
		$propiedades['subtipo'] = (!is_null($propiedades['subtipo'])) ? 			$propiedades['subtipo'] : 'NULL';
		$propiedades['numero'] = (!is_null($propiedades['numero'])) ? 				$propiedades['numero'] : 'NULL';
		$propiedades['titulo'] = (!is_null($propiedades['titulo'])) ? "'". 			$propiedades['titulo'] ."'" : 'NULL';
		$propiedades['autor'] = (!is_null($propiedades['autor'])) ? "'". 			$propiedades['autor'] ."'" : 'NULL';
		$propiedades['fecha'] = (!is_null($propiedades['fecha'])) ? 				$propiedades['fecha'] : 'NULL';
		$propiedades['clave'] = (!is_null($propiedades['clave'])) ? "'". 			$propiedades['clave'] ."'" : 'NULL';
		$propiedades['toponimo'] = (!is_null($propiedades['toponimo'])) ? "'". 		$propiedades['toponimo'] ."'" : 'NULL';
		$propiedades['notas'] = (!is_null($propiedades['notas'])) ? "'". 			$propiedades['notas'] ."'" : 'NULL';
		$propiedades['contratista'] = (!is_null($propiedades['contratista'])) ? "'".$propiedades['contratista'] ."'" : 'NULL';
		$propiedades['ref_letra'] = (!is_null($propiedades['ref_letra'])) ? "'". 	$propiedades['ref_letra'] ."'" : 'NULL';
		$propiedades['ubicacion'] = (!is_null($propiedades['ubicacion'])) ? 		$propiedades['ubicacion'] : 'NULL';
		$propiedades['mueble'] = (!is_null($propiedades['mueble'])) ? "'". 			$propiedades['mueble'] ."'" : 'NULL';
		
		$query = "UPDATE tbl_biblioteca_proyectos 
			SET 
				id_zona = " . $propiedades['zona'] . ",
				id_provincia = " . $propiedades['provincia'] . ",
				id_categoria = " . $propiedades['categoria'] . ",
				id_tipo = " . $propiedades['tipo'] . ",
				id_clase = " . $propiedades['clase'] . ",
				id_municipio = " . $propiedades['municipio'] . ",
				id_subcategoria = " . $propiedades['subcategoria'] . ",
				id_subtipo = " . $propiedades['subtipo'] . ",
				ref_numero = " . $propiedades['numero'] . ",
				referencia = " . $propiedades['numero'] . ",
				titulo = " . $propiedades['titulo'] . ",
				autor = " . $propiedades['autor'] . ",
				fecha = " . $propiedades['fecha'] . ",
				clave = " . $propiedades['clave'] . ",
				toponimo = " . $propiedades['toponimo'] . ",
				notas = " . $propiedades['notas'] . ",
				contratista = " . $propiedades['contratista'] . ",
				ref_letra = " . $propiedades['ref_letra'] . "
			WHERE id_proyecto = ". $propiedades['id_proyecto'] .";";

		ConexionBD::EjecutarConsultaDev($query, false);
		
		$query3 = "SELECT id_archivo from tbl_biblioteca_proyectos_archivos where id_proyecto = ". $propiedades['id_proyecto'] .";";
		$archivos = ConexionBD::EjecutarConsultaDev($query3, true);
		$this->actualizarProyectoUbicacion($archivos, $propiedades['id_proyecto']);
		$this->insertarProyectoUbicacion($propiedades, $propiedades['id_proyecto']);
	}

	function deleteDocumento($id_proyecto)
	{
		$query = "DELETE FROM tbl_biblioteca_proyectos WHERE id_proyecto = $id_proyecto";
		$data = ConexionBD::EjecutarConsultaDev($query, false);
		return $data;
	}

	function actualizarProyectoUbicacion($archivos, $id_proyecto) {
		$archivosArray = array();
		if(count($archivos) > 0) {
			foreach($archivos as $archivo) {
				array_push($archivosArray, $archivo['id_archivo']);
			}
		}
		$archivosString = implode(",", $archivosArray);

		$query4 = "DELETE FROM tbl_biblioteca_proyectos_archivos WHERE id_proyecto = $id_proyecto";
		ConexionBD::EjecutarConsultaDev($query4, false);
	}

	function findPrestadoByIdProyecto($id_proyecto)
	{
		$query = "SELECT * FROM tbl_biblioteca_prestados WHERE id_proyecto = $id_proyecto";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function findZonaById($idZona)
	{
		$query = "SELECT * FROM tbl_biblioteca_zona z WHERE z.id_zona = $idZona";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function findCategoriaById($idCategoria)
	{
		$query = "SELECT * FROM tbl_biblioteca_categoria c WHERE c.id_categoria = $idCategoria";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function findTipoById($idTipo)
	{
		$query = "SELECT * FROM tbl_biblioteca_tipo t WHERE t.id_tipo = $idTipo";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function listaBaldaMueble($id_ubicacion, $mueble=null, $balda=null)
	{
		$query = "SELECT mueble, balda FROM tbl_biblioteca_archivos WHERE id_ubicacion = $id_ubicacion";

		if($mueble !== 'null' && $mueble !== null){
			$query .= " AND mueble like '" . $mueble . "'";
		}

		if($balda !== 'null' && $balda !== null){
			$query .= " AND balda like '" . $balda . "'";
		}


		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;	
	}

	function findClaseById($idClase)
	{
		$query = "SELECT * FROM tbl_biblioteca_clase t WHERE t.id_clase = $idClase";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function findProyectoById($id_proyecto)
	{
		$ficha_biblioteca = new FichaBiblioteca();
		$query = "SELECT tbp.autor, tbp.clave , tbp.contratista , tbp.fecha , tbp.id_categoria ,tbp.id_clase , tbp.id_municipio , tbp.id_provincia, tbp.id_proyecto , 
						 tbp.id_subcategoria , tbp.id_subtipo , tbp.id_tipo , tbp.id_zona , tbp.notas, tbp.prestado , tbp.ref_letra , tbp.ref_numero , tbp.referencia , 
						 tbp.titulo , tbp.toponimo , tba.mueble , tba.balda , tba.id_ubicacion  
		FROM tbl_biblioteca_proyectos tbp 
		LEFT JOIN tbl_biblioteca_proyectos_archivos tbpa ON tbp.id_proyecto = tbpa.id_proyecto 
		LEFT JOIN tbl_biblioteca_archivos tba ON tbpa.id_archivo = tba.id_archivo
		WHERE tbp.id_proyecto = $id_proyecto";
		$data = ConexionBD::EjecutarConsultaDev($query);
		$ficha_biblioteca->setAutor($data[0]['autor']);
		$ficha_biblioteca->setClave($data[0]['clave']);
		$ficha_biblioteca->setContratista($data[0]['contratista']);
		$ficha_biblioteca->setFecha($data[0]['fecha']);
		$ficha_biblioteca->setIdCategoria($data[0]['id_categoria']);
		$ficha_biblioteca->setIdClase($data[0]['id_clase']);
		$ficha_biblioteca->setIdMunicipio($data[0]['id_municipio']);
		$ficha_biblioteca->setIdProvincia($data[0]['id_provincia']);
		$ficha_biblioteca->setIdProyecto($data[0]['id_proyecto']);
		$ficha_biblioteca->setIdSubcategoria($data[0]['id_subcategoria']);
		$ficha_biblioteca->setIdSubtipo($data[0]['id_subtipo']);
		$ficha_biblioteca->setIdTipo($data[0]['id_tipo']);
		$ficha_biblioteca->setIdZona($data[0]['id_zona']);
		$ficha_biblioteca->setNotas($data[0]['notas']);
		$ficha_biblioteca->setPrestado($data[0]['prestado']);
		$ficha_biblioteca->setRefLetra($data[0]['ref_letra']);
		$ficha_biblioteca->setRefNumero($data[0]['ref_numero']);
		$ficha_biblioteca->setReferencia($data[0]['referencia']);
		$ficha_biblioteca->setTitulo($data[0]['titulo']);
		$ficha_biblioteca->setToponimo($data[0]['toponimo']);
		$ficha_biblioteca->setMueble($data[0]['mueble']);
		$ficha_biblioteca->setIdUbicacion($data[0]['id_ubicacion']);

		$baldas = array();
		if(count($data) > 0) {
			foreach($data as $dato) {
				array_push($baldas, $dato['balda']);
			}
		}
		$ficha_biblioteca->setBalda($baldas);
		return $ficha_biblioteca;		
	}

	function altaSolicitud($username, $id_proyecto, $fecha_solicitud)
	{

		$query1 = "select * from tbl_biblioteca_peticiones where username = '$username' and id_proyecto = $id_proyecto and pendiente;";
		$resultado1 = ConexionBD::EjecutarConsultaDev($query1);

		if(count($resultado1)>0){
			return false;
		} else {
			$query2 = "INSERT INTO tbl_biblioteca_peticiones (username, id_proyecto, fecha_solicitud, pendiente) VALUES ('$username', $id_proyecto, '$fecha_solicitud',true)";
			$resultado2 = ConexionBD::EjecutarConsultaDev($query2, false);
			return true;
		}
	}
	
	/*function listaPeticiones()
	{
		$query = "SELECT p.id_proyecto, p.username, p.fecha_solicitud, pr.titulo FROM tbl_biblioteca_peticiones p 
				  LEFT JOIN tbl_biblioteca_proyectos pr ON p.id_proyecto = pr.id_proyecto";
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}*/

	function listaSolicitudes($filters = null){
		$query = "SELECT 
					p.id_proyecto, 
					ref_numero, 
					fecha, 
					nombre_tipo, 
					nombre_subtipo, 
					clave,
					p.titulo, 
					case WHEN (pres.fecha_prestamo is not null AND pres.fecha_devolucion is null) THEN 'Sí' ELSE 'No' end as prestado,
					pe.username, 
					pe.fecha_solicitud,
					u.ubicaciones as ubicacion
				FROM tbl_biblioteca_peticiones pe
				inner join tbl_biblioteca_proyectos p on p.id_proyecto = pe.id_proyecto AND pe.pendiente
				inner join tbl_biblioteca_proyectos_archivos tbpa on tbpa.id_proyecto = p.id_proyecto 
				inner join tbl_biblioteca_archivos tba on tba.id_archivo = tbpa.id_archivo 
				inner join tbl_sec_rel_usuario_biblioteca_ubicacion bu on bu.id_ubicacion = tba.id_ubicacion AND bu.cod_usuario = " . $filters['cod_user'] ."
				left join tbl_biblioteca_prestados pres on pres.id_proyecto = p.id_proyecto and pres.fecha_devolucion is null
				left join tbl_biblioteca_tipo tip on tip.id_tipo = p.id_tipo and pres.fecha_devolucion is null
				left join tbl_biblioteca_subtipo stip on stip.id_subtipo = p.id_subtipo and pres.fecha_devolucion is null
				left join (
					select id_proyecto, 
					string_agg(ubicaciones,', ') ubicaciones
					from(
						select 
							id_proyecto,
							case WHEN (nombre_ubicacion is not null) 
								THEN CONCAT(nombre_ubicacion,': ', string_agg(mueble_balda,', '),'') 
								ELSE null end as ubicaciones
						from(
							SELECT 
								p.id_proyecto, 
								tbu.nombre_ubicacion,
								CONCAT('Mueble ', tba.mueble, ' Balda ', string_agg(tba.balda,', '),'') mueble_balda
							FROM tbl_biblioteca_proyectos p 
							left join tbl_biblioteca_proyectos_archivos tbpa on tbpa.id_proyecto = p.id_proyecto 
							left join tbl_biblioteca_archivos tba on tba.id_archivo = tbpa.id_archivo
							left join tbl_biblioteca_ubicaciones tbu on tbu.id_ubicacion = tba.id_ubicacion
							group by p.id_proyecto, tbu.nombre_ubicacion, tba.mueble
						) as umb
						group by id_proyecto, nombre_ubicacion
					)as umbg
					group by id_proyecto
				) as u on u.id_proyecto = p.id_proyecto";

		if($filters){
			$provincia = $filters['provincia'] ?? null;
			$zona = $filters['zona'] ?? null;
			$municipio = $filters['municipio'] ?? null;
			$tipo = $filters['tipo'] ?? null;
			$subtipo = $filters['subtipo'] ?? null;
			$categoria = $filters['categoria'] ?? null;
			$subcategoria = $filters['subcategoria'] ?? null;
			$clase = $filters['clase'] ?? null;
			$balda = $filters['balda'] ?? null;
			$numero = $filters['numero'] ?? null;
			
			$filterQueries = [];
			if ($provincia != null) {
				array_push($filterQueries, "id_provincia = $provincia");
			}
			if ($zona != null) {
				array_push($filterQueries, "id_zona=$zona");
			}
			if ($municipio != null) {
				array_push($filterQueries, "id_municipio=$municipio");
			}
			if ($tipo != null) {
				array_push($filterQueries, "id_tipo=$tipo");
			}
			if ($subtipo != null) {
				array_push($filterQueries, "id_subtipo=$subtipo");
			}
			if ($categoria != null) {
				array_push($filterQueries, "id_categoria=$categoria");
			}
			if ($subcategoria != null) {
				array_push($filterQueries, "id_subcategoria=$subcategoria");
			}
			if ($clase != null) {
				array_push($filterQueries, "id_clase=$clase");
			}
			if ($balda != null) {
				array_push($filterQueries, "balda='$balda'");
			}
			if ($numero != null) {
				array_push($filterQueries, "ref_numero=$numero");
			}
			if (count($filterQueries) > 0) {
				$query .= implode(" and ", $filterQueries);
			}
		}
		
		$data = ConexionBD::EjecutarConsultaDev($query);
		return $data;
	}

	function borrarSolicitud($id_proyecto, $fecha_solicitud, $username)
	{

		$query1 = "select 1 from tbl_biblioteca_peticiones WHERE id_proyecto = $id_proyecto AND username = '$username' AND fecha_solicitud = '$fecha_solicitud' and pendiente";
		$resultado1 = ConexionBD::EjecutarConsultaDev($query1);

		// actualizar solicitud
		if(count($resultado1)>0){
			$query1 = "UPDATE tbl_biblioteca_peticiones SET pendiente = false WHERE id_proyecto = $id_proyecto AND username = '$username' AND fecha_solicitud = '$fecha_solicitud'";
			ConexionBD::EjecutarConsultaDev($query1, false);
			return true;
		} else {
			return false;
		}
						
	}

	function aceptarSolicitudDAO($fecha_prestamo, $id_proyecto, $username)
	{

		// revisar si existe petición

		$query1 = "select 1 from tbl_biblioteca_peticiones WHERE id_proyecto = $id_proyecto AND username = '$username' AND pendiente";
		$resultado1 = ConexionBD::EjecutarConsultaDev($query1);

		if(count($resultado1)>0){

			// revisar e insertar si el documento puede ser prestado
			$query2 = "INSERT INTO tbl_biblioteca_prestados (username, id_proyecto, fecha_prestamo) 
				select '$username', $id_proyecto, '$fecha_prestamo' 
				where not exists (
					select 1 from tbl_biblioteca_prestados where id_proyecto = $id_proyecto and fecha_devolucion is null
				);";

			$resultado2 = ConexionBD::EjecutarConsultaDev($query2, false);

			// revisar si se ha prestado el documento
			$query3 = "select 1 as prestado from tbl_biblioteca_prestados where username = '$username' and id_proyecto = $id_proyecto and fecha_devolucion is null;";
			$resultado3 = ConexionBD::EjecutarConsultaDev($query3);

			// actualizar solicitud
			if(count($resultado3)>0){
				$query4 = "UPDATE tbl_biblioteca_peticiones SET pendiente = false WHERE id_proyecto = $id_proyecto AND username = '$username'";
				ConexionBD::EjecutarConsultaDev($query4, false);
				return true;
			} else {
				// no borrar solicitud si no se ha prestado el documento
				return false;
			}
		} else {
			// no se ha encontrado la petición pendiente
			return false;
		}
		
	}

	function devolverPrestamo($id_proyecto, $fecha_devolucion, $username)
	{
		
		$query1 = "select 1 from tbl_biblioteca_prestados WHERE id_proyecto = $id_proyecto AND username like '$username' AND fecha_devolucion is null";
		$resultado1 = ConexionBD::EjecutarConsultaDev($query1);

		if(count($resultado1)>0){
			$query2 = "UPDATE tbl_biblioteca_prestados SET fecha_devolucion = '$fecha_devolucion' WHERE id_proyecto = $id_proyecto AND username like '$username' AND fecha_devolucion is null";
			ConexionBD::EjecutarConsultaDev($query2, false);
			return true;
		} else {
			return false;
		}

	}

	/*function updatePrestado($id_proyecto, $prestado)
	{
		$prestadoValue = $prestado ? "Sí" : "No";
		$query = "UPDATE tbl_biblioteca_proyectos SET prestado = '$prestadoValue' WHERE id_proyecto = $id_proyecto";
		ConexionBD::EjecutarConsultaDev($query, false);
	}*/

	function findUbicacionesUser($cod_usuario) 
	{
		$query = "SELECT id_ubicacion FROM tbl_sec_rel_usuario_biblioteca_ubicacion WHERE cod_usuario = $cod_usuario";
		$resultado = ConexionBD::EjecutarConsultaDev($query);
		return $resultado;
	}

	function mailSolicitud($username, $id_proyecto)
	{
		// inicializar datos
		$datosContenidoEmail = [
			"solicitante_usuario" => "",
			"solicitante_nombreCompleto" => "",
			"solicitante_correo" => "",
			"proyecto_identificador" => "",
			"proyecto_referencia" => "",
			"proyecto_clave" => "",
			"proyecto_titulo" => "",
			"proyecto_ubicaciones" => "",
		];

		$correos = [];

		// datos de documento
		$queryDocumento = "select
								p.id_proyecto,
								p.ref_numero,
								p.ref_letra,
								p.titulo,
								p.clave,
								ubicaciones_texto.ubicaciones
							
							from tbl_biblioteca_proyectos p join
							(
								select id_proyecto, 
								string_agg(ubicaciones,', ') ubicaciones
								from(
									select 
										id_proyecto,
										case WHEN (nombre_ubicacion is not null) 
											THEN CONCAT(nombre_ubicacion,': ', string_agg(mueble_balda,', '),'') 
											ELSE null end as ubicaciones
									from(
										SELECT 
											p.id_proyecto, 
											tbu.nombre_ubicacion,
											CONCAT('Mueble ', tba.mueble, ' Balda ', string_agg(tba.balda,', '),'') mueble_balda
										FROM tbl_biblioteca_proyectos p 
										left join tbl_biblioteca_proyectos_archivos tbpa on tbpa.id_proyecto = p.id_proyecto
										left join tbl_biblioteca_archivos tba on tba.id_archivo = tbpa.id_archivo
										left join tbl_biblioteca_ubicaciones tbu on tbu.id_ubicacion = tba.id_ubicacion
										where p.id_proyecto = $id_proyecto
										group by p.id_proyecto, tbu.nombre_ubicacion, tba.mueble
									) as umb
									group by id_proyecto, nombre_ubicacion
								)as umbg
								group by id_proyecto 
							) as ubicaciones_texto on p.id_proyecto = ubicaciones_texto.id_proyecto
							where p.id_proyecto = $id_proyecto";

		
		// lista de correos a quién mandar el aviso, son los usuarios que coinciden con la/s ubicaciones del documento
		$queryCorreos = "select 
							distinct email
						from tbl_biblioteca_proyectos p
						join tbl_biblioteca_proyectos_archivos pa on pa.id_proyecto = p.id_proyecto
						join tbl_biblioteca_archivos a on a.id_archivo = pa.id_archivo
						join tbl_sec_rel_usuario_biblioteca_ubicacion usb on usb.id_ubicacion = a.id_ubicacion
						join tbl_sec_usuarios us on us.cod_usuario = usb.cod_usuario
						LEFT join tbl_sec_rel_nivel_acceso_elemento nae on nae.cod_nivel_acceso = us.cod_nivel_acceso and nae.visible = 1 and nae.cod_elemento like 'GESTION_DOCUMENTAL_BIBLIOTECA_ADMINISTRADOR'
						LEFT join tbl_sec_rel_usuario_elemento ue on ue.cod_usuario = us.cod_usuario and ue.visible = 1 and ue.cod_elemento like 'GESTION_DOCUMENTAL_BIBLIOTECA_ADMINISTRADOR'
						where 
						p.id_proyecto = $id_proyecto 
						and us.enabled_mailing = 1 
						and email is not null";

		// datos de usuario
		$queryDatosSolicitante = "select username, nombre_completo, email from tbl_sec_usuarios where username like '$username'";

		$datosDocumento = ConexionBD::EjecutarConsultaDev($queryDocumento);
		$datosCorreos = ConexionBD::EjecutarConsultaDev($queryCorreos);
		$datosSolicitante = ConexionBD::EjecutarConsultaDev($queryDatosSolicitante);

		if(isset($datosCorreos) && count($datosCorreos)>0){
			foreach($datosCorreos as $datoCorreo){
				$correos[] = $datoCorreo['email'];
			}
		} 

		if(isset($datosDocumento[0])){
			$datosContenidoEmail["proyecto_identificador"] = $datosDocumento[0]["id_proyecto"];
			$datosContenidoEmail["proyecto_referencia"] = $datosDocumento[0]["ref_numero"] . " " . $datosDocumento[0]["ref_letra"];
			$datosContenidoEmail["proyecto_clave"] = $datosDocumento[0]["clave"];
			$datosContenidoEmail["proyecto_titulo"] = $datosDocumento[0]["titulo"];
			$datosContenidoEmail["proyecto_ubicaciones"] = $datosDocumento[0]["ubicaciones"];
		}

		if(isset($datosSolicitante[0])){
			$datosContenidoEmail["solicitante_usuario"] = $datosSolicitante[0]["username"];
			$datosContenidoEmail["solicitante_nombreCompleto"] = $datosSolicitante[0]["nombre_completo"];
			$datosContenidoEmail["solicitante_correo"] = $datosSolicitante[0]["email"];
		}

		if(count($correos)>0 && isset($datosDocumento[0]) && isset($datosSolicitante[0])){

			$plantillaTitulo = "SIRA BIBLIOTECA DOCUMENTAL: Solicitud de proyecto (Ref:{$datosContenidoEmail['proyecto_referencia']})";
		
			$plantillaCotenido = "<html><p>Aviso de solicitud de proyecto:</p>
			<br>
			<p>SOLICITANTE:</p>
			<table>
				<tr>
					<td style='width: 125px;text-align: end;'>Usuario:</td>
					<td><b>{$datosContenidoEmail['solicitante_usuario']}</b></td>
				</tr>
				<tr>
					<td style='width: 125px;text-align: end;'>Nombre completo:</td>
					<td><b>{$datosContenidoEmail['solicitante_nombreCompleto']}</b></td>
				</tr>
				<tr>
					<td style='width: 125px;text-align: end;'>Correo electrónico:</td>
					<td><b>{$datosContenidoEmail['solicitante_correo']}</b></td>
				</tr>
			</table>
			<br>
			<p>PROYECTO SOLICITADO:</p>
			<table>
				<tr>
					<td style='width: 125px;text-align: end;'>Identificador:</td>
					<td><b>{$datosContenidoEmail['proyecto_identificador']}</b></td>             
				</tr>
				<tr>
					<td style='width: 125px;text-align: end;'>Referencia:</td>
					<td><b>{$datosContenidoEmail['proyecto_referencia']}</b></td>             
				</tr>
				<tr>
					<td style='width: 125px;text-align: end;'>Clave:</td>
					<td><b>{$datosContenidoEmail['proyecto_clave']}</b></td>             
				</tr>
				<tr>
					<td style='width: 125px;text-align: end;'>Título:</td>
					<td><b>{$datosContenidoEmail['proyecto_titulo']}</b></td>             
				</tr>
				<tr>
					<td style='width: 125px;text-align: end;'>Ubicación:</td>
					<td><b>{$datosContenidoEmail['proyecto_ubicaciones']}</b></td>             
				</tr>
			</table>
			<br>
			<p>Ha recibido este correo de forma automática al tener las notificaciones activas así como los permisos necesarios para gestionar el proyecto citado.</p>
			<p>Para recibir asistencia por favor contacte con soporte@saihguadiana.com.</p>
			</html>";
	
			$config['protocol'] = 'smtp';
			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';
			$config['smtp_host'] = "smtp.saihguadiana.com";
			$config['smtp_user'] = 'info@saihguadiana.com';
			$config['smtp_pass'] = 'Saihguad.1';
			$config['smtp_port'] = 587;
			$this->email->initialize($config);
			$this->email->from('info@saihguadiana.com', 'INFO');
			$this->email->to($correos);
			$this->email->subject($plantillaTitulo);
			$this->email->message($plantillaCotenido);
			$this->email->send();

		}

	}


	function mailRecordatorioPendientes()
	{

		// lista de correos a quién mandar el aviso, son usuarios con alertas activas con permiso de administrador de biblioteca y ubicaciones asignadas, se incluyen en json los id de proyectos que tienen asociados en estado pendiente
		$queryCorreos = "select 
							email, json_agg(distinct p.id_proyecto) id_proyectos
							from tbl_biblioteca_proyectos p
							join tbl_biblioteca_prestados pres on pres.id_proyecto = p.id_proyecto and pres.fecha_devolucion is null
							join tbl_biblioteca_proyectos_archivos pa on pa.id_proyecto = p.id_proyecto
							join tbl_biblioteca_archivos a on a.id_archivo = pa.id_archivo
							join tbl_sec_rel_usuario_biblioteca_ubicacion usb on usb.id_ubicacion = a.id_ubicacion
							join tbl_sec_usuarios us on us.cod_usuario = usb.cod_usuario
							LEFT join tbl_sec_rel_nivel_acceso_elemento nae on nae.cod_nivel_acceso = us.cod_nivel_acceso and nae.visible = 1 and nae.cod_elemento like 'GESTION_DOCUMENTAL_BIBLIOTECA_ADMINISTRADOR'
							LEFT join tbl_sec_rel_usuario_elemento ue on ue.cod_usuario = us.cod_usuario and ue.visible = 1 and ue.cod_elemento like 'GESTION_DOCUMENTAL_BIBLIOTECA_ADMINISTRADOR'
							where 
							us.enabled_mailing = 1 
							and us.email is not null
							group by us.email";

		// datos de usuarios con proyectos pendientes de devolución, contiene id de proyecto
		$queryDatosPrestamos = "select 
									pr.id_proyecto,
									u.username, 
									u.nombre_completo, 
									u.email 
								from tbl_sec_usuarios u
								join tbl_biblioteca_prestados pr on pr.username = u.username and pr.fecha_devolucion is null";

		// datos detallados de documentos pendientes de devolución, contiene el id de proyecto
		$queryProyectos = "select
								p.id_proyecto,
								p.ref_numero,
								p.ref_letra,
								p.titulo,
								p.clave,
								ubicaciones_texto.ubicaciones,
								pres.fecha_prestamo
							from tbl_biblioteca_proyectos p 
							join tbl_biblioteca_prestados pres on pres.id_proyecto = p.id_proyecto and pres.fecha_devolucion is null
							left join (
								select id_proyecto, 
								string_agg(ubicaciones,', ') ubicaciones
								from(
									select 
										id_proyecto,
										case WHEN (nombre_ubicacion is not null) 
											THEN CONCAT(nombre_ubicacion,': ', string_agg(mueble_balda,', '),'') 
											ELSE null end as ubicaciones
									from(
										SELECT 
											p.id_proyecto, 
											tbu.nombre_ubicacion,
											CONCAT('Mueble ', tba.mueble, ' Balda ', string_agg(tba.balda,', '),'') mueble_balda
										FROM tbl_biblioteca_proyectos p
										join tbl_biblioteca_prestados pres on pres.id_proyecto = p.id_proyecto and pres.fecha_devolucion is null
										left join tbl_biblioteca_proyectos_archivos tbpa on tbpa.id_proyecto = p.id_proyecto
										left join tbl_biblioteca_archivos tba on tba.id_archivo = tbpa.id_archivo
										left join tbl_biblioteca_ubicaciones tbu on tbu.id_ubicacion = tba.id_ubicacion
										group by p.id_proyecto, tbu.nombre_ubicacion, tba.mueble
									) as umb
									group by id_proyecto, nombre_ubicacion
								)as umbg
								group by id_proyecto 
							) as ubicaciones_texto on p.id_proyecto = ubicaciones_texto.id_proyecto";

		$datosCorreos = ConexionBD::EjecutarConsultaDev($queryCorreos);
		$datosProyectos = ConexionBD::EjecutarConsultaDev($queryProyectos);
		$datosPrestamos = ConexionBD::EjecutarConsultaDev($queryDatosPrestamos);

		// pasar a array asociativo los detalles de proyectos pendientes
		$assocDatosProyectos = [];
        if(isset($datosProyectos) && count($datosProyectos)>0){
			foreach($datosProyectos as $datosProyecto){
				if(isset($datosProyecto["id_proyecto"])){
					$assocDatosProyectos[$datosProyecto["id_proyecto"]] = $datosProyecto;
				}
			}
		}

		// pasar a array asociativo los detalles de usuarios con proyectos pendientes
		$assocDatosPrestamos = [];
        if(isset($datosPrestamos) && count($datosPrestamos)>0){
			foreach($datosPrestamos as $datosPrestamo){
				if(isset($datosPrestamo["id_proyecto"])){
					$assocDatosPrestamos[$datosPrestamo["id_proyecto"]] = $datosPrestamo;
				}
			}
		}

		// por cada correo con ids de proyecto se buscan los detalles de proyecto y detalles de usuario y se realiza el envío

		if(isset($datosCorreos) && count($datosCorreos)>0){

			foreach($datosCorreos as $correo){

				$idProyectos = json_decode($correo["id_proyectos"]);

				if(count($idProyectos)>0 /*&& $correo['email'] == "apaniagua@chguadiana.es"*/){


					$plantillaTitulo = "SIRA BIBLIOTECA DOCUMENTAL: Reporte mensual de proyectos pendientes de devolución";
					$plantillaCotenido = "<p>Aviso mensual de proyectos pendientes de devolución:</p>
					<br>";

					$plantillaCotenido .= "<table border=1 cellspacing=0 cellpadding=2 bordercolor='F1F1F1'>
								<tr>
									<td style='background: #ccc; width: 300px; text-align: center;'>Título</td>
									<td style='background: #ccc; text-align: center;'>Clave</td>
									<td style='background: #ccc; text-align: center;'>Referencia</td>
									<td style='background: #ccc; text-align: center;'>Identificador</td>
									<td style='background: #ccc; text-align: center;'>Ubicación</td>
									<td style='background: #ccc; text-align: center;'>Fecha préstamo</td>
									<td style='background: #ccc; text-align: center;'>Usuario</td>
									<td style='background: #ccc; text-align: center;'>Nombre completo</td>
									<td style='background: #ccc; text-align: center;'>Correo electrónico</td>
								</tr>";

					foreach($idProyectos as $idProyecto){
						
						if(isset($assocDatosProyectos[$idProyecto]) && isset($assocDatosPrestamos[$idProyecto])){

							$datosContenidoEmail = [
								"prestado_usuario" => "",
								"prestado_nombreCompleto" => "",
								"prestado_correo" => "",
								"prestado_fecha_prestamo" => "",
								"proyecto_identificador" => "",
								"proyecto_referencia" => "",
								"proyecto_clave" => "",
								"proyecto_titulo" => "",
								"proyecto_ubicaciones" => "",
							];

							$dPrestamo = $assocDatosPrestamos[$idProyecto];
							$dProyecto = $assocDatosProyectos[$idProyecto];

							$datosContenidoEmail["prestado_usuario"] = $dPrestamo["username"];
							$datosContenidoEmail["prestado_nombreCompleto"] = $dPrestamo["nombre_completo"];
							$datosContenidoEmail["prestado_correo"] = $dPrestamo["email"];
							$datosContenidoEmail["prestado_fecha_prestamo"] = $dProyecto["fecha_prestamo"];
							$datosContenidoEmail["proyecto_identificador"] = $dProyecto["id_proyecto"];
							$datosContenidoEmail["proyecto_referencia"] = $dProyecto["ref_numero"] . " " . $dProyecto["ref_letra"];
							$datosContenidoEmail["proyecto_clave"] = $dProyecto["clave"];
							$datosContenidoEmail["proyecto_titulo"] = $dProyecto["titulo"];
							$datosContenidoEmail["proyecto_ubicaciones"] = $dProyecto["ubicaciones"];

							$plantillaCotenido .= "<tr>
								<td>{$datosContenidoEmail['proyecto_titulo']}</td>
								<td>{$datosContenidoEmail['proyecto_clave']}</td>    
								<td>{$datosContenidoEmail['proyecto_referencia']}</td> 
								<td>{$datosContenidoEmail['proyecto_identificador']}</td> 
								<td>{$datosContenidoEmail['proyecto_ubicaciones']}</td>   
								<td>{$datosContenidoEmail['prestado_fecha_prestamo']}</td>
								<td>{$datosContenidoEmail['prestado_usuario']}</td>
								<td>{$datosContenidoEmail['prestado_nombreCompleto']}</td>
								<td>{$datosContenidoEmail['prestado_correo']}</td>          
							</tr>";

						}

					}

					$plantillaCotenido .= "</table><br>";

					$plantillaCotenido .= "<p>Ha recibido este correo de forma automática al tener las notificaciones activas así como los permisos necesarios para gestionar los proyectos mostrados.</p>
					<p>Para recibir asistencia por favor contacte con soporte@saihguadiana.com.</p>";
	
					$config['protocol'] = 'smtp';
					$config['charset'] = 'utf-8';
					$config['wordwrap'] = TRUE;
					$config['mailtype'] = 'html';
					$config['smtp_host'] = "smtp.saihguadiana.com";
					$config['smtp_user'] = 'info@saihguadiana.com';
					$config['smtp_pass'] = 'Saihguad.1';
					$config['smtp_port'] = 587;
					$this->email->initialize($config);
					$this->email->from('info@saihguadiana.com', 'INFO');
					$this->email->to('mselles@adasasistemas.com');
					$this->email->subject($plantillaTitulo);
					$this->email->message($plantillaCotenido);
					$this->email->send();

				}
			}
		}
	}
}
