<?php
require_once APPPATH . '/libraries/visor/class_conexion.php';
require_once APPPATH . '/libraries/visor/class_cache.php';
require_once APPPATH . '/libraries/visor/class_evolucion_estacion.php';
require(APPPATH . "/libraries/visor/security_elements.php");

class ExcelDao extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

    function getVariablesEstaciones()
	{
		$resultado = array();
		$query = "SELECT cod_variable FROM tbl_variable WHERE cod_variable  LIKE 'CR%/NR1' OR cod_variable  LIKE 'CR%/NE1' OR cod_variable  LIKE 'CR%/QR1' OR cod_variable  LIKE 'CR%/PRA' OR cod_variable LIKE 'EM%/NR1' OR cod_variable LIKE 'EM%/NE1' OR cod_variable LIKE 'EM%/QR1' OR cod_variable LIKE 'EM%/PRA' OR cod_variable ~* 'E\d{1}-\d{2}/QR1' OR cod_variable  ~* 'E\d{1}-\d{2}/NE1' OR cod_variable ~* 'E\d{1}-\d{2}/NR1' OR cod_variable ~* 'E\d{1}-\d{2}/PRA' ORDER BY cod_variable ASC;";
		$data = ConexionBD::EjecutarConsultaDev($query);

		if (count($data) > 0) {
			foreach($data as $valor) {
				array_push($resultado, $valor["cod_variable"]);
			}
		}
		return $resultado;
	}

	
    function getEvolucionEstacionDiezMinutal($zoom, $variables, $fecha_i, $fecha_f, $encabezado) {
        $DB = ConexionBD::Conexion();
        $val = EvolucionEstacion::obtener($DB, $zoom, $variables, $fecha_i, $fecha_f, $encabezado, 'DESC');

        ConexionBD::CerrarConexion($DB);
        return $val;
    }
}