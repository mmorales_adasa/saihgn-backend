<?php

if(!function_exists('beggining_of_day'))
{
    function beggining_of_day($timestamp)
    {
        return strtotime("today", $timestamp);
    }
}

if(!function_exists('end_of_day'))
{
    function end_of_day($timestamp)
    {
        return strtotime("tomorrow", $timestamp) - 1;
    }
}